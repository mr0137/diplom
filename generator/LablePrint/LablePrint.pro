QT += core
QT += gui
QT += qml
QT += sql
QT += quick
QT += concurrent
QT += quickcontrols2
QT += printsupport
QT += widgets
QT += gui-private
QT += quickwidgets

CONFIG += c++17
CONFIG += console
DEFINES += QT_DEPRECATED_WARNINGS

QML_IMPORT_NAME = App
QML_IMPORT_MAJOR_VERSION = 1
uri = App

QML_IMPORT_PATH += $$PWD/../plugins

SOURCES += \
    appcore.cpp \
    codeitem.cpp \
    generatortest.cpp \
    main.cpp \
    mainwindow.cpp \
    printerservice.cpp \
    resultdata.cpp

HEADERS += \
    appcore.h \
    codeitem.h \
    generatortest.h \
    helper.h \
    mainwindow.h \
    printerservice.h \
    resultdata.h

FORMS += \
    mainwindow.ui


CONFIG(release, debug|release){
    LIBS += -L$$PWD/../bin/libs -lKOrm
    LIBS += -L$$PWD/../bin/libs -lQXlsx
    LIBS += -L$$PWD/../bin/libs -lklibcorelite
    LIBS += -L$$PWD/../bin/libs -lEwarehouseBase
    LIBS += -L$$PWD/../bin/libs -lQZint
    LIBS += -L$$PWD/../bin/libs -lQEMF
    LIBS += -L$$PWD/../bin/libs -lNrpParser
    LIBS += -L$$PWD/../bin/libs -lqdxf
}else{
    win32: LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
    else:unix: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

    win32: LIBS += -L$$OUT_PWD/../libs/QXlsx/debug/ -lQXlsx
    else:unix: LIBS += -L$$OUT_PWD/../libs/QXlsx/ -lQXlsx

    win32: LIBS += -L$$OUT_PWD/../libs/KOrm/debug/ -lKOrm
    else:unix: LIBS += -L$$OUT_PWD/../libs/KOrm/ -lKOrm

    win32: LIBS += -L$$OUT_PWD/../libs/EwarehouseBase/debug/ -lEwarehouseBase
    else:unix: LIBS += -L$$OUT_PWD/../libs/EwarehouseBase/ -lEwarehouseBase

    win32: LIBS += -L$$OUT_PWD/../libs/QZint/debug/ -lQZint
    else:unix: LIBS += -L$$OUT_PWD/../libs/QZint/ -lQZint

    win32: LIBS += -L$$OUT_PWD/../libs/NrpParser/debug/ -lNrpParser
    else:unix: LIBS += -L$$OUT_PWD/../libs/NrpParser/ -lNrpParser

    win32: LIBS += -L$$OUT_PWD/../libs/QEMF/debug/ -lQEMF
    else:unix: LIBS += -L$$OUT_PWD/../libs/QEMF/ -lQEMF

    win32: LIBS += -L$$OUT_PWD/../libs/qdxf/debug/ -lqdxf
    else:unix: LIBS += -L$$OUT_PWD/../libs/qdxf/ -lqdxf
}

INCLUDEPATH += $$PWD/../libs/EwarehouseBase
DEPENDPATH += $$PWD/../libs/EwarehouseBase

INCLUDEPATH += $$PWD/../libs/KOrm
DEPENDPATH += $$PWD/../libs/KOrm

INCLUDEPATH += $$PWD/../libs/QXlsx
DEPENDPATH += $$PWD/../libs/QXlsx

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite

INCLUDEPATH += $$PWD/../libs/QZint
DEPENDPATH += $$PWD/../libs/QZint

INCLUDEPATH += $$PWD/../libs/NrpParser
DEPENDPATH += $$PWD/../libs/NrpParser

INCLUDEPATH += $$PWD/../libs/QEMF
DEPENDPATH += $$PWD/../libs/QEMF

INCLUDEPATH += $$PWD/../libs/qdxf
DEPENDPATH += $$PWD/../libs/qdxf

RESOURCES += \
    lable.qrc
