#ifndef PRINTERSERVICE_H
#define PRINTERSERVICE_H


#include <QObject>

#include <kmacro.h>
#include <utility/kobservablelist.h>
#include <models/kflexiblemodel.h>

#include <QRectF>
#include <qzint.h>
#include <helper.h>


class QPrinter;
class QPainter;
class PrinterService : public QObject
{
    Q_OBJECT
    K_QML(PrinterService)
    K_READONLY_PROPERTY(double, scale, scale, setScale, scaleChanged, 2)
    K_READONLY_PROPERTY(QVariantList, images, images, setImages, imagesChanged, QVariantList())
    K_READONLY_PROPERTY(QRectF, imageSize, imageSize, setImageSize, imageSizeChanged, QRectF(0, 0, 1100, 740))
    Q_PROPERTY(QImage code READ code WRITE setCode NOTIFY codeChanged)

    public:
        explicit PrinterService(QObject *parent = nullptr);

    void setImage();

    const QImage &code() const;
    void setCode(const QImage &newCode);

public slots:
    void openDialogPrinter(QVariantList list);
    void print();
    void drawLables(QVariantList list);
    QImage encode(QString text, double width, double height);


private:
    QPrinter *m_printer = nullptr;

signals:
    void codeChanged();
    void progressChanged(double progress);

private:
    //QPrinter *m_printer = nullptr;
    QZint* m_codeGen = nullptr;
    QImage m_code;

};

#endif // PRINTERSERVICE_H
