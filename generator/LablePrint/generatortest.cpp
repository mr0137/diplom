 #include "generatortest.h"

#include <QTextStream>
#include <qzint.h>

GeneratorTest::GeneratorTest(QObject *parent) : QObject(parent)
{
    m_model = m_list.model();
    m_model->setParent(this);

    ResultTest *tmp = new ResultTest(this);
    tmp->setCode("Code39");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("Code93");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("UPC-A");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("UPC-E");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("EAN-13");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("Code 128");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("Data Matrix");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("PDF417");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("Aztec");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("QR Code");
    m_list.push_back(tmp);

    tmp = new ResultTest(this);
    tmp->setCode("OWN Code");
    m_list.push_back(tmp);

    autoTest();
}

void GeneratorTest::setText(QString text)
{
    code39(text);
    code93(text);
    codeUPC_A(text);
    codeUPC_E(text);
    codeEAN_13(text);
    code128(text);
    codeEXT(text);
    codeDataMatrix(text);
    codePDF417(text);
    codeAztec(text);
    codeQR(text);
    qDebug() << text.length();
}

void GeneratorTest::autoTest()
{
    QFile file("test.csv");
    file.open(QIODevice::WriteOnly);

    QStringList testStrings = {
        "1",
        "1234567",
        "1234567891011",
        "123456789101112",
        "123456789101112131415161718192",
        "123456789101112131415161718192021222324252627282930313233343",
        "123456789101112131415161718192021222324252627282930313233343444546474849505152535455565758596061626364656667686970717273",
        "123456789101112131415161718192021222324252627282930313233343444546474849505152535455565758596061626364656667686970717273123456789101112131415161718192021222324252627282930313233343444546474849505152535455565758596061626364656667686970717273",
        "12345678910111213141516171819202122232425262728293031323334344454647484950515253545556575859606162636465666768697071727312345678910111213141516171819202122232425262728293031323334344454647484950515253545556575859606162636465666768697071727312345678910111213141516171819202122232425262728293031323334344454647484950515253545556575859606162636465666768697071727312345678910111213141516171819202122232425262728293031323334344454647484950515253545556575859606162636465666768697071727312234312332342134231223123341232"
    };

    QTextStream stream(&file);
    QStringList result ={ "", "", "", "", "", "", "", "", "", "", "", "" };

    for (const auto &test : testStrings){
        setText(test);
        int i = 0;
        result[i++].append(QString::number(test.length()) + ";");

        for (const auto &r : m_list){
            result[i++].append(r->time() + ";");
        }
    }

    stream << result.join("\n");

    file.close();
}

void GeneratorTest::code93(QString text)
{
    QElapsedTimer timer;
    timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE93);


    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(0)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(0)->setImage(img);
    img.save(m_list.get(0)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::code39(QString text)
{
    QElapsedTimer timer;
    timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE39);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(1)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(1)->setImage(img);
    img.save(m_list.get(1)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeUPC_A(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_UPCA);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(2)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(2)->setImage(img);
    img.save(m_list.get(2)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeUPC_E(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_UPCE);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(3)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(3)->setImage(img);
    img.save(m_list.get(3)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeEAN_13(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_EAN14);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(4)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(4)->setImage(img);
    img.save(m_list.get(4)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::code128(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE128);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(5)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    //qDebug() << timer.elapsed();
    m_list.get(5)->setImage(img);
    img.save(m_list.get(5)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeEXT(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE128EXT);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(10)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(10)->setImage(img);
    img.save(m_list.get(10)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeDataMatrix(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_DATAMATRIX);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(6)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(6)->setImage(img);
    img.save(m_list.get(6)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codePDF417(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_PDF417);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(7)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(7)->setImage(img);
    img.save(m_list.get(7)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeAztec(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_AZTEC);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(0,0, m_width, m_height),QZint::AspectRatioMode::CenterBarCode);
    m_list.get(8)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(8)->setImage(img);
    img.save(m_list.get(8)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

void GeneratorTest::codeQR(QString text)
{
    QElapsedTimer timer; timer.start();
    QZint bc;

    QImage img(m_width, m_height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(m_height);
    bc.setWidth(m_width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_QRCODE);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(m_width * 0.05 ,0, m_width - m_width * 0.1, m_height)/*,QZint::AspectRatioMode::CenterBarCode*/);
    m_list.get(9)->setTime(bc.error_message() != "" ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    m_list.get(9)->setImage(img);
    img.save(m_list.get(9)->code() + "_" + QString::number(text.length()) + ".png", "PNG");
}

ResultTest::ResultTest(QObject *parent) : QObject(parent)
{

}
