#ifndef RESULTDATA_H
#define RESULTDATA_H

#include <QObject>
#include <kmacro.h>
#include <QImage>
class AppCore;
class ResultData : public QObject
{
    Q_OBJECT
    K_READONLY_PROPERTY(QString, name, name, setName, nameChanged, "")
    K_AUTO_PROPERTY(bool, checked, checked, setChecked, checkedChanged, true)
    K_READONLY_PROPERTY(QImage, img, img, setImg, imgChanged, QImage())
    K_AUTO_PROPERTY(bool, choosed, choosed, setChoosed, choosedChanged, true)
    K_READONLY_PROPERTY(int, number, number, setNumber, numberChanged, false)
    K_READONLY_PROPERTY(int, count, count, setCount, countChanged, 0)
    K_READONLY_PROPERTY(QVariantList, parts, parts, setParts, partsChanged, QVariantList())
    friend class AppCore;
public:
    explicit ResultData(QObject *parent = nullptr);

signals:

};

#endif // RESULTDATA_H
