import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import App 1.0
import CQML 1.0 as C

ApplicationWindow {
    id: baseForm
    visible: true
    width: 512
    height: 276
    title: qsTr(" Lable ")

    ColumnLayout{
        anchors.fill: parent

        //Flow{
        //    Layout.fillHeight: true
        //    Layout.fillWidth: true
        //    spacing: 10
        //    Layout.margins: 10
        //
        //    Repeater{
        //        model: AppCore.test.model
        //        delegate: C.BaseBackground{
        //            width: 250
        //            height: 150
        //            elevation: 3
        //            backgroundColor: "white"
        //
        //            ColumnLayout{
        //                anchors.fill: parent
        //                C.BaseBackground{
        //                    Layout.fillWidth: true
        //                    Layout.preferredHeight: 120
        //                    elevation: 3
        //                    inverted: true
        //                    backgroundColor: "transparent"
        //
        //                    CodeItem{
        //                        anchors.fill: parent
        //                        source: modelData.image
        //                    }
        //                }
        //
        //                RowLayout{
        //                    Layout.fillWidth: true
        //                    Layout.preferredHeight: 30
        //                    Layout.maximumHeight: 30
        //
        //                    Text{
        //                        Layout.fillHeight: true
        //                        Layout.fillWidth: true
        //                        text: modelData.code
        //                        font.bold: true
        //                        verticalAlignment: Text.AlignVCenter
        //                        font.pointSize: 14
        //                    }
        //
        //                    Text{
        //                        Layout.fillHeight: true
        //                        Layout.fillWidth: true
        //                        text: modelData.time
        //                        font.bold: true
        //                        horizontalAlignment: Text.AlignRight
        //                        verticalAlignment: Text.AlignVCenter
        //                        font.pointSize: 14
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        CodeItem{
            Layout.fillHeight: true
            Layout.fillWidth: true
            source: AppCore.image
        }

        Item{
            Layout.fillWidth: true
            Layout.preferredHeight: 20
            TextField{
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft
                text: ""
                selectByMouse: true
                onTextChanged: {
                    //AppCore.test.setText(text)
                    AppCore.encode(text)
                }
            }
        }
    }
}
