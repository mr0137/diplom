#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <appcore.h>
#include <QAbstractListModel>
#include <QQuickStyle>
#include <QQmlEngine>
#include <QQuickView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QQuickStyle::setStyle("Fusion");
    //setWindowIcon(QIcon(":/main/icon/logo.png"));
    initQmlEngine();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initQmlEngine()
{
    auto engine = ui->quickWidget->engine();
    qmlRegisterSingletonType<AppCore>("App", 1, 0, "AppCore", &AppCore::qmlInstance);
    qmlRegisterAnonymousType<QAbstractListModel>("App", 1);

#ifdef QT_NO_DEBUG
    engine->addImportPath(AppCore::instance()->rootDir() + "/plugins/");
#else
    engine->addImportPath(AppCore::instance()->rootDir() + "/bin/plugins/");
#endif
    qDebug() << engine->importPathList();
    ui->quickWidget->setSource(QStringLiteral("qrc:/lable/lable/PrintList.qml"));
}
