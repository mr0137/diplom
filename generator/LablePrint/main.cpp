#include "mainwindow.h"

#include <QTextCodec>
#include <QApplication>
#include <appcore.h>
#include <QTextStream>
#ifdef Q_OS_WINDOWS
#include <windows.h>
#endif
static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0);

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString & msg)
{
#ifdef QT_NO_DEBUG
    // DevLogger::instance()->addLog(context.category, context.function, msg);
#endif
    if (!msg.contains("QQuickWidget does not support") && !msg.contains("If you wish to create your root"))
        (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, msg);
}

int main(int argc, char *argv[])
{

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("Windows-1251"));
    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));
    // setlocale (LC_CTYPE, "ukr" );
#ifdef Q_OS_WINDOWS
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);
#endif
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    qSetMessagePattern("%{if-category}\033[32m%{category}:%{endif}%{if-warning}\033[37m%{file}:%{line}: %{endif}\033\[31m%{if-debug}\033\[36m%{endif}[%{function}]\033[0m %{message}");
    qInstallMessageHandler(customMessageHandler);


    app.setOrganizationName("TK");
    app.setOrganizationDomain("TK");

    AppCore::instance()->setRootPath(argv[0]);
   // qDebug() << QString(app.arguments().at(1).isNull() ? "" : app.arguments().at(1));
#ifdef QT_NO_DEBUG
   //AppCore::instance()->setPathFile("//192.168.61.7/кза листообробка/Для логістики/В роботу/09011 КМЗ/віддала в роботу/09011_2 zk/09011_2 zk.nrp2");
   AppCore::instance()->setPathFile(app.arguments().at(1).isNull() ? "" : app.arguments().at(1));
#else
#ifdef Q_OS_LINUX
    AppCore::instance()->setPathFile("/home/user/Desktop/09011_2,5 zk.nrp2");
#else
    AppCore::instance()->setPathFile("//192.168.61.7/кза листообробка/Для логістики/В роботу/09011 КМЗ/1,5 zk/09011_1,5 zk.nrp2");

#endif
#endif
    qmlRegisterSingletonType<AppCore>("App", 1, 0, "AppCore", &AppCore::qmlInstance);
    qmlRegisterAnonymousType<QAbstractListModel>("App", 1);

    engine.addImportPath(AppCore::instance()->rootDir() + "/bin/plugins/");

    const QUrl url(QStringLiteral("qrc:/lable/lable/PrintList.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    //MainWindow w;
    //w.show();
    return app.exec();
}
