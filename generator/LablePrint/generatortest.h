#ifndef GENERATORTEST_H
#define GENERATORTEST_H

#include <QObject>
#include <kmacro.h>
#include <utility/kobservablelist.h>
#include <QElapsedTimer>
#include <QImage>

class ResultTest : public QObject
{
    Q_OBJECT
    K_AUTO_PROPERTY(QImage, image, image, setImage, imageChanged, QImage())
    K_AUTO_PROPERTY(QString, time, time, setTime, timeChanged, "")
    K_AUTO_PROPERTY(QString, code, code, setCode, codeChanged, "")

public:
    explicit ResultTest(QObject *parent = nullptr);

};

class GeneratorTest : public QObject
{
    Q_OBJECT
    K_READONLY_PROPERTY(KObservableModel*, model, model, setModel, modelChanged, nullptr)

public:
    explicit GeneratorTest(QObject *parent = nullptr);
public slots:
    void setText(QString text);
    void autoTest();



signals:

private:
    void code39(QString text);
    void code93(QString text);
    void codeUPC_A(QString text);
    void codeUPC_E(QString text);
    void codeEAN_13(QString text);
    void code128(QString text);
    void codeEXT(QString text);
    void codeDataMatrix(QString text);
    void codePDF417(QString text);
    void codeAztec(QString text);
    void codeQR(QString text);

    KObservableList<ResultTest> m_list;
    int m_width = 512;
    int m_height = 512;
    QElapsedTimer m_timer;
};

#endif // GENERATORTEST_H
