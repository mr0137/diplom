#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>
#include <qqml.h>
#include <qqmlengine.h>
#include <fileservice.h>
#include <nrpparser.h>
#include <QFuture>
#include <resultdata.h>
#include "printerservice.h"
#include <utility/kobservablelist.h>
#include <generatortest.h>
class QSqlDatabase;
class DatabaseService;

class AppCore : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(AppCore)
    QML_SINGLETON
    Q_PROPERTY(QString rootDir READ rootDir WRITE setRootDir NOTIFY rootDirChanged)
    Q_PROPERTY(PrinterService* printerService READ printerService WRITE setPrinterService NOTIFY printerServiceChanged)
    Q_PROPERTY(KObservableModel* results READ results WRITE setResults NOTIFY resultsChanged)
    Q_PROPERTY(bool parsing READ parsing WRITE setParsing NOTIFY parsingChanged)
    Q_PROPERTY(double loadingProgress READ loadingProgress WRITE setLoadingProgress NOTIFY loadingProgressChanged)
    Q_PROPERTY(QString loadingText READ loadingText WRITE setLoadingText NOTIFY loadingTextChanged)
    Q_PROPERTY(QImage image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(GeneratorTest* test READ test NOTIFY testChanged)
    explicit AppCore(QObject *parent = nullptr);
public:
    static AppCore* instance();
    void setPathFile(QString path);
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);
    void setRootPath(QString path);
    QString rootDir() const;

    PrinterService* printerService() const;

    FileService *files() const;
    void setFiles(FileService *files);
    ~AppCore(){ delete m_nrp; }

    KObservableModel *results() const;
    void setResults(KObservableModel *newResults);

    bool parsing() const;
    void setParsing(bool newParsing);

    double loadingProgress() const;
    void setLoadingProgress(double newLoadingProgress);

    const QString &loadingText() const;
    void setLoadingText(const QString &newLoadingText);

    const QImage &image() const;
    void setImage(const QImage &newImage);

    GeneratorTest *test() const;
    void setTest(GeneratorTest *newTest);

public slots:
    void setRootDir(QString rootDir);
    void openPrinterDialog();
    void setPrinterService(PrinterService* printerService);
    void selectResults(int from, int to);
    void disselectAll();
    void updateGeneratedLabels();
    void cancelSelection();
    void updatePage();

    void encode(QString text);
private slots:
    void generateFilters();
signals:
    void rootDirChanged(QString rootDir);
    void printerServiceChanged(PrinterService* printerService);
    void resultsChanged();
    void parsingChanged();
    void loadingProgressChanged();
    void loadingTextChanged();
    void imageChanged();

    void testChanged();

private:
    QTimer * m_timer = nullptr;
    QThread * m_workerThread = nullptr;
    QString pathFile;
    QString m_rootDir;
    PrinterService *m_printerService;
    NrpParser::NrpParser *m_nrp = nullptr;
    FileService * m_files = nullptr;
    QFuture<void> m_future;
    KObservableList<ResultData> m_resultsList;
    int checkFile(QSqlDatabase &db);
    bool checkPathFile(QSqlDatabase &db,const int id,const QString path);
    int saveFile(QSqlDatabase &db, QString path);
    bool insertUpackedFile(QSqlDatabase &db,QVector<NrpParser::ObjectFile> vFile, const int file_id);
    bool saveOrderBarcode(QSqlDatabase &db, const int file_id, QVariantList &list);
    void selectPartInfoOnFilter(QStringList list);
    QByteArray selectDxfFile(const int part_id, QSqlDatabase &db);
    KObservableModel *m_results;
    bool m_parsing = false;
    double m_loadingProgress;
    QString m_loadingText;
    QImage m_image;
    GeneratorTest *m_test;
};

#endif // APPCORE_H
