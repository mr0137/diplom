#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "NrpParser_global.h"
#include <QString>
#include <QVector>
#include <QXmlStreamReader>
#include <private/qzipreader_p.h>
#include <QDebug>
#include <QPointF>
#include <xlsxdocument.h>

/*!
 *  \addtogroup NrpParser
 *  @{
 */


namespace NrpParser {
//struct XMLPARSER_EXPORT FileData {

//};

//!
//! \brief The Part struct
//!
struct XMLPARSER_EXPORT Part {
    //    int x;
    //    int y;
    //    int h;
    //    int w;
    int amount;
    int done;
    int amountUsed;
    QString name;
    QString portionName;
    QString uid;
    int handle;
    QString file; // because handleId can match in many nrp files
    int thickness;


    //!
    //! \brief operator << is used to print content of struct to debug
    //! \param dbg
    //! \param type
    //! \return QDebug object
    //!
    friend QDebug operator<<(QDebug dbg, const Part &type)
    {
        return dbg.space() << "Part(" << "name:" <<type.name << "amount" << type.amount << "amount used" << type.amountUsed << "ID:" << type.uid << "handle" << type.handle << ")" << Qt::endl;
    }

    //!
    //! \brief operator << is used to print content of struct to debug ( overloaded with pointer)
    //! \param dbg
    //! \param type
    //! \return QDebug obejct
    //!
    friend QDebug operator<<(QDebug dbg, const Part *type)
    {
        if(type == nullptr) dbg << "Part (" << nullptr << ")" << Qt::endl;
        else dbg.space() << "Part(" << "name:" <<type->name << "amount" << type->amount << "amount used" << type->amountUsed << "ID:" << type->uid << "handle" << type->handle << ")" << Qt::endl;
        return dbg.maybeSpace();
    }
};

//!
//! \brief The Plate struct
//!
struct XMLPARSER_EXPORT Plate{

    //    QVector<Part*> parts;
    int amount;
    //    int h;
    //    int w;
    //    int done;
    int handle;
    QString name;
    QString uid;
    QString portionName;
    QString outlineID;
    QString Class;
    int plateType;
    int amountUsed;
    QString file; // because handleId can match in many nrp files
    //    int x;
    //    int y;
    //!
    //! \brief operator << is used to print Plate struct contents to QDebug
    //! \param dbg
    //! \param type
    //! \return QDebug object
    //!
    friend QDebug operator<<(QDebug dbg, const Plate *type)
    {
        return dbg.space() << "Plate(" << "name:" <<type->name << "amount" << type->amount << "amount used" << type->amountUsed << "ID:" << type->uid << "handle" << type->handle << ")" << Qt::endl;
    }
};

//!
//! \brief The Shape struct
//!
struct XMLPARSER_EXPORT Shape{
    int handle;
    QString Class;
    QVector<Part *> part;
    QString blockName;

    //!
    //! \brief operator << is used to print Shape struct contents to QDebug
    //! \param dbg
    //! \param type
    //! \return QDebug object
    //!
    friend QDebug operator<<(QDebug dbg, const Shape *type)
    {
        return dbg.space() << "Shape(" << "class:" << type->Class << "blockName" << type->blockName << "handle" << type->handle << ")" << Qt::endl;
    }
};


struct XMLPARSER_EXPORT ResultStat{
    QString lengthOfCurve;
    QString lengthOfMove;
    int pierceCount;
    QString cutTime;
    QString moveTime;
    QString pierceTime;
    QString delayTime;
    QString totalTime;
    QString name;
    QString id;
};

//!
//! \brief The Result struct
//!
struct XMLPARSER_EXPORT Result{

    //    QVector<Part*> parts;
    //    int amount;
    //    int h;
    //    int w;
    //    int done;
    int handle;
    QString name;
    QString uid;
    QString portionName;
    QString thikness;
    double utilization;
    Plate * plate;
    int plateGap;
    int plateAmount;
    QString catalogueName;
    QString cutLength;
    QString pierceCount;

    QPointF extMin;
    QPointF extMax;

    QVector<Part*> parts;

    QString file; // because handleId can match in many nrp files

    //!
    //! \brief operator << is used to print Result (pointer) struct contents to QDebug
    //! \param dbg
    //! \param type
    //! \return QDebug object
    //!
    friend QDebug operator<<(QDebug dbg, const Result *type)
    {
        return dbg.space() << "Result(" << "name:" <<type->name << "plateAmount" << type->plateAmount << "ID:" << type->uid << "handle" << type->handle << ")" << Qt::endl
                           << "Plate";
    }
};

//!
//! \brief The ExcelPart struct
//!
struct XMLPARSER_EXPORT ExcelPart {
    QString name;
    int count;
    QString metalType;
    int thickness;
    int position;
    QString file;

    //!
    //! \brief operator << prints to QDebug
    //! \param dbg
    //! \param type
    //! \return QDebug object
    //!
    friend QDebug operator<<(QDebug dbg, const ExcelPart & type)
    {
        return dbg.space() << "ExcelPart(" << "name:" <<type.name << "count" << type.count << "handle" << type.metalType << ")" << Qt::endl;
    }
};

//!
//! \brief The ErrorType enum
//!
enum ErrorType {Mismatch, ExcelNotFound, NestNotFound, Equal};

//!
//! \brief The CompareResult struct
//!
struct XMLPARSER_EXPORT CompareResult {
    QString partName;
    ErrorType type;
    int nestCount;
    int excelSum;
};

//!
//! \ObjectFile
//!
struct ObjectFile{
    QString path = "/";
    QString name{};
    QString ext = " ";
    QByteArray bytes{};
    QString xml{};
};
//!
//! \brief The NrpParser class
//!
class XMLPARSER_EXPORT NrpParser
{
    const QMap<QString, QString> codeMapping = {
        {
            "BC3", "ВСЗ"
        },
        {
            "3CO", "ЗСО"
        }
    };



    QXlsx::Document * m_doc = nullptr;

    QVector<QVector<ExcelPart>> fromExcel;


    QXmlStreamReader reader;
    QVector<QString> fileNames;

    QVector<QString> xlsxFileNames;

    QVector<QMap<QString, QVariant>> columns;
    QVector<QMap<QString, bool>> compareBy;
    QVector<QVariantList> allColumns;


    QVector<QByteArray> partsData;
    //    QList<Part*> parts;
    QMap<QString, Part*> handlePartMapping;

    QVector<QByteArray> plateData;
    QMap <QString , Plate *> handlePlateMapping;


    QVector<QByteArray> resultsData;
    QByteArray nsdsData;

    QMap<int, QString> resultCatalogueMapping;
    QMap<int, QString> partsCatalogueMapping;
    QMap<int, QString> platesCatalogueMapping;

    QVector<Result*> results;
    QMap<QString, ResultStat*> resultStats;

    QVector<ObjectFile> m_objectFiles;



    //!
    //! \brief Init parser
    //! \param data
    //!
    void init(const QByteArray & data);
    //!
    //! \brief Opens file by given  filename \c fileName
    //! \param fileName
    //! \return
    //!
    QByteArray openFile(const QString & fileName);

    //!
    //! \brief Parses session section
    //!
    void parseSession();

    //!
    //! \brief Parses parts section
    //! \param i
    //!
    void parseParts(int i);
    //!
    //! \brief Parses nest part section
    //! \param i
    //!
    void parseNestPart(int i);

    //!
    //! \brief Parses plates section
    //! \param i
    //!
    void parsePlates(int i);
    //!
    //! \brief Parses nest plates section
    //! \param i
    //!
    void parseNestPlate(int i);
    //!
    //! \brief parses results section
    //!
    void parseResults();
    //!
    //! \brief parses result section
    //! \param i
    //!
    void parseResult(int i);

    void parseResultStats(QString data);

    //    void parseShapes(Result *r);
    //    void parseShape(Result * r);
    //    void parseNestShapePart(Shape * s);

    //!
    //! \brief parses NestedShapePart section
    //! \param s
    //!
    void parseNestedShapePart(Shape * s);

    //!
    //! \brief parses BlockName section
    //! \param s
    //!
    void parseBlockName(Shape * s);

    //!
    //! \brief parses ExtMinMax section
    //! \param r
    //!
    void parseExtMinMax(Result *r);
    //!
    //! \brief parseNestedParts
    //! \param r
    //!
    void parseNestedParts(Result *r);
    //!
    //! \brief parse NestedPart section
    //! \param r
    //!
    void parseNestedPart(Result *r);

    //!
    //! \brief converts code to predefined cyrillic equivalent
    //! \param name
    //! \return
    //!
    QString &convertToCyrillic(QString & name);

    //!
    //! \brief Saves to file (e.g. CSV)
    //! \param fileName
    //! \param format
    //!
    void saveToFile(QString fileName, QString format);


    QString codeColIndex;
    QString metalTypeColIndex;
    QString thicknessColIndex;
    QString countColIndex;

    QVector<CompareResult> compareResults;

public:
    //!
    //! \brief NrpParser constructor
    //!
    explicit NrpParser();
    //!
    //! NrpParser destructor
    //!
    ~NrpParser();
    //    NrpParser(const QString &fileName);
    //!
    //! \brief NrpParser constructor with filename
    //! \param fileName
    //!
    explicit NrpParser(const QVector<QString> &fileName);
    //!
    //! \brief NrpParser ctor from raw data
    //! \param binary
    //!
    explicit NrpParser(const QByteArray & binary);
    //!
    //! \brief NrpParser ctor with folder
    //! \param folder
    //!
    explicit NrpParser(const QString & folder);
    //!
    //! \brief NrpParser ctor with raw string
    //!
    explicit NrpParser(const char *);

    //!
    //! \brief Loads xlsx from file name
    //! \param xlsxName
    //!
    void loadXlsx(const QString & xlsxName);
    //!
    //! \brief Loads XLSX by given index \p i
    //! \param i
    //!
    void loadXlsx(int i);
    //!
    //! \brief Loads xlsx
    //!
    void loadXlsx();

    //!
    //! \brief Reloads xlsx by given index \p i
    //! \param i
    //!
    void reloadXlsx(int i);

    //!
    //! \brief Add XLSX by given name \p xlsxName
    //! \param xlsxName
    //! \return
    //!
    QVariantList addXlsx(const QString & xlsxName);
    //!
    //! \brief Set filters
    //! \param filters
    //!
    void setFilters(const QVariantMap & filters);
    //!
    //! \brief set filters to document with index \p i
    //! \param filters
    //! \param i
    //!
    void setFilters(const QVariantMap & filters, int i);
    //!
    //! \brief set filter to document with index \p i
    //! \param filter
    //! \param i
    //!
    void setFilter(const QPair<QString, QVariant> & filter, int i);
    //!
    //! \brief Compares xlsx data to Nrp data and returns results
    //! \return
    //!
    QVariantList checkXlsx();


    //!
    //! \brief Sets rules for comparing NRP and EXCEL
    //! \param rules
    //! \param i
    //!
    void setCompareBy(const QString & name, bool check);
    //!
    //! \brief Sets rules for comparing NRP and EXCEL to document with index \p i
    //! \param name
    //! \param check
    //! \param i
    //!
    void setCompareBy(const QString & name, bool check, int i);
    //!
    //! \brief Adds rule for comparing NRP and EXCEL
    //! \param rules
    //! \param i
    //!
    void setCompareBy(const QMap<QString, bool> & rules);
    //!
    //! \brief Sets rules for comparing NRP and EXCEL
    //! \param rules
    //! \param i
    //!
    void setCompareBy(const QMap<QString, bool> & rules, int i);


    //!
    //! \brief Loads NRP file
    //! \param fileName
    //!
    void load(const QString &fileName);
    //!
    //! \brief Loads from raw data
    //! \param binary
    //!
    void load(const QByteArray & binary);

    //!
    //! \brief Loads set of NRP files
    //! \param files
    //!
    void loadFiles(const QVector<QString> & files);

    //!
    //! \brief Loads folder of NRP files
    //! \param path
    //!
    void loadFolder(const QString & path);

    //!
    //! \brief Prints list of result
    //!
    void printResultList();

    //!
    //! \brief Overloaded function. Parses specific section in XML file
    //! \param what
    //! \param i
    //!
    void parse(const QString &what, int i);
    //!
    //! \brief Parses XML
    //!
    void parse();


    //!
    //! \brief Get parts as QList
    //! \return list of part pointers
    //!
    QList<Part *> getPartsAsList() const;
    //!
    //! \brief Get plates as QList
    //! \return list of Plate objects
    //!
    QList<Plate *> getPlatesAsList() const;

    //!
    //! \brief get result list
    //! \return set of Result pointers
    //!
    QVector<Result*> resultList() {return results;}

    //!
    //! \brief Get part mapping
    //! \return map of parts
    //!
    QMap<QString, Part*> getPartMapping() {return handlePartMapping;}
    //!
    //! \brief Get plate mapping
    //! \return map of plates
    //!
    QMap<QString, Plate*> getPlateMapping(){return handlePlateMapping;}

    //!
    //! \brief Clears all mappings, buffers, lists and all variables
    //!
    void clearAll();

    //!
    //! \brief Clears xlsx buffers
    //!
    void clearXlsx();

    //!
    //! \brief Provides compare results
    //! \return set of CompareResult objects
    //!
    QVector<CompareResult> & getCompareResults() {return compareResults;}

    //!
    //! \brief Removes Xlsx document from buffer
    //! \param index
    //!
    void removeXlsxDocument(int index);

    //!
    //! \brief Get all columns
    //! \param index
    //! \return QVariantList of columns
    //!
    QVariantList getAllColumns(int index);
    //!
    //! \brief Returns filters by index of xlsx document
    //! \param index
    //! \return filters of documents as QVariantMap
    //!
    QVariantMap getFilters(int index);

    //!
    //! \brief Get compare parameters
    //! \param index
    //! \return compare parameters as QMap<QString, bool> which means <parameter: on/off>
    //!
    QMap<QString, bool> getCompareBy(int index);

    QVector<ObjectFile> getObjectFiles() const;
};
}

/*! @} End of Doxygen Groups*/

#endif // XMLPARSER_H
