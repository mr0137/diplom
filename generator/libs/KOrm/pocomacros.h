#pragma once

#include "customproxymodel.h"
#include "compareengine.h"
#include "klist.h"
#include <QtQml>

#ifndef Q_MOC_RUN
     #  define FIELD_SIGNAL_TAG
     #  define COMPLEX_FIELD_SIGNAL_TAG
#endif

#define K_LIST(TYPE, NAME) \
Q_PROPERTY(KShellModel* NAME##Model READ NAME##Model NOTIFY NAME##ModelChanged) \
public: \
    KShellModel* NAME##Model() { return this->m_ ## NAME.model(); } \
    Q_SIGNAL void NAME##ModelChanged(KShellModel* value); \
    KList<TYPE>& NAME() {return m_##NAME;} \
private: \
    KList<TYPE> m_##NAME;

#define K_LIST_PROXED(TYPE, NAME) \
Q_PROPERTY(CustomProxyModel* NAME##Model READ NAME##Model NOTIFY NAME##ModelChanged) \
public: \
    CustomProxyModel* NAME##Model() { \
        if(m_##NAME##_proxy_model == nullptr){\
            m_##NAME##_proxy_model = new CustomProxyModel(this);\
            m_ ## NAME.model()->setParent(this); \
            m_##NAME##_proxy_model->setSourceModel(m_ ## NAME.model());\
        }\
        return m_##NAME##_proxy_model;\
    }\
    Q_SIGNAL void NAME##ModelChanged(CustomProxyModel* value); \
    KList<TYPE>& NAME() {return m_##NAME;} \
private: \
    KList<TYPE> m_##NAME;\
    CustomProxyModel* m_##NAME##_proxy_model = nullptr;

#define K_POCO_DEFCONSTRUCTOR(NAME) \
    public: NAME(QObject *parent = nullptr): KPoco(parent){}

#define K_FIELD(TYPE, NAME, DEFAULT) \
Q_PROPERTY(TYPE NAME READ NAME WRITE set_##NAME NOTIFY NAME##changed) \
public: \
    TYPE NAME() const { return this->m_ ## NAME; } \
    Q_SLOT void set_##NAME(TYPE value) { \
        if (KOrmCompareEngine<TYPE>::compare(this->m_ ## NAME, value)) return; \
        this->m_ ## NAME = value; \
        emit NAME##changed(value); \
        emit fieldChanged(#NAME); \
    } \
    Q_SIGNAL FIELD_SIGNAL_TAG void NAME##changed(TYPE value); \
private: \
    TYPE m_ ## NAME = DEFAULT;

#define K_COMPLEX_FIELD(TYPE, NAME, DEFAULT) \
Q_PROPERTY(TYPE* NAME READ NAME WRITE set_##NAME NOTIFY NAME##changed) \
public: \
    TYPE* NAME() const { return this->m_ ## NAME; } \
    Q_SLOT void set_##NAME(TYPE* value) { \
        if(value == this->m_ ## NAME) return; \
        this->m_ ## NAME = value; \
        emit NAME##changed(value); \
        emit fieldChanged(#NAME); \
        connectComplexField(#NAME);\
    } \
    Q_SIGNAL COMPLEX_FIELD_SIGNAL_TAG void NAME##changed(TYPE* value); \
private: \
    TYPE* m_ ## NAME = DEFAULT;

