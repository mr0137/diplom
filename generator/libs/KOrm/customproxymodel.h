#ifndef CUSTOMPROXYMODEL_H
#define CUSTOMPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <functional>
#include "KOrm_global.h"

class KORM_EXPORT CustomProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
public:
    Q_PROPERTY(QAbstractItemModel *source READ source WRITE setSource)
    //Q_PROPERTY(QSortFilterProxyModel *source READ source WRITE setSource)

    CustomProxyModel(QObject * parent = nullptr);
    void setFilter(std::function<bool (QObject*)> filter);

public slots:
    QAbstractItemModel * source() const;
    void setSource(QAbstractItemModel * source);
    void setFilterRow(const int source_row);
    QObject * getObject(const int row);
    void sortModelOnColumn(const int role, const int column);
    void shovActiveComponent(const int role, QString);
    void setFilterOnRole(const int role, const QString value);
    void resetModel();

private:
    int roleKey(const QByteArray &role) const;
    std::function<bool (QObject *)> m_filter = nullptr;

    QAbstractItemModel * m_source;

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    //virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
};

#endif // CUSTOMPROXYMODEL_H
