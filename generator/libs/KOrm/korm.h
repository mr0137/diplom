#ifndef KORM_H
#define KORM_H

#include "KOrm_global.h"
#include "klist.h"

#include<QSqlQuery>
#include<QSqlRecord>

class KORM_EXPORT KOrm
{
public:
    KOrm();

    template<class T>
    void populateList(KList<T>* lst, QString sql);
};

template<class T>
void KOrm::populateList(KList<T> *lst, QString sql)
{
    QSqlQuery q;
    q.exec(sql);

    //q.record().count()
}

/*
Korm korm;
korm.populateList(users, "SELECT id, name FROM users", db);

*/


#endif // KORM_H
