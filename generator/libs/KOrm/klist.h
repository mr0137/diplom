#ifndef KLIST_H
#define KLIST_H

#include "kpoco.h"
#include "KOrm_global.h"
#include <QObject>
#include <QAbstractItemModel>
#include <QRegularExpression>
#include <QDebug>

class KShellModel;

template <class T>
class KORM_EXPORT KList {
public:
    explicit KList();
    ~KList();
    void push_back(T*);
    void push_front(T*);
    T* get(int);
    T* first();
    T* last();
    void remove(int);
    void remove(T*);
    void append(QList<T*> &);
    void append(T*);
    T* takeAt(int ind);
    inline int indexOf(T*);
    inline int indexOf(KPoco*);
    void clear();
    void insert(int, T*);
    int count(){return m_data.count();}
    auto begin() const { return m_data.begin(); }
    auto end() const { return m_data.end(); }
    void setFilterRule(const QString &property, const QRegularExpression &regex);

    KShellModel* model();


    T* find(std::function<bool(T*)> predicate);
private:
    QList<T*> m_data;
    KShellModel *m_modelShell = nullptr;
    QPair<QString, QRegularExpression> m_filterRule;
    bool filt(QVariant value);
    void connectObject(KPoco*);
};


class KORM_EXPORT KShellModel : public QAbstractListModel
{
    Q_OBJECT
    template<typename> friend class KList;
public:
    //ObservableModel(){}
    int rowCount(const QModelIndex &) const override {
        return m_sizzer();
    }
    QVariant data(const QModelIndex &index, int) const override;

    QHash<int, QByteArray> roleNames() const override { return m_roles; /*{{ Qt::UserRole + 1, "object" }};*/}



    QMap<QString, int> getFieldToRoleMap() const;
    int getIndexRoleOnName(QString nameRole)const;
    void setFieldToRoleMap(const QMap<QString, int> &value);

public slots:
    void updateModel() { emit modelReset({}); }
    int count() { return m_sizzer(); }



    QObject *getObject(int);


private:
    std::function<QObject*(int)> m_getter;
    std::function<int()> m_sizzer;
    std::function<bool(QObject*)> m_filter;
    QHash<int, QByteArray> m_roles;
    QHash<QByteArray, QVector<int>> m_rolesName2indexes;
    void _buildIndex();
    QMap<QString, int> fieldToRoleMap;
};


template<class T>
KList<T>::KList() {
    m_modelShell = new KShellModel();
    m_modelShell->m_getter = [this](int ind){ return static_cast<QObject*>(this->m_data[ind]); };
    m_modelShell->m_sizzer = [this](){ return this->m_data.count(); };
    m_modelShell->m_filter = [this](QObject* object) { return m_filterRule.first.isEmpty() ? true : filt(object->property(m_filterRule.first.toStdString().c_str())); };

    QHash<int, QByteArray> roles;
    auto lst = T().getFields(4);
    int i = Qt::UserRole+2;
    for (auto r : lst) {
        m_modelShell->fieldToRoleMap[r] = i;
        roles[i++] = r;
    }

    roles[Qt::UserRole+1] = "object";
    m_modelShell->m_roles = roles;
    m_modelShell->_buildIndex();
}


template<class T>
KShellModel *KList<T>::model()
{
    return m_modelShell;
}

template<class T>
KList<T>::~KList() {
    delete m_modelShell;
}

template<class T>
void KList<T>::push_front(T *v)
{
    if (m_data.length() == 0) init(v);
    m_modelShell->beginInsertRows(QModelIndex(), 0, 0);
    m_data.push_front(v);
    m_modelShell->endInsertRows();
    connectObject(v);
}

template<class T>
void KList<T>::push_back(T *v)
{
    m_modelShell->beginInsertRows(QModelIndex(), m_data.count(), m_data.count());
    m_data.append(v);
    m_modelShell->endInsertRows();
    connectObject(v);
}

template<class T>
T* KList<T>::first()
{
    return m_data.first();
}

template<class T>
T* KList<T>::last()
{
    return m_data.last();
}

template<class T>
T *KList<T>::get(int ind)
{
    return m_data[ind];
}

template<class T>
void KList<T>::append(QList<T *> &list)
{
    if (m_data.length() == 0) {
        if (!list.isEmpty())
        init(list.first());
    }
    int stIndex = m_data.length();
    m_modelShell->beginInsertRows(QModelIndex(), stIndex, stIndex + list.length() - 1);
    m_data.append(list);
    m_modelShell->endInsertColumns();
}

template<class T>
void KList<T>::append(T *v)
{
    push_back(v);
}

template<class T>
void KList<T>::remove(int ind)
{
    m_modelShell->beginRemoveRows(QModelIndex(), ind, ind);
    auto r =  m_data.takeAt(ind);
    Q_UNUSED(r)
    m_modelShell->endRemoveRows();
}

template<class T>
T* KList<T>::takeAt(int ind)
{
    m_modelShell->beginRemoveRows(QModelIndex(), ind, ind);
    auto r =  m_data.takeAt(ind);
    m_modelShell->endRemoveRows();
    return r;
}

template<class T>
void KList<T>::remove(T *v)
{
    int ind = m_data.indexOf(v);
    m_modelShell->beginRemoveRows(QModelIndex(), ind, ind);
    m_data.removeAt(ind);
    m_modelShell->endRemoveRows();
}

template<class T>
int KList<T>::indexOf(T *v)
{
    return m_data.indexOf(v);
}

template<class T>
int KList<T>::indexOf(KPoco *v)
{
    auto vv = qobject_cast<T*>(v);
    return m_data.indexOf(vv);
}


template<class T>
void KList<T>::clear()
{
    m_modelShell->beginResetModel();
    m_data.clear();
    m_modelShell->endResetModel();
}

template<class T>
void KList<T>::insert(int pos, T *v)
{
    if (pos > m_data.size() - 1) push_back(v);
    else if (pos < 0){

        m_modelShell->beginInsertRows(QModelIndex(), 0, 0);
        m_data.push_front(v);
        m_modelShell->endInsertRows();
    }
    else
    {
        m_modelShell->beginInsertRows(QModelIndex(), pos, pos);
        m_data.insert(pos, v);
        m_modelShell->endInsertRows();
    }
    connectObject(v);
}

template<class T>
void KList<T>::setFilterRule(const QString &property, const QRegularExpression &regex) {
    m_filterRule = { property, regex };
    m_modelShell->dataChanged(m_modelShell->index(0), m_modelShell->index(m_modelShell->m_sizzer()));
}

template<class T>
bool KList<T>::filt(QVariant value) {
    if(value.isNull())
        return false;

    QRegularExpressionMatch match = m_filterRule.second.match(value.toString());
    return match.capturedTexts().size() > 0;
}

template <class T>
void KList<T>::connectObject(KPoco* poco)
{
    QObject::connect(poco, &KPoco::fieldChanged, m_modelShell, [this, poco](QByteArray propName){
        int index = indexOf(poco);
        auto modelIndex = m_modelShell->index(index);
        auto search = m_modelShell->m_rolesName2indexes.find(propName);
        if (search != m_modelShell->m_rolesName2indexes.end())
            emit m_modelShell->dataChanged(modelIndex, modelIndex, search.value());
    });
}


template<class T>
T *KList<T>::find(std::function<bool (T *)> predicate)
{
    for (int  i =0, l = m_data.count();i <l; ++i) {
        if (predicate(m_data[i])) return m_data[i];
    }
    return nullptr;
}

#endif // KLIST_H
