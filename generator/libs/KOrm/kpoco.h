#ifndef KPOCO_H
#define KPOCO_H

#include "KOrm_global.h"

#include <QVariant>
#include <QObject>

class KORM_EXPORT KPoco : public QObject
{
    Q_OBJECT
signals:
    void fieldChanged(QByteArray);

public:
    KPoco(QObject *parent = nullptr);

    void init(QVariantMap);
    QVariant getField(QByteArray field);
    void setField(QByteArray field, QVariant);

    //structure
    QList<QByteArray> getFields(int level);
    void printStruct(int level);

protected:
    void connectComplexField(QString fieldName);
    //template<class T> T setFieldProperty(QString name, std::function<QVariant()> getter, T defaultVal);
    //template<class T> T setComplexFieldProperty(QString name, std::function<T()> getter, T defaultVal);

private:
    struct FieldInfo
    {
        FieldInfo() = default;
        QMetaObject::Connection connection = QMetaObject::Connection();
    };
    std::map<QString, FieldInfo> m_fieldsInfo;

    void _printRecursive(int lvl, const QMetaObject*, QString tabs);
    QList<QByteArray> _getFieldsRecursive(int lvl, const QMetaObject*, QString prefix, QString delimeter);
};

//
//template<class T> T KPoco::setFieldProperty(QString name, std::function<QVariant()> getter, T defaultVal)
//{
//    m_fieldsInfo[name] = FieldInfo(name, false);
//    m_fieldsInfo[name].get = getter;
//    return defaultVal;
//}
//
//template<class T> T KPoco::setComplexFieldProperty(QString name, std::function<T()> getter, T defaultVal)
//{
//    m_fieldsInfo[name] = FieldInfo(name, true);
//    m_fieldsInfo[name].getPoco = getter;
//    return defaultVal;
//}

#endif // KPOCO_H
