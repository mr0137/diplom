#include "kpoco.h"

#include <QVariant>
#include <QMetaObject>
#include <QMetaProperty>
#include <QDebug>

KPoco::KPoco(QObject *parent) : QObject(parent)
{

}


QVariant KPoco::getField(QByteArray field)
{
    auto path = field.split('_');
    QObject* obj = this;
    for (int i =0,l=path.size(); i<l; ++i) {
        auto mo = obj->metaObject();
        auto propInd = mo->indexOfProperty(path[i]);
        auto prop = mo->property(propInd);
        auto tag = prop.notifySignal().tag();
        if (0==strcmp(tag,"FIELD_SIGNAL_TAG")){
            if (i != (l-1)) {
                qDebug() << "KPoco::getField -> path not finished: " << field;
                return QVariant();
            }
            return prop.read(obj);
        }
        if (0==strcmp(tag,"COMPLEX_FIELD_SIGNAL_TAG")){
            if (i==(l-1)) {
                return prop.read(obj);
            }
            obj = prop.read(obj).value<QObject*>();
            if (obj == nullptr) {
                qDebug() << "KPoco::getField -> prop " << path[i] << " is empty";
                return QVariant();
            }
        }
    }
    return QVariant();
}

void KPoco::setField(QByteArray field, QVariant val)
{
    auto path = field.split('_');
    QObject* obj = this;
    for (int i =0,l=path.size(); i<l; ++i) {
        auto mo = obj->metaObject();
        auto propInd = mo->indexOfProperty(path[i]);
        auto prop = mo->property(propInd);
        auto tag = prop.notifySignal().tag();
        if (0==strcmp(tag,"FIELD_SIGNAL_TAG")){
            if (i != (l-1)) {
                qDebug() << "KPoco::getField -> path not finished: " << field;
                return;
            }
            prop.write(obj, val);
        }
        if (0==strcmp(tag,"COMPLEX_FIELD_SIGNAL_TAG")){
            if (i==(l-1)) {
                prop.write(obj, val);
            }
            obj = prop.read(obj).value<QObject*>();
            if (obj == nullptr) {
                qDebug() << "KPoco::getField -> prop " << path[i] << " is empty";
                return;
            }
        }
    }
}

//QVariant KPoco::getField(QString field)
//{
//    auto p = field.split("_");
//    auto search = m_fieldsInfo.find(p.first());
//    if (search != m_fieldsInfo.end()) {
//        if (search->second.complex) {
//            auto pp = search->second.getPoco();
//            if (pp == nullptr) return QVariant();
//            return pp->getField(p.mid(1).join("_"));
//        }
//        return search->second.get();
//    }
//    return QVariant();
//}

void KPoco::connectComplexField(QString fieldName)
{
    auto prop = metaObject()->property(metaObject()->indexOfProperty(fieldName.toUtf8()));
    auto propObj = prop.read(this).value<KPoco*>();
    if (propObj) {
        auto propName = prop.name();
        auto search = m_fieldsInfo.find(propName);
        if (search != m_fieldsInfo.end()) {
            disconnect(search->second.connection);
        }
        auto c = connect(propObj, &KPoco::fieldChanged, this, [this,propName](QString subPropName){
            emit fieldChanged((propName + QString("_") + subPropName).toUtf8());
        });
        m_fieldsInfo[propName].connection = c;
    }
}


void KPoco::_printRecursive(int lvl, const QMetaObject *mo, QString tabs)
{
    if (lvl == 0) {
        qDebug() << "maxLvl";
        return;
    }
    auto cnt = mo->propertyCount();
    for (int  i=mo->propertyOffset();i<cnt;++i){
        auto prop = mo->property(i);
        auto propTag = prop.notifySignal().tag();
        if (0 == strcmp(propTag,"FIELD_SIGNAL_TAG")) {
            qDebug() << tabs << prop.name();
        }
        if (0 == strcmp(propTag,"COMPLEX_FIELD_SIGNAL_TAG")) {
            qDebug() << tabs << "c  "<<prop.name();

            _printRecursive(lvl-1, QMetaType::metaObjectForType(prop.userType()), tabs+"  ");
        }
    }
}

QList<QByteArray> KPoco::_getFieldsRecursive(int lvl, const QMetaObject *mo, QString prefix, QString delimeter)
{
    QList<QByteArray> res;
    if (lvl == 0 || mo == nullptr) {
        return res;
    }
    auto cnt = mo->propertyCount();
    for (int  i=mo->propertyOffset();i<cnt;++i){
        auto prop = mo->property(i);
        auto propTag = prop.notifySignal().tag();
        if (0 == strcmp(propTag,"FIELD_SIGNAL_TAG")) {
            res.push_back((prefix + delimeter + prop.name()).toUtf8());
        }
        if (0 == strcmp(propTag,"COMPLEX_FIELD_SIGNAL_TAG")) {
            auto propName = (prefix + delimeter + prop.name()).toUtf8();
            res.push_back(propName);

            res.append(_getFieldsRecursive(lvl-1, QMetaType::metaObjectForType(prop.userType()), propName, "_"));
        }
    }
    return res;
}

QList<QByteArray> KPoco::getFields(int level)
{
    return _getFieldsRecursive(level, metaObject(), "", "");
}

void KPoco::printStruct(int level)
{
    _printRecursive(level, metaObject(), "");
}

void KPoco::init(QVariantMap m)
{
    for(auto p : m.toStdMap()) {
        setField(p.first.toUtf8(), p.second);
    }
}
