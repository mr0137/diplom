#include <backend-qt/mtexttohtml.h>
#include <QRegExp>
#include <QTextDocument>
#include <QTextCursor>
#include <QDebug>

QString MTextToHTML::convert(QString mtext)
{
    QRegExp font("f(.+)\\|b(.)\\|i(.)\\|c.+\\|p.+;");
    font.setMinimal(true);

    //mtext = "{\\fArial|b0|i0|c238|p34;Mezní úchylka\\PISO 2768 - mK}";
    //mtext = "Mezní úchylka\\PISO 2768 - mK";
//    mtext.replace(font, "");

    QTextDocument doc;

    QTextCursor cur(&doc);

    QTextCharFormat format;

    QRegExp onechars("[LlOoKkP\\\\]");

    bool istag = false;
    QString tag;

    for(int i=0; i< mtext.length(); i++)
    {
        QChar c = mtext[i];

        if(c == '\\' && istag==false)
        {
            istag = true;
            continue;
        }

        if(istag)
            tag += c;

        if(!istag)
            cur.insertText(c);

        if(istag && tag.length() == 1)
        {
            if(onechars.exactMatch(tag))
            {
                switch( tag[0].toLatin1() )
                {
                case 'L':
                    format.setFontUnderline(true);
                    break;
                case 'l':
                    format.setFontUnderline(false);
                    break;

                case 'O':
                    format.setFontOverline(true);
                    break;

                case 'o':
                    format.setFontOverline(false);
                    break;

                case 'K':
                    format.setFontStrikeOut(true);
                    break;

                case 'k':
                    format.setFontStrikeOut(false);
                    break;

                case 'X':
                case 'P':
                    cur.insertBlock();
                    break;

                case '\\':
                    cur.insertText("\\");
                    break;
                }

                istag = false;
                tag.clear();
            }

            else
            {
                int index = mtext.indexOf(";", i);
                int n = index-i;

                tag += mtext.mid(i+1, n);

                qDebug() << "TAG: " << tag;

                if(font.indexIn(tag) != -1)
                {
                    // capturedTexts: [1] font family [2] bold [3] italic
                    format.setFontFamily(font.capturedTexts()[1]);
                    format.setFontWeight( font.capturedTexts()[2] == "1" ? QFont::Bold : QFont::Normal);
                    format.setFontItalic(font.capturedTexts()[3].toInt());
                }

                i = index;
                istag = false;
                tag.clear();
            }
        }



    }
    mtext = doc.toHtml();

    return mtext;
}
