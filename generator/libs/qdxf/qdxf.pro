#-------------------------------------------------
#
# Project created by QtCreator 2011-03-22T19:33:11
#
#-------------------------------------------------

TEMPLATE = lib
TARGET = qdxf

QT += gui
QT += core
QT += widgets
CONFIG += shared dll

include($$PWD/../destidir.pri)

SOURCES += \
    backend-qt/dxfinterface.cpp \
    backend-qt/dxfsceneview.cpp \
    backend-qt/mtexttohtml.cpp \
    backend-qt/spline.cpp \
    backend/drw_entities.cpp \
    backend/drw_objects.cpp \
    backend/intern/drw_textcodec.cpp \
    backend/intern/dxfreader.cpp \
    backend/intern/dxfwriter.cpp \
    backend/libdxfrw.cpp

HEADERS += \
    backend-qt/dxfinterface.h \
    backend-qt/dxfsceneview.h \
    backend-qt/mtexttohtml.h \
    backend-qt/scene_items.h \
    backend-qt/spline.h \
    backend/drw_base.h \
    backend/drw_entities.h \
    backend/drw_interface.h \
    backend/drw_objects.h \
    backend/intern/drw_cptable932.h \
    backend/intern/drw_cptable936.h \
    backend/intern/drw_cptable949.h \
    backend/intern/drw_cptable950.h \
    backend/intern/drw_cptables.h \
    backend/intern/drw_textcodec.h \
    backend/intern/dxfreader.h \
    backend/intern/dxfwriter.h \
    backend/libdxfrw.h
