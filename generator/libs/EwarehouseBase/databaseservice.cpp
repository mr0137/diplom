#include "databaseservice.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDatabase>
#include <QDebug>
#include <QThread>

static int no = 0;
static QMap<QString, QSqlDatabase> storage;
static QString connection = "";//"Server=192.168.71.190\\NEWSQL";

//!============================== CONNECTIONS WINDOWS =======================================
//!
//!    "Driver={SQL Server};Server=192.168.71.142\\SQL142;Database=Produce;Integrated Security = true"
//!
//!    "Driver={SQL Server};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true"
//!
//!
//!=============================== CONNECTIONS LINUX =======================================
//!
//!    "Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.142\\SQL142;Database=Produce;Integrated Security = true"
//!
//!    "Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true"
//!

DatabaseService::DatabaseService(QObject *parent):QObject(parent)
{
    //connectDB();
}

QString DatabaseService::getLastProduceOrder(QVariantMap &map, QString strQuery)
{
    QSqlQuery query;
    query.prepare(strQuery);

    query.exec();
    bool firstN = true;
    QString lastN;
    while(query.next()){
        QVariantMap row;
        if(firstN){
            lastN = query.record().value(0).toString().remove(' ');
            firstN = false;
        }
        auto n_ = query.record().value(0).toString().remove(' ');
        map.insert(n_, "");

    }
    return lastN;
}

bool DatabaseService::isNumberFree(QString strQuery)
{
    QSqlQuery query;
    query.prepare(strQuery);

    query.exec();
    query.next();

    return query.record().value(0).toBool();
}

QSqlDatabase DatabaseService::createConnection(QString connectionName)
{
    connection = "Server=192.168.71.142\\SQL142";
    //connection = "Server=192.168.71.190\\NEWSQL";

    //qDebug()<<connectionName;

    QSqlDatabase msqls;

    auto f = [](){
        QSqlDatabase msqls = QSqlDatabase::addDatabase("QODBC", QString::number(no++));
#ifdef Q_OS_LINUX
        msqls.setDatabaseName("Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true");
        msqls.setUserName("User123");
        msqls.setPassword("User_123");
#else
        msqls.setDatabaseName("Driver={SQL Server};" + connection + ";Database=Produce;Integrated Security = true");
        msqls.setUserName("User123");
        msqls.setPassword("User_123");
#endif
        return msqls;
    };

    QString key = connectionName + "_" + QString::number((quint64)QThread::currentThread(), 16);
    if (!storage.contains(key)){
        msqls = f();
        if (!msqls.open()) {
            qWarning() << "MSqlS error: " << msqls.lastError();
        }
        storage.insert({{key, msqls}});
    }else{
        msqls = storage[key];
    }

    return msqls;
}

void DatabaseService::closeConnection(QString connectionName)
{
    QString key = connectionName + "_" + QString::number((quint64)QThread::currentThread(), 16);
    if (storage.contains(key)){
        storage[key].close();
        storage.remove(key);
    }
}

void DatabaseService::closeAllConnections(QString connectionName)
{
    for (const auto &k : storage.keys()){
        if (k.contains(connectionName)){
            storage.remove(k);
        }
    }
}

QSqlDatabase DatabaseService::createLocalConnection()
{
    QSqlDatabase msqls = QSqlDatabase::addDatabase("QSQLITE", QString::number(no++));
    msqls.setDatabaseName("config.sqlite");

    if (!msqls.open()) {
        qWarning() << "MSqlS error: " << msqls.lastError();
    }

    if (connection == ""){
        init(msqls);
    }
    return msqls;
}

bool DatabaseService::isDatabaseAvailable()
{
    return !createConnection("testConnection").isOpenError();
}

QString DatabaseService::currentConnection()
{
    return connection;
}

void DatabaseService::connectDB()
{
    QSqlDatabase msqls = QSqlDatabase::addDatabase("QODBC");
#ifdef Q_OS_LINUX
    msqls.setDatabaseName("Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true");
#else
    msqls.setDatabaseName("Driver={SQL Server};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true");
#endif
    msqls.setUserName("User123");
    msqls.setPassword("User_123");

    if (!msqls.open()) {
        qDebug() << "MSqlS error: " << msqls.lastError();
    }

    qDebug()<<"data base connect";
}

void DatabaseService::init(QSqlDatabase db)
{
    QSqlQuery q(db);
    QString query = QString(
                "CREATE TABLE IF NOT EXISTS CONFIG ("
                "Field text,"
                "Value BLOB"
                ");");

    q.prepare(query);
    q.exec(query);
    query = QString(
                "SELECT * FROM CONFIG WHERE Field=\"CorrectClose\""
                );

    q.exec(query);
    if (!q.next()){
        query = QString(
                    "INSERT INTO CONFIG (Field, Value) VALUES (\"CorrectClose\",\"0\");"
                    );
        q.exec(query);
    }
    query = QString(
                "SELECT * FROM CONFIG WHERE Field=\"DEBUG_MODE\""
                );
    q.exec(query);
    if (!q.next()){
        query = QString(
                    "INSERT INTO CONFIG (Field, Value) VALUES (\"DEBUG_MODE\",\"0\");"
                    );
        q.exec(query);

        query = QString(
                    "INSERT INTO CONFIG (Field, Value) VALUES (\"SQL_CONNECT_DEBUG\",\"Server=192.168.71.190\\NEWSQL\");"
                    );
        q.exec(query);

        query = QString(
                    "INSERT INTO CONFIG (Field, Value) VALUES (\"SQL_CONNECT_RELEASE\",\"Server=192.168.71.142\\SQL142\");"
                    );
        q.exec(query);
        connection = "Server=192.168.71.142\\SQL142";
    }else{
        query = QString(
                    "SELECT Value FROM CONFIG WHERE Field=\"DEBUG_MODE\";"
                    );
        q.exec(query);
        if (q.next()){
            query = QString("SELECT Value FROM CONFIG WHERE Field=" + QString(q.value(0).toInt() == 0 ? "\"SQL_CONNECT_RELEASE\"" : "\"SQL_CONNECT_DEBUG\"") + ";");
            q.exec(query);
            q.next();
            connection = q.value(0).toString();
            qDebug() << "Current connection" << connection;
        }
    }
}
