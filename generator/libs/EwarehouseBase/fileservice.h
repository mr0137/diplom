#ifndef FILESERVICE_H
#define FILESERVICE_H

#include <QObject>
#include "crcchecksum.h"
//#include <kmacro.h>
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

//class crcCheckSum
//{
//public:
//    crcCheckSum();
//    quint16 crc16ForModbus(const QByteArray &data);
//
//    quint8 Crc8(const QByteArray &data);
//    quint16 Crc16(const QByteArray &data);
//    u32 Crc32(u8 *_pBuff, u16 _size);
//};

class EWAREHOUSEBASE_EXPORT FileService: public QObject
{
    Q_OBJECT
   // K_READONLY_PROPERTY(QString, nameFile, nameFile, setNameFile,nameFileChanged, "");
   // K_READONLY_PROPERTY(bool, isHas, isHas, setIsHas, isHasChanged, false);
public:
    explicit FileService(QObject *parent = nullptr);
    static QByteArray getByteFile(QString path);
    void setFile(QString path);
    QString name() const;
    QByteArray bits() const;
    QString crc() const;
    int size() const;
    QString path() const;

    int row_max() const;
    void setRow_max(int row_max);

    QString note() const;
    void setNote(const QString &note);

    int id() const;
    void setId(int id);

    QString ext() const;

    bool hasDb() const;
    void setHasDb(bool hasDb);

    QString date() const;
    void setDate(const QString &date);

private:
    QString m_name = "";
    QByteArray m_bits = 0;
    QString m_crc = "";
    int m_size = 0;
    QString m_path;
    crcCheckSum crcCheck;
    int m_row_max = 0;
    QString m_note;
    int m_id = 0;
    QString m_ext = 0;
    bool m_hasDb = false;
    QString m_date;
};

#endif // FILESERVICE_H
