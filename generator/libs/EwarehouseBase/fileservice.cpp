#include "fileservice.h"
#include <QFileInfo>
#include <QDir>
#include <QFileInfoList>
#include <QDateTime>


FileService::FileService(QObject *parent): QObject(parent)
{

}

QByteArray FileService::getByteFile(QString path)
{
    QDir dir(path);

    dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);

    QFile file(dir.filePath(path));

    qDebug()<<path;

    if (!file.open(QIODevice::ReadOnly)){
        qDebug() << file.errorString();
        return {};
    }

    QByteArray byte = file.readAll();
    file.close();
    return byte;
}

QString FileService::name() const
{
    return m_name;
}

QByteArray FileService::bits() const
{
    return m_bits;
}

QString FileService::crc() const
{
    return m_crc;
}

int FileService::size() const
{
    return m_size;
}

QString FileService::path() const
{
    return m_path;
}

int FileService::row_max() const
{
    return m_row_max;
}

void FileService::setRow_max(int row_max)
{
    m_row_max = row_max;
}

QString FileService::note() const
{
    return m_note;
}

void FileService::setNote(const QString &note)
{
    m_note = note;
}

int FileService::id() const
{
    return m_id;
}

void FileService::setId(int id)
{
    if(m_id == id){
        return;
    }
    m_id = id;
}

QString FileService::ext() const
{
    return m_ext;
}

bool FileService::hasDb() const
{
    return m_hasDb;
}

void FileService::setHasDb(bool hasDb)
{
    if(m_hasDb == hasDb){
        return;
    }
    //setIsHas(hasDb);
    //emit isHasChanged(true);
    m_hasDb = hasDb;
}

QString FileService::date() const
{
    return m_date;
}

void FileService::setDate(const QString &date)
{
    m_date = date;
}


void FileService::setFile(QString path)
{
    QDir dir(path);

    dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);

    QFile file(dir.filePath(path));

    if (!file.open(QIODevice::ReadOnly)){
        qDebug() << file.errorString();
        return ;
    }

    QByteArray dllFileData = file.readAll();

    auto data = QByteArray::fromHex(dllFileData);

    quint32 crc32;
    crc32 = crcCheck.Crc16(data);
    QFileInfo fileInfo(file);

    //auto f = fileInfo.birthTime();
    m_size = fileInfo.size();
    m_crc = QString::number(crc32,16).toUpper();
    m_ext = fileInfo.suffix();
    m_name = fileInfo.fileName().remove('.' + m_ext);
    m_path = path.remove(fileInfo.fileName());
    m_bits = dllFileData;
   // m_nameFile = m_name;
    m_date = fileInfo.birthTime().toString("dd-MM-yyyy hh:mm:ss");
    file.close();
}

