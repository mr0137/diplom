#ifndef IPagePlugin_H
#define IPagePlugin_H

#include <blockdata.h>
#include <QObject>
#include <QMap>
#include <QString>

class ITableDataProvider;
class IPagePlugin{
public:
    virtual ~IPagePlugin() = default;
    virtual QMap<QString, std::function<BlockData* ()>> getConfig() = 0;
    virtual QMap<QString, QString> getComponents() = 0;
    virtual QMap<QString, std::function<ITableDataProvider *(ITableDataProvider*)>>  getListing() = 0;
    virtual QMap<QString, QString> getToolButtons() = 0;
    virtual void importQmlPath(QString path) = 0;
};

QT_BEGIN_NAMESPACE

#define IPagePlugin_iid "ewarehouse_se.IPagePlugin"

Q_DECLARE_INTERFACE(IPagePlugin, IPagePlugin_iid)
QT_END_NAMESPACE

#endif // IPagePlugin_H
