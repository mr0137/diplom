#ifndef ITABLEDATAPROVIDER_H
#define ITABLEDATAPROVIDER_H

#include <QColor>
#include <QObject>
#include <QVariant>
#include "tablerenderbase_global.h"

/*!
 * \struct Cell
 * \brief Struct used as container for cell data.
 */
struct Cell {
    /*!
     * \brief Cell's text color. Default color \a black.
     */
    QColor textColor = "black";
    /*!
     * \brief Cell's background color. Default color \a white.
     */
    QColor backgroundColor = "white";
    /*!
     * \brief Cell's text.
     */
    QString displayText;
    /*!
     * \brief Experimental thing: type of cell.
     */
    bool isCheckBox = false;

    enum Flags {
        DEFAULT,
        OWN_BGCOLOR,
        OWN_TXTCOLOR
    };

    Q_DECLARE_FLAGS(CellFlags, Flags)
    /*!
     * \brief Flags that give information about text color and background color.
     */
    CellFlags flags;
};

/*!
 * \struct Row
 * \brief Struct used as container for Cell sequance.
 */
struct Row {
    int id = -1;
    QList<Cell> cells;
};

/*!
 * \struct Header
 * \brief The Header struct used for styling headers and columns
 */
struct Header {
    QString text;
    QColor textColor = "black";
    QColor backgroundColor = "transparent";
    double width = 0;
    quint32 headerAlignment = 0;
    quint32 cellAlignment = 0;

    bool operator == (const Header h1) const{
        if (h1.backgroundColor != backgroundColor){
            return false;
        }else if (h1.textColor != textColor){
            return false;
        }else if (h1.width != width){
            return false;
        }else if (h1.text != text){
            return false;
        }else if (h1.cellAlignment != cellAlignment){
            return false;
        }else if (h1.headerAlignment != headerAlignment){
            return false;
        }
        return true;
    }

    QVariant serialize() const{
        return QVariantMap{
            { "text", text },
            { "textColor", textColor },
            { "backgroundColor", backgroundColor },
            { "width", width },
            { "headerAlignment", headerAlignment },
            { "cellAlignment", cellAlignment}
        };
    }

    void deserialize(const QVariant &data){
        QVariantMap varmap = data.toMap();
        if (!varmap.isEmpty()){
            text = varmap.value("text", "").toString();
            textColor = varmap.value("textColor", "black").toString();
            backgroundColor = varmap.value("backgroundColor", "transparent").toString();
            width = varmap.value("width", .0).toDouble();
            cellAlignment = varmap.value("cellAlignment", 0).toUInt();
        }
    }
};

/*!
 * \interface ITableDataProvider
 * \brief The ITableDataProvider interface used as data supplier for Table.
 */
class TABLERENDERBASE_EXPORT ITableDataProvider : public QObject
{
    Q_OBJECT
public:
    /*!
     * \enum Reason
     * \brief Reasun of row update.
     */
    enum Reason{ADDED, UPDATED, REMOVED};
    Q_ENUM(Reason)

    /*!
     * \fn explicit ITableDataProvider(QObject *parent = nullptr)
     * \brief
     */
    explicit ITableDataProvider(QObject *parent = nullptr) : QObject(parent) { qRegisterMetaType<Reason>("Reason"); }

    /*!
     * \fn  virtual int rowCount() const = 0
     * \brief Method return rows count.
     */
    virtual int rowCount() const = 0;

    /*!
     * \fn virtual int columnCount() const = 0
     * \brief Method number of columns.
     */
    virtual int columnCount() const = 0;

    /*!
     * \fn virtual QVector<QString> headerData() const = 0
     * \brief return headers text.
     */
    virtual QVector<Header> headerData() const = 0;

    /*!
     * \fn virtual Row getRow(int row) = 0
     * \brief Return row by index.
     */
    virtual Row getRow(int row) = 0;

    /*!
     * \fn virtual void sort(int column) = 0
     * \brief Sort data and emit reset signal.
     */
    virtual void sort(int column) = 0;

    /*!
     * \fn virtual bool isFullLoaded() = 0
     * \brief Return true, when all data has been loaded.
     */
    virtual bool isFullLoaded() = 0;
    /*!
     * \fn virtual void fetchMore()
     * \brief idk
     */
    virtual void fetchMore() {};
    /*!
     * \fn virtual int rowById(int id) = 0
     * \brief rowById
     */
    virtual int rowById(int id) = 0;
    /*!
     * \brief isEditable
     */
    virtual bool isEditable() { return false; }
    /*!
     * \fn virtual void setCellData(int row, int column, QVariant value)
     * \brief Set cell data by \c row and \c column.
     */
    virtual void setCellData(int row, int column, QVariant value) {
        Q_UNUSED(row)
        Q_UNUSED(column)
        Q_UNUSED(value)
    }
    /*!
     * \fn virtual QVector<int> headersWithButton()
     * \brief Return QVector<int> with indeces of headers with filter button.
     */
    virtual QVector<int> headersWithButton() { return {}; }
    /*!
     * \fn virtual QVariant serialize()
     * \brief Return all data from provider.
     */
    virtual QVariant serialize() { return {}; }
    /*!
     * \fn virtual void deserialize(const QVariant &data)
     * \brief Set data from QVariant to internal fields.
     */
    virtual void deserialize(const QVariant &data) { Q_UNUSED(data) }
    /*!
     * \fn virtual void
     * \brief reload
     */
    virtual void reload() {}

signals:
    void filterChanged(int column, QString iconPath = "");
    void sortChanged(int columnm, QString iconPath = "");
    void reseted(bool resetVisualPos = true);
    void rowsUpdated(int start, int end, Reason r = ADDED);
    void fullLoaded();
    void columnCountChanged();
    void loadingProgress(double progress);
};
#endif // ITABLEDATAPROVIDER_H
