#ifndef SMAZ_H
#define SMAZ_H

int smaz_compress(char *in, int inlen, char *out, int outlen);
int smaz_decompress(char *in, int inlen, char *out, int outlen);

#endif // SMAZ_H
