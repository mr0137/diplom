TEMPLATE = subdirs

SUBDIRS += \
    klibcorelite \
    QEMF \
    QZint \
    QXlsx \
    TableRenderBase \
    EwarehouseBase\
    KOrm \
    NrpParser \
    qdxf

TableRenderBase.subdir = TableRenderBase
EwarehouseBase.subdir = EwarehouseBase
klibcorelite.subdir = klibcorelite
QXlsx.subdir = QXlsx
KOrm.subdir = KOrm

EwarehouseBase.depends = TableRenderBase

