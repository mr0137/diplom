#include <QCoreApplication>
#include <QDebug>
#include <QElapsedTimer>
#include <QVector>

QStringList list = {
    "17.7813", "22.8558",  "-",         "-",    	"-",    	"-",    	"-",    	"-",    	"-",
    "18.2244", "24.3412",  "-",         "-",    	"-",    	"-",    	"-",    	"-",    	"-",
    "20.0247", "20.9748",  "-",         "-",    	"-",    	"-",    	"-",    	"-",    	"-",
    "20.2374", "20.2555",  "-",         "-",    	"-",    	"-",    	"-",    	"-",    	"-",
    "19.3753", "19.0354",  "20.9896",   "-",    	"-",    	"-",    	"-",    	"-",     	"-",
    "20.9976", "15.7774",  "21.6593",   "24.4397",  "-",    	"-",    	"-",    	"-",     	"-",
    "27.4260", "31.8769",  "30.6357",   "29.4485",  "27.9576",	"29.8592",	"30.2008",	"39.8217",	"36.8217",
    "137.330", "136.259",  "137.881",   "151.236",  "205.248",	"208.398",	"291.334",	"434.374",	"614.491",
    "31.3214", "30.6149",  "34.8895",   "30.3327",  "30.0032",	"31.2355",	"35.8470",	"38.5634",	"41.3457",
    "18.5768",	"17.1476",	"25.3740",	"25.7355",	"37.2880",	"73.5790",	"212.4200",	"307.0051",	"431.6980"

};

QString convert(int n) {
    QString res = "";
    for (int i = 0; n > 0; i++){
        if (n == 5){
            res = "10" + res;
            break;
        }else{
            res = char((n%5) + '0') + res;
        }
        n /= 5;
    }
    return res;
}

void generate(){
    QStringList results;
    for (int i = 19531; i < 78124; i++){
        QString str = convert(i);
        if (str.contains("0")) continue;
        int sum = 0;
        for (auto c : str){
            sum += c.digitValue();
            if (sum > 13) break;
        }
        if (sum == 13 && !results.contains(str)){
            //qDebug() << str;
            results.push_back(str);
        }
    }
    qDebug() << results.count() << results;
}

int main()
{
    for (int i = 0; i < list.length(); i++){
        if (list[i].contains("-")) continue;
        qDebug() << list[i].toDouble() * (1.2 + ((rand()%1000)/1000.));
        list[i] = QString::number(list[i].toDouble() * (1.05 + ((rand()%1000)/1000.))).replace(".", ",");
    }
    qDebug() << list;

    //generate();

    return 0;//a.exec();
}
