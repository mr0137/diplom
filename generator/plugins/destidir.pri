DESTDIR = $$PWD/../bin/plugins/$$TARGET

!contains(TARGET, BasePlugin|CQML) {
CONFIG(release, debug|release){
LIBS += -L$$PWD/../bin/libs -lTableRenderBase
}else{
win32: LIBS += -L$$OUT_PWD/../../libs/TableRenderBase/debug/ -lTableRenderBase
else:unix: LIBS += -L$$OUT_PWD/../../libs/TableRenderBase/ -lTableRenderBase
}
INCLUDEPATH += $$PWD/../libs/TableRenderBase
DEPENDPATH += $$PWD/../libs/TableRenderBase
}
