.pragma library

var map = {
    'ship'                      : '\uF21A' ,    // 
    'compass'                   : '\uF14E' ,    //
    'pencil'                    : '\uF040' ,    //
    'waves'                     : '\uF773' ,    //
    'move'                      : '\uF0B2' ,    //
    'trajectory'                : '\uF542' ,    //
    'skull'                     : '\uF54C' ,    //
    'target'                    : '\uF05B' ,    //
    'torpedo'                   : '\uF304' ,    //
    'magic'                     : '\uF0D0' ,    //
    'filter'                    : '\uF0B0' ,    //
    'trash'                     : '\uF1F8' ,    //
    'play'                      : '\uF04B' ,    //
    'pause'                     : '\uF04C' ,    //
    'stop'                      : '\uF04D' ,    //
    'thumbtag'                  : '\uF08D' ,    //
    'map_marker'                : '\uF041' ,    //
    'map_marker_edit'           : '\uF607' ,    //    'map_marker_plus'           : '\uF60A' ,    //
    'map_marker_minus'          : '\uF609' ,    //
    'map_marker_check'          : '\uF606' ,    //
    'map_marker_alt'            : '\uF3C5' ,    //
    'map_marker_question'       : '\uF60A' ,    //
    'map_marker_exclamation'    : '\uF60B' ,    //
    'crosshair'                 : '\uF05B' ,    //
    'copy'                      : '\uF0C5' ,    //
    'times'                     : '\uF00D' ,    //
    'plus'                      : '\uF067' ,    //
    'minus'                     : '\uF068' ,    //
    'chevron-right'             : '\uF054' ,    //
    'chevron-left'              : '\uF053' ,    //
    'update'                    : '\uF021' ,    //
    'save'                      : '\uF0C7' ,    //  
    'undo-alt'                  : '\uF2EA' ,    //
    'arrow-right'               : '\uF061' ,    //
    'arrow-left'                : '\uF060' ,    //
    'square'                    : '\uF0C8' ,    //
    'square-checked'            : '\uF14A' ,    //
    'broom'                     : '\uF51A' ,    //
    'plug'                      : '\uF1E6' ,    //
    'power-off'                 : '\uF011' ,    //
    'cogs'                      : '\uF085' ,    //
    'arrow-left'                : '\uF060' ,    //  
    'arrow-right'               : '\uF061' ,    //  
    'arrow-up'                  : '\uF062' ,    //  
    'arrow-down'                : '\uF053' ,    //  
    'user'                      : '\uF007' ,    //  
    'load'                      : '\uF019'      //  
};
