#include "headerstyle.h"

/*!
 * \class HeaderStyle
 * \brief The HeaderStyle class allows user to configure header's style.
 */
class HeaderStyle;

/*!
 * \fn HeaderStyle::HeaderStyle(HeaderType type, QObject *parent)
 * \brief DefaultConstructor
 */
HeaderStyle::HeaderStyle(HeaderType type, QObject *parent) : QObject(parent)
{
    m_headerType = type;
    m_font = new CellFontStyle(this);

    connect(m_font, &CellFontStyle::fontChanged, this, [this](){
        emit this->headerChanged();
    });
}

/*!
 * \property double HeaderStyle::height
 * \brief This property holds heignt of header.
 */
double HeaderStyle::height() const
{
    return m_height;
}

/*!
 * \fn void HeaderStyle::setHeight(double newHeight)
 * \brief Header's height setter.
 */
void HeaderStyle::setHeight(double newHeight)
{
    if (qFuzzyCompare(m_height, newHeight))
        return;
    m_height = newHeight;
    emit heightChanged();
    emit headerChanged();
}

/*!
 * \property double HeaderStyle::width
 * \brief This property holds width of header.
 */
double HeaderStyle::width() const
{
    return m_width;
}

/*!
 * \fn void HeaderStyle::setWidth(double newWidth)
 * \brief Header's width setter.
 */
void HeaderStyle::setWidth(double newWidth)
{
    if (!m_widthChangeable) {
        qDebug() << "You can't change width of the header";
        return;
    }

    if (qFuzzyCompare(m_width, newWidth))
        return;
    m_width = newWidth;
    emit widthChanged();
    emit headerChanged();
}

/*!
 * \property QColor HeaderStyle::backgroundColor
 * \brief This property holds background color of header.
 */
const QColor HeaderStyle::backgroundColor() const
{
    return m_backgroundColor;
}

/*!
 * \fn void HeaderStyle::setBackgroundColor(const QColor &newBackgroundColor)
 * \brief HEader's background setter.
 */
void HeaderStyle::setBackgroundColor(const QColor &newBackgroundColor)
{
    if (m_backgroundColor == newBackgroundColor)
        return;
    m_backgroundColor = newBackgroundColor;
    emit backgroundColorChanged();
    emit headerChanged();
}

/*!
 * \property QColor HeaderStyle::textColor
 * \brief This property holds header's text color.
 */
const QColor HeaderStyle::textColor() const
{
    return m_textColor;
}

/*!
 * \fn void HeaderStyle::setTextColor(const QColor &newTextColor)
 * \brief Header's text color setter.
 */
void HeaderStyle::setTextColor(const QColor &newTextColor)
{
    if (m_textColor == newTextColor)
        return;
    m_textColor = newTextColor;
    emit textColorChanged();
    emit headerChanged();
}

/*!
 * \property int HeaderStyle::leftMargin
 * \brief This property holds header's left margin value.
 */
int HeaderStyle::leftMargin() const
{
    return m_leftMargin;
}

/*!
 * \fn void HeaderStyle::setLeftMargin(int newLeftMargin)
 * \brief Header's left margin setter.
 */
void HeaderStyle::setLeftMargin(int newLeftMargin)
{
    if (m_leftMargin == newLeftMargin)
        return;
    m_leftMargin = newLeftMargin;
    emit leftMarginChanged();
    emit headerChanged();
}

/*!
 * \fn int HeaderStyle::rightMargin
 * \brief This property holds header's right margin.
 */
int HeaderStyle::rightMargin() const
{
    return m_rightMargin;
}

/*!
 * \fn void HeaderStyle::setRightMargin(int newRightMargin)
 * \brief Header's right margin.
 */
void HeaderStyle::setRightMargin(int newRightMargin)
{
    if (m_rightMargin == newRightMargin)
        return;
    m_rightMargin = newRightMargin;
    emit rightMarginChanged();
    emit headerChanged();
}
/*!
 * \fn CellFontStyle* HeaderStyle::font
 * \brief This property holds header's font.
 */
CellFontStyle *HeaderStyle::fontStyle()
{
    return m_font;
}

/*!
 * \fn HeaderStyle::HeaderType HeaderStyle::headerType
 * \brief This property holds header's type.
 */
HeaderStyle::HeaderType HeaderStyle::headerType() const
{
    return m_headerType;
}

/*!
 * \fn quint32 HeaderStyle::alignment
 * \brief This property holds header's right margin.
 */
quint32 HeaderStyle::alignment() const
{
    return m_alignment;
}

/*!
 * \fn void HeaderStyle::setAlignment(quint32 newAlignment)
 * \brief Header's text alignment.
 */
void HeaderStyle::setAlignment(quint32 newAlignment)
{
    if (m_alignment == newAlignment)
        return;
    m_alignment = newAlignment;
    emit alignmentChanged();
}

/*!
 * \property QColor HeaderStyle::pinnedColor
 * \brief This property holds pinned header's color.
 */
QColor HeaderStyle::pinnedColor() const
{
    return m_pinnedColor;
}

/*!
 * \fn void HeaderStyle::setPinnedColor(QColor pinnedColor)
 * \brief Pinned header's color setted.
 */
void HeaderStyle::setPinnedColor(QColor pinnedColor)
{
    if (m_pinnedColor == pinnedColor)
        return;

    m_pinnedColor = pinnedColor;
    emit pinnedColorChanged();
}
