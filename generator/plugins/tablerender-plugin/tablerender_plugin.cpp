#include "tablerender_plugin.h"

#include <tableprivate.h>
#include <styles/cellfontstyle.h>
#include <styles/lazytablestyle.h>
#include <QProcess>
#include <QQmlModuleRegistration>
#include <qqml.h>

void LightTablePlugin::registerTypes(const char *uri)
{
    // @uri TableRender
    qmlRegisterSingletonType<LazyTableStyle>(uri, 1, 0, "LazyTableStyle", &LazyTableStyle::qmlInstance);
    qmlRegisterType<Table>("TableRenderPrivate", 1, 0, "TablePrivate");
    qmlRegisterType<TableStyle>(uri, 1, 0, "TableStyle");
    qmlRegisterUncreatableType<CellStyle>(uri, 1, 0, "CellStyle", "Uncreatable type");
    qmlRegisterUncreatableType<CellFontStyle>(uri, 1, 0, "CellFontStyle", "Uncreatable type");
    qmlRegisterUncreatableType<HeaderStyle>(uri, 1, 0, "HeaderStyle", "Uncreatable type");
    qmlRegisterUncreatableType<ITableDataProvider>(uri, 1, 0, "ITableDataProvider" ,"Uncreatable type");
}


