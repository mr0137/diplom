#ifndef LOCALCONFSERVICE_H
#define LOCALCONFSERVICE_H

#include <QObject>
#include <utility/helper.h>
#include <kmacro.h>

class LocalConfService : public QObject
{
    Q_OBJECT
    K_READONLY_PROPERTY(bool, isCrashed, isCrashed, setIsCrashed, isCrashedChanged, false)
    Q_DISABLE_COPY(LocalConfService)
    K_QML(LocalConfService)
public:
    explicit LocalConfService(QObject *parent = nullptr);
    ~LocalConfService();

public slots:
    void saveTabs(QVariantList tabsList);
    QVariantList loadTabs();
signals:
private:

};

#endif // LOCALCONFSERVICE_H
