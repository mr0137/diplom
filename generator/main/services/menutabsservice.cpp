#include "menutabsservice.h"

MenuTabsService::MenuTabsService(QObject *parent) : QObject(parent)
{
    m_root = new MenuData(this);
    m_root->setTitle("root");

}

void MenuTabsService::proceed(QVariantList list)
{
    m_root->appendChilds(loadTabsData(list, root()));
}

MenuData *MenuTabsService::root() const
{
    return m_root;
}

void MenuTabsService::setRoot(MenuData *newRoot)
{
    if (m_root == newRoot)
        return;
    m_root = newRoot;
    emit rootChanged();
}

QList<MenuData*> MenuTabsService::loadTabsData(QVariantList list, MenuData *parent)
{
    QList<MenuData*> result;
    for (const auto &l : list){
        QVariantMap testMap = l.toMap();
        if (!testMap.isEmpty()){
            for (const auto &k : testMap.keys()){
                MenuData *t = nullptr;
                if (m_createdMenu.contains(k)){
                    t = m_createdMenu[k];
                }else{
                    t = new MenuData(parent);
                    t->setTitle(k);
                    m_createdMenu.insert(k, t);
                    result.push_back(t);
                }
                auto atom = testMap[k];
                auto ll = atom.toList();
                if (!ll.isEmpty()){
                    t->appendChilds(loadTabsData(ll, t));
                }else{
                    //qDebug() << "code" << atom.toString();
                    t->setCode(atom.toString());
                }
            }
        }
    }
    return result;
}
