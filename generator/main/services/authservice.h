#ifndef AUTHSERVICE_H
#define AUTHSERVICE_H

#include <QObject>
#include <QSqlDatabase>

#include <QVariant>
#include <QVariantMap>
#include <utility/helper.h>

class AppCore;
class BlockData;
class ITableDataProvider;
class AuthService : public QObject
{
    Q_OBJECT
    K_QML(AuthService)
    Q_DISABLE_COPY(AuthService)
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(bool autologin READ autologin WRITE setAutologin NOTIFY autologinChanged)
    friend class AppCore;
public:
    explicit AuthService(QObject *parent = nullptr);
    bool autologin() const;
    void setAutologin(bool newAutologin);

    QString username() const;
    void setUsername(const QString &newUsername);
    QStringList listingsSequance() const;
    QMap<QString, QString> getToolButtons() const;

public slots:
    void login(QString user, QString password);
    void logout();

private:
    //! для AppCore
    QVariantList getMenuTabs();
    QMap<QString, QString> getComponents();
    QMap<QString, std::function<ITableDataProvider *(ITableDataProvider *)> > getListings();
    QMap<QString, std::function<BlockData*()>> getBlocks();
    QVariantMap getTabs();
    //! для AppCore
signals:
    void loginProcessBegin();
    void loginProcessEnd(QString errorString);
    void pluginLoadingEnd(QString errorString);
    void logoutProcessBegin(QVariantMap *event);
    void pluginLoadingBegin();
    void autologinChanged();
    void usernameChanged();
    void responseAwaiterChanged();

private:
    void loadPlugins();
    QString loadPlugin(QString pluginPath);

    bool m_autologin = true;
    QString m_username = "";
    QList<QString> m_pluginsPaths;
    QMap<QString, QString> m_components;
    QMap<QString, QString> m_toolButtons;
    QMap<QString, std::function<BlockData*()>> m_creators;
    QStringList m_listingsSequance;
    QMap<QString, std::function<ITableDataProvider *(ITableDataProvider*)>> m_listings;
};

#endif // AUTHSERVICE_H
