#ifndef TABSSERVICE_H
#define TABSSERVICE_H

#include <QObject>
#include <data_structures/tabdata.h>
#include <klist.h>
#include <kpoco.h>
#include <utility/helper.h>

class TabsService : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(TabsService)
    K_QML(TabsService)
    Q_PROPERTY(TabData* responseAwaiter READ responseAwaiter WRITE setResponseAwaiter NOTIFY responseAwaiterChanged)
    Q_PROPERTY(QString responseBlockNameAwaiter READ responseBlockNameAwaiter WRITE setResponseBlockNameAwaiter NOTIFY responseBlockNameAwaiterChanged)
    K_LIST(TabData,tabItems);

public:
    explicit TabsService(QObject *parent = nullptr);
    TabData *responseAwaiter() const;
    void setResponseAwaiter(TabData *newResponseAwaiter);
    void setTabs(QVariantMap tabs);
    void setToolButtons(QMap<QString, QString> toolButtons);
    QVariantList serialize();
    void deserialize(const QVariantList &list);
    void init();
    const QString &responseBlockNameAwaiter() const;
    void setResponseBlockNameAwaiter(const QString &newResponseBlockNameAwaiter);

public slots:
    void addTabItem(QString name, QString type);
    QString getToolButton(QString type);
    void removeTabItem(int index);
    void tryRemoveTabItem(int index);
    void focusTab(QString type, QString n);

    bool containsTabName(QString name);
    int tabIndex(QString tabname);
    QVariantList nonSavedTabs();
    void saveTab(int index);
    bool isReloadAvailable(int index);
signals:
    void responseAwaiterChanged();
    void changeFocusTab(int index);
    void closeError(int index, QString tabName);

    void responseBlockNameAwaiterChanged();

private:
    QVariantMap m_tabs;
    QMap<QString, QString> m_toolButtons;
    QVariantList m_preloadTabs = {
        /*QVariantMap{{"name", "Peoples"}, {"type", "People List Tab"}}*/
        /*QVariantMap{{"name", "Orders"}, {"type", "Orders List Tab"}}/*,
        QVariantMap{{"name", "04028"}, {"type", "Order Tab"}}*/

        QVariantMap{{"name", "Print Tab"}, {"type", "Print Tab"}}//,
        //QVariantMap{{"name", "Orders"}, {"type", "Orders List Tab"}}
    };
    TabData *m_responseAwaiter = nullptr;
    QString m_responseBlockNameAwaiter;
};

#endif // TABSSERVICE_H
