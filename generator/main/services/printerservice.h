#ifndef PRINTERSERVICE_H
#define PRINTERSERVICE_H

#include <QObject>

#include <kmacro.h>
#include <utility/kobservablelist.h>
#include <models/kflexiblemodel.h>


#include <qzint.h>
#include <utility/helper.h>


class QPrinter;
class QPainter;
class PrinterService : public QObject
{
    Q_OBJECT
    K_QML(PrinterService)
    K_READONLY_PROPERTY(QVariantList, images, images, setImages, imagesChanged, QVariantList())
    Q_PROPERTY(QImage code READ code WRITE setCode NOTIFY codeChanged)

    public:
        explicit PrinterService(QObject *parent = nullptr);

    void setImage();

    const QImage &code() const;
    void setCode(const QImage &newCode);

public slots:
    void openDialogPrinter();

    void drawAndPrintLable(QVariantList list);


private:
    QPrinter *m_printer = nullptr;
    QImage encode(QString text, double width, double height);

signals:
    void codeChanged();

private:
    //QPrinter *m_printer = nullptr;
    QZint* m_codeGen = nullptr;
    QImage m_code;

};

#endif // PRINTERSERVICE_H
