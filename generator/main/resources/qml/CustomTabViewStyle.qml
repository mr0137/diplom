import QtQuick 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls 1.4 as Old
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0
import App 1.0

Style {
    id: style
    readonly property Old.TabView control: __control
    property bool tabsMovable: false
    property int tabsAlignment: Qt.AlignLeft
    property int tabOverlap: 1
    property int frameOverlap: 2

    property Component frame: Rectangle {
        color: Qt.darker("#dcdcdc", 1.2)
        border.color: "#aaa"
        clip: true

        Rectangle {
            anchors.fill: parent
            color: "transparent"
            border.color: "#66ffffff"
            anchors.margins: 2
        }
    }

    property Component tab: Item {
        id: styleTab
        scale: style.control.tabPosition === Qt.TopEdge ? 1 : -1
        activeFocusOnTab: false
        focus: false
        visible: (styleData.index !== tabView.count - 1)
        property int totalOverlap: style.tabOverlap * (control.count - 1)
        property real maxTabWidth: style.control.count > 0 ? (styleData.availableWidth + totalOverlap) / control.count : 0
        // Basket basket: BlockData.getBasket(styleData.index)

        implicitWidth: styleData.index === tabView.count - 1 ? 1 : Math.round(Math.min(maxTabWidth, textitem.implicitWidth + 20 + closeRect.width /*+ loadingImage.width*/ + reloadRect.width)) + (styleData.index === tabView.currentIndex ? 5 : 0)
        implicitHeight: Math.round(textitem.implicitHeight + 10)

        Behavior on implicitWidth { NumberAnimation { duration: 200 } }

        Item {
            anchors.fill: parent
            anchors.bottomMargin: styleData.selected ? 1 : 2
            Rectangle{
                anchors.fill: parent
                color: styleData.index === tabView.currentIndex ? tabView.selectedTabColor : "#dcdcdc"
                radius: 2
                border.color: "#aaa"
                border.width: 1
                Rectangle{
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 2
                    anchors.leftMargin: 1
                    anchors.rightMargin: 1
                    color: parent.color
                }
            }
        }

        Text {
            id: textitem
            anchors.fill: parent
            anchors.leftMargin: 4 /*+ loadingImage.width*/
            anchors.rightMargin: 4 + closeRect.width + reloadRect.width
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: styleData.title
            elide: Text.ElideMiddle
            renderType: Text.NativeRendering
            scale: control.tabPosition === Qt.TopEdge ? 1 : -1
            color: SystemPaletteSingleton.text(styleData.enabled)

            Rectangle {
                anchors.centerIn: parent
                width: textitem.paintedWidth + 6
                height: textitem.paintedHeight + 4
                visible: (styleData.activeFocus && styleData.selected)
                radius: 3
                color: "#224f9fef"
                border.color: "#47b"
            }

            MouseArea{
                anchors.fill: parent
                acceptedButtons: Qt.AllButtons
                onReleased: {
                    if (mouse.button == Qt.LeftButton){
                        if (styleData.index !== tabView.count - 1)
                            tabView.currentIndex = styleData.index
                    }else if (mouse.button == Qt.MiddleButton){
                        var m = new Map
                        m["accepted"] = false
                        m["index"] = styleData.index
                        m["automove"] = true
                        tabView.closeTab(m)
                        if (!m.accepted){
                            tabView.removing = true
                            if (styleData.index === tabView.count - 2){
                                if (styleData.index - 1 > - 1 && m.automove){
                                    tabView.currentIndex = styleData.index - 1
                                }
                            }
                            tabView.removing = false
                        }
                    }
                }
                preventStealing: false
            }
        }

        Rectangle{
            id: reloadRect
            anchors.right: closeRect.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.margins: 4
            width: style.control.isReloadAvailable(styleData.index) ? height : 0
            color:  Qt.darker("#dcdcdc", !AppCore.listingService.loaded ? 1.6 : 1.1)
            radius: 3
            visible: width !== 0

            Behavior on color { ColorAnimation { duration: 200 } }

            Text {
                anchors.centerIn: parent
                anchors.leftMargin: -1
                text: "↺"
                color: !AppCore.listingService.loaded ? "gray" : "white"

                Behavior on color { ColorAnimation { duration: 200 } }
            }

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                enabled: AppCore.listingService.loaded
                cursorShape: Qt.PointingHandCursor
                onReleased: {
                    tabView.reload(styleData.index)
                }
            }
        }

        Rectangle{
            id: closeRect
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.margins: 4
            width: height
            color: Qt.darker("#dcdcdc", 1.1)
            radius: 3
            visible: width !== 0

            Text {
                anchors.centerIn: parent
                anchors.leftMargin: -1
                text: "\u2716"
                color: "white"
            }
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onReleased: {
                    var m = new Map
                    m["accepted"] = false
                    m["index"] = styleData.index
                    m["automove"] = true
                    tabView.closeTab(m)
                    if (!m.accepted){
                        tabView.removing = true
                        if (styleData.index === tabView.count - 2){
                            if (styleData.index - 1 > - 1 && m.automove){
                                tabView.currentIndex = styleData.index - 1
                            }
                        }
                        tabView.removing = false
                    }
                }
            }
        }

        //AnimatedImage{
        //    id: loadingImage
        //    property bool procceed: false
        //    anchors.left: parent.left
        //    anchors.bottom: parent.bottom
        //    anchors.top: parent.top
        //    anchors.margins: 4
        //    width: height
        //    source: "qrc:/BasePlugin/resources/icons/loading.gif"
        //
        //    //Connections{
        //    //    target: AppCore.listingService
        //    //}
        //}
    }

    /*! This defines the left corner. */
    property Component leftCorner: null

    /*! This defines the right corner. */
    property Component rightCorner: null

    /*! This defines the tab bar background. */
    property Component tabBar: null
}
