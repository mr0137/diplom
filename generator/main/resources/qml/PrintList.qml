import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import BasePlugin 1.0
import App 1.0
import CQML 1.0 as C
import TableRender 1.0

BaseForm {
    id: baseForm
    ColumnLayout{
        anchors.fill: parent
        ListView{
            Layout.fillHeight: true
            Layout.fillWidth: true
            id: printList
            model: AppCore.printerService.images
            spacing: 5

            delegate: CodeItem{
                width: 1100
                height: 740
                source: modelData
            }

            RowLayout{
                Layout.fillWidth: true
                Layout.preferredHeight: 70
                Button{
                    text: "Print"
                    onClicked: {
                        AppCore.printerService.drawAndPrintLable([])
                    }
                }
                Button{
                    text: "Cancel"
                    onClicked: {

                    }
                }
                TextField{
                    onTextChanged: {
                        AppCore.printerService.setTextCode(text)
                    }
                }
            }
        }

        //CodeItem{
        //    anchors.centerIn: parent
        //    width: 225
        //    height: 28
        //    source: AppCore.printerService.code
        //}
    }
}
