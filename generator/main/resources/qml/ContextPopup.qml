import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0

Popup {
    id: root
    required property Item source
    signal closingResult(bool result)
    modal: false

    Binding{
        target: root
        property: "width"
        value: internal.modal ? source.width : 0
    }

    Binding{
        target: root
        property: "height"
        value: internal.modal ? source.height : 0
    }

    onActiveFocusChanged: {
        if (activeFocus)
            background.forceActiveFocus()
    }

    background: Item{
        focus: false
        activeFocusOnTab: false
        onActiveFocusChanged: {
            backgroundLoader.forceActiveFocus()
        }

        MouseArea{
            anchors.fill: parent
            enabled: internal.modal
            onReleased: {
                root.closingResult(false)
                root.close()
            }
        }

        ShaderEffectSource {
            id: effectSource
            enabled: internal.modal
            sourceItem: root.source
            anchors.fill: parent
            visible: enabled
            sourceRect: Qt.rect(root.x, root.x, width, height)
        }

        FastBlur{
            id: blur
            anchors.fill: effectSource
            enabled: internal.modal
            source: effectSource
            radius: 32
            visible: enabled
            transparentBorder: false
        }

        Rectangle{
            anchors.fill: parent
            visible: internal.modal
            color: "gray"
            opacity: 0.5
        }

        MouseArea{
            x: internal.x
            y: internal.y
            width: internal.width
            height: internal.height
            onReleased: {}
            preventStealing: true
        }

        Loader{
            id: backgroundLoader
            anchors.fill: !internal.modal ? parent : undefined
            focus: false
            onLoaded: {
                if (internal.modal){
                    backgroundLoader.item.x = internal.x
                    backgroundLoader.item.y = internal.y
                    backgroundLoader.item.width = internal.width
                    backgroundLoader.item.height = internal.height
                }

                for (var obj in internal.onLoadedProperties){
                    if (backgroundLoader.item.hasOwnProperty(obj)){
                        backgroundLoader.item[obj] = internal.onLoadedProperties[obj]
                    }
                }
            }

            Connections{
                target: backgroundLoader.item
                function onClose(result){
                    if (typeof(result) === 'undefined'){
                        root.closingResult(false)
                    }else{
                        root.closingResult(result)
                    }

                    root.close()
                }
            }
        }
        DropShadow {
            anchors.fill: backgroundLoader
            horizontalOffset: 3
            verticalOffset: 3
            radius: 8.0
            samples: 17
            color: "#80000000"
            source: backgroundLoader
        }
    }

    QtObject{
        id: internal
        property real x: 0
        property real y: 0
        property real width: 0
        property real height: 0
        property var onLoadedProperties: []
        property bool modal: false
    }

    onClosed: {
        internal.modal = false
        backgroundLoader.sourceComponent = undefined
        internal.onLoadedProperties = []
    }

    function openPopup(x, y, width, height, modal, popupComponent, properties = []){
        internal.x = x
        internal.y = y
        internal.width = width
        internal.height = height
        internal.modal = modal
        internal.onLoadedProperties = properties
        if (!internal.modal){
            root.x = x
            root.y = y
            root.width = internal.width
            root.height = internal.height
        }else{
            root.x = 0
            root.y = 0
        }

        //console.log(y, internal.y, root.y, internal.modal, modal)
        backgroundLoader.sourceComponent = popupComponent
        root.open()
    }
}
