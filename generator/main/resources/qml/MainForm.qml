import QtQuick 2.15
import QtQuick.Controls 1.4 as Old
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Styles 1.4

import App 1.0
import CQML 1.0 as C

Item{
    id: root
    property bool waitResponse: false
    property int readyToCloseIndex
    Connections{
        enabled: AppCore
        target: AppCore.tabsService
        function onChangeFocusTab(index_){
            tabView.currentIndex = index_
        }

        function onCloseError(index, orderNum){
            root.waitResponse = true
            root.readyToCloseIndex = index
            contextPopup.openPopup(window.width / 2 - 600 / 2, window.height / 2 - 150 / 2, 600, 150, true, closeMsgComponent, {"orderNumber" : orderNum, "tabIndex" : index})
        }
    }

    Connections{
        enabled: root.waitResponse
        target: contextPopup
        function onClose(result){
            if (result){
                AppCore.tabsService.removeTabItem(readyToCloseIndex)
                root.waitResponse = false
                readyToCloseIndex = -1
            }
        }
    }

    ColumnLayout{
        anchors.fill: parent
        spacing: 0
        RowLayout{
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.maximumHeight: 30

            Item{
                MenuBar {
                    id: menuBar
                    Repeater{
                        model: AppCore.menuService.root.submenu
                        delegate: Item{
                            MenuChooser{
                                id: chooser
                                menuParent: menuBar
                                menus: [{"menu" : cmenu}, {"item" : null}]
                            }
                            Loader{
                                id: componentLoader
                                sourceComponent: chooser.currentComponent
                                onLoaded: {
                                    componentLoader.item.parent = menuBar
                                    if (modelData.code === ""){
                                        componentLoader.item.mmodel = modelData
                                        menuBar.addMenu(componentLoader.item)
                                    }else{
                                        menuBar.addAction(componentLoader.item)
                                    }
                                }
                            }
                        }
                    }
                }

                Component{
                    id: cmenu
                    Menu{
                        id: localMenu
                        title: mmodel.title
                        property var mmodel

                        Repeater{
                            model: localMenu.mmodel.submenu
                            delegate: Item{
                                MenuChooser{
                                    id: chooser
                                    menuParent: menuBar
                                    menus: [ {"menu" : cmenu}, {"item" : null }]
                                }
                                Loader{
                                    id: componentLoader
                                    sourceComponent: chooser.currentComponent
                                    onLoaded: {
                                        if (modelData.code === ""){
                                            //componentLoader.item.parent = localMenu
                                            componentLoader.item.mmodel = modelData
                                            localMenu.addMenu(componentLoader.item)
                                        }else{
                                            localMenu.addAction(componentLoader.item)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Item{
                Layout.fillWidth: true
            }

            C.BaseBackground{
                Layout.preferredWidth: height
                Layout.margins: 2
                Layout.fillHeight: true
                radius: height / 2
                backgroundColor: AppCore.palette.app.mainColor
                clipContent: true
                Text {
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: AppCore.authService.username.charAt(0)
                    font.pixelSize: parent.height / 2
                    font.capitalization: Font.AllUppercase
                    color: AppCore.palette.text.textColor
                }

                C.WavedMouseArea{
                    anchors.fill: parent
                    circleRadius: parent.radius
                    onReleased: {
                        quitPopup.open()
                    }
                }

                Popup{
                    id: quitPopup
                    x: -(width - parent.width)
                    y: parent.height + 2
                    width: 70
                    height: parent.height

                    enter: Transition {
                        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
                        NumberAnimation { property: "width"; from: 0.0; to: 70 }
                    }

                    exit: Transition {
                        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
                    }

                    background: Rectangle{
                        color: AppCore.palette.app.mainColor
                        radius: height / 2
                        clip: true

                        Text {
                            anchors.fill: parent
                            text: "Logout"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            color: AppCore.palette.text.textColor
                        }

                        C.WavedMouseArea{
                            anchors.fill: parent
                            circleRadius: parent.radius
                            onReleased: {
                                AppCore.authService.logout()
                                quitPopup.close()
                            }
                        }
                    }
                }

                elevation: 3
            }
        }

        Rectangle{
            Layout.fillHeight: true
            Layout.fillWidth: true

            color: AppCore.palette.app.secondBackgroundColor

            Item{
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: 35
                WheelHandler{
                    id: wheelHandler
                    property bool down: false
                    onWheel: {
                        //console.log(tabView.currentIndex)
                        if (event.angleDelta.y > 0){
                            down = true
                            if (tabView.currentIndex - 1 >= 0)
                                tabView.currentIndex--
                            down = false
                        }else{
                            down = true
                            if (tabView.currentIndex + 1 <= tabView.count - 2)
                                tabView.currentIndex++
                            down = false
                        }
                    }
                }
            }

            Old.TabView {
                id:tabView
                anchors.fill: parent
                anchors.topMargin: 5
                property color selectedTabColor: AppCore.palette.app.firstBackgroundColor
                property bool removing: false
                signal closeTab(var event)
                signal reload(int index)
                focus: false
                style: CustomTabViewStyle {}

                Connections{
                    target: tabView
                    function onCloseTab(event){
                        event.accepted = true
                        AppCore.tabsService.tryRemoveTabItem(event.index)
                    }

                    function onReload(index){
                        console.log(index)
                    }
                }

                onCountChanged: {
                    if (count !== 1 && !tabView.removing && currentIndex === tabView.count - 1){
                        currentIndex = tabView.count - 2
                    }
                }

                Repeater{
                    model: AppCore.tabsService.tabItemsModel
                    delegate: Old.Tab{
                        id: tab
                        title: model.object.name + (model.object.isEdited ? "*" : "")

                        Binding{
                            target: model.object
                            property: "index"
                            value: index
                        }

                        Item{
                            anchors.fill: parent
                            anchors.margins: 3

                            RowLayout{
                                id: toolBar
                                anchors.top: parent.top
                                anchors.left: parent.left
                                anchors.right: parent.right
                                height: grid.tabData.toolButtons.length === 0 ? 0 : 30
                                Repeater{
                                    model: grid.tabData.toolButtons
                                    delegate: Item{
                                        Layout.preferredWidth: childrenRect.width
                                        Layout.fillHeight: true
                                        Loader{
                                            id: toolButtonLoader
                                            source: AppCore.tabsService.getToolButton(modelData)
                                            onLoaded: {
                                                toolButtonLoader.item.tabData = grid.tabData
                                            }
                                        }
                                    }
                                }
                                Item {
                                    Layout.fillWidth: true
                                }
                            }

                            GridLayout{
                                id: grid
                                anchors.fill: parent
                                anchors.topMargin: toolBar.height
                                columns: model.object.columns
                                rows: model.object.rows
                                columnSpacing: 1
                                rowSpacing: 1
                                property TabData tabData: model.object
                                property var mmodel: model.object.blocks

                                Repeater{
                                    model: grid.mmodel
                                    delegate: Item{
                                        Layout.fillHeight: true
                                        Layout.fillWidth: true
                                        Layout.row: modelData.row
                                        Layout.rowSpan: modelData.rowSpan
                                        Layout.column: modelData.column
                                        Layout.columnSpan: modelData.columnSpan
                                        Layout.maximumHeight: modelData.maximumHeight === -1 ? Number.POSITIVE_INFINITY : modelData.maximumHeight
                                        Layout.maximumWidth: modelData.maximumWidth === -1 ? Number.POSITIVE_INFINITY : modelData.maximumWidth
                                        Layout.minimumHeight: modelData.minimumHeight === -1 ? 0 : modelData.minimumHeight
                                        Layout.minimumWidth: modelData.minimumWidth === -1 ? 0 : modelData.minimumWidth
                                        Loader{
                                            id: formLoader
                                            anchors.fill: parent
                                            source: modelData.componentPath
                                            onLoaded: {
                                                formLoader.item.tabData = modelData.tabData
                                                formLoader.item.blockData = modelData.blockData
                                            }
                                        }
                                        Component.onCompleted: console.log(modelData)
                                    }
                                }
                            }
                        }

                        Component.onCompleted: {
                            tabView.moveTab(tabView.count - 1, tabView.count - 2)
                            tabView.currentIndex = tabView.count - 2
                            model.object.index = tabView.currentIndex
                        }
                    }
                }

                Old.Tab{
                    title : "fix"
                    focus: false
                    activeFocusOnTab: false
                }

                function isReloadAvailable(index){
                    return AppCore.tabsService.isReloadAvailable(index)
                }
            }
        }
    }

    C.BaseBackground{
        clipContent: true
        anchors.right: parent.right
        anchors.top: parent.top
        elevation:  !AppCore.listingService.loaded /*&& AppCore.debugMode */? 3: 0
        anchors.rightMargin:  !AppCore.listingService.loaded /*&& AppCore.debugMode*/ ? parent.width / 2 - width / 2 : 0
        width: AppCore.listingService.loaded ? 0 : /*AppCore.debugMode ? text.paintedWidth + 5 :*/ parent.width
        height: AppCore.listingService.loaded ? 0 : /*AppCore.debugMode ? 40 :*/ parent.height
        radius: 5
        backgroundColor: AppCore.palette.app.secondBackgroundColor

        Text {
            id: text
            anchors.centerIn: parent
            text: "Loading: " + "\"" + AppCore.listingService.currentLoadingListing + "\" " + AppCore.listingService.currentLoadingProgress + "%"
            font.pointSize: 11
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignHCenter
        }

        C.WavedMouseArea{
            anchors.fill: parent
            preventStealing: true
            onClicked: {}
            onPressed: {}
            onReleased: {}
            onWheel: {}
        }
    }

    Component{
        id: closeMsgComponent
        CloseTabWindow {}
    }
}

