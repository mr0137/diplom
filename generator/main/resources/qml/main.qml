import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15
import BasePlugin 1.0
import App 1.0

ApplicationWindow{
    id: window
    visible: true
    width: 880
    height: 640
    title: qsTr(" Produce ")
    Component.onCompleted: loginPopup.open()

    MainForm{
        anchors.fill: parent
    }

    Popup{
        id: loginPopup
        width: window.width
        height: window.height

        background: LoginPage{}

        enter: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }

        onOpened: {
            if (AppCore.autologin && AppCore.debugMode){
                AppCore.authService.login("debug", "debug")
                loginPopup.close()
            }else if (AppCore.autologin){

            }
        }
    }

    ContextPopup{
        id: contextPopup
        source: window.contentItem
    }

    ContextWindow{
        id: contextWindow
        sourceWindow: window
    }
}
