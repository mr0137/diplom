#include "blockitemdata.h"

#include <appcore.h>
#include <data_structures/tabdata.h>

BlockItemData::BlockItemData(QObject *parent) : QObject(parent)
{

}

void BlockItemData::deserialize(const QVariantMap &map)
{
    if (map.isEmpty()) return;
    setRow(map.value("row", 0).toInt());
    setRowSpan(map.value("rowSpan", 0).toInt());
    setColumn(map.value("column", 0).toInt());
    setColumnSpan(map.value("columnSpan", 0).toInt());
    setBlockName(map.value("blockName", "error").toString());
    setBackend(map.value("backend", "error").toString());
    setBlockData(tabData()->getBlockData(backend()));
    setMaximumHeight(map.value("maximumHeight", -1).toDouble());
    setMaximumWidth(map.value("maximumWidth", -1).toDouble());
    setMinimumHeight(map.value("minimumHeight", -1).toDouble());
    setMinimumWidth(map.value("minimumWidth", -1).toDouble());
    setComponentPath(AppCore::instance()->getComponentPath(map.value("component", "error").toString()));
    setComponent(map.value("component", "error").toString());
}

void BlockItemData::setData(const QVariantMap &map)
{
    if (blockData() != nullptr){
        blockData()->deserialize(map, AppCore::instance()->listingService()->getListing(map.value("listing", "").toString()));
    }
}
