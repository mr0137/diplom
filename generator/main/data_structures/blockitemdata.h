#ifndef BLOCKITEMDATA_H
#define BLOCKITEMDATA_H

#include <QObject>
#include <kmacro.h>
#include <blockdata.h>

class TabData;
class BlockItemData : public QObject
{
    Q_OBJECT
    K_AUTO_PROPERTY(int, row, row, setRow, rowChanged, -1)
    K_AUTO_PROPERTY(int, column, column, setColumn, columnChanged, -1)
    K_AUTO_PROPERTY(int, rowSpan, rowSpan, setRowSpan, rowSpanChanged, -1)
    K_AUTO_PROPERTY(int, columnSpan, columnSpan, setColumnSpan, columnSpanChanged, -1)
    K_AUTO_PROPERTY(QString, blockName, blockName, setBlockName, blockNameChaneged, "")
    K_AUTO_PROPERTY(BlockData*, blockData, blockData, setBlockData, blockDataChanged, nullptr)
    K_AUTO_PROPERTY(QString, componentPath, componentPath, setComponentPath, componentPathChanged, "")
    K_AUTO_PROPERTY(double, maximumHeight, maximumHeight, setMaximumHeight, maximumHeightChanged, -1)
    K_AUTO_PROPERTY(double, maximumWidth, maximumWidth, setMaximumWidth, maximumWidthChanged, -1)
    K_AUTO_PROPERTY(double, minimumHeight, minimumHeight, setMinimumHeight, minimumHeightChanged, -1)
    K_AUTO_PROPERTY(double, minimumWidth, minimumWidth, setMinimumWidth, minimumWidthChanged, -1)
    K_AUTO_PROPERTY(QString, backend, backend, setBackend, backendChanged, "")
    K_AUTO_PROPERTY(TabData*, tabData, tabData, setTabData, tabDataChanged, nullptr)
    K_AUTO_PROPERTY(QString, component, component, setComponent, componentChanged, "")
public:
    explicit BlockItemData(QObject *parent = nullptr);
    void deserialize(const QVariantMap &map);
    void setData(const QVariantMap &map);
};

#endif // BLOCKITEMDATA_H
