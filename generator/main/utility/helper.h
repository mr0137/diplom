#ifndef HELPER_H
#define HELPER_H

#include <pocomacros.h>

class AppDeclaration
{
public:
    static inline const char *module = "App";
    static inline const int major_version = 1;
    static inline const int minor_version = 0;
};

#define K_QML(TYPE) \
private: \
    static inline const int TYPE ## QMLRegistration = qmlRegisterType<TYPE>(AppDeclaration::module, AppDeclaration::major_version, AppDeclaration::minor_version, # TYPE); \

#endif // HELPER_H
