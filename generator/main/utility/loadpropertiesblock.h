#ifndef LOADPROPERTIESBLOCK_H
#define LOADPROPERTIESBLOCK_H

#include <blockdata.h>
#include <QObject>
#include <QDateTime>

class LoadPropertiesBlock : public BlockData
{
    Q_OBJECT
    Q_PROPERTY(QDateTime dateFrom READ dateFrom WRITE setDateFrom NOTIFY dateFromChanged)
    Q_PROPERTY(QDateTime dateTo READ dateTo WRITE setDateTo NOTIFY dateToChanged)
public:
    explicit LoadPropertiesBlock(QObject *parent = nullptr);

    // BlockData interface
    const QDateTime &dateFrom() const;
    void setDateFrom(const QDateTime &newDateFrom);

    const QDateTime &dateTo() const;
    void setDateTo(const QDateTime &newDateTo);

public slots:
    virtual bool save(QVariantList result) override;
    virtual QVariant serialize() override;
    virtual void deserialize(const QVariant &data, ITableDataProvider *listing) override;

signals:
    void dateFromChanged();
    void dateToChanged();
private:
    QDateTime m_dateFrom;
    QDateTime m_dateTo;
};

#endif // LOADPROPERTIESBLOCK_H
