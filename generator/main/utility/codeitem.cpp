#include "codeitem.h"

#include <QSGFlatColorMaterial>
#include <QSGGeometryNode>
#include <QSGSimpleTextureNode>
#include <QQuickWindow>


CodeItem::CodeItem(QQuickItem *parent) : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

const QImage &CodeItem::source() const
{
    return m_source;
}

void CodeItem::setSource(const QImage &newSource)
{
    if (m_source == newSource)
        return;
    m_source = newSource;
    update();
    emit sourceChanged();
}

QSGNode *CodeItem::updatePaintNode(QSGNode *node, UpdatePaintNodeData *)
{
    QSGGeometryNode *rectNode;
    QSGGeometry *g;
    if (node == nullptr) {
        rectNode = new QSGGeometryNode;
        // geometry
        g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rectNode->setGeometry(g);
        // rect color
        auto m = new QSGFlatColorMaterial();
        m->setColor("transparent");
        rectNode->setMaterial(m);
    } else {
        rectNode = static_cast<QSGGeometryNode*>(node);
        g = rectNode->geometry();
    }

    //update rect geom
    g->vertexDataAsPoint2D()[0].set(0, 0);
    g->vertexDataAsPoint2D()[1].set(width(), 0);
    g->vertexDataAsPoint2D()[2].set(width(), height());
    g->vertexDataAsPoint2D()[3].set(0, height());
    rectNode->markDirty(QSGNode::DirtyGeometry);
    //header text node
    // setup text node
    QSGSimpleTextureNode *textNode = nullptr;
    if (rectNode->childCount()>0) {
        textNode = static_cast<QSGSimpleTextureNode *>(rectNode->childAtIndex(0));
    } else {
        textNode = new QSGSimpleTextureNode();
        textNode->setOwnsTexture(true);
        rectNode->appendChildNode(textNode);
    }
    //draw image with texts

    auto t = window()->createTextureFromImage(m_source, QQuickWindow::TextureHasAlphaChannel);
    textNode->setRect(QRectF(0,0, m_source.width(), m_source.height()));
    textNode->setTexture(t);

    return rectNode;

}
