#include "appcore.h"
#include <interfaces/ipageplugin.h>
#include <databaseservice.h>
#include <QCoreApplication>
#include <QDataStream>
#include <QDebug>
#include <QDir>
#include <QDir>
#include <QPluginLoader>
#include <QSaveFile>
#include <QString>
#include <data_structures/menudata.h>
#include <services/tabsservice.h>
#include <QtConcurrent>
#include <itabledataprovider.h>
//#include "produceorderform.h"

AppCore::AppCore(QObject * parent):QObject(parent)
{
    m_authService = new AuthService(this);
    m_localConfService = new LocalConfService(this);
    m_menuService = new MenuTabsService(this);
    m_tabsService = new TabsService(this);
    m_printerService = new PrinterService(this);
    m_listingService = new ListingService(this);

    m_palette = {
        { "button", QVariantMap{
              {"backgroundColor", "#4B99BB"},
              {"textColor", "#2D667E"}
          }
        },
        { "header", QVariantMap{
              {"backgroundColor", "#4B99BB"},
              {"textColor", "#295062"}
          }
        },
        { "text", QVariantMap{
              {"textColor", "#2D667E"}
          }
        },
        { "app", QVariantMap{
              { "mainColor", "#6699CC" },
              { "firstBackgroundColor", "#DFF5FF" },
              { "secondBackgroundColor", "#C5D8E1" },
              { "firstTextColor", "black"},
              { "secondTextColor", "blue"}
          }}
    };

    connect(m_authService, &AuthService::pluginLoadingEnd, this, [this](QString error){
        if (error != "")
            qDebug() << error;

        m_menuService->proceed(m_authService->getMenuTabs());
        //menuService()->root()->submenu()->updateModel();
        m_components = m_authService->getComponents();
        m_blockDataCreators = m_authService->getBlocks();
        m_listingService->m_listingsCreators = m_authService->getListings();
        m_listingsCreators = m_authService->getListings();

        //for (const auto &k : m_authService->listingsSequance()){
        //    m_loadingDataContainer.updateQueue.push_back(k);
        //    qDebug() <<" kkkkkkkkkkkk = "<< k;
        //    //connect(m_listingsCreators[k], &ITableDataProvider::fullLoaded, this, )
        //   // updateListing(k);
        //}
        //m_loadingDataContainer.updateQueue.push_back("orders");
        //m_loadingDataContainer.updateQueue.push_back("goods");
        m_listingService->m_listingsNames = m_authService->listingsSequance();
        m_loadingDataContainer.listingsCount = m_loadingDataContainer.updateQueue.count();

        m_tabsService->setTabs(m_authService->getTabs());
        m_tabsService->setToolButtons(m_authService->getToolButtons());

        m_dataBaseService = new DatabaseService(this);
        setIsDatabaseAvailable(DatabaseService::isDatabaseAvailable());
        if (isDatabaseAvailable()){
            m_listingService->reloadAll();
            m_tabsService->init();
        }else{
            m_listingService->setCurrentLoadingListing("Connection failed: " + DatabaseService::currentConnection());
        }
    });

    //connect(this, &AuthService::)

    connect(m_authService, &AuthService::logoutProcessBegin, this, [this](QVariantMap *map){
        if (m_tabsService->nonSavedTabs().length() == 0) map->operator[]("accepted") = true;
    });

    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [this](){

        m_future = QtConcurrent::run([this](){
            auto list = m_tabsService->serialize();
            m_localConfService->saveTabs(list);
        });
    });
    //m_timer->start(30000);
    QTimer::singleShot(1000, this, [this](){
        if (m_localConfService->isCrashed()){
            m_tabsService->deserialize(m_localConfService->loadTabs());
        }
    });
}

AppCore *AppCore::instance()
{
    static AppCore * m_AppCore;
    if(m_AppCore == nullptr){
        m_AppCore = new AppCore();
    }
    return m_AppCore;
}

QObject *AppCore::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(scriptEngine)
    Q_UNUSED(engine)
    return AppCore::instance();
}

bool AppCore::isNumberFree(QString strQuery)
{
    return m_dataBaseService->isNumberFree(strQuery);
}

bool AppCore::isNumberOpen(QString type, QString name)
{
    auto tab = m_tabsService->tabItems().find([type, name](TabData * ti){return ti->type() == type && ti->name() == name;});
    if(tab == nullptr){
        return false;
    }
    return true;
}

void AppCore::setRootPath(QString path)
{
#ifdef QT_NO_DEBUG
    int times = 1;
#else
#ifdef Q_OS_LINUX
    int times = 3;
#endif
#ifdef Q_OS_WINDOWS
    int times = 4;
#endif
#endif
    while(times > 0 && !path.isEmpty()){
#ifdef Q_OS_LINUX
        if (path.endsWith("/")) times--;
#endif
#ifdef Q_OS_WINDOWS
        if (path.endsWith("\\")) times--;
#endif
        path.chop(1);
    }
    setRootDir(path);
}

BlockData *AppCore::createBlockData(QString id)
{
    BlockData* result = nullptr;
    if (m_blockDataCreators.contains(id)) {
        result = m_blockDataCreators[id]();
    }
    return result;
}



QString AppCore::getComponentPath(QString key)
{
    if (m_components.contains(key)) return m_components[key];
    return "";
}

bool AppCore::increaseNumber(QString &number)
{
    for(int i = 0; i < number.count(); ++i){
        auto ln = number.count() - (1 + i);
        auto num = number[ln].digitValue();
        if(num == -1){
            continue;
        }
        ++num;

        if(num == 10){
            num = 0;
        }

        number.replace(ln,1,QString::number(num));
        if(num == 0){
            continue;
        }
        return true;
    }

    return false;
}

bool AppCore::debugMode() const
{
    return m_debugMode;
}

void AppCore::setDebugMode(bool newDebugMode)
{
    if (m_debugMode == newDebugMode)
        return;
    m_debugMode = newDebugMode;
    emit debugModeChanged();
}

bool AppCore::autologin() const
{
    return m_authService->autologin();
}

AuthService *AppCore::authService() const
{
    return m_authService;
}

QVariantMap AppCore::palette() const
{
    return m_palette;
}

void AppCore::setPalette(const QVariantMap &newPalette)
{
    if (m_palette == newPalette)
        return;
    m_palette = newPalette;
    emit paletteChanged();
}

QVariant AppCore::getDataFromBlock(QString tabName, QString blockName)
{
    QVariant result;
    for (const auto &t: m_tabsService->tabItems()){
        if (t->name() == tabName){
            auto bi = t->getBlockItemData(blockName);
            if (bi){
                auto b = bi->blockData();
                result = b->serialize();
            }
        }
    }
    return result;
}

void AppCore::setDataToBlock(QString tabName, QString blockName, QVariant data)
{
    for (const auto &t: m_tabsService->tabItems()){
        if (t->name() == tabName){
            auto bi = t->getBlockItemData(blockName);
            if (bi){
                auto b = bi->blockData();
                b->deserialize(data);
                return;
            }
        }
    }
}

MenuTabsService *AppCore::menuService() const
{
    return m_menuService;
}

QString AppCore::rootDir() const
{
    return m_rootDir;
}

void AppCore::setRootDir(const QString &newRootDir)
{
    if (m_rootDir == newRootDir)
        return;
    m_rootDir = newRootDir;
    emit rootDirChanged();
}

TabsService *AppCore::tabsService() const
{
    return m_tabsService;
}

void AppCore::setTabsService(TabsService *newTabsService)
{
    if (m_tabsService == newTabsService)
        return;
    m_tabsService = newTabsService;
    emit tabsServiceChanged();
}

LocalConfService *AppCore::localConfService() const
{
    return m_localConfService;
}

void AppCore::setLocalConfService(LocalConfService *newLocalConfService)
{
    if (m_localConfService == newLocalConfService)
        return;
    m_localConfService = newLocalConfService;
    emit localConfServiceChanged();
}

ListingService *AppCore::listingService() const
{
    return m_listingService;
}

void AppCore::setListingService(ListingService *newListingService)
{
    if (m_listingService == newListingService)
        return;
    m_listingService = newListingService;
    emit listingServiceChanged();
}

bool AppCore::isDatabaseAvailable() const
{
    return m_isDatabaseAvailable;
}

void AppCore::setIsDatabaseAvailable(bool newIsDatabaseAvailable)
{
    if (m_isDatabaseAvailable == newIsDatabaseAvailable)
        return;
    m_isDatabaseAvailable = newIsDatabaseAvailable;
    emit isDatabaseAvailableChanged();
}

PrinterService *AppCore::printerService() const
{
    return m_printerService;
}

void AppCore::setPrinterService(PrinterService *newPrinterService)
{
    if (m_printerService == newPrinterService)
        return;
    m_printerService = newPrinterService;
    emit printerServiceChanged();
}
