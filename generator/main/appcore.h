#ifndef AppCoreCORE_H
#define AppCoreCORE_H

#include <QObject>
#include <QDebug>
#include <QVariantMap>
#include <QVariantList>
#include <QVariantMap>

#include <kpoco.h>
#include <klist.h>
#include <pocomacros.h>
#include <services/authservice.h>
#include <data_structures/tabdata.h>
#include <services/menutabsservice.h>
#include <databaseservice.h>
#include <services/authservice.h>
#include <services/listingservice.h>
#include <services/localconfservice.h>
#include <services/printerservice.h>

class MenuData;
class BlockData;
class TabsService;
class DatabaseService;

struct SturtupLoading{
    int loadedCount;
    int count;
    QString listing;
    int currentNumber;
    int listingsCount;
    QStringList updateQueue;
    bool isDone = false;
};

class AppCore: public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(AppCore)
    QML_SINGLETON
    Q_PROPERTY(bool autologin READ autologin NOTIFY autologinChanged)
    Q_PROPERTY(QString rootDir READ rootDir WRITE setRootDir NOTIFY rootDirChanged)
    Q_PROPERTY(AuthService* authService READ authService NOTIFY authServiceChanged)
    Q_PROPERTY(MenuTabsService* menuService READ menuService NOTIFY menuServiceChanged)
    Q_PROPERTY(QVariantMap palette READ palette WRITE setPalette NOTIFY paletteChanged)
    Q_PROPERTY(bool debugMode READ debugMode WRITE setDebugMode NOTIFY debugModeChanged)
    Q_PROPERTY(TabsService* tabsService READ tabsService WRITE setTabsService NOTIFY tabsServiceChanged)
    Q_PROPERTY(PrinterService* printerService READ printerService WRITE setPrinterService NOTIFY printerServiceChanged)
    Q_PROPERTY(ListingService* listingService READ listingService WRITE setListingService NOTIFY listingServiceChanged)
    Q_PROPERTY(bool isDatabaseAvailable READ isDatabaseAvailable WRITE setIsDatabaseAvailable NOTIFY isDatabaseAvailableChanged)
    Q_PROPERTY(LocalConfService* localConfService READ localConfService WRITE setLocalConfService NOTIFY localConfServiceChanged)
    AppCore(QObject * parent = nullptr);
signals:
    void paletteChanged();
    void rootDirChanged();
    void autologinChanged();
    void debugModeChanged();
    void menuServiceChanged();
    void authServiceChanged();
    void tabsServiceChanged();
    void responseAwaiterChanged();
    void listingUpdated(QString name);
    void message(QString key,QVariant var);
    void nameChanged();
    void localConfServiceChanged();
    void listingServiceChanged();

    void isDatabaseAvailableChanged();

    void printerServiceChanged();

public:
    static AppCore* instance();
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);
    // [property]
    bool autologin() const;
    bool debugMode() const;
    QString rootDir() const;
    MenuData *rootMenu() const;
    QVariantMap palette() const;
    AuthService *authService() const;
    MenuTabsService *menuService() const;

    void setRootPath(QString path);
    void setDebugMode(bool newDebugMode);
    void setAutologin(bool newAutologin);
    void setRootMenu(MenuData *newRootMenu);
    void setRootDir(const QString &newRootDir);
    void setPalette(const QVariantMap &newPalette);
    // [property]

    QVariant getDataFromBlock(QString tabName, QString blockName);
    void setDataToBlock(QString tabName, QString blockName, QVariant data);

    TabsService *tabsService() const;
    void setTabsService(TabsService *newTabsService);
    LocalConfService *localConfService() const;
    void setLocalConfService(LocalConfService *newLocalConfService);

    ListingService *listingService() const;
    void setListingService(ListingService *newListingService);

    bool isDatabaseAvailable() const;
    void setIsDatabaseAvailable(bool newIsDatabaseAvailable);

    PrinterService *printerService() const;
    void setPrinterService(PrinterService *newPrinterService);

public slots:
    bool isNumberFree(QString strQuery);
    bool isNumberOpen(QString type, QString name);

    QString getComponentPath(QString key);
    BlockData * createBlockData(QString id);

private:
    bool increaseNumber(QString &number);
    QTimer *m_timer;
    QMap<QString, std::function<BlockData *()> > m_blockDataCreators;
    QMap<QString, QString> m_components;
    QMap<QString, std::function<ITableDataProvider *(ITableDataProvider *model)>> m_listingsCreators;
    QMap<QString, ITableDataProvider*> m_listings;
    QFuture<void> m_future;
    bool m_debugMode = true;
    QVariantMap m_palette;
    AuthService *m_authService;
    MenuTabsService *m_menuService;
    ListingService *m_listingService;
    DatabaseService * m_dataBaseService;

    QVariantMap tabOrderN;
    QString m_rootDir = "";
    TabsService *m_tabsService;
    LocalConfService* m_localConfService;
    QString m_loadingListingMessage;
    SturtupLoading m_loadingDataContainer;
    bool m_isDatabaseAvailable = false;
    PrinterService *m_printerService;
};

#endif // AppCoreCORE_H
