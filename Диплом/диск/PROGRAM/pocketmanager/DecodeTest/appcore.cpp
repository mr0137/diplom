#include "appcore.h"

#include <QTimer>

AppCore::AppCore(QObject *parent) : QObject(parent)
{
    m_thread = new QThread();
    m_zxing = new QZXing();


    connect(m_zxing, &QZXing::tagFound, this, [this](QString str){
        qDebug() << "Tag" << str;
        qDebug() << "scanned";
        setReadyForScan(true);
    });

    //connect(m_zxing, &QZXing::decodingFinished, this, [this](bool state){
    //    setReadyForScan(true);
    //});

    connect(m_zxing, &QZXing::error, this, [this](QString err){
        qDebug() <<"error" << err;
        setReadyForScan(true);
    });

    m_file = new QFile("test.csv");
    m_file->open(QIODevice::WriteOnly);
    m_stream = new QTextStream(m_file);

    connect(this, &AppCore::decodeImage, m_zxing, [this](QImage img, QString path){
        QElapsedTimer timer;
        timer.start();
        m_zxing->decodeImage(img);
        int idx = 0;
        if (path.contains("Code39"))            { idx = 1;  qDebug() << "decoding" << "Code 39";}
        else if (path.contains("Code93"))       { idx = 2;  qDebug() << "decoding" << "Code 93";}
        else if (path.contains("UPC-A"))        { idx = 3;  qDebug() << "decoding" << "UPC-A";}
        else if (path.contains("UPC-E"))        { idx = 4;  qDebug() << "decoding" << "UPC-E";}
        else if (path.contains("EAN-13"))       { idx = 5;  qDebug() << "decoding" << "EAN-13";}
        else if (path.contains("Code 128"))     { idx = 6;  qDebug() << "decoding" << "Code 128";}
        else if (path.contains("Data Matrix"))  { idx = 7;  qDebug() << "decoding" << "Data Matrix";}
        else if (path.contains("PDF417"))       { idx = 8;  qDebug() << "decoding" << "PDF417";}
        else if (path.contains("Aztec"))        { idx = 9;  qDebug() << "decoding" << "Aztec";}
        else if (path.contains("QR Code"))      { idx = 10; qDebug() << "decoding" << "QR Code";}
        else if (path.contains("OWN Code"))     { idx = 11; qDebug() << "decoding" << "OWN Code";}
        m_resultList[idx].append(m_zxing->isError ? "-" : QString::number(timer.nsecsElapsed()/1000000.).replace(".", ","));
    });

    m_zxing->moveToThread(m_thread);
    m_thread->start(QThread::HighPriority);

    m_resultList.append(QStringList{"1", "7", "13", "15", "30", "60", "120", "240", "512"});
    for (int i = 0; i < 11; i++) m_resultList.push_back(QStringList());
}

void AppCore::autoTest()
{
    test("C:/Users/mr013/Desktop/Codes/", 1);
    test("C:/Users/mr013/Desktop/Codes/", 7);
    test("C:/Users/mr013/Desktop/Codes/", 13);
    test("C:/Users/mr013/Desktop/Codes/", 15);
    test("C:/Users/mr013/Desktop/Codes/", 30);
    test("C:/Users/mr013/Desktop/Codes/", 60);
    test("C:/Users/mr013/Desktop/Codes/", 120);
    test("C:/Users/mr013/Desktop/Codes/", 240);
    test("C:/Users/mr013/Desktop/Codes/", 512);
}

void AppCore::test(QString pathFolder, int length)
{
    setReadyForScan(false);

    for (const auto &l : m_list){
        m_paths.append(pathFolder + l + "_" + QString::number(length) + ".png");
    }
    setReadyForScan(true);
    setTesting(true);
}

bool AppCore::readyForScan() const
{
    return m_readyForScan;
}

void AppCore::setReadyForScan(bool newReadyForScan)
{
    if (m_readyForScan == newReadyForScan)
        return;
    m_readyForScan = newReadyForScan;
    emit readyForScanChanged();

    if (!m_readyForScan) return ;
    if (m_paths.length() > 0){
        m_readyForScan = false;
        QString path = m_paths.takeFirst();
        QImage img(path);
        if (!img.isNull()){

            //if (path.contains("Code39"))            m_zxing->setDecoder(QZXing::DecoderFormat_CODE_39);
            //else if (path.contains("Code93"))       m_zxing->setDecoder(QZXing::DecoderFormat_CODE_93);
            //else if (path.contains("UPC-A"))        m_zxing->setDecoder(QZXing::DecoderFormat_UPC_A);
            //else if (path.contains("UPC-E"))        m_zxing->setDecoder(QZXing::DecoderFormat_UPC_E);
            //else if (path.contains("EAN-13"))       m_zxing->setDecoder(QZXing::DecoderFormat_EAN_13);
            //else if (path.contains("Code 128"))     m_zxing->setDecoder(QZXing::DecoderFormat_CODE_128);
            //else if (path.contains("Data Matrix"))  m_zxing->setDecoder(QZXing::DecoderFormat_DATA_MATRIX);
            //else if (path.contains("PDF417"))       m_zxing->setDecoder(QZXing::DecoderFormat_PDF_417);
            //else if (path.contains("Aztec"))        m_zxing->setDecoder(QZXing::DecoderFormat_Aztec);
            //else if (path.contains("QR Code"))      m_zxing->setDecoder(QZXing::DecoderFormat_QR_CODE);
             if (path.contains("OWN Code"))     m_zxing->setDecoder(QZXing::DecoderFormat_CODE_128_EXT);
            else{ m_zxing->setDecoder(QZXing::DecoderFormat_Aztec); }

            emit this->decodeImage(img, path);
        }
    }else{
        setTesting(false);
        QTimer::singleShot(1000, this, [this](){
            QString result;
            for (const auto &m : qAsConst(m_resultList)){
                qDebug() << m.join(" ");
                result += m.join(";") + "\n";
            }
            *m_stream << result;
            m_stream->flush();
            m_file->close();
            exit(0);
        });
    }

}
