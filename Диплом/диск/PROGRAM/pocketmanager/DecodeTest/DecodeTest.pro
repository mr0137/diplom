QT += widgets
QT += gui
QT += quick
QT += core
CONFIG += c++17

SOURCES += \
        appcore.cpp \
        main.cpp

HEADERS += \
    appcore.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libs/QZXing/release/ -lQZXing
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/QZXing/debug/ -lQZXing
else:unix: LIBS += -L$$OUT_PWD/../libs/QZXing/ -lQZXing

INCLUDEPATH += $$PWD/../libs/QZXing
DEPENDPATH += $$PWD/../libs/QZXing

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libs/klibcorelite/release/ -lklibcorelite
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
else:unix: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite
