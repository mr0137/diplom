#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>
#include <QThread>
#include <QZXing.h>
#include <kmacro.h>
#include <QStringList>
#include <QImage>
#include <QTextStream>
#include <QFile>

class AppCore : public QObject
{
    Q_OBJECT
    K_SINGLETON(AppCore)
    Q_PROPERTY(bool readyForScan READ readyForScan WRITE setReadyForScan NOTIFY readyForScanChanged)
    K_READONLY_PROPERTY(bool, testing, testing, setTesting, testingChanged, false)
    K_READONLY_PROPERTY(QStringList, paths, paths, setPaths, pathsChanged, QStringList())

    explicit AppCore(QObject *parent = nullptr);
public:
    void autoTest();
    void test(QString pathFolder, int length);

    bool readyForScan() const;
    void setReadyForScan(bool newReadyForScan);

signals:
    void decodeImage(QImage image, QString path);
    void readyForScanChanged();

private:
    QZXing *m_zxing;
    QThread *m_thread;
    QFile *m_file;
    QTextStream *m_stream;
    int m_n = 0;

    QList<QStringList> m_resultList;
    const QStringList m_list = {
        "Code39",
        "Code93",
        "UPC-A",
        "UPC-E",
        "EAN-13",
        "Code 128",
        "Data Matrix",
        "PDF417",
        "Aztec",
        "QR Code",
        "OWN Code"
    };
    bool m_readyForScan = true;
};

#endif // APPCORE_H
