#ifndef DBSERVICE_H
#define DBSERVICE_H

#include <QObject>
#include <QSqlDatabase>
#include <kmacro.h>
#include <QVariantMap>
#include <QGuiApplication>
#include <functional>

class Client;
QT_BEGIN_NAMESPACE
class DBService {

    class Local {
        Q_DISABLE_COPY(Local)
    public:
        Local();
        QVariantList getClients();
        void changeAccess(QString uid, bool access);
        void addClientToDB(Client *client);
        void updateDeviceName(QString uid, QString deviceName);
        QString addLog(QString message, QString uid = "PC", QString deviceName = "ADMIN");
        void setAddLogHandler(std::function<void(QString time, QString uid, QString deviceName, QString message)> handler);
        ~Local();
    private:
        QSqlDatabase m_sql;
        QString currentTime();
        void connect();
        std::function<void(QString time, QString uid, QString deviceName, QString message)> m_handler = nullptr;
    };

    class Global{
        Q_DISABLE_COPY(Global)
    public:
        Global();
        static Global *instance();
        QVariantMap selectPartInfo(const int id);
        bool isDatabaseAvailable();
        void closeAllConnections(QString connectionName);
        void closeConnection(QString connectionName);
        QSqlDatabase createConnection(QString connectionName);
        void setErrorHandler(std::function<void()> handler);
        ~Global();
    private:
         std::function<void()> m_handler = nullptr;
    };

public:
    inline static Local *local(){
        static Local *m_localInstance;
        if (m_localInstance == nullptr) {
            m_localInstance = new Local();
        }
        return m_localInstance;

    }

    inline static Global *global(){
        static Global *m_globalInstance;
        if (m_globalInstance == nullptr) {
            m_globalInstance = new Global();
        }
        return m_globalInstance;
    }
};
QT_END_NAMESPACE
#endif // DBSERVICE_H
