#include "client.h"

Client::Client(QObject *parent) : QObject(parent)
{
    m_row->id = 0;
    m_row->cells << Cell{ .textColor = "red", .displayText =  "Offline", .flags = Cell::OWN_TXTCOLOR }
                 << Cell{ }
                 << Cell{ }
                 << Cell{ }
                 << Cell{ .textColor = "red", .displayText =  "Denied", .flags = Cell::OWN_TXTCOLOR };
    qDebug() << m_row->cells[0].displayText;
}

Client::~Client()
{
    qDebug() << "dead" << m_uid;
}

const QString &Client::addr() const
{
    return m_addr;
}

void Client::setAddr(const QString &newAddr)
{
    if (m_addr == newAddr)
        return;
    m_addr = newAddr;
    m_row->cells[3] = Cell{ .displayText = QString(newAddr)};
    emit addrChanged();
        emit updated();
}

bool Client::connected() const
{
    return m_connected;
}

void Client::setConnected(bool newConnected)
{
    if (m_connected == newConnected)
        return;
    m_connected = newConnected;
    m_row->cells[0] = Cell{
            .textColor = newConnected ? "green" : "red",
            .displayText = newConnected ? "Online" : "Offline",
            .flags = Cell::OWN_TXTCOLOR
};
    emit connectedChanged();
        emit updated();
}

const QString &Client::uid() const
{
    return m_uid;
}

void Client::setUid(const QString &newUid)
{
    if (m_uid == newUid)
        return;
    m_uid = newUid;
    m_row->cells[2] = Cell{ .displayText = QString(newUid)};
    emit uidChanged();
        emit updated();
}

bool Client::isPermitted() const
{
    return m_isPermitted;
}

void Client::setIsPermitted(bool newIsPermitted)
{
    if (m_isPermitted == newIsPermitted)
        return;
    m_isPermitted = newIsPermitted;
    m_row->cells[4] = Cell{
            .textColor = newIsPermitted ? "green" : "red",
            .displayText = newIsPermitted ? "Accepted" : "Denied",
            .flags = Cell::OWN_TXTCOLOR};
    emit isPermittedChanged();
        emit updated();
}

const QString &Client::deviceName() const
{
    return m_deviceName;
}

void Client::setDeviceName(const QString &newDeviceName)
{
    if (m_deviceName == newDeviceName)
        return;
    m_deviceName = newDeviceName;
    m_row->cells[1] = Cell{ .displayText = QString(newDeviceName)};
    emit deviceNameChanged();
    emit updated();
}

const int &Client::absoluteId() const
{
    return m_absoluteId;
}

void Client::setAbsoluteId(const int &newAbsoluteId)
{
    if (m_absoluteId == newAbsoluteId)
        return;
    m_absoluteId = newAbsoluteId;
    m_row->id = newAbsoluteId;
    emit absoluteIdChanged();
        emit updated();
}
