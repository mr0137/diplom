#ifndef LOG_H
#define LOG_H

#include <itabledataprovider.h>
#include <QObject>

class Log : public ITableDataProvider
{
    Q_OBJECT
public:
    explicit Log(QObject *parent = nullptr);

    virtual int rowCount() const override;
    virtual int columnCount() const override;
    virtual QVector<Header> headerData() const override;
    virtual Row getRow(int row) override;
    virtual void sort(int column) override;
    virtual bool isFullLoaded() override;
    virtual int rowById(int id) override;
public slots:
    void appendLog(QString time, QString message, QString uid = "PC", QString deviceName = "ADMIN");
private:
    QVector<Row> m_rowsCache;
    QVector<int> m_filteredIndeces;
    int m_rowsCount = 0;
};

#endif // LOG_H
