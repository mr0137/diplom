#ifndef SERVERSERVICE_H
#define SERVERSERVICE_H

#include <client.h>
#include <tcpserverworker.h>

#include <QByteArray>
#include <QObject>
#include <kmacro.h>
#include <QTcpServer>
#include <QTimer>
#include <QFuture>
#include <QSqlDatabase>
#include <utility/kobservablelist.h>

class ClientsTable;

class ServerService : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    K_AUTO_PROPERTY(KObservableModel*, clientsModel, clientsModel, setClientsModel, clientsModelChanged, nullptr)
    K_READONLY_PROPERTY(QString, ip, ip, setIp, ipChanged, "")
    K_READONLY_PROPERTY(int, port, port, setPort, portChanged, 45454)
    K_READONLY_PROPERTY(bool, isProceeding, isProceeding, setIsProceeding, isProceedingChanged, false)
    friend class ClientsTable;
public:
    explicit ServerService(QObject * parent = nullptr);
    ~ServerService();

public slots:
    void newConnection( QHostAddress, int );
    void removeClient(int socketDescriptor);
    void changeAccess(int index, bool access);
signals:
    void updateRow(int row);
private:
    KObservableList<Client> m_clientsList;
    TcpServerWorker *m_worker;
    Client *m_waitingClient = nullptr;
    int m_countOfClients = 0;
    int m_lastSendedId = 0;
    QString m_lastScannedCode;

    void loadClients();
    Client *getClient(int socketDescriptor);
};


#endif // SERVERSERVICE_H
