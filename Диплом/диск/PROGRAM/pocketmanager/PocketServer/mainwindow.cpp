#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <appcore.h>
#include <QAbstractListModel>
#include <QQmlEngine>
#include <QQuickView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/main/icon/logo.png"));
    initQmlEngine();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initQmlEngine()
{
    QQmlEngine* engine = ui->quickWidget->engine();
    qmlRegisterSingletonType<AppCore>("App", 1, 0, "AppCore", &AppCore::qmlInstance);
    qmlRegisterAnonymousType<QAbstractListModel>("App", 1);
    qmlRegisterType<ServerService>("App", 1, 0, "ServerService");
    qmlRegisterType<ClientsTable>("App", 1, 0, "ClientsTable");

#ifdef QT_NO_DEBUG
    engine->addImportPath(AppCore::instance()->rootDir() + "/plugins/");
#else
    engine->addImportPath(AppCore::instance()->rootDir() + "/bin/plugins/");
#endif
    qDebug() << engine->importPathList();
    ui->quickWidget->setSource(QStringLiteral("qrc:/main/main.qml"));
}
