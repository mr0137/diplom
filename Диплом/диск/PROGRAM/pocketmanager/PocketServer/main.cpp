#include <QAbstractItemModel>
#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <appcore.h>
#include <dbservice.h>
#include <mainwindow.h>

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0);

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString & msg)
{
#ifdef QT_NO_DEBUG
    //DevLogger::instance()->addLog(context.category, context.function, msg);
#endif
    if (!msg.contains("QQuickWidget does not support") && !msg.contains("If you wish to create your root"))
        (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, msg);
}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    qSetMessagePattern("%{if-category}\033[32m%{category}:%{endif}%{if-warning}\033[37m%{file}:%{line}: %{endif}\033\[31m%{if-debug}\033\[36m%{endif}[%{function}]\033[0m %{message}");
    qInstallMessageHandler(customMessageHandler);

    app.setOrganizationName("TK");
    app.setOrganizationDomain("TK");

    AppCore::instance()->setRootPath(argv[0]);

    MainWindow w;

    int result = app.exec();

    //для правильного завершения
    delete DBService::local();
    delete DBService::global();

    return result;
}
