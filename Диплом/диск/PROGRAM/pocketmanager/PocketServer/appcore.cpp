#include "appcore.h"
#include <qguiapplication.h>
#include <QNetworkInterface>
#include <streamconnector.h>
#include <QDataStream>
#include <Protocol.h>
#include <clientstable.h>
#include <dbservice.h>

AppCore::AppCore(QObject *parent) : QObject(parent)
{
    DBService::local()->setAddLogHandler([this](QString time, QString uid, QString deviceName, QString message){
        m_logTable->appendLog(time, message, uid, deviceName);
    });

    DBService::global()->setErrorHandler([this](){
        setIsDBAvailable(false);
    });

    m_thread = new QThread(this);

    m_server = new ServerService(m_thread);
    m_server->moveToThread(m_thread);
   // m_thread->start();
    m_table = new ClientsTable(m_server);
    m_timer = new QTimer(this);
    m_logTable = new Log(this);

    m_palette = {
        { "button", QVariantMap{
              {"backgroundColor", "#4B99BB"},
              {"textColor", "#2D667E"}
          }
        },
        { "header", QVariantMap{
              {"backgroundColor", "#4B99BB"},
              {"textColor", "#295062"}
          }
        },
        { "text", QVariantMap{
              {"textColor", "#2D667E"}
          }
        },
        { "app", QVariantMap{
              { "mainColor", "#6699CC" },
              { "firstBackgroundColor", "#DFF5FF" },
              { "darkBackgroundColor", "#232e3c"},
              { "lightBackgroundColor", QColor("#24303d").lighter(130)},
              { "secondBackgroundColor", "#C5D8E1" },
              { "firstTextColor", "black"},
              { "secondTextColor", "blue"},
              { "errorColor", "#ec3942"}
          }}
    };

    setIsDBAvailable(DBService::global()->isDatabaseAvailable());

    connect(m_timer, &QTimer::timeout, this, [this](){
        setIsDBAvailable(DBService::global()->isDatabaseAvailable());
    });
}

AppCore *AppCore::instance()
{
    static AppCore * m_AppCore;
    if(m_AppCore == nullptr){
        m_AppCore = new AppCore(qApp);
    }
    return m_AppCore;
}

AppCore::~AppCore()
{
    m_thread->terminate();
    delete m_server;
}

QObject *AppCore::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(scriptEngine)
    Q_UNUSED(engine)
    return AppCore::instance();
}

void AppCore::setRootPath(QString path)
{
#ifdef QT_NO_DEBUG
    int times = 1;
#else
#ifdef Q_OS_LINUX
    int times = 5;
#endif
#ifdef Q_OS_WINDOWS
    int times = 6;
#endif
#endif
    while(times > 0 && !path.isEmpty()){
#ifdef Q_OS_LINUX
        if (path.endsWith("/")) times--;
#endif
#ifdef Q_OS_WINDOWS
        if (path.endsWith("\\")) times--;
#endif
        path.chop(1);
    }
    setRootDir(path);
}

const QString &AppCore::rootDir() const
{
    return m_rootDir;
}

void AppCore::setRootDir(const QString &newRootDir)
{
    if (m_rootDir == newRootDir)
        return;
    m_rootDir = newRootDir;
    emit rootDirChanged();
}

ServerService *AppCore::server() const
{
    return m_server;
}

void AppCore::setServer(ServerService *newServer)
{
    if (m_server == newServer)
        return;
    m_server = newServer;
    emit serverChanged();
}

ClientsTable *AppCore::table() const
{
    return m_table;
}

const QVariantMap &AppCore::palette() const
{
    return m_palette;
}

void AppCore::setPalette(const QVariantMap &newPalette)
{
    if (m_palette == newPalette)
        return;
    m_palette = newPalette;
    emit paletteChanged();
}

bool AppCore::isDBAvailable() const
{
    return m_isDBAvailable;
}

void AppCore::setIsDBAvailable(bool newIsDBAvailable)
{
    if (!newIsDBAvailable) m_timer->start(1000);
    else m_timer->stop();

    if (m_isDBAvailable == newIsDBAvailable)
        return;
    DBService::local()->addLog("Database: " + QString(newIsDBAvailable ? "Connected" : "Disconnected"));
    m_isDBAvailable = newIsDBAvailable;
    emit isDBAvailableChanged();
}

Log *AppCore::logTable() const
{
    return m_logTable;
}

void AppCore::setLogTable(Log *newLogTable)
{
    if (m_logTable == newLogTable)
        return;
    m_logTable = newLogTable;
    emit logTableChanged();
}
