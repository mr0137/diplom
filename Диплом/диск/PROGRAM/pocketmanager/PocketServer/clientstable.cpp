#include "clientstable.h"

#include <serverservice.h>

ClientsTable::ClientsTable(QObject *parent) : ITableDataProvider(parent),
    m_server(qobject_cast<ServerService*>(parent))
{
    connect(m_server->clientsModel(), &QAbstractListModel::rowsInserted, this, [this](){
        emit reseted(false);
    });

    connect(m_server->clientsModel(), &QAbstractListModel::dataChanged, this, [this](const QModelIndex &topLeft, const QModelIndex, const QVector<int>){
        emit rowsUpdated(topLeft.row(), topLeft.row());
    });

    connect(m_server, &ServerService::updateRow, this, [this](int row){
        emit rowsUpdated(row, row);
    });
}

int ClientsTable::rowCount() const
{
    //return m_filteredIndeces.length();
    return m_server->m_clientsList.count();
}

int ClientsTable::columnCount() const
{
    return headerData().length();
}

QVector<Header> ClientsTable::headerData() const
{
    return QVector<Header>{
        Header{
            .text = "Status",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 90,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
        },
        Header{
            .text = "Name",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 200,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        },
        Header{
            .text = "UID",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 200,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        },
        Header{
            .text = "Address",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 150,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        },
        Header{
            .text = "Permission",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 115,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
        }
    };
}

Row ClientsTable::getRow(int row)
{
    if (m_server->m_clientsList.count() > row)
        return *m_server->m_clientsList.get(row)->row();
    else
        return Row{};
}

void ClientsTable::sort(int column)
{

}

bool ClientsTable::isFullLoaded()
{
    return true;
}

int ClientsTable::rowById(int id)
{
    for (int i = 0; i < m_server->m_clientsList.count(); i++){
        auto c = m_server->m_clientsList.get(i);
        if (c->row()->id == id) return i;
    }
    return -1;
}

bool ClientsTable::isEditable()
{
    return true;
}

bool ClientsTable::isPermitted(int row)
{
    if (row >= rowCount() || row <= -1) return false;
    return m_server->m_clientsList.get(row)->isPermitted();
}

