import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0
import TableRender 1.0

ApplicationWindow {
    width: 768
    height: 480
    visible: true
    title: qsTr("Pocket Server")

    property Component first: FirstComponent {}
    property Component second: SecondComponent {}

    StackView{
        id: stackView
        anchors.fill: parent
        initialItem: first
    }
}
