#include "log.h"

Log::Log(QObject *parent) : ITableDataProvider(parent)
{

}

int Log::rowCount() const
{
    return m_rowsCount;
}

int Log::columnCount() const
{
    return headerData().length();
}

QVector<Header> Log::headerData() const
{
    return QVector<Header>{
        Header{
            .text = "Time",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 200,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
        },
        Header{
            .text = "UID",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 120,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        },
        Header{
            .text = "Device Name",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 200,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        },
        Header{
            .text = "Message",
            .textColor = "black",
            .backgroundColor = "transparent",
            .width = 350,
            .headerAlignment = Qt::AlignHCenter | Qt::AlignVCenter,
            .cellAlignment = Qt::AlignRight | Qt::AlignVCenter,
        }
    };
}

Row Log::getRow(int row)
{
    if (row > -1 && row < m_filteredIndeces.length()){
        return m_rowsCache[m_filteredIndeces[row]];
    }
    return Row();
}

void Log::sort(int column)
{

}

bool Log::isFullLoaded()
{
    return true;
}

int Log::rowById(int id)
{
    int i = 0;
    for (const auto &r: qAsConst(m_rowsCache)){
        if (r.id == id) return i;
        i++;
    }
    return -1;
}

void Log::appendLog(QString time, QString message, QString uid, QString deviceName)
{
    Row row;
    row.id = m_rowsCount+1;
    row.cells << Cell{ .displayText = time } << Cell {.displayText = uid} << Cell{ .displayText = deviceName} << Cell{ .displayText = message};

    m_rowsCache.append(row);
    m_filteredIndeces.push_back(m_rowsCache.length() - 1);

    m_rowsCount = m_filteredIndeces.length();

    emit rowsUpdated(m_rowsCache.length() - 1, m_rowsCache.length() - 1);
    emit fullLoaded();
}

