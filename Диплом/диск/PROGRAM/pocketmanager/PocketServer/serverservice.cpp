#include "serverservice.h"

#include <QDataStream>
#include <QtConcurrent>
#include <QFile>
#include <QTcpSocket>
#include <QVariantMap>
#include <streamconnector.h>
#include <Protocol.h>
#include <QNetworkInterface>
#include <QHostInfo>
#include <dbservice.h>

#include <backend-qt/dxfinterface.h>

ServerService::ServerService(QObject * parent) : QObject(parent)
{
    if (QString(SERVER_IP) != "10.8.0.1"){
        m_ip = QString(SERVER_IP);
    }else{
        const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
        for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
            if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost)
                setIp(address.toString());
        }
    }

    qDebug() << ip();

    loadClients();

    m_worker = new TcpServerWorker(ip(), port(), this);

    connect(m_worker, SIGNAL(newConnection(QHostAddress, int)), this, SLOT(newConnection(QHostAddress, int)));
    connect(m_worker, SIGNAL(connectionRemoved(int)), this, SLOT(removeClient(int)));
    setClientsModel(m_clientsList.model());
    m_clientsModel->setParent(this);

    m_worker->setReceiver([this](const Frame& frame){
        QDataStream ss(frame.data);
        quint8 typeOfData;
        ss >> typeOfData;
        switch(typeOfData) {

        case CONNECTION_TYPE: {
            QString id;
            ss >> id;
            if(m_clientsList.count()) {
                for(auto && client : m_clientsList) {
                    if(client->uid() == id) {
                        if (m_waitingClient != nullptr){
                            client->setAddr(m_waitingClient->addr());
                            client->setPort(m_waitingClient->port());
                            client->setSocketDescriptor(m_waitingClient->socketDescriptor());
                            disconnect(m_waitingClient, &Client::updated, this, nullptr);
                            delete m_waitingClient;
                        }
                        client->setConnected(true);
                        m_waitingClient = nullptr;
                        Frame requestFrame;
                        QDataStream ans(&requestFrame.data, QIODevice::WriteOnly);
                        ans << CONNECTION_PERMISSION << (client->isPermitted() ? ACCESS_ACCEPTED : ACCESS_DENIED);
                        requestFrame.type = SendType::P2P;
                        requestFrame.socketDescriptor = frame.socketDescriptor;
                        DBService::local()->addLog("Client connected: " + client->addr(), client->uid(), client->deviceName());
                        DBService::local()->addLog("Client access: " + QString(client->isPermitted() ? "Confirmed" : "Denied"));
                        m_worker->writeData(requestFrame);
                        qDebug() << "connected to" << frame.socketDescriptor << "id:" << id;
                    }
                }
            }

            if(m_waitingClient != nullptr) {
                m_waitingClient->setUid(id);
                DBService::local()->addClientToDB(m_waitingClient);
                Frame requestFrame;
                QDataStream ans(&requestFrame.data, QIODevice::WriteOnly);
                ans << CONNECTION_PERMISSION << ACCESS_DENIED;
                requestFrame.type = SendType::P2P;
                requestFrame.socketDescriptor = m_waitingClient->socketDescriptor();
                m_worker->writeData(requestFrame);

                DBService::local()->addLog("Client connected: " + m_waitingClient->addr(), m_waitingClient->uid(), m_waitingClient->deviceName());
                DBService::local()->addLog("Client access: " + QString(m_waitingClient->isPermitted() ? "Accepted" : "Denied"));

                auto tmpClient = m_waitingClient;
                m_clientsList.push_back(m_waitingClient);
                connect(tmpClient, &Client::updated, this, [=](){
                    emit updateRow(m_clientsList.indexOf(tmpClient));
                });
                qDebug() << "connected to" << m_waitingClient->socketDescriptor() << "id:" << id;
                m_waitingClient = nullptr;
            }

        }
        case DEVICE_NAME:{
            if(m_clientsList.count()) {
                for(auto && client : m_clientsList) {
                    if(client->socketDescriptor() == frame.socketDescriptor) {
                        QString name;
                        ss >> name;
                        if (name == "") break;
                        if (client->deviceName() != name){
                            DBService::local()->addLog("Client name changed from \"" + client->deviceName() + "\" to \"" + name + "\"");
                            client->setDeviceName(name);
                            DBService::local()->updateDeviceName(client->uid(), name);
                        }
                        break;
                    }
                }
            }
            break;
        }
        case SCANNED_CODE:{
            QString code;
            ss >> code;
            m_lastScannedCode = code;
            auto result = DBService::global()->selectPartInfo(code.toInt());
            QString path = result.value("part_path", "").toString();
            QFile tmp("tmp.dxf");
            tmp.open(QIODevice::WriteOnly);
            QTextStream stream(&tmp);
#ifdef Q_OS_LINUX
            stream << result.value("file_bit", "").toByteArray();
#else
             stream << result.value("file_bit", "").toByteArray();
#endif
            //stream << readFile.readAll();
            tmp.close();
            DXFInterface w("tmp.dxf");
            //qDebug() << "line width" << w.lineWidth();
            DXFInterface dxfmin("tmp.dxf", w.lineWidth());
            DXFInterface dxfmax("tmp.dxf", 1.2);
            Frame requestFrame;

            if (dxfmin.getImg().isNull() || dxfmax.getImg().isNull()){
                result["imageMin"] = QImage(":/main/error.png");
                result["imageMax"] = QImage(":/main/error.png");
            }else{
                result["imageMax"] = dxfmax.getImg();
                result["imageMin"] = dxfmin.getImg();
            }
            qDebug() << result;
            QDataStream ans(&requestFrame.data, QIODevice::WriteOnly);
            ans << SCANNED_CODE << result;

            requestFrame.type = SendType::P2P;
            requestFrame.socketDescriptor = frame.socketDescriptor;
            m_worker->writeData(requestFrame);
            auto c = getClient(frame.socketDescriptor);
            if (c)DBService::local()->addLog("Client scanned code: " + code, c->uid(), c->deviceName());
            tmp.remove();
            break;
        }
        case UPDATE_COUNT:{
            QVariantMap map;
            ss >> map;
            qDebug() << "map" << map;
            break;
        }
        case DEBUG_IMAGE:{
            QVariantMap m;
            QImage img = qvariant_cast<QImage>(m.value("img", ""));
            ss >> m;
            if (!img.isNull()){
                qDebug() << "image saved" << m_lastScannedCode+".png";
                img.save("sss.png", "PNG");
            }
            break;
        }
        default:
            break;
        }
    });
}

ServerService::~ServerService()
{
}

void ServerService::newConnection(QHostAddress addr, int port)
{
    for(auto && client : m_clientsList) {
        if(client->addr() == addr.toString()) {
            qDebug() << "client exists, bind to new socket and setting alive";
            client->setSocketDescriptor(m_worker->getSocketByAddr(addr.toString())->socketDescriptor());
            client->setPort(port);
            client->setConnected(true);
            emit clientsModelChanged(m_clientsModel);
            return;
        }
    }

    m_waitingClient = new Client(this);
    m_waitingClient->setAddr(addr.toString());
    m_waitingClient->setPort(port);
    m_waitingClient->setAbsoluteId(m_clientsList.count());
    m_waitingClient->setConnected(true);
    connect(m_waitingClient, &Client::updated, this, [=](){
        emit updateRow(m_clientsList.indexOf(m_waitingClient));
    });
    QTcpSocket *soc = m_worker->socket(m_worker->clientCount() - 1);
    m_waitingClient->setSocketDescriptor(soc->socketDescriptor());

    emit clientsModelChanged(m_clientsModel);
}

void ServerService::removeClient(int socketDescriptor)
{
    for(auto && client : m_clientsList) {
        if(client->socketDescriptor() == socketDescriptor) {
            DBService::local()->addLog("Client disconnected", client->uid(), client->deviceName());
            client->setConnected(false);
            client->setAddr("Unknown");
        }
    }
}

void ServerService::changeAccess(int index, bool access)
{
    auto client = m_clientsList.get(index);
    //qDebug() << "change" << client->connected();
    if (client->connected() && client->isPermitted() != access){
        Frame requestFrame;
        QDataStream ans(&requestFrame.data, QIODevice::WriteOnly);
        ans << CONNECTION_PERMISSION << (access ? ACCESS_ACCEPTED : ACCESS_DENIED);
        requestFrame.type = SendType::P2P;
        requestFrame.socketDescriptor = client->socketDescriptor();
        m_worker->writeData(requestFrame);

    }

    if (client->isPermitted() != access){
        client->setIsPermitted(access);
        DBService::local()->changeAccess(client->uid(), access);
        DBService::local()->addLog("Client access (\'" + client->uid() + "\') changed to " + QString(client->isPermitted() ? "Accepted" : "Denied"));
    }
}

void ServerService::loadClients()
{
    QVariantList list = DBService::local()->getClients();

    for (const auto &el : list){
        auto llist = el.toList();
        Client * newClient = new Client(this);
        newClient->setAddr("Unknown");
        newClient->setPort(-1);
        newClient->setUid(llist[2].toString());
        newClient->setIsPermitted(llist[3].toBool());
        newClient->setAbsoluteId(m_clientsList.count());
        newClient->setConnected(false);
        newClient->setDeviceName(llist[1].toString());
        connect(newClient, &Client::updated, this, [=](){
            emit updateRow(m_clientsList.indexOf(newClient));
        });
        m_clientsList.push_back(newClient);
    }
}

Client *ServerService::getClient(int socketDescriptor)
{
    for (auto *c: m_clientsList){
        if (c->socketDescriptor() == socketDescriptor) return c;
    }
    return nullptr;
}
