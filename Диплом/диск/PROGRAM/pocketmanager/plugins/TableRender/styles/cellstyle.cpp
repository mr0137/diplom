#include "cellstyle.h"

/*!
 *  \class CellStyle
 *  \brief The CellStyle class allows user to configure cell's style.
 */
class CellStyle;

/*!
 * \fn CellStyle::CellStyle(QObject *parent)
 * \brief Default contructor.
 */
CellStyle::CellStyle(QObject *parent) : QObject(parent)
{
    m_font = new CellFontStyle(this);
}

/*!
 * \property CellFontStyle* CellStyle::font
 * \brief This property holds font of cell.
 */
CellFontStyle *CellStyle::fontStyle()
{
    return m_font;
}

/*!
 * \property int CellStyle::rightMargin
 * \brief This property holds value of right margin.
 */
int CellStyle::rightMargin() const
{
    return m_rightMargin;
}

/*!
 * \fn void CellStyle::setRightMargin(int newRightMargin)
 * \brief Right margin's value setter.
 */
void CellStyle::setRightMargin(int newRightMargin)
{
    if (m_rightMargin == newRightMargin)
        return;
    m_rightMargin = newRightMargin;
    emit rightMarginChanged();
}

/*!
 * \fn int CellStyle::leftMargin
 * \brief CellStyle::leftMargin
 */
int CellStyle::leftMargin() const
{
    return m_leftMargin;
}

/*!
 * \fn void CellStyle::setLeftMargin(int newLeftMargin)
 * \brief Left margin's value setter.
 */
void CellStyle::setLeftMargin(int newLeftMargin)
{
    if (m_leftMargin == newLeftMargin)
        return;
    m_leftMargin = newLeftMargin;
    emit leftMarginChanged();
}

/*!
 * \property QColor CellStyle::textColor
 * \brief This property holds value of cell's text color.
 */
const QColor &CellStyle::textColor() const
{
    return m_textColor;
}

/*!
 * \fn void CellStyle::setTextColor(const QColor &newTextColor)
 * \brief Text color setter.
 */
void CellStyle::setTextColor(const QColor &newTextColor)
{
    if (m_textColor == newTextColor)
        return;
    m_textColor = newTextColor;
    emit textColorChanged();
}

/*!
 * \property QColor &CellStyle::backgroundColor
 * \brief This property holds value of cell's background color.
 */
const QColor &CellStyle::backgroundColor() const
{
    return m_backgroundColor;
}

/*!
 * \fn void CellStyle::setBackgroundColor(const QColor &newBackgroundColor)
 * \brief Background color setter.
 */
void CellStyle::setBackgroundColor(const QColor &newBackgroundColor)
{
    if (m_backgroundColor == newBackgroundColor)
        return;
    m_backgroundColor = newBackgroundColor;
    emit backgroundColorChanged();
}

/*!
 * \property double CellStyle::height
 * \brief This property holds value of cell's height.
 */
double CellStyle::height() const
{
    return m_height;
}

/*!
 * \fn void CellStyle::setHeight(double newHeight)
 * \brief Cell's height setter.
 * \param newHeight
 */
void CellStyle::setHeight(double newHeight)
{
    if (qFuzzyCompare(m_height, newHeight))
        return;
    m_height = newHeight;
    emit heightChanged();
}

/*!
 * \property quint32 CellStyle::alignment
 * \brief This property holds value of cell's text alignment.
 */
quint32 CellStyle::alignment() const
{
    return m_alignment;
}

/*!
 * \fn void CellStyle::setAlignment(quint32 newAlignment)
 * \brief Cell's alignment setter.
 */
void CellStyle::setAlignment(quint32 newAlignment)
{
    if (m_alignment == newAlignment)
        return;
    m_alignment = newAlignment;
    emit alignmentChanged();
}

/*!
 * \property quint8 CellStyle::elide()
 * \brief This property holds value of cell's text elide mode.
 */
quint8 CellStyle::elide() const
{
    return m_elide;
}

/*!
 * \fn void CellStyle::setElide(quint8 newElide)
 * \brief Cell's elide mode setter.
 */
void CellStyle::setElide(quint8 newElide)
{
    if (m_elide == newElide)
        return;
    m_elide = newElide;
    emit elideChanged();
}
