#ifndef CELLFONTSTYLE_H
#define CELLFONTSTYLE_H

#include <QObject>
#include <QFont>
#include <qqml.h>
#include "tablerender_global.h"

class CellFontStyle : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString family READ family WRITE setFamily NOTIFY familyChanged)
    Q_PROPERTY(qreal pointSize READ pointSize WRITE setPointSize NOTIFY pointSizeChanged)
    Q_PROPERTY(qreal letterSpacing READ letterSpacing WRITE setLetterSpacing NOTIFY letterSpacingChanged)
    Q_PROPERTY(bool overline READ overline WRITE setOverline NOTIFY overlineChanged)
    Q_PROPERTY(bool bold READ bold WRITE setBold NOTIFY boldChanged)
    Q_PROPERTY(bool underline READ underline WRITE setUnderline NOTIFY underlineChanged)
    Q_PROPERTY(qreal wordSpacing READ wordSpacing WRITE setWordSpacing NOTIFY wordSpacingChanged)
    friend class Table;
public:
    explicit CellFontStyle(QObject *parent = nullptr);
    QFont font();
    const QString family() const;
    void setFamily(const QString &newFamily);

    qreal pointSize() const;
    void setPointSize(qreal newPointSize);

    qreal letterSpacing() const;
    void setLetterSpacing(qreal newLetterSpacing);

    bool overline() const;
    void setOverline(bool newOverline);

    bool bold() const;
    void setBold(bool newBold);

    bool underline() const;
    void setUnderline(bool newUnderline);

    qreal wordSpacing() const;
    void setWordSpacing(qreal newWordSpacing);

public slots:
    void reset();

signals:
    void boldChanged();
    void familyChanged();
    void overlineChanged();
    void pointSizeChanged();
    void underlineChanged();
    void wordSpacingChanged();
    void letterSpacingChanged();
    void fontChanged();

private:
    QFont m_font;
};

#endif // CELLFONTSTYLE_H
