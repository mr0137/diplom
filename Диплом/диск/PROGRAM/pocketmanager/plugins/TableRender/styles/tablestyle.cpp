#include "tablestyle.h"

#include <QIcon>
#include <QPainter>
#include <QSvgRenderer>

TableStyle::TableStyle(QObject *parent) : QObject(parent)
{
    m_cellStyle = new CellStyle(this);
    m_horizontalHeader = new HeaderStyle(HeaderStyle::Horizontal, this);
    m_verticalHeader = new HeaderStyle(HeaderStyle::Vertical, this);

    connect(m_horizontalHeader, &HeaderStyle::headerChanged, this, [this](){
        emit this->updateHeaders();
    });
    connect(m_verticalHeader, &HeaderStyle::headerChanged, this, [this](){
        emit this->updateHeaders();
    });
    connect(m_cellStyle, &CellStyle::cellChanged, this, [this](){
        emit this->updateAllPages();
    });
}

bool TableStyle::verticalHeaderAvailable() const
{
    return m_verticalHeaderAvailable;
}

void TableStyle::setVerticalHeaderAvailable(bool newVerticalHeaderAvailable)
{
    if (numericalHeaderAvailable()){
        qDebug() << "You can't change \"verticalHeaderAvailable\" while \"numericalHeaderAvailable\" is true";
        return;
    }
    if (m_verticalHeaderAvailable == newVerticalHeaderAvailable)
        return;
    m_verticalHeaderAvailable = newVerticalHeaderAvailable;
    emit updateHeaders();
    emit verticalHeaderAvailableChanged();
}

CellStyle *TableStyle::cellStyle()
{
    return m_cellStyle;
}

HeaderStyle *TableStyle::verticalHeader()
{
    return m_verticalHeader;
}

HeaderStyle *TableStyle::horizontalHeader()
{
    return m_horizontalHeader;
}


const QColor &TableStyle::highlightColor() const
{
    return m_highlightColor;
}

void TableStyle::setHighlightColor(const QColor &newHighlightColor)
{
    if (m_highlightColor == newHighlightColor)
        return;
    m_highlightColor = newHighlightColor;
    emit updateHeaders();
    emit highlightColorChanged();
}

QColor TableStyle::iconsColor() const
{
    return m_iconsColor;
}

double TableStyle::iconsWidth() const
{
    return m_iconsWidth;
}

QImage &TableStyle::icon(QString iconName)
{
    if (m_iconsMap.contains(iconName)) return m_iconsMap[iconName];
    return m_invalid;
}

void TableStyle::setGridColor(QColor gridColor)
{
    if (m_gridColor == gridColor)
        return;

    m_gridColor = gridColor;
    emit gridColorChanged(m_gridColor);
}

void TableStyle::setNumericalHeaderAvailable(bool numericalHeaderAvailable)
{
    if (m_numericalHeaderAvailable == numericalHeaderAvailable)
        return;

    m_verticalHeader->m_widthChangeable = numericalHeaderAvailable;
    setVerticalHeaderAvailable(numericalHeaderAvailable);

    m_numericalHeaderAvailable = numericalHeaderAvailable;
    emit updateHeaders();
    emit numericalHeaderAvailableChanged(m_numericalHeaderAvailable);
}

void TableStyle::setSelectionOpacity(double selectionOpacity)
{
    if (qFuzzyCompare(m_selectionOpacity, selectionOpacity))
        return;

    m_selectionOpacity = selectionOpacity;
    emit selectionOpacityChanged(m_selectionOpacity);
}

void TableStyle::setSelectionColor(QColor selectionColor)
{
    if (m_selectionColor == selectionColor)
        return;

    m_selectionColor = selectionColor;
    emit selectionColorChanged(m_selectionColor);
}

bool TableStyle::numericalHeaderAvailable() const
{
    return m_numericalHeaderAvailable;
}

void TableStyle::setIconsWidth(double iconsWidth)
{
    if (qFuzzyCompare(m_iconsWidth, iconsWidth))
        return;

    m_iconsWidth = iconsWidth;
    emit updateHeaders();
    emit iconsWidthChanged();
}

double TableStyle::selectionOpacity() const
{
    return m_selectionOpacity;
}

QColor TableStyle::selectionColor() const
{
    return m_selectionColor;
}

void TableStyle::setIconsColor(QColor iconsColor)
{
    if (m_iconsColor == iconsColor)
        return;

    m_iconsColor = iconsColor;
    emit updateHeaders();
    emit iconsColorChanged();
}

void TableStyle::addImage(QString name, QString path, QColor color, double width)
{
    QSvgRenderer renderer(path);
    double iconWidth = width == 0 ? iconsWidth() : width;
    QImage image(iconWidth, iconWidth, QImage::Format_ARGB32);
    image.fill("transparent");  // partly transparent red-ish background

    // Get QPainter that paints to the image
    QPainter painter(&image);
    renderer.render(&painter);

    auto ccolor = color == "transparent" ? QColor(m_iconsColor) : color;
    for(int y = 0; y < image.height(); y++)
    {
        for(int x= 0; x < image.width(); x++)
        {
            // Read the alpha value each pixel, keeping the RGB values of your color
            ccolor.setAlpha(image.pixelColor(x,y).alpha());

            // Apply the pixel color
            image.setPixelColor(x,y,ccolor);
        }
    }

    m_iconsMap[name] = image;
}

bool TableStyle::combineColorProviders() const
{
    return m_combineColorProviders;
}

void TableStyle::setCombineColorProviders(bool newCombineColorProviders)
{
    if (m_combineColorProviders == newCombineColorProviders)
        return;
    m_combineColorProviders = newCombineColorProviders;
    emit combineColorProvidersChanged();
}

QColor TableStyle::gridColor() const
{
    return m_gridColor;
}
