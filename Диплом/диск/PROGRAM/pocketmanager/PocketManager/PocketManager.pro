QT += quick
QT += core
QT += gui
QT += concurrent
QT += multimedia
QT += network
QT += qml
QT += widgets

CONFIG += c++17
DEFINES += QT_DEBUG_PLUGINS
SOURCES += \
        appcore.cpp \
        clientservice.cpp \
        codeitem.cpp \
        main.cpp \
        pageprovider.cpp \
        scanner.cpp

RESOURCES += resources/resources.qrc

QML_IMPORT_NAME = App
QML_IMPORT_MAJOR_VERSION = 1
uri = App
QML_IMPORT_PATH = $$PWD/../plugins

HEADERS += \
    appcore.h \
    clientservice.h \
    codeitem.h \
    pageprovider.h \
    scanner.h

include($$PWD/../config.pri)

android :{
QT += androidextras
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../android-sources
LIBS += -L$$OUT_PWD/../libs/QZXing/ -lQZXing_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/TCPWorkers/ -lTCPWorkers_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/QZXing/ -lQZXing_armeabi-v7a
}else:CONFIG(debug, debug|release){
win32: LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
else:unix: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

win32: LIBS += -L$$OUT_PWD/../libs/TCPWorkers/debug/ -lTCPWorkers
else:unix: LIBS += -L$$OUT_PWD/../libs/TCPWorkers/ -lTCPWorkers

win32: LIBS += -L$$OUT_PWD/../libs/tablerenderbase/debug/ -lTableRenderBase
else:unix: LIBS += -L$$OUT_PWD/../libs/tablerenderbase/ -lTableRenderBase

win32: LIBS += -L$$OUT_PWD/../libs/qdxf/debug/ -lqdxf
else:unix: LIBS += -L$$OUT_PWD/../libs/qdxf/ -lqdxf

win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libs/QZXing/debug/ -lQZXing
else:unix: LIBS += -L$$OUT_PWD/../libs/QZXing/ -lQZXing
}else{
android :{
}else{
LIBS += -L$$PWD/../bin/libs -lklibcorelite
LIBS += -L$$PWD/../bin/libs -lTableRenderBase
LIBS += -L$$PWD/../bin/libs -lTCPWorkers
LIBS += -L$$PWD/../bin/libs -lqdxf
LIBS += -L$$PWD/../bin/libs -lQZXing
}
}

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite

INCLUDEPATH += $$PWD/../libs/QZXing
DEPENDPATH += $$PWD/../libs/QZXing

INCLUDEPATH += $$PWD/../libs/TCPWorkers
DEPENDPATH += $$PWD/../libs/TCPWorkers

INCLUDEPATH += $$PWD/../libs/QZXing
DEPENDPATH += $$PWD/../libs/QZXing

DISTFILES += \
../android-sources/AndroidManifest.xml
