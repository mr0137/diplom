#ifndef CLIENTSERVICE_H
#define CLIENTSERVICE_H

#include <QImage>
#include <QObject>
#include <QTimer>
#include <tcpclientworker.h>

//клиентская часть
//логика работы сетевой части находится тут
class ClientService : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ip READ ip WRITE setIp NOTIFY ipChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QString serverIP READ serverIP WRITE setServerIP NOTIFY serverIPChanged)
    //показатель, есть ли коннект с сервером
    Q_PROPERTY(bool isConnected READ isConnected WRITE setIsConnected NOTIFY isConnectedChanged)
    //уникальный идентификатор устройства
    Q_PROPERTY(QString localuid READ localuid WRITE setLocaluid NOTIFY localuidChanged)
    //было ли получено разрешение
    Q_PROPERTY(bool isPermitted READ isPermitted WRITE setIsPermitted NOTIFY isPermittedChanged)
    //имя девайса
    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
public:
    explicit ClientService(QObject *parent = nullptr);
    ~ClientService();

    const QString &ip() const;
    void setIp(const QString &newIp);

    int port() const;
    void setPort(int newPort);

    const QString &serverIP() const;
    void setServerIP(const QString &newServerIP);

    bool isConnected() const;
    void setIsConnected(bool newIsConnected);

    const QString &localuid() const;
    void setLocaluid(const QString &newLocaluid);

    bool isPermitted() const;
    void setIsPermitted(bool newIsPermitted);

    const QString &deviceName() const;
    void setDeviceName(const QString &newDeviceName);

    void sendScannedCode(QString code);
    void sendScannedImage(QImage img);

public slots:
    bool connect();
    void updateData(QVariantMap data);

signals:
    void ipChanged();
    void portChanged();
    void serverIPChanged();
    void isConnectedChanged();
    void localuidChanged();
    void isPermittedChanged();
    void deviceNameChanged();
    void macChanged();
    void receivedScanData(QVariantMap map);

private:
    TcpClientWorker *m_worker;
    QString m_serverIP = QString(SERVER_IP);
    int m_port = 45454;
    QString m_ip;
    QString m_uid;
    QString m_localuid;
    QString m_deviceName;
    bool m_isConnected = false;
    bool m_isPermitted = false;

    void genUID();
    QTimer *m_reconnectTimer;
};

#endif // ClientService_H
