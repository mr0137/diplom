#include "clientservice.h"
#include <Protocol.h>
#ifdef ANDROID
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#endif
#include <QDataStream>
#include <QFile>
#include <QHostInfo>
#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>
#include <QNetworkInterface>
#include <QNetworkSession>
#include <QUuid>

ClientService::ClientService(QObject *parent) : QObject(parent)
{
    const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
    for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost)
            setIp(address.toString());
    }

    genUID();

    qDebug() << ip() << localuid();

    m_worker = new TcpClientWorker(serverIP(), port(), false, this);

    QObject::connect(m_worker, &TcpClientWorker::disconnected, this, [this](){
        setIsConnected(false);
    });

    m_reconnectTimer = new QTimer(this);

    QObject::connect(m_reconnectTimer, &QTimer::timeout, this, [this](){
        if(m_worker->connectToServer()){
            m_reconnectTimer->stop();
        }
    });

    //проверка, если есть дисконнект, чтобы в дальнейшем автоматичесски подключится
    QObject::connect(m_worker->socket(), &QTcpSocket::disconnected, this, [this] () {
        m_reconnectTimer->start(500);
    });

    //заглушка под лямбду, Саша придумал
    using namespace std::placeholders;
    m_worker->setOnConnectionPerformed(std::bind(&ClientService::connect, this));

    m_worker->setReceiver([this](const Frame &frame){
        QDataStream ss(frame.data);
        quint8 typeOfData;
        ss >> typeOfData;
        switch(typeOfData) {

        case CONNECTION_PERMISSION: {
            bool access;
            ss >> access;
            setIsPermitted(access);
            setIsConnected(true);
            Frame fr;
            QDataStream s(&fr.data, QIODevice::WriteOnly);
            fr.type = SendType::P2P;
            fr.socketDescriptor = frame.socketDescriptor;
            s << DEVICE_NAME << deviceName();
            m_worker->writeData(fr);
            break;
        }
        case SCANNED_CODE:{
            QVariantMap map;
            ss >> map;
            if (!map.isEmpty()){
                //qDebug() << map;
                emit receivedScanData(map);
            }
            break;
        }
        }
    });

    //заглушка
    m_worker->setSender([](const Frame &){
    });

    m_reconnectTimer->start(500);
}

ClientService::~ClientService()
{

}

const QString &ClientService::ip() const
{
    return m_ip;
}

void ClientService::setIp(const QString &newIp)
{
    if (m_ip == newIp)
        return;
    m_ip = newIp;
    emit ipChanged();
}

int ClientService::port() const
{
    return m_port;
}

void ClientService::setPort(int newPort)
{
    if (m_port == newPort)
        return;
    m_port = newPort;
    emit portChanged();
}

bool ClientService::connect()
{
    bool isError = false;
    if(!m_worker->socket()->isOpen()) {
        isError = m_worker->connectToServer();
    }
    Frame frame;
    frame.socketDescriptor = m_worker->socket()->socketDescriptor();
    frame.type = SendType::P2P;

    QDataStream s(&frame.data, QIODevice::WriteOnly);
    s << CONNECTION_TYPE << localuid();
    m_worker->writeData(frame);
    return isError;
}

void ClientService::updateData(QVariantMap data)
{
    Frame frame;
    frame.socketDescriptor = m_worker->socket()->socketDescriptor();
    frame.type = SendType::P2P;
    QDataStream s(&frame.data, QIODevice::WriteOnly);
    s << UPDATE_COUNT << data;
    m_worker->writeData(frame);
}

const QString &ClientService::serverIP() const
{
    return m_serverIP;
}

void ClientService::setServerIP(const QString &newServerIP)
{
    if (m_serverIP == newServerIP)
        return;
    m_serverIP = newServerIP;
    emit serverIPChanged();
}

bool ClientService::isConnected() const
{
    return m_isConnected;
}

void ClientService::setIsConnected(bool newIsConnected)
{
    if (m_isConnected == newIsConnected)
        return;
    m_isConnected = newIsConnected;
    emit isConnectedChanged();
}

void ClientService::genUID()
{
    //генерация уникального айди в зависимости от платформы
#ifdef ANDROID
    // Trying to get ANDROID_ID from system
    //получение уникального айди через систему
    QAndroidJniObject myID = QAndroidJniObject::fromString("android_id");
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject appctx = activity.callObjectMethod("getApplicationContext","()Landroid/content/Context;");
    QAndroidJniObject contentR = appctx.callObjectMethod("getContentResolver", "()Landroid/content/ContentResolver;");

    QAndroidJniObject androidId = QAndroidJniObject::callStaticObjectMethod("android/provider/Settings$Secure","getString",
                                                                            "(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;",
                                                                            contentR.object<jobject>(),
                                                                            myID.object<jstring>());

    QAndroidJniObject device_name = QAndroidJniObject::fromString("device_name");
    QAndroidJniObject ws = QAndroidJniObject::fromString("wifi");

    //получение названия устройства через систему
    QAndroidJniObject androidName = QAndroidJniObject::callStaticObjectMethod("android/provider/Settings$Global","getString",
                                                                              "(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;",
                                                                              contentR.object<jobject>(),
                                                                              device_name.object<jstring>());
    setDeviceName(androidName.toString());

    if(androidId.isValid() && !androidId.toString().isEmpty()){
        if(androidId.toString().endsWith("9774d56d682e549c")){
            qWarning()<< Q_FUNC_INFO <<"This device has the bug with unique id"; //https://code.google.com/p/android/issues/detail?id=10603
        }else{
            qDebug() << "Using androidId" << androidId.toString();
            setLocaluid(androidId.toString());
            return;
        }
    }
    qDebug() << "Using randomUuid";
    setLocaluid(QUuid::createUuid().toString());
    return;
#endif
    setLocaluid(QSysInfo::machineUniqueId());
}

const QString &ClientService::localuid() const
{
    return m_localuid;
}

void ClientService::setLocaluid(const QString &newLocaluid)
{
    if (m_localuid == newLocaluid)
        return;
    m_localuid = newLocaluid;
    emit localuidChanged();
}

bool ClientService::isPermitted() const
{
    return m_isPermitted;
}

void ClientService::setIsPermitted(bool newIsPermitted)
{
    if (m_isPermitted == newIsPermitted)
        return;
    m_isPermitted = newIsPermitted;
    emit isPermittedChanged();
}

const QString &ClientService::deviceName() const
{
    return m_deviceName;
}

void ClientService::setDeviceName(const QString &newDeviceName)
{
    if (m_deviceName == newDeviceName)
        return;
    m_deviceName = newDeviceName;
    emit deviceNameChanged();
}

void ClientService::sendScannedCode(QString code)
{
    //отправляем код, если есть доступ
    if (isPermitted()){
        qDebug() << "send scanned code";
        Frame frame;
        frame.socketDescriptor = m_worker->socket()->socketDescriptor();
        frame.type = SendType::P2P;

        QDataStream s(&frame.data, QIODevice::WriteOnly);
        s << SCANNED_CODE << code;
        m_worker->writeData(frame);
    }
}

void ClientService::sendScannedImage(QImage img)
{
     if (isPermitted()){
         qDebug() << "send scanned image";
         QVariantMap m;
         Frame frame;
         frame.socketDescriptor = m_worker->socket()->socketDescriptor();
         frame.type = SendType::P2P;
         m["img"] = img;
         QDataStream s(&frame.data, QIODevice::WriteOnly);
         s << DEBUG_IMAGE << m;
         m_worker->writeData(frame);
     }
}
