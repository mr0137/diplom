#include "appcore.h"

#include <QCoreApplication>
#include <QDataStream>
#include <QDebug>
#include <QDir>
#include <QDir>
#include <QPluginLoader>
#include <QSaveFile>
#include <QString>
//#include <databaseservice.h>
#include <QtConcurrent>
#include <QScreen>
#include <QGuiApplication>
#include <scanner.h>

//#include "produceorderform.h"

AppCore::AppCore(QObject * parent):QObject(parent)
{
    QScreen *screen = QGuiApplication::screens().at(0);
    qDebug() << screen->availableGeometry() << screen->devicePixelRatio() << screen->isPortrait(Qt::ScreenOrientation::PortraitOrientation) << screen->logicalDotsPerInch();
    m_scanner = new Scanner(this);
    QImage img("C:/Users/mr013/Desktop/test2.png");
    m_scanner->decode(img);
    //m_timer = new QTimer(this);
    m_pageProvider = new PageProvider(this);
    m_client = new ClientService(this);

    m_palette = {
        { "button", QVariantMap{
              {"backgroundColor", "#30384f"},
              {"textColor", "#6ab3f3"}
          }
        },
        { "header", QVariantMap{
              {"backgroundColor", "#4B99BB"},
              {"textColor", "#295062"}
          }
        },
        { "text", QVariantMap{
              {"textColor", "#2D667E"}
          }
        },
        { "app", QVariantMap{
              { "mainColor", "#6699CC" },
              { "firstBackgroundColor", "#DFF5FF" },
              { "secondBackgroundColor", "#C5D8E1" },
              { "firstTextColor", "black"},
              { "secondTextColor", "blue"},
              { "errorColor", "#ec3942"}
          }}
    };

    //QVariantMap map{
    //    {"Count", "2"},
    //    {"Name", "nco-24.02.611.04 (Разметить под сверловку 7 отв.)"},
    //    {"Thickness", ""},
    //    {"dimension", ""},
    //    {"id", "411"},
    //    {"material", ""},
    //    {"note", ""},
    //    {"order_name", "08008+08019_10 st3"}
    //};
    //m_pageProvider->label()->deserialize(map);


    if (debugMode()) initDebug();

    connect(m_client, &ClientService::isPermittedChanged, this, [this](){
        if (!m_client->isPermitted()) m_pageProvider->initialState();
    });

    connect(m_client, &ClientService::isConnectedChanged, this, [this](){
        if (!m_client->isConnected()){
            m_client->setIsPermitted(false);
        }
    });

    connect(m_scanner, &Scanner::decodeImage, this, [this](QImage img){
        //m_client->sendScannedImage(img);
    });

    connect(m_scanner, &Scanner::scanned, this, [this](QString code){
        m_client->sendScannedCode(code);
        //m_client->sendScannedImage(m_scanner->img());
        m_pageProvider->setCurrentState("view");
    });

    connect(m_client, &ClientService::receivedScanData, this, [this](QVariantMap map){
        m_pageProvider->label()->setReady(false);
        m_pageProvider->label()->deserialize(map);
        m_pageProvider->label()->setReady(true);
    });

    //m_timer->start(1000);

    //QTimer::singleShot(1000, this, [this](){
    //    emit m_scanner->scanned("760");
    //});
}

AppCore *AppCore::instance()
{
    static AppCore * m_AppCore;
    if(m_AppCore == nullptr){
        m_AppCore = new AppCore(qApp);
    }
    return m_AppCore;
}

QObject *AppCore::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(scriptEngine)
    Q_UNUSED(engine)
    return AppCore::instance();
}

void AppCore::setRootPath(QString path)
{
#ifdef QT_NO_DEBUG
    int times = 1;
#else
#ifdef Q_OS_LINUX
    int times = 5;
#endif
#ifdef Q_OS_WINDOWS
    int times = 6;
#endif
#endif
    while(times > 0 && !path.isEmpty()){
#ifdef Q_OS_LINUX
        if (path.endsWith("/")) times--;
#endif
#ifdef Q_OS_WINDOWS
        if (path.endsWith("\\")) times--;
#endif
        path.chop(1);
    }
    setRootDir(path);
}

void AppCore::setDebugMode(bool newDebugMode)
{
    if (m_debugMode == newDebugMode)
        return;
    m_debugMode = newDebugMode;
    emit debugModeChanged();
}

bool AppCore::debugMode() const
{
    return m_debugMode;
}

QVariantMap AppCore::palette() const
{
    return m_palette;
}

QString AppCore::trFix() const
{
    return m_trFix;
}

void AppCore::setPalette(const QVariantMap &newPalette)
{
    if (m_palette == newPalette)
        return;
    m_palette = newPalette;
    emit paletteChanged();
}

QString AppCore::rootDir() const
{
    return m_rootDir;
}

void AppCore::setRootDir(const QString &newRootDir)
{
    if (m_rootDir == newRootDir)
        return;
    m_rootDir = newRootDir;
    emit rootDirChanged();
}

void AppCore::setTrFix(const QString &newTrFix)
{
    if (m_trFix == newTrFix)
        return;
    m_trFix = newTrFix;
    emit trFixChanged();
}

void AppCore::updateTranslation()
{
    setTrFix(" ");
    setTrFix("");
}

void AppCore::updateLabel()
{
    m_client->updateData(m_pageProvider->label()->serialize());
    m_pageProvider->label()->setCountUpdated(false);
}

void AppCore::initDebug()
{
    m_debugStat.append(QVariantMap{
                           {"name", "device"},
                           {"value", m_client->deviceName()}
                       });
    m_debugStat.append(QVariantMap{
                           {"name", "uid"},
                           {"value", m_client->localuid()}
                       });
    m_debugStat.append(QVariantMap{
                           {"name", "ip"},
                           {"value", m_client->ip()}
                       });
    m_debugStat.append(QVariantMap{
                           {"name", "server ip"},
                           {"value", m_client->serverIP()}
                       });

    m_debugStat.append(QVariantMap{
                           {"name", "isConnected"},
                           {"value", m_client->isConnected()}
                       });

    m_debugStat.append(QVariantMap{
                           {"name", "isPermitted"},
                           {"value", m_client->isPermitted()}
                       });

    m_debugStat.append(QVariantMap{
                           {"name", "scanned_text"},
                           {"value", m_scanner->lastReadedCode()}
                       });

    m_debugStat.append(QVariantMap{
                           {"name", "reading_time"},
                           {"value", m_scanner->readingTime()}
                       });

    connect(m_client, &ClientService::deviceNameChanged, this, [this](){
        m_debugStat[0] = QVariantMap{
        {"name", "device"},
        {"value", m_client->deviceName()}
    };
        emit debugStatChanged();
    });

    connect(m_client, &ClientService::localuidChanged, this, [this](){
        m_debugStat[1] = QVariantMap{
        {"name", "uid"},
        {"value", m_client->localuid()}
    };
        emit debugStatChanged();
    });

    connect(m_client, &ClientService::ipChanged, this, [this](){
        m_debugStat[2] = QVariantMap{
        {"name", "ip"},
        {"value", m_client->ip()}
    };
        emit debugStatChanged();
    });

    connect(m_client, &ClientService::serverIPChanged, this, [this](){
        m_debugStat[3] = QVariantMap{
        {"name", "server ip"},
        {"value", m_client->serverIP()}
    };
        emit debugStatChanged();
    });

    connect(m_client, &ClientService::isConnectedChanged, this, [this](){
        m_debugStat[4] = QVariantMap{
        {"name", "isConnected"},
        {"value", m_client->isConnected()}
    };
        emit debugStatChanged();
    });

    connect(m_client, &ClientService::isPermittedChanged, this, [this](){
        m_debugStat[5] = QVariantMap{
        {"name", "isPermitted"},
        {"value", QVariant(m_client->isPermitted())}
    };
        emit debugStatChanged();
    });

    connect(m_scanner, &Scanner::lastReadedCodeChanged, this, [this](){
        m_debugStat[6] = QVariantMap{
        {"name", "scanned_text"},
        {"value", QVariant(m_scanner->lastReadedCode())}
    };
        emit debugStatChanged();
    });

    connect(m_scanner, &Scanner::readingTimeChanged, this, [this](){
        m_debugStat[7] = QVariantMap{
        {"name", "reading_time"},
        {"value", QVariant(QString::number(m_scanner->readingTime()))}
    };
        emit debugStatChanged();
    });
}

qreal AppCore::mult() const
{
    return m_mult;
}

void AppCore::setMult(qreal newMult)
{
    if (qFuzzyCompare(m_mult, newMult))
        return;
    m_mult = newMult;
    emit multChanged();
}

PageProvider *AppCore::pageProvider() const
{
    return m_pageProvider;
}

ClientService *AppCore::client() const
{
    return m_client;
}

Scanner *AppCore::scanner() const
{
    return m_scanner;
}

void AppCore::setScanner(Scanner *newScanner)
{
    if (m_scanner == newScanner)
        return;
    m_scanner = newScanner;
    emit scannerChanged();
}

QVariantList AppCore::debugStat() const
{
    return m_debugStat;
}

void AppCore::setDebugStat(const QVariantList &newDebugStat)
{
    if (m_debugStat == newDebugStat)
        return;
    m_debugStat = newDebugStat;
    emit debugStatChanged();
}
