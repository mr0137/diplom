#ifndef PAGEPROVIDER_H
#define PAGEPROVIDER_H

#include <QObject>
#include <qqml.h>
#include <kmacro.h>
#include <QVariantMap>
#include <QImage>

class QQmlComponent;
// бекенд ярлыка
class Label : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    K_AUTO_PROPERTY(bool, ready, ready, setReady, readyChanged, false)
    K_READONLY_PROPERTY(int, id, id, setId, idChanged, 0)
    K_AUTO_PROPERTY(int, count, count, setCount, countChanged, 0)
    K_READONLY_PROPERTY(QString, name1, name1, setName1, name1Changed, "")
    K_READONLY_PROPERTY(QString, name2, name2, setName2, name2Changed, "")
    K_READONLY_PROPERTY(double, thickness, thickness, setThickness, thicknessChanged, 0.0)
    K_READONLY_PROPERTY(QString, dimension, dimension, setDimension, dimensionChanged, "")
    K_READONLY_PROPERTY(QString, material, material, setmaterial, materialChanged, "")
    K_READONLY_PROPERTY(QString, orderName, orderName, setOrderName, orderNameChanged, "")
    K_READONLY_PROPERTY(QString, note, note, setNote, noteChanged, "")
    K_READONLY_PROPERTY(QImage, minImage, minImage, setMinImage, minImageChanged, QImage())
    K_READONLY_PROPERTY(QImage, maxImage, maxImage, setMaxImage, maxImageChanged, QImage())
    K_READONLY_PROPERTY(QString, part_path, part_path, setPart_path, part_pathChanged, "")
    K_AUTO_PROPERTY(int, failedCount, failedCount, setFailesCount, failedCountChanged, 0)
    K_READONLY_PROPERTY(QString, result, result, setResult, resultChanged, "")
    K_AUTO_PROPERTY(bool, countUpdated, coutnUpdated, setCountUpdated, countupdatedChanged, false)
public:
    Label(QObject *parent = nullptr);
    void deserialize(QVariantMap map);
    QVariantMap serialize();
private:
    bool m_des = false;
};

//сервис, который имеет в себе стейт-машину
//используется для контроля текущей страницы
//дада, овериндженеринг, но с ним дебажить очень просто
//для выставления стартовой страницы хватит всего изменения:
//45| initialItem: AppCore.pageProvider.getComponent("initial") в файле main.qml на любое другое значение
class PageProvider : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    K_AUTO_PROPERTY(QString, currentState, currentState, setCurrentState, currentStateChanged, "initial")
    K_AUTO_PROPERTY(QVariantMap, components, components, setComponents, componentsChanged, QVariantMap())
    K_CONST_PROPERTY(Label*, label, new Label(this))
public:
    explicit PageProvider(QObject *parent = nullptr);
    void setEngine(QQmlEngine *engine);
public slots:
    QQmlComponent * getComponent(QString componentName);
    void scanState();
    void initialState();

private:
    QQmlEngine *m_engine = nullptr;
};

#endif // PAGEPROVIDER_H
