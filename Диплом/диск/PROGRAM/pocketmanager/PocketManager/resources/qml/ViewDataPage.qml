import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.0
import CQML 1.0 as C
import App 1.0

C.BaseBackground{
    id: viewItem
    backgroundColor: AppCore.palette.app.mainColor

    LabelItem{
        id: labelitem
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: parent.height * 0.2
        anchors.top: parent.top
        width: Math.min(height * 1.8, parent.width * 0.9)
        height: 7 * parent.height * 0.04
        //z: 1
    }

    Text{
        id: countText
        anchors.top: labelitem.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        text: "К-сть виготовлених"
        color: AppCore.palette.header.textColor
        font.pixelSize: parent.height * 0.04
    }

    Counter{
        id: counter
        anchors.top: countText.bottom
        anchors.topMargin: 10
        width: parent.width * 0.75
        height: parent.height * 0.07
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10
        prop: AppCore.pageProvider.label.count
    }

    Text{
        id: countErrText
        anchors.top: counter.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        text: "К-сть забракованих"
        color: AppCore.palette.header.textColor
        font.pixelSize: parent.height * 0.04

        //layer.enabled: true
        //layer.effect: DropShadow {
        //    verticalOffset: 3
        //    horizontalOffset: 3
        //    color: "#80000000"
        //    radius: 3
        //    samples: 5
        //}
    }

    CounterFailed{
        id: counterFailed
        anchors.top: countErrText.bottom
        anchors.topMargin: 10
        width: parent.width * 0.75
        height: parent.height * 0.07
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10
    }

    C.ImageButton{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.035
        anchors.bottomMargin: parent.height * 0.02
        width: height
        height: AppCore.mult * parent.height * 0.07
        elevation: 3
        buttonRadius: height / 2
        iconCode: "qrc:/manager/icons/back.png"//"\uF32E"
        //description: "BACK"
        textColor: AppCore.palette.button.textColor
        buttonBackground: AppCore.palette.button.backgroundColor
        font.family: C.IconHelper.iconsPressed.name
        onReleased: {
            AppCore.pageProvider.initialState()
        }
    }

    C.Button{
        anchors.right: parent.right
        text: "Зберегти"
        anchors.rightMargin: AppCore.pageProvider.label.countUpdated ? parent.width * 0.035 : -width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.02
        height: AppCore.mult * parent.height * 0.07
        width: height * 2.7
        elevation: 3
        buttonRadius: height / 2
        textColor: AppCore.palette.button.textColor
        buttonBackground: AppCore.palette.button.backgroundColor
        font.family: C.IconHelper.iconsPressed.name
        Behavior on anchors.rightMargin { NumberAnimation { duration: 200} }
        onReleased: {
            AppCore.updateLabel()
            console.log(height)
        }
    }

    Rectangle{
        anchors.fill: parent
        opacity: maxImage.children.length > 0

        Behavior on opacity { NumberAnimation { duration: 200 } }

        Rectangle{
            id: maxImage
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            height: width / 880 * 684

            onChildrenChanged: {
                console.log(children.length)
            }
        }
    }

    Rectangle{
        color: "transparent"
        visible: !AppCore.pageProvider.label.ready

        //! [background]
        ShaderEffectSource {
            id: effectSource

            sourceItem: viewItem
            anchors.fill: parent

            sourceRect: Qt.rect(0, 0, width, height)

            Component.onCompleted: { if (!sourceItem) console.log("Warning area: Missed source element")}
        }

        FastBlur{
            id: blur
            anchors.fill: effectSource

            source: effectSource
            radius: 64
            transparentBorder: false
        }
        //! [background]
    }

}
