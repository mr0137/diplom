import QtQuick 2.15
import QtQuick.Shapes 1.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls 1.4 as Old
import QtGraphicalEffects 1.0
import App 1.0
import CQML 1.0 as C

Popup{
    id: debugPopup
    closePolicy: "NoAutoClose"

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 200 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 200 }
    }

    background: Rectangle{
        color: "transparent"

        //! [background]
        ShaderEffectSource {
            id: effectSource

            sourceItem: window.contentItem
            anchors.fill: parent

            sourceRect: Qt.rect(0, 0, width, height)

            Component.onCompleted: { if (!sourceItem) console.log("Warning area: Missed source element")}
        }

        FastBlur{
            id: blur
            anchors.fill: effectSource

            source: effectSource
            radius: 64
            transparentBorder: false
        }
        //! [background]

        ColumnLayout{
            anchors.fill: parent

            Repeater{
                model: AppCore.debugStat
                delegate: Text{
                    font.pixelSize: parent.height * 0.02
                    text: modelData.name + " === " + modelData.value
                }
            }

            Item{
                Layout.fillHeight: true
            }
        }


        C.Button{
            anchors.right: parent.right
            anchors.top: parent.top
            width: parent.height * 0.1
            height: parent.height * 0.05
            buttonRadius: 5
            text: "close"
            buttonBackground: AppCore.palette.button.backgroundColor
            onPressed: {
                debugPopup.close()
            }
        }
    }
}
