import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import CQML 1.0 as C
import App 1.0
import QtMultimedia 5.15
import QtQuick.Controls 1.4 as Old
import QtQuick.Controls.Private 1.0
import Qt.labs.settings 1.0

C.BaseBackground{
    id: mainWnd
    backgroundColor: AppCore.palette.app.mainColor

    Timer{
        id: timer
        interval:1000
        triggeredOnStart: false
        onTriggered: {
            AppCore.scanner.scan(viewfinder)
        }
        running: true
        repeat: true
    }

    Connections{
        target: AppCore.scanner
        function onScanned(idScanned) {
            timer.stop()
            console.log("done qrdecode : good", idScanned)
        }

        function onError(message) {
            //qrCodeFound.text = message;
            //qrCodeFound.color = "red";
        }
    }

    Camera{
        //our Camera to play with
        id:camera
        captureMode: Camera.CaptureStillImage
        focus {
            focusMode: Camera.FocusContinuous
            focusPointMode: Camera.FocusPointCenter
        }

        exposure {
            exposureMode: Camera.ExposureAuto
        }
    }

    VideoOutput{
        id:viewfinder
        anchors.fill: parent
        //our viewfinder to show what the Camera sees
        source: camera
        fillMode: VideoOutput.PreserveAspectFit
        autoOrientation: true
    }

    //Text{
    //    id:qrCodeFound
    //    anchors.centerIn: parent
    //    width:200; height:50
    //    text:"Not found"
    //    color:"white"
    //}
    C.ImageButton{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.035
        anchors.bottomMargin: parent.height * 0.02
        width: height
        height: AppCore.mult * parent.height * 0.07
        elevation: 3
        buttonRadius: height / 2
        iconCode: "qrc:/manager/icons/back.png"//"\uF32E"
        //description: "BACK"
        textColor: "#6ab3f3"
        buttonBackground: "#30384f"
        font.family: C.IconHelper.iconsPressed.name
        onReleased: {
            AppCore.pageProvider.initialState()
        }
    }
}
