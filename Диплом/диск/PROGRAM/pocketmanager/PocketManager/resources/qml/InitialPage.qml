import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import CQML 1.0 as C
import App 1.0

C.BaseBackground{
    id: root
    backgroundColor: AppCore.palette.app.mainColor

    C.ImageButton{
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: parent.height * 0.12
        width: height
        height: AppCore.mult * parent.height * 0.1
        elevation: 3
        enabled: AppCore.client.isPermitted
        buttonRadius: height / 2
        iconCode: "qrc:/manager/icons/barcode.png"//"\uF02A"
        description: "SCAN"
        textColor: "#6ab3f3"
        buttonBackground: "#30384f"
        font.family: C.IconHelper.iconsPressed.name
        onReleased: {
            AppCore.pageProvider.scanState()
        }
    }
}

