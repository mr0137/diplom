#ifndef SCENE_ITEMS_H
#define SCENE_ITEMS_H

#include <QGraphicsItem>
#include <backend/libdxfrw.h>
#include <QDebug>
#include <QPainter>
#include <QGraphicsEllipseItem>


class SceneArc : public QAbstractGraphicsShapeItem
{
public:
    SceneArc(DRW_Arc a)
    {
        arc = a;
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /* widget */)
    {
        painter->setPen(pen());
        if(arc.isccw == 0)
            qDebug() << "not ccw";

        //qDebug() << "painting arc";
        int startAngle = -arc.staangle*(180*16/M_PI);
        int spanAngle;

        if(arc.endangle > arc.staangle)
            spanAngle = (arc.endangle-arc.staangle)*(180*16/M_PI);
        else
            spanAngle = (2*M_PI-arc.staangle+arc.endangle)*(180*16/M_PI);

        if(spanAngle > 0)
            spanAngle *= -1;

        /*qDebug() << "startangle:" << startAngle/16;
        qDebug() << "spanangle:" << spanAngle/16;

        qDebug() << "dxf: startangle" << arc.staangle*(180/M_PI);
        qDebug() << "dxf: endangle" << arc.endangle*(180/M_PI);*/

        painter->drawArc( QRectF(arc.basePoint.x-arc.radious, arc.basePoint.y-arc.radious, arc.radious*2, arc.radious*2), startAngle, spanAngle);
    }

    QRectF boundingRect() const
    {
        return QRectF(arc.basePoint.x-arc.radious, arc.basePoint.y-arc.radious, arc.radious*2, arc.radious*2);
    }

private:
    DRW_Arc arc;
};

#endif // SCENE_ITEMS_H
