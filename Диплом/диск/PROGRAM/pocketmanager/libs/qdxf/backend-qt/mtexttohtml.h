#ifndef MTEXTTOHTML_H
#define MTEXTTOHTML_H
#include <QString>

class MTextToHTML
{
public:
    static QString convert(QString mtext);
};

#endif // MTEXTTOHTML_H
