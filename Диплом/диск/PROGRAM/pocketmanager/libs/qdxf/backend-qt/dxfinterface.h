#ifndef DXFINTERFACE_H
#define DXFINTERFACE_H
#include <QList>
#include <QLineF>
#include <QPen>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <backend/drw_interface.h>
#include <backend-qt/scene_items.h>
#include <backend-qt/dxfsceneview.h>

class DXFInterface : public DRW_Interface
{
public:
    DXFInterface(QString filename, double width = 0.98);
    ~DXFInterface();

    void setLineWidth(double width);

    /** Called when header is parsed.  */
    void addHeader(const DRW_Header *);

    /** Called for every line Type.  */
    void addLType(const DRW_LType &);
    /** Called for every layer. */
    void addLayer(const DRW_Layer& data);
    /** Called for every dim style. */
    void addDimStyle(const DRW_Dimstyle &);
    /** Called for every VPORT table. */
    void addVport(const DRW_Vport &);
    /** Called for every text style. */
    void addTextStyle(const DRW_Textstyle &);

    /**
     * Called for every block. Note: all entities added after this
     * command go into this block until endBlock() is called.
    *
     * @see endBlock()
     */
    void addBlock(const DRW_Block &);

    /**
     * In DWG called when the following entities corresponding to a
     * block different from the current. Note: all entities added after this
     * command go into this block until setBlock() is called already.
     *
     * int handle are the value of DRW_Block::handleBlock added with addBlock()
     */

    void setBlock(const int);

    /** Called to end the current block */
    void endBlock();

    /** Called for every point */
    void addPoint(const DRW_Point& data);

    /** Called for every line */
    void addLine(const DRW_Line& data);

    /** Called for every ray */
    void addRay(const DRW_Ray &);

    /** Called for every xline */
    void addXline(const DRW_Xline &);

    /** Called for every arc */
    void addArc(const DRW_Arc& data);

    /** Called for every circle */
    void addCircle(const DRW_Circle& data);

    /** Called for every ellipse */
    void addEllipse(const DRW_Ellipse& data);

    /** Called for every lwpolyline */
    void addLWPolyline(const DRW_LWPolyline& data);

    /** Called for every polyline start */
    void addPolyline(const DRW_Polyline& data);

    /** Called for every spline */
    void addSpline(const DRW_Spline* data);

    /** Called for every spline knot value */
    void addKnot(const DRW_Entity &);

    /** Called for every insert. */
    void addInsert(const DRW_Insert &);

    /** Called for every trace start */
    void addTrace(const DRW_Trace &);

    /** Called for every 3dface start */
    void add3dFace(const DRW_3Dface &);

    /** Called for every solid start */
    void addSolid(const DRW_Solid &);


    /** Called for every Multi Text entity. */
    void addMText(const DRW_MText&);

    /** Called for every Text entity. */
    void addText(const DRW_Text&);

    /**
     * Called for every aligned dimension entity.
     */
    void addDimAlign(const DRW_DimAligned *);
    /**
     * Called for every linear or rotated dimension entity.
     */
    void addDimLinear(const DRW_DimLinear *);

    /**
     * Called for every radial dimension entity.
     */
    void addDimRadial(const DRW_DimRadial *);

    /**
     * Called for every diametric dimension entity.
     */
    void addDimDiametric(const DRW_DimDiametric *);

    /**
     * Called for every angular dimension (2 lines version) entity.
     */
    void addDimAngular(const DRW_DimAngular *);

    /**
     * Called for every angular dimension (3 points version) entity.
     */
    void addDimAngular3P(const DRW_DimAngular3p *);

    /**
     * Called for every ordinate dimension entity.
     */
    void addDimOrdinate(const DRW_DimOrdinate *);

    /**
     * Called for every leader start.
     */
    void addLeader(const DRW_Leader *data);

    /**
     * Called for every hatch entity.
     */
    void addHatch(const DRW_Hatch *);

    /**
     * Called for every viewport entity.
     */
    void addViewport(const DRW_Viewport &);

    /**
     * Called for every image entity.
     */
    void addImage(const DRW_Image *);

    /**
     * Called for every image definition.
     */
    void linkImage(const DRW_ImageDef *);

    /**
     * Called for every comment in the DXF file (code 999).
     */
    void addComment(const char *);

    void writeHeader(DRW_Header &);
    void writeBlocks();
    void writeBlockRecords();
    void writeEntities();
    void writeLTypes();
    void writeLayers();
    void writeTextstyles();
    void writeVports();
    void writeDimstyles();

    QPen attributesToPen(const DRW_Entity *e);
    QColor numberToColor(int col);
    void setQPenLinetype(QPen & p, std::string linetype);

    DRW_Layer getLayer(std::string name);

    void drawPolyline(std::vector<DRW_Vertex*> vertlist, QPen pen);

    QGraphicsScene * scene();
    const QImage &getImg() const;

    double lineWidth() const;

private:
    QGraphicsScene mScene;
    DXFSceneView view;
    QList<DRW_Layer> layers;
    QList<SceneArc*> arches;
    QImage img;
    bool isWidth = false;
    double m_lineWidth = 1.;
};

#endif // DXFINTERFACE_H
