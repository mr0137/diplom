#ifndef TCPSERVERWORKER_H
#define TCPSERVERWORKER_H

#include "TCPWorkers_global.h"

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <Protocol.h>

//!
//! \brief The TcpServerWorker class
//!
class TCPWORKERS_EXPORT TcpServerWorker : public QObject
{
    Q_OBJECT
public:
    //!
    //! \brief TcpServerWorker constructor
    //! \param address
    //! \param port
    //! \param parent
    //!
    explicit TcpServerWorker(QString address, int port, QObject *parent = nullptr);

    //!
    //! \brief Get clients list
    //! \return list of available and opened sockets
    //!
    QList<QTcpSocket *> clientsList() const;

    ~TcpServerWorker();

    //!
    //! \brief Get all socket descriptors
    //! \return socket descriptors list
    //!
    QList<int> socketDescriptors() const;

    //!
    //! \brief Writes data to socket
    //!
    void writeData(Frame &);

    //!
    //! \brief Reads data from socket
    //! \param ok
    //! \return Frame object
    //!
    Frame readData(bool *ok);

    //!
    //! \brief Set sender callback
    //! \param sender
    //!
    void setSender(const std::function<void (const Frame &)> &sender);


    //!
    //! \brief Sets receiver callback
    //! \param receiver
    //!
    void setReceiver(const std::function<void (const Frame &)> &receiver);

    //!
    //! \brief Gets socket by given \a index
    //! \param index
    //! \return QTcpSocket pointer
    //!
    QTcpSocket * socket(int index);

    //!
    //! \brief Gets client count
    //! \return client count
    //!
    int clientCount();

    //!
    //! \brief Gets socket by IP address
    //! \param addr
    //! \return QTcpSocket pointer to socket
    //!
    QTcpSocket * getSocketByAddr(const QString & addr);

    //!
    //! \brief Gets server address
    //! \return IP address string
    //!
    QString getAddress() const;

    //!
    //! \brief Sets address
    //! \param address
    //!
    void setAddress(const QString &address);

    int getPort() const;
    //!
    //! \brief Set new port number
    //! \param value
    //!
    void setPort(int value);

    //!
    //! \brief Set mapping between \c socket and \c id
    //! \param id
    //! \param socket
    //!
    void setMapping(const QString & id, QTcpSocket * socket);

    //!
    //! \brief Set mapping between socket descriptor and \c id
    //! \param socketDescriptor
    //! \param id
    //!
    void setMapping(int socketDescriptor, const QString & id);

    //!
    //! \brief Send data by specific id
    //! \param id
    //! \param data
    //!
    void sendDataByID(const QString & id, QByteArray &data);

private slots:
    //!
    //! \brief Proceeds new connection
    //!
    void newConnection();

    //!
    //! \brief Appends new client to clients list
    //! \param socket
    //! \return
    //!
    QString appendToClientsList(QTcpSocket * socket);

    //!
    //! \brief Reads socket
    //!
    void readSocket();

    //!
    //! \brief Discards socket when connection was lost
    //!
    void discardSocket(QTcpSocket *socket = nullptr);

    //!
    //! \brief Send data to client
    //! \param socket
    //! \param data
    //!
    void sendData(QTcpSocket* socket, const QByteArray & data);

    //!
    //! \brief Send data to client
    //! \param data
    //! \param receiverID
    //! \param type
    //!
    void sendData(QByteArray &data, int receiverID, SendType type);

    //!
    //! \brief Function that receives message and handles it
    //! \param block
    //! \param socket
    //!
    void messageHandler(const QByteArray & block, QTcpSocket *socket);

    //!
    //! \brief Stops server
    //!
    void stopServer();


signals:

    //!
    //! \brief This signal is emitted when new message is arrived
    //!
    void newMessage(const QByteArray &, QTcpSocket *);

    //!
    //! \brief Emits this signal when new connection was created
    //! \param ip
    //! \param port
    //!
    void newConnection(QHostAddress ip, int port);

    //!
    //! \brief Emits this singal when connection with client was lost
    //! \param socketDescriptor
    //!
    void connectionRemoved(int socketDescriptor);
private:

    QTcpServer * m_server = nullptr;

    QList<QTcpSocket*> m_clientsList;

    QList<int> m_socketDescriptors;
    QMap<QString, QTcpSocket*> mappingToSocket;
    QMap<QTcpSocket*, QString> mappingToID;

    std::function<void(const Frame&)> m_sender;
    std::function<void(const Frame&)> m_receiver;

    QString m_address = "192.168.81.161";
    int port;
};

#endif // TCPSERVERWORKER_H
