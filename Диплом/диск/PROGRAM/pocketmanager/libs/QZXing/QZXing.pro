TEMPLATE = lib
TARGET = QZXing
QT += quick
QT += core
QT += gui

CONFIG += c++17
CONFIG += shared dll

uri = QZXing

include($$PWD/../destidir.pri)

DEFINES += QZXING_LIBRARY \
           ZXING_ICONV_CONST \
           DISABLE_LIBRARY_FEATURES
           
SOURCES += \
        BarcodeFormat.cpp \
        Binarizer.cpp \
        BinaryBitmap.cpp \
        CameraImageWrapper.cpp \
        ChecksumException.cpp \
        DecodeHints.cpp \
        EncodeHint.cpp \
        Exception.cpp \
        FormatException.cpp \
        InvertedLuminanceSource.cpp \
        LuminanceSource.cpp \
        MultiFormatReader.cpp \
        QZXingImageProvider.cpp \
        Reader.cpp \
        Result.cpp \
        ResultIO.cpp \
        ResultPoint.cpp \
        ResultPointCallback.cpp \
        bigint/BigInteger.cc \
        bigint/BigIntegerAlgorithms.cc \
        bigint/BigIntegerUtils.cc \
        bigint/BigUnsigned.cc \
        bigint/BigUnsignedInABase.cc \
        common/BitArray.cpp \
        common/BitArrayIO.cpp \
        common/BitMatrix.cpp \
        common/BitSource.cpp \
        common/CharacterSetECI.cpp \
        common/DecoderResult.cpp \
        common/DetectorResult.cpp \
        common/GlobalHistogramBinarizer.cpp \
        common/GreyscaleLuminanceSource.cpp \
        common/GreyscaleRotatedLuminanceSource.cpp \
        common/GridSampler.cpp \
        common/HybridBinarizer.cpp \
        common/IllegalArgumentException.cpp \
        common/PerspectiveTransform.cpp \
        common/Str.cpp \
        common/StringUtils.cpp \
        common/detector/MonochromeRectangleDetector.cpp \
        common/detector/WhiteRectangleDetector.cpp \
        common/reedsolomon/GenericGF.cpp \
        common/reedsolomon/GenericGFPoly.cpp \
        common/reedsolomon/ReedSolomonDecoder.cpp \
        common/reedsolomon/ReedSolomonEncoder.cpp \
        common/reedsolomon/ReedSolomonException.cpp \
        imagehandler.cpp \
        oned/CodaBarReader.cpp \
        oned/Code128EXTReader.cpp \
        oned/Code128Reader.cpp \
        oned/Code39Reader.cpp \
        oned/Code93Reader.cpp \
        oned/EAN13Reader.cpp \
        oned/EAN8Reader.cpp \
        oned/ITFReader.cpp \
        oned/MultiFormatOneDReader.cpp \
        oned/MultiFormatUPCEANReader.cpp \
        oned/OneDReader.cpp \
        oned/OneDResultPoint.cpp \
        oned/UPCAReader.cpp \
        oned/UPCEANReader.cpp \
        oned/UPCEReader.cpp \
        qrcode/QRCodeReader.cpp \
        qrcode/QRErrorCorrectionLevel.cpp \
        qrcode/QRFormatInformation.cpp \
        qrcode/QRVersion.cpp \
        qrcode/decoder/QRBitMatrixParser.cpp \
        qrcode/decoder/QRDataBlock.cpp \
        qrcode/decoder/QRDataMask.cpp \
        qrcode/decoder/QRDecodedBitStreamParser.cpp \
        qrcode/decoder/QRDecoder.cpp \
        qrcode/decoder/QRMode.cpp \
        qrcode/detector/QRAlignmentPattern.cpp \
        qrcode/detector/QRAlignmentPatternFinder.cpp \
        qrcode/detector/QRDetector.cpp \
        qrcode/detector/QRFinderPattern.cpp \
        qrcode/detector/QRFinderPatternFinder.cpp \
        qrcode/detector/QRFinderPatternInfo.cpp \
        qrcode/encoder/ByteMatrix.cpp \
        qrcode/encoder/MaskUtil.cpp \
        qrcode/encoder/MatrixUtil.cpp \
        qrcode/encoder/QRCode.cpp \
        qrcode/encoder/QREncoder.cpp \
        qzxing.cpp \
           
HEADERS += \
        BarcodeFormat.h \
        Binarizer.h \
        BinaryBitmap.h \
        CameraImageWrapper.h \
        ChecksumException.h \
        DecodeHints.h \
        EncodeHint.h \
        Exception.h \
        FormatException.h \
        IllegalStateException.h \
        InvertedLuminanceSource.h \
        LuminanceSource.h \
        MultiFormatReader.h \
        NotFoundException.h \
        QZXing.h \
        QZXingImageProvider.h \
        Reader.h \
        ReaderException.h \
        Result.h \
        ResultPoint.h \
        ResultPointCallback.h \
        UnsupportedEncodingException.h \
        WriterException.h \
        ZXing.h \
        bigint/BigInteger.hh \
        bigint/BigIntegerAlgorithms.hh \
        bigint/BigIntegerLibrary.hh \
        bigint/BigIntegerUtils.hh \
        bigint/BigUnsigned.hh \
        bigint/BigUnsignedInABase.hh \
        bigint/NumberlikeArray.hh \
        common/Array.h \
        common/BitArray.h \
        common/BitMatrix.h \
        common/BitSource.h \
        common/CharacterSetECI.h \
        common/Counted.h \
        common/DecoderResult.h \
        common/DetectorResult.h \
        common/GlobalHistogramBinarizer.h \
        common/GreyscaleLuminanceSource.h \
        common/GreyscaleRotatedLuminanceSource.h \
        common/GridSampler.h \
        common/HybridBinarizer.h \
        common/IllegalArgumentException.h \
        common/PerspectiveTransform.h \
        common/Point.h \
        common/Str.h \
        common/StringUtils.h \
        common/Types.h \
        common/detector/JavaMath.h \
        common/detector/MathUtils.h \
        common/detector/MonochromeRectangleDetector.h \
        common/detector/WhiteRectangleDetector.h \
        common/reedsolomon/GenericGF.h \
        common/reedsolomon/GenericGFPoly.h \
        common/reedsolomon/ReedSolomonDecoder.h \
        common/reedsolomon/ReedSolomonEncoder.h \
        common/reedsolomon/ReedSolomonException.h \
        imagehandler.h \
        oned/CodaBarReader.h \
        oned/Code128EXTReader.h \
        oned/Code128Reader.h \
        oned/Code39Reader.h \
        oned/Code93Reader.h \
        oned/EAN13Reader.h \
        oned/EAN8Reader.h \
        oned/ITFReader.h \
        oned/MultiFormatOneDReader.h \
        oned/MultiFormatUPCEANReader.h \
        oned/OneDReader.h \
        oned/OneDResultPoint.h \
        oned/UPCAReader.h \
        oned/UPCEANReader.h \
        oned/UPCEReader.h \
        qrcode/ErrorCorrectionLevel.h \
        qrcode/FormatInformation.h \
        qrcode/QRCodeReader.h \
        qrcode/Version.h \
        qrcode/decoder/BitMatrixParser.h \
        qrcode/decoder/DataBlock.h \
        qrcode/decoder/DataMask.h \
        qrcode/decoder/DecodedBitStreamParser.h \
        qrcode/decoder/Decoder.h \
        qrcode/decoder/Mode.h \
        qrcode/detector/AlignmentPattern.h \
        qrcode/detector/AlignmentPatternFinder.h \
        qrcode/detector/Detector.h \
        qrcode/detector/FinderPattern.h \
        qrcode/detector/FinderPatternFinder.h \
        qrcode/detector/FinderPatternInfo.h \
        qrcode/encoder/BlockPair.h \
        qrcode/encoder/ByteMatrix.h \
        qrcode/encoder/Encoder.h \
        qrcode/encoder/MaskUtil.h \
        qrcode/encoder/MatrixUtil.h \
        qrcode/encoder/QRCode.h \
        qzxing_global.h
        
win32-g++{
    INCLUDEPATH += $$PWD/win32/zxing
    HEADERS += $$PWD/win32/zxing/iconv.h
    SOURCES += $$PWD/win32/zxing/win_iconv.cpp
}

!win32{
    DEFINES += NO_ICONV
}
winrt {
    DEFINES += NO_ICONV
}
        
