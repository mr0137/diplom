#ifndef __RESULT_H__
#define __RESULT_H__

/*
 *  Result.h
 *  zxing
 *
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <common/Array.h>
#include <common/Counted.h>
#include <common/Str.h>
#include <common/Types.h>
#include <ResultPoint.h>
#include <BarcodeFormat.h>

namespace zxing {
class OneDReader;
class Result : public Counted {
private:
  Ref<String> text_;
  ArrayRef<bbyte> rawBytes_;
  ArrayRef< Ref<ResultPoint> > resultPoints_;
  BarcodeFormat format_;
  std::string charSet_;

public:
  Result(Ref<String> text,
         ArrayRef<bbyte> rawBytes,
         ArrayRef< Ref<ResultPoint> > resultPoints,
         BarcodeFormat format, std::string charSet = "");
  ~Result();
  Ref<String> getText();
  void appendText(Ref<String> text);
  ArrayRef<bbyte> getRawBytes();
  ArrayRef< Ref<ResultPoint> > const& getResultPoints() const;
  ArrayRef< Ref<ResultPoint> >& getResultPoints();
  BarcodeFormat getBarcodeFormat() const;
  std::string getCharSet() const;

  friend std::ostream& operator<<(std::ostream &out, Result& result);
  friend class OneDReader;
};

}
#endif // __RESULT_H__
