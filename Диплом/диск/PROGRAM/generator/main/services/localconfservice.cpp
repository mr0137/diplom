#include "localconfservice.h"
#include <databaseservice.h>
#include <QVariant>
#include <QJsonDocument>

LocalConfService::LocalConfService(QObject *parent) : QObject(parent)
{
    auto db = DatabaseService::createLocalConnection();
    QSqlQuery q(db);

    QString query = QString(
                "CREATE TABLE IF NOT EXISTS CONFIG ("
                "Field text,"
                "Value BLOB"
                ");");

    q.prepare(query);
    q.exec(query);
    query = QString(
                "SELECT * FROM CONFIG WHERE Field=\"CorrectClose\""
                );

    q.exec(query);
    if (!q.next()){
        query = QString(
                    "INSERT INTO CONFIG (Field, Value) VALUES (\"CorrectClose\",\"0\");"
                    );
        q.exec(query);
    }else if (q.value(1).toString() == "0"){
        //setIsCrashed(true);
    }else{
    }
    query = QString(
                "UPDATE CONFIG SET Value=\"0\" WHERE Field=\"CorrectClose\"; "
                );
    q.exec(query);

    //query = QString(
    //            "SELECT * FROM CONFIG WHERE Field=\"Tabs\""
    //            );
    //
    //q.exec(query);
    //if (!q.next()){
    //    query = QString(
    //                "INSERT INTO CONFIG (Field, Value) VALUES (\"Tabs\",\"\");"
    //                );
    //    q.exec(query);
    //}else if (!isCrashed()){
    //    query = QString(
    //                "UPDATE CONFIG SET Value=\"\" WHERE Field=\"Tabs\"; "
    //                );
    //    q.exec(query);
    //
    //}

    db.close();
}

LocalConfService::~LocalConfService()
{
    auto db = DatabaseService::createLocalConnection();
    QSqlQuery q(db);

    QString query = QString(
                "UPDATE CONFIG SET Value=\"1\" WHERE Field=\"CorrectClose\"; "
                );
    q.prepare(query);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return;
    }

    db.close();
}

void LocalConfService::saveTabs(QVariantList tabsList)
{
    auto db = DatabaseService::createLocalConnection();
    QSqlQuery q(db);

    QVariant var = QVariant(tabsList);
    if (var.isValid()){
        auto r = QJsonDocument::fromVariant(var).toJson();
        QFile file("dump.json");
        file.open(QIODevice::WriteOnly);
        file.write(r);
        QString query = QString(
                    "UPDATE CONFIG SET Value=:val WHERE Field=\"Tabs\";"
                    );
        q.prepare(query);
        q.bindValue(":val", QFileInfo(file).absoluteFilePath());
        file.close();
        if (!q.exec()) {
            qDebug() << "\nError:" << q.lastError()
                     << "\nQuery:" << q.lastQuery();
            return;
        }
    }

    db.close();
}

QVariantList LocalConfService::loadTabs()
{
    auto db = DatabaseService::createLocalConnection();
    QSqlQuery q(db);
    QVariantList result;
    QString query = QString(
                "SELECT Value FROM CONFIG WHERE Field=\"Tabs\";"
                );
    q.prepare(query);
    if (!q.exec()) {
        qDebug() << "\nError:" << q.lastError()
                 << "\nQuery:" << q.lastQuery();
        return {};
    }
    q.next();
    QFile file(q.value(0).toString());
    if (file.open(QIODevice::ReadOnly)){
        QVariant var = QJsonDocument::fromJson(file.readAll()).toVariant();
        result = var.toList();
    }

    db.close();
    return result;
}
