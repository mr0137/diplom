#include "authservice.h"
#include <QGuiApplication>
#include <QQmlEngine>
#include <appcore.h>
#include <QStringList>
#include <interfaces/ipageplugin.h>
#include <utility/loadpropertiesblock.h>
#include <utility/printerbackend.h>

AuthService::AuthService(QObject *parent) : QObject(parent)
{
#ifdef QT_NO_DEBUG
#ifdef Q_OS_LINUX
    m_pluginsPaths = {"/plugins/ProduceOrder/libProduceOrder.so",  "/plugins/CustomerPlugin/libCustomerPlugin.so"};
#else
    m_pluginsPaths = {"\\plugins\\ProduceOrder\\ProduceOrder.dll",  "\\plugins\\CustomerPlugin\\CustomerPlugin.dll", /*"\\bin\\plugins\\PeoplesPlugin\\PeoplesPlugin.dll",*/
                      "\\plugins\\GoodsPlugin\\GoodsPlugin.dll"};
#endif
#else
#ifdef Q_OS_LINUX
    m_pluginsPaths = {"/bin/plugins/ProduceOrder/libProduceOrder.so",  "/bin/plugins/CustomerPlugin/libCustomerPlugin.so"/*,"/bin/plugins/PeoplesPlugin/libPeoplesPlugin.so",*/
                      /*"/bin/plugins/GoodsPlugin/libGoodsPlugin.so"*/};
#else
    m_pluginsPaths = {"/bin/plugins/ProduceOrder/ProduceOrder.dll"  ,"/bin/plugins/CustomerPlugin/CustomerPlugin.dll"/*, "/bin/plugins/PeoplesPlugin/PeoplesPlugin.dll",*/
                      /*,"/bin/plugins/GoodsPlugin/GoodsPlugin.dll"*/};
#endif
#endif
}

void AuthService::login(QString user, QString password)
{
    emit loginProcessBegin();

    emit loginProcessEnd("");
    setUsername(user);
    loadPlugins();
}

void AuthService::logout()
{
    QVariantMap map = QVariantMap{{"accepted", false}};
    emit logoutProcessBegin(&map);
    if (map["accepted"].toBool()){
        qApp->exit(123);
    }
}

QVariantList AuthService::getMenuTabs()
{
    //! need backend
    return QVariantList{
        QVariantMap{{"&Add" , QVariantList{
                    QVariantMap{{"&Order", "Action { text: qsTr(\"&Order\"); onTriggered:{ AppCore.tabsService.addTabItem(\"Order\", \"Order Tab\") } }"}},
                    QVariantMap{{"&Customer", "Action { text: qsTr(\"&Customer\"); onTriggered:{ AppCore.tabsService.addTabItem(\"Customer\", \"Customer Tab\") } }"}}
                }
                    }},
        QVariantMap{{"&List" , QVariantList{
                    QVariantMap{{"&Orders", "Action { text: qsTr(\"&Orders\"); onTriggered: { AppCore.tabsService.addTabItem(\"Orders\", \"Orders List Tab\") } }"}},
                    QVariantMap{{"&Customers", "Action { text: qsTr(\"&Customers\"); onTriggered: { AppCore.tabsService.addTabItem(\"Customers\", \"Customers List Tab\") } }"}},
                    //                    QVariantMap{{"&People", "Action { text: qsTr(\"&People\"); onTriggered: { AppCore.tabsService.addTabItem(\"People\", \"People List Tab\") } }"}},
                    QVariantMap{{"&Goods", "Action { text: qsTr(\"&Goods\"); onTriggered: { AppCore.tabsService.addTabItem(\"Goods\", \"Goods List Tab\") } }"}},
                }}},
        QVariantMap{{"&Tools" , QVariantList{
                    QVariantMap{{"&Parse Task Files", "Action { text: qsTr(\"&Parse Task Files\"); onTriggered: { AppCore.tabsService.addTabItem(\"Parse Task Files\", \"Parse Task Files\") } }"}},
                    QVariantMap{{"&Print Tab", "Action { text: qsTr(\"&Print Tab\"); onTriggered: { AppCore.tabsService.addTabItem(\"Print Tab\", \"Print Tab\") } }"}}
                    //QVariantMap{{"&Loading preferences", "Action { text: qsTr(\"&Loading preferences\"); onTriggered: { AppCore.tabsService.addTabItem(\"Loading preferences\", \"Loading preferences\") } }"}},
                }}}/*,

        QVariantMap{{"&Test" , QVariantList{
                    QVariantMap{{"&Test1", "Action { text: qsTr(\"&Test1\"); onTriggered:{ AppCore.tabsService.addTabItem(\"Test1\", \"Test1\") } }"}},
                    QVariantMap{{"&Test2", "Action { text: qsTr(\"&Test2\"); onTriggered:{ AppCore.tabsService.addTabItem(\"Test2\", \"Test2\") } }"}}
                }
                    }},*/
    };
}

QMap<QString, QString> AuthService::getComponents()
{
    return m_components;
}

QMap<QString, std::function<ITableDataProvider *(ITableDataProvider*)>> AuthService::getListings()
{
                                                                        return m_listings;
                                                                        }

                                                                        QMap<QString, std::function<BlockData *()> > AuthService::getBlocks()
{
    return m_creators;
}

QVariantMap AuthService::getTabs()
{
    return {
        {"Order Tab", QVariantMap{
                { "rows", 10},
                { "columns", 2},
                { "mainBlockName", "orderInfo"},
                { "mainBackend", "orderInfo"},
                { "isReloaded", false },
                { "toolButtons", QVariantList{
                        "SaveButton"
                    }},
                { "config", QVariantList{
                        // QVariantMap {
                        //     {   "row",              0 },
                        //     {   "column",           0 },
                        //     {   "rowSpan",          2 },
                        //     {   "columnSpan",       1 },
                        //     {   "maximumHeight",    50 },
                        //     {   "maximumWidth",     -1 },
                        //     {   "minimumHeight",    50 },
                        //     {   "minimumWidth",     -1 },
                        //     {   "component",        "ToolBarComponent" },
                        //     {   "backend",          "ToolBarComponent" },
                        //     {   "blockName",        "ToolBarComponent" }
                        // },
                        QVariantMap {
                            {   "row",              0 },
                            {   "column",           0 },
                            {   "rowSpan",          3 },
                            {   "columnSpan",       1 },
                            {   "maximumHeight",    180 },
                            {   "maximumWidth",     -1 },
                            {   "minimumHeight",    180 },
                            {   "minimumWidth",     -1 },
                            {   "component",        "orderInfo" },
                            {   "backend",          "orderInfo" },
                            {   "blockName",        "orderInfo" }
                        },
                        QVariantMap {
                            {   "row",              0 },
                            {   "column",           1 },
                            {   "rowSpan",          3 },
                            {   "columnSpan",       1 },
                            {   "maximumHeight",    180 },
                            {   "maximumWidth",     -1 },
                            {   "minimumHeight",    180 },
                            {   "minimumWidth",     -1 },
                            {   "component",        "customerInfo" },
                            {   "backend",          "customerInfo" },
                            {   "blockName",        "customerInfo_int" }

                        },
                        QVariantMap {
                            {   "row",              3 },
                            {   "column",           0 },
                            {   "rowSpan",          2 },
                            {   "columnSpan",       2 },
                            {   "maximumHeight",    120 },
                            {   "maximumWidth",     -1 },
                            {   "minimumHeight",    100 },
                            {   "minimumWidth",     -1 },
                            {   "component",        "orderNotes_int" },
                            {   "backend",          "orderInfo"      },
                            {   "blockName",        "orderNotes_int" }
                        },
                        QVariantMap {
                            {   "row",              5 },
                            {   "column",           0 },
                            {   "rowSpan",          5 },
                            {   "columnSpan",       2 },
                            {   "maximumHeight",    -1 },
                            {   "maximumWidth",     -1 },
                            {   "minimumHeight",    200 },
                            {   "minimumWidth",     -1 },
                            {   "component",        "orderTabs_int" },
                            {   "backend",          "orderInfo" },
                            {   "blockName",        "orderTabs_int" }
                        }
                    }
                }}
        },
        {"Customers List Tab", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                //{ "mainBackend", "customersListForm"},
                { "mainBlockName", "customersListForm"},
                { "isReloaded", true },
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "customersListForm" },
                            {   "backend",      "customersListForm" },
                            {   "blockName",    "customersListForm" }
                        }
                    }
                }}
        },
        //  {"People List Tab", QVariantMap{
        //          { "rows", 1},
        //          { "columns", 1},
        //          { "isReloaded", false },
        //          { "mainBlockName", "peopleListForm"},
        //          { "config", QVariantList{
        //                  QVariantMap {
        //                      {   "row",          0 },
        //                      {   "column",       0 },
        //                      {   "rowSpan",      1 },
        //                      {   "columnSpan",   1 },
        //                      {   "component",    "peopleListForm" },
        //                      {   "backend",      "peopleListForm" },
        //                      {   "blockName",    "peopleListForm" }
        //                  }
        //              }
        //          }}
        //  },
        {"Orders List Tab", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "ordersList"},
                { "isReloaded", true},
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "ordersList" },
                            {   "backend",      "ordersList" },
                            {   "blockName",    "ordersList" }
                        }
                    }
                }}
        },
        {"Goods List Tab", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "goodsListForm"},
                { "isReloaded", true },
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "goodsListForm" },
                            {   "backend",      "goodsListForm" },
                            {   "blockName",    "goodsListForm" }
                        }
                    }
                }}
        },
        {"Customer Tab", QVariantMap{
                { "rows", 2},
                { "columns", 1},
                { "mainBlockName", "customerForm2"},
                { "mainBackend", "customerForm"},
                { "isReloaded", false},
                { "toolButtons", QVariantList{
                        "AddCustomerButton"
                    }},
                { "config", QVariantList{
                        QVariantMap{
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "customerForm1" },
                            {   "backend",      "customerForm" },
                            {   "blockName",    "customerForm1" }
                        },
                        QVariantMap{
                            {   "row",          1 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "customerForm2" },
                            {   "backend",      "customerForm" },
                            {   "blockName",    "customerForm2" }
                        }
                    }}}
        },
        {"Test1", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "testInfo"},
                { "isReloaded", false},
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "testInfo1" },
                            {   "backend",      "testInfo" },
                            {   "blockName",    "testInfo" }
                        }
                    }
                }}
        },
        {"Test2", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "testInfo"},
                { "isReloaded", false},
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "testInfo2" },
                            {   "backend",      "testInfo" },
                            {   "blockName",    "testInfo" }
                        }
                    }
                }}
        },
        {"Parse Task Files", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "parseTaskFiles"},
                { "isReloaded", false},
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "parseTaskFiles" },
                            {   "backend",      "parseTaskFiles" },
                            {   "blockName",    "parseTaskFiles" }
                        }
                    }
                }}
        },
        {"Loading preferences", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "Loading preferences"},
                { "isReloaded", false},
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "Loading preferences" },
                            {   "backend",      "Loading preferences" },
                            {   "blockName",    "Loading preferences" }
                        }
                    }
                }}
        },
        {"Print Tab", QVariantMap{
                { "rows", 1},
                { "columns", 1},
                { "mainBlockName", "printer"},
                { "mainBackend", "printer"},
                { "isReloaded", false },
                { "config", QVariantList{
                        QVariantMap {
                            {   "row",          0 },
                            {   "column",       0 },
                            {   "rowSpan",      1 },
                            {   "columnSpan",   1 },
                            {   "component",    "Print Tab" },
                            {   "backend",      "Print Tab" },
                            {   "blockName",    "Print Tab" }
                        }
                    }}
            }
        }
    };
}

void AuthService::loadPlugins()
{
    emit pluginLoadingBegin();
    QString result = "";
    for (const auto &p : qAsConst(m_pluginsPaths)){
        qDebug() << AppCore::instance()->rootDir() + p;
        auto str = loadPlugin(AppCore::instance()->rootDir() + p);
        if (result != "") result += "\n";
        result += str;
    }

    m_creators.insert("Loading preferences", [this]()->BlockData*{
                          return new LoadPropertiesBlock(this);
                      });

    m_creators.insert("Print Tab", [this]()->BlockData*{
                          return new PrinterBackend(this);
                      });

    m_components.insert("Loading preferences", "qrc:/main/qml/LoadingPreferencesTab.qml");
    m_components.insert("Print Tab", "qrc:/main/qml/PrintList.qml");

    emit pluginLoadingEnd(result);
}

QString AuthService::loadPlugin(QString pluginPath)
{
    pluginPath = pluginPath.replace("\\", "/");
    QPluginLoader pluginLoader(pluginPath);

    pluginLoader.load();
    auto instance = pluginLoader.instance();
    if (instance) {
        auto page = qobject_cast<IPagePlugin*>(instance);
        m_components.insert(page->getComponents());
        m_creators.insert(page->getConfig());
        m_toolButtons.insert(page->getToolButtons());
        auto l = page->getListing();
        m_listings.insert(l);
        m_listingsSequance.push_back(l.begin().key());
    }else {
        return "Error while loading plugin: \"" + pluginPath + "\", errcode:" + pluginLoader.errorString();
    }
    return "";
}

QMap<QString, QString> AuthService::getToolButtons() const
{
    return m_toolButtons;
}

QStringList AuthService::listingsSequance() const
{
    return m_listingsSequance;
}

bool AuthService::autologin() const
{
    return m_autologin;
}

void AuthService::setAutologin(bool newAutologin)
{
    if (m_autologin == newAutologin)
        return;
    m_autologin = newAutologin;
    emit autologinChanged();
}

QString AuthService::username() const
{
    return m_username;
}

void AuthService::setUsername(const QString &newUsername)
{
    if (m_username == newUsername)
        return;
    m_username = newUsername;
    emit usernameChanged();
}
