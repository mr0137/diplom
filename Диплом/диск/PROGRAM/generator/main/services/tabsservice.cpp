#include "tabsservice.h"
#include <databaseservice.h>
#include <appcore.h>

TabsService::TabsService(QObject *parent) : QObject(parent)
{
    //QTimer::singleShot(2000, this, [this](){
    //    QFile file("test.json");
    //    file.open(QIODevice::ReadOnly);
    //    QVariantMap m = QJsonDocument::fromJson(file.readAll()).toVariant().toMap();
    //
    //    auto tItem = new TabData(this);
    //    tItem->set_name(m.value("tabName", "").toString());
    //    tItem->set_type(m.value("tabType", "").toString());
    //    tItem->set_index(m_tabItems.count());
    //    tItem->deserialize(m_tabs[tItem->type()].toMap());
    //    tItem->setTabData(m);
    //    m_tabItems.push_back(tItem);
    //});
    QTimer::singleShot(15000, this, [this](){
        addTabItem("Print Tab", "Print Tab");
    });
}

TabData *TabsService::responseAwaiter() const
{
    return m_responseAwaiter;
}

void TabsService::setResponseAwaiter(TabData *newResponseAwaiter)
{
    if (m_responseAwaiter == newResponseAwaiter)
        return;
    m_responseAwaiter = newResponseAwaiter;
    emit responseAwaiterChanged();
}

void TabsService::setTabs(QVariantMap tabs)
{
    m_tabs = tabs;
}

void TabsService::setToolButtons(QMap<QString, QString> toolButtons)
{
    m_toolButtons = toolButtons;
}

QVariantList TabsService::serialize()
{
    QVariantList result;
    for (const auto &tab : m_tabItems){
        result.push_back(tab->getTabData());
    }
    return result;
}

void TabsService::deserialize(const QVariantList &list)
{
    for (const auto &l : list){
        auto m = l.toMap();
        auto tItem = new TabData(this);
        tItem->set_name(m.value("tabName", "").toString());
        tItem->set_type(m.value("tabType", "").toString());
        tItem->set_index(m_tabItems.count());
        tItem->deserialize(m_tabs[tItem->type()].toMap());
        tItem->setTabData(m);
        m_tabItems.push_back(tItem);
    }
}

void TabsService::init()
{
    if (!m_preloadTabs.isEmpty()){
        for (const auto &l : qAsConst(m_preloadTabs)){
            auto m = l.toMap();
            QTimer::singleShot(1000, this, [this, m](){
                addTabItem(m["name"].toString(), m["type"].toString());
            });
        }
    }
}

void TabsService::addTabItem(QString name, QString type)
{
    TabData * tItem = nullptr;

    if (m_tabs.contains(type)){
        tItem = m_tabItems.find([type, name](TabData * ti){return ti->type() == type && ti->name() == name;});

        if (responseAwaiter() != nullptr){
            auto block = responseAwaiter()->getBlockData(responseBlockNameAwaiter());
            //block->deserialize(QVariantMap{{"n", name}}, AppCore::instance()->getListing(block->listingName()));
            tItem = responseAwaiter();
            setResponseAwaiter(nullptr);

            if (block){
                emit block->tryLoadByName(responseBlockNameAwaiter(), name);
            }
            setResponseBlockNameAwaiter("");
        }

        if(tItem == nullptr){
            tItem = new TabData(this);
            tItem->set_name(name);
            tItem->set_type(type);
            tItem->set_index(m_tabItems.count());
            tItem->deserialize(m_tabs[type].toMap());
            m_tabItems.push_back(tItem);
        }
    }

    if (tItem != nullptr){
        emit changeFocusTab(tItem->index());
    }
}

QString TabsService::getToolButton(QString type)
{
    if (m_toolButtons.contains(type)) return m_toolButtons[type];
    return "";
}

void TabsService::removeTabItem(int index)
{
    auto tItem = m_tabItems.find([index](TabData * ti){return ti->index() == index;});

    if(tItem == nullptr){
        return;
    }

    setResponseAwaiter(nullptr);
    int j = 0;
    for(int i = index; i < m_tabItems.count(); ++i){
        if (tItem != m_tabItems.get(i))
            m_tabItems.get(i)->set_index(i - j);
        else j = 1;
    }

    m_tabItems.remove(tItem);
    tItem->deleteLater();
}

void TabsService::tryRemoveTabItem(int index)
{
    auto tItem = m_tabItems.find([index](TabData * ti){return ti->index() == index;});

    if(tItem == nullptr){
        return;
    }
    setResponseAwaiter(nullptr);

    if (!tItem->isEdited()){
        int j = 0;
        for(int i = index; i < m_tabItems.count(); ++i){
            if (tItem != m_tabItems.get(i))
                m_tabItems.get(i)->set_index(i - j);
            else j = 1;
        }
        m_tabItems.remove(tItem);
        tItem->deleteLater();
    }else{
        emit closeError(tItem->index(), tItem->name());
        return;
    }
}

void TabsService::focusTab(QString type, QString name)
{
    auto fTab = m_tabItems.find([name,type](TabData * ti){return ti->type() == type && ti->name() == name;});
    if(fTab != nullptr){
        emit changeFocusTab(fTab->index());
    }
}

bool TabsService::containsTabName(QString name)
{
    for (const auto &t: m_tabItems){
        //qDebug()<<"tab name - "<<t->name();
        if (t->name() == name){
            return true;
        }
    }
    return false;
}

int TabsService::tabIndex(QString tabname)
{
    for (const auto &t: m_tabItems){
        if (t->name() == tabname){
            return t->index();
        }
    }
    return -1;
}

QVariantList TabsService::nonSavedTabs()
{
    QVariantList result;
    for (const auto &t: m_tabItems){
        if (t->isEdited()){
            result.push_back(t->name());
        }
    }
    return result;
}

void TabsService::saveTab(int index)
{
    auto tItem = m_tabItems.find([index](TabData * ti){return ti->index() == index;});
    if (tItem != nullptr){
        tItem->save();
    }
}

bool TabsService::isReloadAvailable(int index)
{
    auto tItem = m_tabItems.find([index](TabData * ti){return ti->index() == index;});
    if (tItem != nullptr){
        return tItem->isReloaded();
    }
    return false;
}


const QString &TabsService::responseBlockNameAwaiter() const
{
    return m_responseBlockNameAwaiter;
}

void TabsService::setResponseBlockNameAwaiter(const QString &newResponseBlockNameAwaiter)
{
    if (m_responseBlockNameAwaiter == newResponseBlockNameAwaiter)
        return;
    m_responseBlockNameAwaiter = newResponseBlockNameAwaiter;
    emit responseBlockNameAwaiterChanged();
}
