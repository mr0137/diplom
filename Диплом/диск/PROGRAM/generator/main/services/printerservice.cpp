#include "printerservice.h"
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QDebug>
#include <QPageLayout>

PrinterService::PrinterService(QObject *parent) : QObject(parent)
{
    m_printer = new QPrinter(QPrinter::HighResolution);



    m_codeGen = new QZint();

}

void PrinterService::setImage()
{

}

void PrinterService::openDialogPrinter()
{
}

void PrinterService::drawAndPrintLable(QVariantList list)
{
    QVariantMap part_info;
    part_info["name"] = "Shaiba";
    part_info["dec"] = "BC380-21.00.0169";
    part_info["material"] = "SSMT";
    part_info["thricnes"] = "0.6";
    part_info["count"] = "50";
    part_info["order_num"] = "04062";
    part_info["customer"] = "OLIS";

    QImage image(1100, 740, QImage::Format_ARGB32);
    image.fill(QColor("white"));
    QPainter painter(&image);
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            int rect_w = 265;
            int rect_h = 170;
            int row_h = rect_h / 7;
            int s_pos_y = (rect_w * i) + ( i * 5 );
            int s_pos_x = (rect_h * j) + ( j * 5 );
            QRect frame  = QRect(                 s_pos_y,      row_h + s_pos_x,           rect_w,       rect_h - 2);
            QRect rect1  = QRect(                 s_pos_y,      row_h + s_pos_x,           rect_w,       row_h);
            QRect rect2  = QRect((rect_w * 0.85)+ s_pos_y, (row_h * 2) + s_pos_x,   rect_w * 0.15,       row_h);
            QRect rect3  = QRect(                 s_pos_y, (row_h * 2)+ s_pos_x,    rect_w * 0.85,       row_h);
            QRect rect4  = QRect(                 s_pos_y, (row_h * 3)+ s_pos_x,           rect_w,       row_h);
            QRect rect5  = QRect(                 s_pos_y, (row_h * 4)+ s_pos_x,     rect_w * 0.5,       row_h);
            QRect rect6  = QRect((rect_w * 0.5) + s_pos_y, (row_h * 4)+ s_pos_x,    rect_w * 0.25,       row_h);
            QRect rect7  = QRect((rect_w * 0.75)+ s_pos_y, (row_h * 4)+ s_pos_x,    rect_w * 0.25,       row_h);
            QRect rect8  = QRect(                 s_pos_y, (row_h * 5)+ s_pos_x,     rect_w * 0.4,       row_h);
            QRect rect9  = QRect((rect_w * 0.4) + s_pos_y, (row_h * 5)+ s_pos_x,     rect_w * 0.6,       row_h);
            QRect rect10 = QRect(                 s_pos_y, (row_h * 6)+ s_pos_x,     rect_w * 0.6,       row_h);
            QRect rect11 = QRect((rect_w * 0.6) + s_pos_y, (row_h * 6)+ s_pos_x,     rect_w * 0.4,       row_h);
            QRect rect12 = QRect(                 s_pos_y, (row_h * 7)+ s_pos_x,           rect_w,       row_h);

            QFont font = painter.font();
            font.setPixelSize(16);
            painter.setFont(font);

            painter.drawRect(rect1);
            QImage img = encode(part_info["dec"].toString(), rect_w, row_h);
            painter.drawImage(rect1, img);
            //painter.drawText(rect1, Qt::AlignCenter,"Штрих код");
            painter.drawRect(rect2);
            painter.drawText(rect2, Qt::AlignCenter,"Г");
            painter.drawRect(rect3);
            painter.drawText(rect3, Qt::AlignCenter, part_info["name"].toString());
            painter.drawRect(rect4);
            font.setBold(true);
            painter.setFont(font);
            painter.drawText(rect4, Qt::AlignCenter,part_info["dec"].toString());
            font.setBold(false);
            painter.setFont(font);
            painter.drawRect(rect5);
            painter.drawText(rect5, Qt::AlignCenter,part_info["material"].toString());
            painter.drawRect(rect6);
            painter.drawText(rect6,Qt::AlignLeft | Qt::AlignVCenter," \u2260 ");
            painter.drawText(rect6,Qt::AlignCenter, part_info["thricnes"].toString());
            painter.drawRect(rect7);
            painter.drawText(rect7, Qt::AlignCenter,"");
            painter.drawRect(rect8);
            painter.drawText(rect8, Qt::AlignLeft | Qt::AlignVCenter," К-cть: " + part_info["count"].toString());
            painter.drawRect(rect9);
            painter.drawText(rect9, Qt::AlignLeft | Qt::AlignVCenter," Габ.: ");
            painter.drawRect(rect10);
            painter.drawText(rect10, Qt::AlignLeft | Qt::AlignVCenter," № Зам. " + part_info["order_num"].toString());
            painter.drawRect(rect11);
            painter.drawText(rect11, Qt::AlignLeft | Qt::AlignVCenter," " +  part_info["customer"].toString());
            painter.drawRect(rect12);
            painter.drawText(rect12, Qt::AlignLeft | Qt::AlignVCenter," Прим.: ");
            QPen pen;
            pen.setWidth(2);
            painter.setPen(pen);
            painter.drawRect(frame);
            pen.setWidth(1);
            painter.setPen(pen);
        }
    }
    image.save("test.png");
    m_images.push_back(image);
    imagesChanged(m_images);
    painter.end();
}

QImage PrinterService::encode(QString text, double width, double height)
{
    if (text == "") return QImage();
    QZint bc;

    QImage img(width, height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(height);
    bc.setWidth(width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE128);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(2,2, width-4, height+2), QZint::AspectRatioMode::CenterBarCode);
    qDebug() << bc.lastError() << text;
    emit codeChanged();
    return img;
}

const QImage &PrinterService::code() const
{
    return m_code;
}

void PrinterService::setCode(const QImage &newCode)
{
    if (m_code == newCode)
        return;
    m_code = newCode;
    emit codeChanged();
}
