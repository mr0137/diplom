#ifndef MENUTABSSERVICE_H
#define MENUTABSSERVICE_H

#include <QObject>
#include <QVariantList>
#include <data_structures/menudata.h>
#include <utility/helper.h>

class MenuTabsService : public QObject
{
    Q_OBJECT
    K_QML(MenuTabsService)
    Q_PROPERTY(MenuData* root READ root WRITE setRoot NOTIFY rootChanged)
public:
    explicit MenuTabsService(QObject *parent = nullptr);
    void proceed(QVariantList list);

    MenuData *root() const;

signals:
    void rootChanged();

private:
    QList<MenuData *> loadTabsData(QVariantList list, MenuData *parent);
    void setRoot(MenuData *newRoot);
    QMap<QString, MenuData*> m_createdMenu;
    MenuData *m_root = nullptr;
};

#endif // MENUTABSSERVICE_H
