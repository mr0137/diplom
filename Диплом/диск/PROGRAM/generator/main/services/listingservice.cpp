#include "listingservice.h"
#include <QDebug>

ListingService::ListingService(QObject *parent) : QObject(parent)
{

}

const QString ListingService::currentLoadingListing() const
{
    return m_currentLoadingListing;
}

void ListingService::setCurrentLoadingListing(const QString &newCurrentLoadingListing)
{
    if (m_currentLoadingListing == newCurrentLoadingListing)
        return;
    m_currentLoadingListing = newCurrentLoadingListing;
    emit currentLoadingListingChanged();
}

qreal ListingService::currentLoadingProgress() const
{
    return m_currentLoadingProgress;
}

void ListingService::setCurrentLoadingProgress(qreal newCurrentLoadingProgress)
{
    if (qFuzzyCompare(m_currentLoadingProgress, newCurrentLoadingProgress))
        return;
    m_currentLoadingProgress = newCurrentLoadingProgress;
    emit currentLoadingProgressChanged();
}

int ListingService::loadingQueueCount() const
{
    return m_updateQueue.count();
}

void ListingService::updateListing(QString name)
{
    if (m_listingsCreators.contains(name)){
        ITableDataProvider *model = nullptr;
        emit listingUpdateStarted(name);
        if (m_listings.contains(name)){
            model = m_listings[name];
        }
        setLoaded(false);
        if (model == nullptr){
            m_listings[name] = m_listingsCreators[name](model);

            setCurrentLoadingProgress(qAbs(m_listings.count() - m_updateQueue.count()));
            setCurrentLoadingListing(name);

            connect(m_listings[name], &ITableDataProvider::fullLoaded, this, [this, name](){
                if (!m_updateQueue.empty()){
                    qDebug()<<"name "<<name;
                    updateListing(m_updateQueue.takeFirst());
                }else if (!m_loaded){
                    setLoaded(true);
                    emit listingUpdated(name);
                    //m_tabsService->init();
                }
            });

            connect(m_listings[name], &ITableDataProvider::loadingProgress, this, [this](double progress){
               setCurrentLoadingProgress(progress);
            });
        }else{
            m_listings[name] = m_listingsCreators[name](model);
        }
        m_listings[name]->setParent(this);
    }
}

ITableDataProvider *ListingService::getListing(QString name)
{
    if (m_listings.contains(name)){
        return m_listings[name];
    }
    return nullptr;
}

void ListingService::reloadAll()
{
    for (const auto &k : qAsConst(m_listingsNames)){
        m_updateQueue.push_back(k);
        qDebug() << k;
        //connect(m_listingsCreators[k], &ITableDataProvider::fullLoaded, this, )
        //updateListing(k);
    }
    //m_loadingDataContainer.updateQueue.push_back("orders");
    //m_loadingDataContainer.updateQueue.push_back("goods");
    emit loadingQueueCount();
    updateListing(m_updateQueue.takeFirst());
}

bool ListingService::loaded() const
{
    return m_loaded;
}

void ListingService::setLoaded(bool newLoaded)
{
    if (m_loaded == newLoaded)
        return;
    m_loaded = newLoaded;
    emit loadedChanged();
}

int ListingService::loadingNumber() const
{
    return m_loadingNumber;
}

void ListingService::setLoadingNumber(int newLoadingNumber)
{
    if (m_loadingNumber == newLoadingNumber)
        return;
    m_loadingNumber = newLoadingNumber;
    emit loadingNumberChanged();
}
