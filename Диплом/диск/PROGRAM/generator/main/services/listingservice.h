#ifndef LISTINGSERVICE_H
#define LISTINGSERVICE_H

#include <QObject>
#include <itabledataprovider.h>
#include <utility/helper.h>

class ListingService : public QObject
{
    Q_OBJECT
    K_QML(ListingService)
    Q_DISABLE_COPY(ListingService)
    Q_PROPERTY(QString currentLoadingListing READ currentLoadingListing WRITE setCurrentLoadingListing NOTIFY currentLoadingListingChanged)
    Q_PROPERTY(qreal currentLoadingProgress READ currentLoadingProgress WRITE setCurrentLoadingProgress NOTIFY currentLoadingProgressChanged)
    Q_PROPERTY(int loadingQueueCount READ loadingQueueCount NOTIFY loadingQueueCountChanged)
    Q_PROPERTY(int loadingNumber READ loadingNumber WRITE setLoadingNumber NOTIFY loadingNumberChanged)
    Q_PROPERTY(bool loaded READ loaded WRITE setLoaded NOTIFY loadedChanged)
    friend class AppCore;
public:
    explicit ListingService(QObject *parent = nullptr);

    const QString currentLoadingListing() const;
    void setCurrentLoadingListing(const QString &newCurrentLoadingListing);

    qreal currentLoadingProgress() const;
    void setCurrentLoadingProgress(qreal newCurrentLoadingProgress);

    int loadingQueueCount() const;
    bool loaded() const;
    void setLoaded(bool newLoaded);

    int loadingNumber() const;
    void setLoadingNumber(int newLoadingNumber);

public slots:
    void updateListing(QString name);
    ITableDataProvider * getListing(QString name);
    void reloadAll();
signals:
    void currentLoadingListingChanged();
    void currentLoadingProgressChanged();
    void loadingQueueCountChanged();
    void loadedChanged();
    void listingUpdated(QString name);
    void loadingNumberChanged();
    void listingUpdateStarted(QString name);

private:
    QString m_currentLoadingListing = "";
    qreal m_currentLoadingProgress = 0.0;
    int m_loadingNumber = 0;
    bool m_loaded = false;

    QStringList m_updateQueue;
    QStringList m_listingsNames;
    QMap<QString, ITableDataProvider*> m_listings;
    QMap<QString, std::function<ITableDataProvider *(ITableDataProvider *model)>> m_listingsCreators;
};

#endif // LISTINGSERVICE_H
