#include <QGuiApplication>
#include <QPalette>
#include <QPluginLoader>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>
#include <blockdata.h>

#include <QQuickStyle>
#include <QThread>
#include <QTextCodec>
#include <appcore.h>
#include <utility/helper.h>
#include <utility/devlogger.h>
//#include "produceorderform.h"

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0);

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString & msg)
{
#ifdef QT_NO_DEBUG
    DevLogger::instance()->addLog(context.category, context.function, msg);
#endif
    (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, msg);
}


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qSetMessagePattern("%{if-category}\033[32m%{category}:%{endif}%{if-warning}\033[37m%{file}:%{line}: %{endif}\033\[31m%{if-debug}\033\[36m%{endif}[%{function}]\033[0m %{message}");
    qInstallMessageHandler(customMessageHandler);

    // QThreadPool::globalInstance()->setMaxThreadCount(QThread::idealThreadCount() - 1);

    app.setWindowIcon(QIcon(":/icon/logo.png"));

    qmlRegisterSingletonType<AppCore>("App", 1, 0, "AppCore", &AppCore::qmlInstance);
    qmlRegisterAnonymousType<QAbstractListModel>("App", 1);

    AppCore::instance()->setRootPath(argv[0]);
    QQuickStyle::setStyle("Fusion");

    QQmlApplicationEngine engine;
    // [importing plugins]
#ifdef QT_NO_DEBUG
    engine.addImportPath(AppCore::instance()->rootDir() + "/plugins/");
#else
    engine.addImportPath(AppCore::instance()->rootDir() + "/bin/plugins/");
#endif
    qDebug() << engine.importPathList();
    // [importing plugins]
    app.setOrganizationName("TK");
    app.setOrganizationDomain("TK");

    const QUrl url(QStringLiteral("qrc:/main/qml/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

}
