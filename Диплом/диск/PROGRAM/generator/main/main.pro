QT += core
QT += gui
QT += qml
QT += sql
QT += quick
QT += concurrent
QT += quickcontrols2
QT += printsupport

CONFIG += c++17
#CONFIG += console

DEFINES += QT_DEPRECATED_WARNINGS

QML_IMPORT_NAME = App
QML_IMPORT_MAJOR_VERSION = 1
uri = App

SOURCES += \
    appcore.cpp \
    data_structures/blockitemdata.cpp \
    data_structures/tabdata.cpp \
    services/authservice.cpp \
    main.cpp \
    data_structures/menudata.cpp \
    services/listingservice.cpp \
    services/localconfservice.cpp \
    services/menutabsservice.cpp \
    services/printerservice.cpp \
    services/tabsservice.cpp \
    utility/codeitem.cpp \
    utility/devlogger.cpp \
    utility/loadpropertiesblock.cpp \
    utility/menuchooser.cpp \
    utility/printerbackend.cpp

HEADERS += \
    appcore.h \
    data_structures/blockitemdata.h \
    data_structures/tabdata.h \
    services/authservice.h \
    services/listingservice.h \
    services/localconfservice.h \
    services/printerservice.h \
    services/tabsservice.h \
    utility/codeitem.h \
    utility/devlogger.h \
    utility/helper.h \
    data_structures/menudata.h \
    services/menutabsservice.h \
    utility/loadpropertiesblock.h \
    utility/menuchooser.h \
    databaseservice.h \
    utility/printerbackend.h

QML_IMPORT_PATH += $$PWD/../plugins

RESOURCES += \
    resources/qml.qrc

CONFIG(release, debug|release){
    LIBS += -L$$PWD/../bin/libs -lKOrm
    LIBS += -L$$PWD/../bin/libs -lQXlsx
    LIBS += -L$$PWD/../bin/libs -lklibcorelite
    LIBS += -L$$PWD/../bin/libs -lEwarehouseBase
    LIBS += -L$$PWD/../bin/libs -lTableRenderBase
    LIBS += -L$$PWD/../bin/libs -lQZint
}else{
    win32: LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
    else:unix: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

    win32: LIBS += -L$$OUT_PWD/../libs/QXlsx/debug/ -lQXlsx
    else:unix: LIBS += -L$$OUT_PWD/../libs/QXlsx/ -lQXlsx

    win32: LIBS += -L$$OUT_PWD/../libs/KOrm/debug/ -lKOrm
    else:unix: LIBS += -L$$OUT_PWD/../libs/KOrm/ -lKOrm

    win32: LIBS += -L$$OUT_PWD/../libs/EwarehouseBase/debug/ -lEwarehouseBase
    else:unix: LIBS += -L$$OUT_PWD/../libs/EwarehouseBase/ -lEwarehouseBase

    win32: LIBS += -L$$OUT_PWD/../libs/TableRenderBase/debug/ -lTableRenderBase
    else:unix: LIBS += -L$$OUT_PWD/../libs/TableRenderBase/ -lTableRenderBase

    win32: LIBS += -L$$OUT_PWD/../libs/QZint/debug/ -lQZint
    else:unix: LIBS += -L$$OUT_PWD/../libs/QZint/ -lQZint


}
INCLUDEPATH += $$PWD/../libs/EwarehouseBase
DEPENDPATH += $$PWD/../libs/EwarehouseBase

INCLUDEPATH += $$PWD/../libs/KOrm
DEPENDPATH += $$PWD/../libs/KOrm

INCLUDEPATH += $$PWD/../libs/QXlsx
DEPENDPATH += $$PWD/../libs/QXlsx

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite

INCLUDEPATH += $$PWD/../libs/TableRenderBase
DEPENDPATH += $$PWD/../libs/TableRenderBase

INCLUDEPATH += $$PWD/../libs/QZint
DEPENDPATH += $$PWD/../libs/QZint
