import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0
import BasePlugin 1.0

C.BaseBackground {
    radius: 5
    backgroundColor: AppCore.palette.app.firstBackgroundColor
    elevation: 3
    property string orderNumber: ""
    property int tabIndex

    signal close(bool result)
    C.BaseBackground{
        anchors.fill: parent
        backgroundColor: AppCore.palette.app.firstBackgroundColor
        anchors.margins: 4
        radius: 5
        elevation: 3
        internalShadow: true
        ColumnLayout{
            anchors.fill: parent
            anchors.margins: 5
            Text{
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "Вкладка з назвою \"" + orderNumber + "\" не збережена!"
                color: AppCore.palette.text.textColor
                font.pointSize: 14
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
            }
            Text{
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "Оберіть наступну операцію"
                color: AppCore.palette.text.textColor
                font.pointSize: 14
                horizontalAlignment: Text.AlignHCenter
            }
            RowLayout{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumHeight: 20
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                spacing: 10

                C.Button{
                    text: "ЗАКРИТИ"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: 70
                    buttonBackground: AppCore.palette.button.backgroundColor
                    textColor: AppCore.palette.button.textColor
                    elevation: 4
                    buttonRadius: 5
                    onReleased: {
                        close(true)
                    }
                }

                C.Button{
                    text: "ЗБЕРЕГТИ ТА ЗАКРИТИ"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: 150
                    buttonBackground: AppCore.palette.button.backgroundColor
                    textColor: AppCore.palette.button.textColor
                    elevation: 4
                    buttonRadius: 5
                    onReleased: {
                        AppCore.tabsService.saveTab(tabIndex)
                        close(true)
                    }
                }

                C.Button{
                    text: "ВІДМІНИТИ"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: 80
                    buttonBackground: AppCore.palette.button.backgroundColor
                    textColor: AppCore.palette.button.textColor
                    elevation: 4
                    buttonRadius: 5
                    onReleased: {
                        close(false)
                    }
                }
            }
        }
    }
}
