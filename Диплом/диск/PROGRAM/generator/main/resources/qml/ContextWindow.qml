import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0
import QtQuick.Window 2.15

Window {
    id: root
    //parent: Overlay.overlay
    //required property Item source
    modality: internal.modal ?  Qt.WindowModal : Qt.NonModal
    x: internal.x
    y: internal.y
    width: internal.width
    height: internal.height

    onActiveFocusItemChanged: {
        if (!activeFocusItem){
            close()
        }
    }

    onAfterRendering: {
        requestActivate()
        backgroundLoader.forceActiveFocus()
    }

    flags: Qt.Tool | Qt.FramelessWindowHint
    required property Window sourceWindow

    Connections{
        target: root
        function onClose(){
            internal.modal = false
            internal.onLoadedProperties = []
        }
    }


    Loader{
        id: backgroundLoader
        anchors.fill: parent
        focus: false

        onLoaded: {
            for (var obj in internal.onLoadedProperties){
                if (backgroundLoader.item.hasOwnProperty(obj)){
                    backgroundLoader.item[obj] = internal.onLoadedProperties[obj]
                }
            }
        }
    }

    Connections{
        target: backgroundLoader.item
        function onClose(){
            root.close()
        }
    }

    QtObject{
        id: internal
        property real x: 0
        property real y: 0
        property real width: 0
        property real height: 0
        property var onLoadedProperties: []
        property bool modal: false
    }

    function openWindow(x, y, width, height, modal, popupComponent, properties = []){
        backgroundLoader.sourceComponent = null
        internal.x = x + sourceWindow.x
        internal.y = y + sourceWindow.y
        internal.width = width
        internal.height = height
        internal.modal = modal
        internal.onLoadedProperties = properties
        backgroundLoader.sourceComponent = popupComponent

        root.show()
    }

    function openGlobalWindow(x, y, width, height, modal, popupComponent, properties = []){
        backgroundLoader.sourceComponent = null
        internal.x = x
        internal.y = y
        internal.width = width
        internal.height = height
        internal.modal = modal
        internal.onLoadedProperties = properties
        backgroundLoader.sourceComponent = popupComponent
        root.show()
    }
}


