import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.0
import App 1.0
import CQML 1.0 as C

Rectangle{
    color: AppCore.palette.app.secondBackgroundColor

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 5
        spacing: 0

        Item{
            Layout.fillHeight: true
        }

        C.BaseBackground{
            backgroundColor: AppCore.palette.app.secondBackgroundColor
            radius: width / 2
            Layout.preferredHeight: parent.height * 0.35
            Layout.preferredWidth: height
            Layout.alignment: Qt.AlignHCenter
            clipContent: true
            elevation: 2
            Text{
                anchors.fill: parent
                font.pixelSize: height / 1.3
                text: 'P'
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: AppCore.palette.text.textColor
                font.family: "Book Antiqua"
                clip: true
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.08
        }

        C.BaseBackground{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.07
            Layout.fillWidth: true
            Layout.maximumWidth: Math.min(parent.width * 0.8, 500)
            Layout.alignment: Qt.AlignHCenter
            radius: 5
            backgroundColor: AppCore.palette.app.secondBackgroundColor
            elevation: 2
            clipContent: true
            Behavior on Layout.maximumHeight { NumberAnimation { duration: 200 } }

            C.TextField{
                id: loginTextField
                anchors.fill: parent
                anchors.topMargin: activeFocus || text !== "" ? 10 : 0
                floatingText: "Username"
                font.family: "Book Antiqua"
                placeholderTextColor: Qt.darker(AppCore.palette.text.textColor, 1.5)
                color: AppCore.palette.text.textColor
                hintColor: AppCore.palette.text.textColor
                Behavior on anchors.topMargin { NumberAnimation { duration: 200 } }
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.03
        }

        C.BaseBackground{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.07
            Layout.fillWidth: true
            Layout.maximumWidth: Math.min(parent.width * 0.8, 500)
            Layout.alignment: Qt.AlignHCenter
            radius: 5
            backgroundColor: AppCore.palette.app.secondBackgroundColor
            elevation: 2
            clipContent: true
            Behavior on Layout.maximumHeight { NumberAnimation { duration: 200 } }

            C.TextField{
                id: passwordField
                anchors.fill: parent
                anchors.topMargin: activeFocus || text !== "" ? 10 : 0
                floatingText: "Password"
                font.family: "Book Antiqua"
                placeholderTextColor: Qt.darker(AppCore.palette.text.textColor, 1.5)
                color: AppCore.palette.text.textColor
                hintColor: AppCore.palette.text.textColor
                echoMode : TextInput.Password
                Behavior on anchors.topMargin { NumberAnimation { duration: 200 } }
            }
        }

        Item{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.01
        }

        C.CheckBox{
            Layout.preferredHeight: parent.height * 0.035
            Layout.fillWidth: true
            Layout.maximumWidth: Math.min(parent.width * 0.8, 500)
            Layout.alignment: Qt.AlignHCenter
            text: "remember me?"
            checked: true
            font.family: "Book Antiqua"
            textColor:  AppCore.palette.text.textColor
            indicatorBorderColor: AppCore.palette.text.textColor
            indicatorRectColor: AppCore.palette.text.textColor
            indicatorMarkColor: AppCore.palette.app.secondBackgroundColor
        }

        Item{
            Layout.fillHeight: true
        }

        C.Button{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.05
            Layout.fillWidth: true
            Layout.maximumWidth: Math.min(parent.width * 0.8, 500)
            Layout.alignment: Qt.AlignHCenter
            text: "Login"
            font.family: "Book Antiqua"
            elevation: 2
            textColor: AppCore.palette.text.textColor
            buttonBackground: AppCore.palette.app.secondBackgroundColor
            onReleased: {
                if (AppCore.authService.login(loginTextField.text, passwordField.text)){
                    loginPopup.close()
                }else{

                }

            }
        }

        Item{
            Layout.fillHeight: true
            Layout.maximumHeight: parent.height * 0.05
        }
    }
}


