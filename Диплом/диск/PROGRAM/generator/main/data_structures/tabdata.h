#ifndef TABDATA_H
#define TABDATA_H

#include <klist.h>
#include <blockdata.h>
#include <utility/helper.h>
#include <utility/kobservablelist.h>
#include <data_structures/blockitemdata.h>

class TabsService;
class TabData: public KPoco
{
    Q_OBJECT
    K_QML(TabData)
    K_FIELD(int, index,-1)
    K_FIELD(QString, name,"")
    K_FIELD(bool, isEdited,false)
    K_FIELD(QString, type, "")
    K_FIELD(int, rows, 0)
    K_FIELD(QString, mainBlockName, "")
    K_FIELD(QString, mainBackend, "")
    K_FIELD(int, columns, 0)
    K_FIELD(bool, isReloaded, false)
    K_FIELD(KObservableModel*, blocks, nullptr)
    K_FIELD(bool, busy, true)
    K_FIELD(QVariantList, toolButtons, {})
    friend class TabsService;
public:
    TabData(QObject *parent = nullptr);
    void deserialize(const QVariantMap &map);
    BlockItemData *getBlockItemData(QString type);
    QVariantMap getTabData();
public slots:
    QVariant save();
    BlockData *getBlockData(QString type);
    void reload(QString listing);
    QVariant getDataFromBlock(QString blockName, QString tabName = "");
    void setDataToBlock(QString blockName, QString tabName, QVariant value);
private:
    void setTabData(const QVariantMap &data);
    KObservableList<BlockItemData> m_blocksList;
    QMap<QString, BlockData*> m_pages;
};

#endif // TABDATA_H
