#include "tabdata.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDatabase>
#include <appcore.h>

TabData::TabData(QObject *parent): KPoco(parent)
{
    connect(this, &TabData::indexchanged, this, [this](){
        qDebug() << index();
    });
    m_blocks = m_blocksList.model();
    m_blocks->setParent(this);
}

void TabData::deserialize(const QVariantMap &map)
{
    if (m_blocksList.count() != 0){
        for (int i = m_blocksList.count(); i >= 0; i--){
            auto block = m_blocksList.takeAt(i);
            disconnect(block->blockData(), nullptr, this, nullptr);
        }
    }

    set_rows(map.value("rows", 0).toInt());
    set_toolButtons(map.value("toolButtons", "").toList());
    set_columns(map.value("columns", 0).toInt());
    set_mainBlockName(map.value("mainBlockName", "").toString());
    set_mainBackend(map.value("mainBackend", "").toString());
    set_isReloaded(map.value("isReloaded", "").toBool());
    QVariantList list = map.value("config", {}).toList();

    if (list.isEmpty()) return;

    for (const auto &l: list){
        auto b = new BlockItemData(this);
        b->setTabData(this);
        b->deserialize(l.toMap());
        m_blocksList.push_back(b);
        if (b->blockData() != nullptr){
            connect(b->blockData(), &BlockData::isEditedChanged, this, [this, b](){
                if (b->blockData()->isEdited()){
                    set_isEdited(true);
                }else{
                    bool r = false;
                    for (const auto &l: m_blocksList){
                        if (l->blockData()->isEdited()){
                            r = true;
                        }
                    }
                    set_isEdited(r);
                }
            });
            b->blockData()->setIsEdited(false);
        }
    }
}

BlockData *TabData::getBlockData(QString type)
{
    BlockData *page = nullptr;
    if (m_pages.contains(type)){
        page = m_pages[type];
    }else{
        page = AppCore::instance()->createBlockData(type);
        if (page != nullptr){
            page->setParent(this);
            m_pages.insert(type, page);
        }
    }
    return page;
}

BlockItemData *TabData::getBlockItemData(QString type)
{
    for (const auto &bi : m_blocksList){
        if (bi->blockName() == type){
            return bi;
        }
    }
    return nullptr;
}

QVariantMap TabData::getTabData()
{
    QVariantList list;
    for (const auto &bi : m_blocksList){
        list.push_back(QVariantMap{
                           {"blockName", bi->blockName()},
                           {"listing", bi->blockData()->listingName()},
                           {"backend", bi->backend()},
                           {"blockData", bi->blockData()->serialize()}
                       });
    }
    return {
        {  "tabName", name() },
        {  "tabType", type() },
        {  "blocks",  list   }
    };
}

void TabData::setTabData(const QVariantMap &data)
{
    QVariantList list = data.value("blocks", "").toList();
    set_type(data.value("tabType", "").toString());
    set_name(data.value("tabName", "").toString());
    QStringList backends;
    for (const auto &d: list){
        auto dat = d.toMap();
        QString backend = dat.value("backend", "").toString();
        if (!backends.contains(backend)){
            backends.push_back(backend);
            if (m_pages.contains(backend)){
                m_pages[backend]->setIsCached(true);
                m_pages[backend]->deserialize(dat.value("blockData", ""));
                m_pages[backend]->setIsEdited(true);
            }
        }
    }
}

QVariant TabData::save()
{
    //! TODO realisation;
    QVariantList list;
    QStringList backendNames;
    BlockItemData *mainBlock = nullptr;
    //QVariantMap propertyBlock;
    for (const auto &bi : m_blocksList){
        qDebug()<< "save bi = " <<bi->blockName();
        qDebug()<< "type = " <<bi->blockData();
        qDebug()<< "component = " <<bi->component();
        if (bi->blockName() == mainBlockName()){
            mainBlock = bi;
            continue;
        }
        if (!backendNames.contains(bi->backend()) && bi->backend() != mainBackend()){
            backendNames.push_back(bi->backend());
            list.push_back(QVariantMap{
                               {"blockName", bi->blockName()},
                               {"result", bi->blockData()->save()},
                               {"serealizedData", bi->blockData()->serialize()}
                           });
        }
    }

    mainBlock->blockData()->save(list);

    return list;
}

void TabData::reload(QString listing)
{
    if (isReloaded()) AppCore::instance()->listingService()->updateListing(listing);
}

QVariant TabData::getDataFromBlock(QString blockName, QString tabName)
{
    return AppCore::instance()->getDataFromBlock(tabName == "" ? this->name() : tabName, blockName);
}

void TabData::setDataToBlock(QString blockName, QString tabName, QVariant value)
{
    AppCore::instance()->setDataToBlock(tabName, blockName, value);
}



