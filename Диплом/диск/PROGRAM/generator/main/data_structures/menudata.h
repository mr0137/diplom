#ifndef MENUDATA_H
#define MENUDATA_H

#include <QObject>
#include <QVariantList>
#include <kpoco.h>
#include <klist.h>
#include <utility/helper.h>
#include <utility/kobservablelist.h>
#include <kmacro.h>

class MenuTabsService;

class MenuData : public QObject
{
    Q_OBJECT
    K_QML(MenuData)
    K_READONLY_PROPERTY(KObservableModel*, submenu, submenu, setSubmenu, modelChanged, nullptr)
    K_READONLY_PROPERTY(QString, code, code, setCode, codeChanged, "")
    K_READONLY_PROPERTY(QString, title, title, setTitle, titleChanged, "")
    friend class MenuTabsService;
    public:
        explicit MenuData(QObject *parent = nullptr);
        void appendChilds(QList<MenuData*> menus){ m_submenuList.append(menus); }
private:
        KObservableList<MenuData> m_submenuList;
};

#endif // MENUDATA_H
