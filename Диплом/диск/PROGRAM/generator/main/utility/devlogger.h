#ifndef DEVLOGGER_H
#define DEVLOGGER_H

#include <QObject>
#include <kmacro.h>
#include <utility/kobservablelist.h>
#include <QElapsedTimer>
#include <models/kflexiblemodel.h>

class QTimer;
class DevLogger : public QObject
{
    Q_OBJECT
    K_SINGLETON(DevLogger)
    K_QML_SINGLETON(DevLogger)
    K_READONLY_PROPERTY(QString, logText, logText, setLogText, logTextChanged, "")
    K_AUTO_PROPERTY(bool, available, available, setAvailable, availableChanged, true)
    explicit DevLogger(QObject *parent = nullptr);
public:
    ~DevLogger(){ m_file->close(); }
    void addLog(QString category, QString functionName, QString msg);
signals:
    void addLogSignal(QString category, QString functioName, QString msg);
private:
    void proceed();
    QFile *m_file;
};

#endif // DEVLOGGER_H

