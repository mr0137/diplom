#include "loadpropertiesblock.h"

LoadPropertiesBlock::LoadPropertiesBlock(QObject *parent) : BlockData(parent)
{

}

bool LoadPropertiesBlock::save(QVariantList result)
{

}

QVariant LoadPropertiesBlock::serialize()
{
    return QVariantMap{
        { "dateFrom",   dateFrom()  },
        { "dateTo",     dateTo()    }
    };
}

void LoadPropertiesBlock::deserialize(const QVariant &data, ITableDataProvider *listing)
{

}

const QDateTime &LoadPropertiesBlock::dateFrom() const
{
    return m_dateFrom;
}

void LoadPropertiesBlock::setDateFrom(const QDateTime &newDateFrom)
{
    if (m_dateFrom == newDateFrom)
        return;
    m_dateFrom = newDateFrom;
    emit dateFromChanged();
}

const QDateTime &LoadPropertiesBlock::dateTo() const
{
    return m_dateTo;
}

void LoadPropertiesBlock::setDateTo(const QDateTime &newDateTo)
{
    if (m_dateTo == newDateTo)
        return;
    m_dateTo = newDateTo;
    emit dateToChanged();
}
