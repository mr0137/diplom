#include "menuchooser.h"

MenuChooser::MenuChooser(QQuickItem *parent) : QQuickItem(parent)
{
    QMetaObject::invokeMethod(this, [this](){
        QString type = "item";
        auto modelData = qmlContext(this)->contextProperty("modelData");
        auto object = modelData.value<QObject*>();
        QString code = "";
        if(object) {
            for (int i = 0; i < object->metaObject()->propertyCount(); i++){
                auto property = object->metaObject()->property(i);
                if (qstrcmp("code", property.name()) == 0){
                    code = property.read(object).toString();
                    type = code != "" ? "item" : "menu";
                    //qDebug() << "type" << type;
                }else if (qstrcmp("title", property.name()) == 0){
                    //qDebug() << property.read(object).toString();
                    break;
                }
            }
        }
        for(const auto &d : qAsConst(m_menus)) {
            //qDebug() << d;
            auto m = d.toMap();
            if(m.keys().contains(type)) {
                QQmlComponent *component = qvariant_cast<QQmlComponent*>(m[type]);
                if (code != ""){
                    //qDebug() << "via code";
                    if (m_ownComponent != nullptr) m_ownComponent->deleteLater();
                    m_ownComponent = new QQmlComponent(qmlEngine(this), this);
                    m_ownComponent->setData("import QtQuick 2.15; import QtQuick.Controls 2.15; import App 1.0; " + code.toLatin1(), QUrl());
                    setCurrentComponent(m_ownComponent);
                    break;
                }else if (component){

                    setCurrentComponent(component);
                    break;
                }
            }
        }
        //qDebug() << currentComponent() << (currentComponent() == nullptr);
    }, Qt::QueuedConnection);
}

//QQmlListProperty<QVariantMap> MenuChooser::menus()
//{
//    return QQmlListProperty<QVariantMap>(this, &m_menus);
//}

QObject *MenuChooser::currentMenu() const
{
    return m_currentMenu;
}

void MenuChooser::setCurrentMenu(QObject *newCurrentMenu)
{
    if (m_currentMenu == newCurrentMenu)
        return;
    m_currentMenu = newCurrentMenu;
    emit currentMenuChanged();
}

QVariantList MenuChooser::menus()
{
    return m_menus;
    //return QQmlListProperty<QVariantMap>(this, &m_menus);
}

void MenuChooser::setMenus(const QVariantList &newMenus)
{
    if (m_menus == newMenus)
        return;
    m_menus = newMenus;
    emit menusChanged();
}

QQuickItem *MenuChooser::menuParent() const
{
    return m_menuParent;
}

void MenuChooser::setMenuParent(QQuickItem *newMenuParent)
{
    if (m_menuParent == newMenuParent)
        return;
    m_menuParent = newMenuParent;
    emit menuParentChanged();
}

QQmlComponent *MenuChooser::currentComponent() const
{
    return m_currentComponent;
}

void MenuChooser::setCurrentComponent(QQmlComponent *newCurrentComponent)
{
    if (m_currentComponent == newCurrentComponent)
        return;
    m_currentComponent = newCurrentComponent;
    emit currentComponentChanged();
}
