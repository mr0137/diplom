#include "devlogger.h"

#include <QDateTime>
#include <QDir>
#include <QGuiApplication>
#include <QTimer>

DevLogger::DevLogger(QObject *parent) : QObject(parent)
{
    m_file = new QFile(QDateTime::currentDateTime().toString("hh_mm_ss") + "_log.txt", this);

    if (!m_file->open(QIODevice::WriteOnly | QIODevice::Append)){
        qDebug() << m_file->errorString();
        exit(-1);
    }

    connect(this, &DevLogger::addLogSignal, this, [this](QString category, QString functionName, QString msg){
        while(msg.contains("\033[")){
            int idx = msg.indexOf("\033[");
            msg.remove(idx, 5);
        }
        while (m_logText.count("<br>") > 50){
            int idx = m_logText.indexOf("<br>");
            m_logText.remove(0, idx + 4);
        }
        QString txt = "<font color=\"#e94040\">" + category + "</font><font color=\"#8ECECB\">[" + functionName + "]</font>: </font><font color=\"#1f2936\">" + msg + "</font><br>";
        QString txt2 = (category == "default" ? "" : category) + " [" + functionName + "]: " + msg;
        QTextStream textStream(m_file);
        QString dt = QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss");
        textStream << QString("[%1] ").arg(dt) << txt2 << Qt::endl;

        setLogText(m_logText + txt);
    });
}

void DevLogger::addLog(QString category, QString functionName, QString msg)
{
    if (available())
        emit addLogSignal(category, functionName, msg);
}

