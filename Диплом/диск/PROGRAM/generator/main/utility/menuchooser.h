#ifndef MENUCHOOSER_H
#define MENUCHOOSER_H

#include <QQuickItem>
#include <utility/helper.h>
//#include <utility/menudata.h>
#include <QQmlListProperty>
#include <kmacro.h>
#include <QVariantMap>

class MenuChooser : public QQuickItem
{
    Q_OBJECT
    K_QML(MenuChooser)
    Q_PROPERTY(QVariantList menus READ menus WRITE setMenus NOTIFY menusChanged)
    Q_PROPERTY(QQuickItem* menuParent READ menuParent WRITE setMenuParent NOTIFY menuParentChanged)
    Q_PROPERTY(QQmlComponent* currentComponent READ currentComponent WRITE setCurrentComponent NOTIFY currentComponentChanged)
    //Q_PROPERTY(QQmlListProperty<QVariantMap> menus READ menus NOTIFY menusChanged)
    //K_LIST_PROPERTY(QVariantMap, menus, m_menus)
    Q_PROPERTY(QObject* currentMenu READ currentMenu WRITE setCurrentMenu NOTIFY currentMenuChanged)

public:
    MenuChooser(QQuickItem *parent = nullptr);
    QObject *currentMenu() const;
    void setCurrentMenu(QObject *newCurrentMenu);

    void setMenus(const QVariantList &newMenus);

    QQuickItem *menuParent() const;
    void setMenuParent(QQuickItem *newMenuParent);

    QQmlComponent *currentComponent() const;
    void setCurrentComponent(QQmlComponent *newCurrentComponent);

public slots:
    QVariantList menus();

signals:
    void menusChanged();
    void currentMenuChanged();

    void menuParentChanged();

    void currentComponentChanged();

private:
    //QList<QVariantMap*> m_menus;
    QObject *m_currentMenu = nullptr;
    QVariantList m_menus;
    QQuickItem *m_menuParent = nullptr;
    QQmlComponent *m_currentComponent = nullptr;
    QQmlComponent *m_ownComponent = nullptr;
};

#endif // MENUCHOOSER_H
