#include "appcore.h"
#include <databaseservice.h>
#include <QtConcurrent>
#include <QThread>
#include <QSettings>
#include <EmfParser.h>
#include <QPainter>
#include <QEmfRenderer.h>
#include <QTimer>

AppCore::AppCore(QObject *parent) : QObject(parent)
{
    m_printerService = new PrinterService(this);
    m_files = new FileService(this);

    /*! AbCd1
     *  230Gb
     *  A012C
     *  093sc
     */
    //m_image = m_printerService->encode("AbCd1230GbA012C093s*AbCd1230GbA012C093sc", 512, 512);
    //m_image = m_printerService->encode("abcdefghijklmnopqrstuvwxyz", 512, 512);
    //m_image = m_printerService->encode("Hello World!Hello World!Hello World!Hello World!Hello World!", 512, 512);
    m_image.save("test.png", "PNG");
    m_test = new GeneratorTest(this);

    //m_timer = new QTimer(this);
    //connect(m_timer, SIGNAL(timeout()), this, SLOT(showGPS()));
    //m_timer->start(5000);
    m_workerThread = new QThread(this);
    m_nrp = new NrpParser::NrpParser();
    m_results = m_resultsList.model();
    m_results->setParent(this);

    connect(this, &AppCore::parsingChanged, this, [this](){
        if (!parsing()) generateFilters();
    });

    connect(m_printerService, &PrinterService::progressChanged, this, [this](double update){
        setLoadingProgress(update);
    });

    QTimer::singleShot(1000, this, &AppCore::disselectAll);
}

AppCore *AppCore::instance()
{
    static AppCore * m_AppCore;
    if(m_AppCore == nullptr){
        m_AppCore = new AppCore(qApp);
    }
    return m_AppCore;
}

void AppCore::setPathFile(QString path)
{
    if(path.isEmpty()) return;

    path.replace("\\","/");

    m_files->setFile(path);


    if(m_files->ext() != "nrp2" || parsing()) return;
    qDebug() << "future1" << path;
    m_future = QtConcurrent::run([this, path](){
        setParsing(true);
        qDebug() << "concurrentRun";
        auto db = DatabaseService::createConnection("Produce");
        bool ok = false;
        QVariantList list;

        int file_id = checkFile(db);
        if(file_id > 0){
            checkPathFile(db,file_id,path);
            m_nrp->load(path);
        }else {
            file_id = saveFile(db,path);
        }
        if(file_id > 0){
            ok = saveOrderBarcode(db,file_id,list);
        }

        m_files->setId(file_id);

        if(ok){
            db.commit();
        }else{
            db.rollback();
        }

        DatabaseService::closeConnection("Produce");
        m_printerService->drawLables(list);
        setParsing(false);
    });
}

QObject *AppCore::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(scriptEngine)
    Q_UNUSED(engine)
    return AppCore::instance();
}

void AppCore::setRootPath(QString path)
{
#ifdef QT_NO_DEBUG
    int times = 1;
#else
#ifdef Q_OS_LINUX
    int times = 3;
#endif
#ifdef Q_OS_WINDOWS
    int times = 4;
#endif
#endif
    while(times > 0 && !path.isEmpty()){
#ifdef Q_OS_LINUX
        if (path.endsWith("/")) times--;
#endif
#ifdef Q_OS_WINDOWS
        if (path.endsWith("\\")) times--;
#endif
        path.chop(1);
    }
    setRootDir(path);
}

QString AppCore::rootDir() const
{
    return m_rootDir;
}

PrinterService *AppCore::printerService() const
{
    return m_printerService;
}

void AppCore::setRootDir(QString rootDir)
{
    if (m_rootDir == rootDir)
        return;

    m_rootDir = rootDir;
    emit rootDirChanged(m_rootDir);
}

void AppCore::openPrinterDialog()
{
    m_printerService->print();
}

void AppCore::setPrinterService(PrinterService *printerService)
{
    if (m_printerService == printerService)
        return;

    m_printerService = printerService;
    emit printerServiceChanged(m_printerService);
}

void AppCore::selectResults(int from, int to)
{
    for (int i = 1; i <= m_resultsList.count(); i++){
        if (i >= from && i <= to){
            m_resultsList.get(i-1)->setChoosed(true);
        }else{
            m_resultsList.get(i-1)->setChoosed(false);
        }
    }
}

void AppCore::disselectAll()
{
    for (int i = 0; i < m_resultsList.count(); i++)
        m_resultsList.get(i)->setChoosed(false);
}

void AppCore::updateGeneratedLabels()
{
    //! accepted
    QStringList list;

    for (auto *r: m_resultsList){
        r->setChecked(r->choosed());
        if (r->checked()) list.append(r->name());
    }
    qDebug() << list;

    selectPartInfoOnFilter(list);

}

void AppCore::cancelSelection()
{
    for (int i = 0; i < m_resultsList.count(); i++)
        m_resultsList.get(i)->setChoosed(m_resultsList.get(i)->checked());
}

void AppCore::updatePage()
{
    QString full_path = m_files->path() + '\\' + m_files->name() + '.' + m_files->ext();
    setPathFile(full_path);
}

void AppCore::encode(QString text)
{
    setImage(m_printerService->encode(text, 512, 512));
    m_image.save("test.png", "PNG");
}

void AppCore::generateFilters()
{
    auto objs = m_nrp->getObjectFiles();
    for (int i = m_resultsList.count() - 1; i>= 0; i--)
        m_resultsList.takeAt(i)->deleteLater();

    for (const auto &obj : objs){
        if (obj.ext == "emf"){
            ResultData *res = new ResultData(this);
            QSize size = {600, 400};
            QImage img(size.width(), size.height(), QImage::Format_ARGB32);
            img.fill(QColor(Qt::gray));
            QPainter painter(&img);
            painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform );

            QString name = obj.path;
            int number = name.remove("Nest2D/Results/").remove("/").toInt();
            name = "Result" + QString::number(number);
            res->setName(name);
            res->setNumber(number);

            QEmf::QEmfRenderer renderer(painter, size, true);
            renderer.load(obj.bytes);
            res->setImg(img);
            m_resultsList.push_back(res);
        }
    }

    std::sort(m_resultsList.begin(), m_resultsList.end(), [](const ResultData *d1, const ResultData *d2){
        return d1->number() < d2->number();
    });

    qDebug() << m_resultsList.count();

    emit m_results->updateModel();
}

int AppCore::checkFile(QSqlDatabase &db)
{
    QSqlQuery query(db);

    query.prepare("SELECT id FROM [Produce].[dbo].[Xml_file] WHERE [Name] = :name AND [Ext] = :ext AND [Size] = :size AND [CRC] = :crc");
    query.bindValue(":name",m_files->name());
    query.bindValue(":ext",m_files->ext());
    query.bindValue(":size",m_files->size());
    query.bindValue(":crc",m_files->crc());

    if(!query.exec()){
        qDebug() << "error insert into ";
        qDebug() << query.lastError().text();
        return -1;
    }
    int id = 0;
    if (query.next()){
        id = query.record().value(0).toInt();
        //query.prepare("SELECT [Name] FROM [Produce].[dbo].[Xml_file] WHERE [id] = :id");
        //query.bindValue(":id", id);
        //if(!query.exec()){
        //    qDebug() << "error insert into ";
        //    qDebug() << query.lastError().text();
        //    return -1;
        //}
    }
    return id;
}

bool AppCore::checkPathFile(QSqlDatabase &db, const int id, const QString path)
{
    QSqlQuery query(db);
    query.prepare("SELECT top 1 id FROM [Produce].[dbo].[Xml_file_path] WHERE [Xml_file] = :file AND [path] LIKE :path ORDER BY [d_create] DESC");
    query.bindValue(":file",id);
    query.bindValue(":path",path);

    if(!query.exec()){
        qDebug() << "error insert into ";
        qDebug() << query.lastError().text();
        return false;
    }

    query.next();
    if(query.record().value(0).toBool()){
        return true;
    }else{
        query.prepare("INSERT INTO [Produce].[dbo].[Xml_file_path] ([Xml_file], [path], [d_create])"
        "VALUES (:file,:path,:date)");
        query.bindValue(":file",id);
        query.bindValue(":path",path);
        query.bindValue(":date",QDateTime::currentDateTime());
        if(!query.exec()){
            qDebug() << "error insert into ";
            qDebug() << query.lastError().text();
            return false;
        }
    }
    return true;
}

int AppCore::saveFile(QSqlDatabase &db,QString path)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO [Produce].[dbo].[Xml_file] ([Name],[Ext],[Size],[D_create],[CRC],[Task],[D_load])"
                  " VALUES (:name,:ext,:size,:create,:crc,:task,GETDATE())");
    query.bindValue(":name",m_files->name());
    query.bindValue(":ext",m_files->ext());
    query.bindValue(":size",m_files->size());
    query.bindValue(":create",QDateTime::fromString(m_files->date(),"dd-MM-yyyy hh:mm:ss"));
    query.bindValue(":crc",m_files->crc());
    query.bindValue(":task","");

    if(!query.exec()){
        qDebug() << "error insert into ";
        qDebug() << query.lastError().text();
        return -1;
    }

    int file_id = query.lastInsertId().toInt();
    if(!checkPathFile(db,file_id,path)) return -1;

    m_nrp->load(path);
    if(!insertUpackedFile(db,m_nrp->getObjectFiles(),file_id)) return -1;

    // if(!query.exec("EXEC [dbo].[File_Xml_Parser]")){
    //     qDebug() << "error insert into ";
    //     qDebug() << query.lastError().text();
    //     return -1;
    // }

    return file_id;
}

bool AppCore::insertUpackedFile(QSqlDatabase &db,QVector<NrpParser::ObjectFile> vFile, const int file_id)
{
    if(vFile.isEmpty())return false;

    QSqlQuery query(db);

    for(const auto &x: vFile){
        query.prepare("INSERT INTO [Produce].[dbo].[Xml_content] ([Xml_file],[Path],[Name],[Extension],[Content_XML],[Content_bin],[D_create])"
                      " VALUES (:file,:path,:name,:ext,:xml,:bin,:date)");
        query.bindValue(":file",  file_id);
        query.bindValue(":path",  x.path);
        query.bindValue(":name",  x.name);
        query.bindValue(":ext",   x.ext);
        query.bindValue(":xml",   x.xml);
        query.bindValue(":bin",   x.bytes);
        query.bindValue(":date",  QDateTime::currentDateTime());

        if(!query.exec()){
            qDebug() << "error insert [Produce].[dbo].[Xml_contents] !!!";
            qDebug() << query.lastError().text();
            return false;
        }
    }
    return true;
}

bool AppCore::saveOrderBarcode(QSqlDatabase &db, const int file_id,QVariantList &list)
{
    QSqlQuery query(db);
    //QSqlQuery query = db.exec("EXEC [dbo].[File_Xml_Parser]");

    //db.exec("EXEC [dbo].[File_Xml_Parser]");
    query.prepare("SELECT id FROM [Produce].[dbo].[Xml_nest] WHERE [Xml_file] = :file_id");
    query.bindValue(":file_id",file_id);
    if(!query.exec()){
        qDebug() << "error SELECT [Produce].[dbo].[Xml_nest] !!!";
        qDebug() << query.lastError().text();
        return false;
    }
    query.next();
    if(query.record().value(0).toInt() == 0){
        query.prepare("EXEC [dbo].[Nrp2_Parser] @File_id = :file_id");
        query.bindValue(":file_id",file_id);
        if(!query.exec(/*"EXEC [dbo].[File_Xml_Parser]"*/)){
            qDebug() << "error EXEC [Produce].[dbo].[File_Xml_Parser] !!!";
            qDebug() << query.lastError().text();
            return false;
        }
    }

    query.prepare("SELECT parts.[id], barcode.[xml_part], barcode.[id] FROM [Produce].[dbo].[Xml_part] as parts"
                   " LEFT JOIN [Produce].[dbo].[Order_Barcode] as barcode ON parts.[id] = barcode.[xml_part]"
                   " WHERE parts.[Xml_file] = :file");
    query.bindValue(":file",file_id);
    if(!query.exec()){
        qDebug() << "error SELECT [Produce].[dbo].[Xml_part] !!!";
        qDebug() << query.lastError().text();
        return false;
    }
    QVector<int> insert_pats_id;
    QMap<int,int> barcode_parts;
    QSet<int> ret_insert_barcode;
    QString q_id;
    while (query.next()) {
        if(query.record().value(1).isNull()){
            insert_pats_id.push_back(query.record().value(0).toInt());
        }else{
            ret_insert_barcode.insert(query.record().value(2).toInt());
            q_id += query.record().value(0).toString() + ',';
        }
    }

    for(const auto &x:insert_pats_id){
        query.prepare("INSERT INTO [Produce].[dbo].[Order_Barcode] ([xml_part])"
                       " VALUES (:xml_part)");
        query.bindValue(":xml_part",x);
        if(!query.exec()){
            qDebug() << "error INSERT [Produce].[dbo].[Order_Barcode] !!!";
            qDebug() << query.lastError().text();
            return false;
        }
        ret_insert_barcode.insert(query.lastInsertId().toInt());
        q_id += QString::number(x) + ',';
    }


    QString q_query = "SELECT *,("
    " SELECT  "
    " STRING_AGG( tmp_list.[Result],',')"
    " FROM [Test_View_info_for_part_lable] as tmp_list where tmp_list.part_id = [Test_View_info_for_part_lable].part_id"
    ") AS results FROM [Produce].[dbo].[Test_View_info_for_part_lable]"
    " WHERE [part_id] IN (";
    if(q_id.isEmpty())return false;

    q_id.replace(q_id.count() - 1,1,")");
    q_query += q_id;
    q_query += " ORDER BY [Result];";
    query.prepare(q_query);
    //qDebug() << q_query;

    if(!query.exec()){
        qDebug() << "error SELECT [Produce].[dbo].[View_info_for_part_lable] !!!";
        qDebug() << query.lastError().text();
        return false;
    }

    QVariantMap chackMap;
    QVariantList l_list;
    auto db_dxf = DatabaseService::createConnection("Dxf_file");
    while (query.next()) {

        if(chackMap.contains(query.record().value(6).toString())){
            auto p = chackMap[query.record().value(6).toString()].toMap();
            qDebug()<<"count = "<<p["count"].toInt()<<" "<<query.record().value(11).toInt();
            int c = p["count"].toInt() + (1 * query.record().value(11).toInt());

            p["count"] = c;

            if(p["result"].toString().count(query.record().value(10).toString()) == 0){
                p["result"] = QString(p["result"].toString() + "," + query.record().value(10).toString());
            };
            chackMap[query.record().value(6).toString()] = p;
            continue;
        }
        QVariantMap part;
        QStringList dec_name;
        QString dec,name;
        auto foul_name = query.record().value(0).toString();
        if(foul_name.count() < 23){
            dec = foul_name;
        }else if(foul_name.count('(')){
            dec_name = foul_name.split('(');
        }else if(foul_name.count(' ')){
            dec_name = foul_name.replace('_',' ').split(' ');
        }else{
            dec = foul_name;
        }

        // auto odec_name = query.record().value(0).toString().replace('_',' ').split(' ');
        for(int i = 0; i < dec_name.count(); ++i){
            if(dec_name[i] == ' ' || dec_name[i] == '_' || dec_name[i] == '-' || dec_name[i] == '(') continue;
            if(i == 0){
                dec =  dec_name[i];
            }else{
                name =  dec_name[i];
                name.remove('(');
                name.remove(')');
                break;
            }
        }
        auto id = query.record().value(7).toString();

        while(id.length() < 17){
            //id = "0" + id;
            id.push_front('0');
        }

        //qDebug()<<"full - "<< query.record().value(14).toString();
        auto str_l = query.record().value(17).toString().split(',');
        qDebug()<<query.record().value(17).toString();
        str_l.removeDuplicates();
        std::sort(str_l.begin(),str_l.end(),[](QString &rhs, QString &lhs){
            return  rhs.toInt() < lhs.toInt();
        });
        auto str_res = str_l.join(',');
        //qDebug()<<" remote ->"<<str_res;
        part["id"] = id;
        part["name"] = name;
        part["dec"] = dec;
        part["material"] = query.record().value(1).toString();
        part["thricnes"] = query.record().value(3).toString();
        part["count"] = query.record().value(5).toString();
        part["order_num"] = query.record().value(4).toString();
        part["customer"] = "";
        part["size"] = QString::number(query.record().value(8).toInt()) + " x " + QString::number(query.record().value(9).toInt());
        part["result"] = str_res;
        part["file_bit"] =  selectDxfFile(query.record().value(6).toInt(),db_dxf);
        chackMap[query.record().value(6).toString()] = part;
        //list.push_back(part);

    }

    for(const auto &x:qAsConst(chackMap)){
        list.append(x);
    }

    DatabaseService::closeConnection("Dxf_file");
    return true;
}

void AppCore::selectPartInfoOnFilter(QStringList list)
{
    qDebug() << "future2";
    m_future = QtConcurrent::run([this, list](){
        if(list.empty()) return;
        setParsing(true);
        QString q = "SELECT * FROM [Produce].[dbo].[Test_View_info_for_part_lable] WHERE [R_name] IN (";

        for(const auto &x: list){
            q += "'" + x + "',";
        }

        q.replace(q.count() - 1,1,')');
        q += " AND [f_id] = " + QString::number(m_files->id()) + " ORDER BY [Result]";
        qDebug()<<q;
        auto db = DatabaseService::createConnection("Produce");
        QSqlQuery query(db);
        query.prepare(q);
        if(!query.exec()){
            qDebug() << "error SELECT [Produce].[dbo].[View_info_for_part_lable] !!!";
            qDebug() << query.lastError().text();
            return ;
        }
        QVariantMap map;
        auto db_dxf = DatabaseService::createConnection("Dxf_file");
        while(query.next()){

            if(map.contains(query.record().value(6).toString())){
                auto p = map[query.record().value(6).toString()].toMap();
                qDebug()<<"count = "<<p["count"].toInt()<<" "<<query.record().value(11).toInt();
                int c = p["count"].toInt() + (1 * query.record().value(11).toInt());

                p["count"] = c;

                if(p["result"].toString().count(query.record().value(10).toString()) == 0){
                    p["result"] = QString(p["result"].toString() + "," + query.record().value(10).toString());
                };
                map[query.record().value(6).toString()] = p;
                continue;
            }

            QVariantMap part;
            QStringList dec_name;
            QString dec,name;
            auto foul_name = query.record().value(0).toString();
            if(foul_name.count('(')){
                dec_name = foul_name.split('(');
            }else if(foul_name.count(' ')){
                dec_name = foul_name.replace('_',' ').split(' ');
            }else{
                dec = foul_name;
            }

            for(int i = 0; i < dec_name.count(); ++i){
                if(dec_name[i] == ' ' || dec_name[i] == '_' || dec_name[i] == '-' || dec_name[i] == '(') continue;
                if(i == 0){
                    dec =  dec_name[i];
                }else{
                    name =  dec_name[i];
                    name.remove('(');
                    name.remove(')');
                    break;
                }
            }
            auto id = query.record().value(7).toString();

            while(id.length() < 9){
                //id = "0" + id;
                id.push_front('0');
            }

            part["id"] = id;
            part["id_part"] = query.record().value(6).toInt();
            part["name"] = name;
            part["dec"] = dec;
            part["material"] = query.record().value(1).toString();
            part["thricnes"] = query.record().value(3).toString();
            part["count"] = QString::number(1 * query.record().value(11).toInt());
            part["order_num"] = query.record().value(4).toString();
            part["customer"] = "";
            part["file_bit"] = selectDxfFile(query.record().value(6).toInt(),db_dxf);
            part["size"] = QString::number(query.record().value(8).toInt()) + " x " + QString::number(query.record().value(9).toInt());
            part["result"] = query.record().value(10).toString();
            map[query.record().value(6).toString()] = part;
        }

        QVariantList ll;
        for(const auto &x:qAsConst(map)){
            ll.append(x);
        }

        m_printerService->drawLables(ll);
        DatabaseService::closeAllConnections("Dxf_file");
        DatabaseService::closeAllConnections("Produce");
        setParsing(false);
    });
}

QByteArray AppCore::selectDxfFile(const int part_id, QSqlDatabase &db)
{
    QSqlQuery query(db);
    query.prepare("SELECT top 1 * FROM [Produce].[dbo].[View_dxf_file_to_xml_file] WHERE id = :id");
    query.bindValue(":id",part_id);
    if(!query.exec()){
        qDebug() << "error SELECT [Produce].[dbo].[View_dxf_file_to_xml_file] !!!";
        qDebug() << query.lastError().text();
        return {};
    }

    query.next();

    if(query.record().value(0).isNull()){
        // FileService f;
        // f.setFile(query.record().value(2).toString());
        return FileService::getByteFile(query.record().value(2).toString());
        //  return f.bits();
    }else{
        return qUncompress(query.record().value(0).toByteArray());
    }
}

FileService *AppCore::files() const
{
    return m_files;
}

void AppCore::setFiles(FileService *files)
{
    m_files = files;
}

KObservableModel *AppCore::results() const
{
    return m_results;
}

void AppCore::setResults(KObservableModel *newResults)
{
    if (m_results == newResults)
        return;
    m_results = newResults;
    emit resultsChanged();
}

bool AppCore::parsing() const
{
    return m_parsing;
}

void AppCore::setParsing(bool newParsing)
{
    if (m_parsing == newParsing)
        return;
    m_parsing = newParsing;
    emit parsingChanged();
}

double AppCore::loadingProgress() const
{
    return m_loadingProgress;
}

void AppCore::setLoadingProgress(double newLoadingProgress)
{
    if (qFuzzyCompare(m_loadingProgress, newLoadingProgress))
        return;
    m_loadingProgress = newLoadingProgress;
    emit loadingProgressChanged();
}

const QString &AppCore::loadingText() const
{
    return m_loadingText;
}

void AppCore::setLoadingText(const QString &newLoadingText)
{
    if (m_loadingText == newLoadingText)
        return;
    m_loadingText = newLoadingText;
    emit loadingTextChanged();
}

const QImage &AppCore::image() const
{
    return m_image;
}

void AppCore::setImage(const QImage &newImage)
{
    if (m_image == newImage)
        return;
    m_image = newImage;
    emit imageChanged();
}

GeneratorTest *AppCore::test() const
{
    return m_test;
}

void AppCore::setTest(GeneratorTest *newTest)
{
    if (m_test == newTest)
        return;
    m_test = newTest;
    emit testChanged();
}
