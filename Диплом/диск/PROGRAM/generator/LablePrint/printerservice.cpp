#include "printerservice.h"

#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QDebug>
#include <QPageLayout>
#include <QPageSize>

#include <backend-qt/dxfinterface.h>

PrinterService::PrinterService(QObject *parent) : QObject(parent)
{
    m_printer = new QPrinter(QPrinter::HighResolution);
    m_codeGen = new QZint();

}

void PrinterService::setImage()
{

}

void PrinterService::openDialogPrinter(QVariantList list)
{
    drawLables(list);
}

void PrinterService::print()
{
    QPrinter printer(QPrinter::HighResolution);
    printer.setPageOrientation(QPageLayout::Landscape);

    QPrintDialog printDialog(&printer);
    QPageSize pz(QPageSize::A4);
    printer.setPageSize(pz);
    printDialog.setFromTo(0,(int)m_images.length()/16 + 1);

    if (printDialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        qDebug() << printer.width() << printer.height();
        for (int i = 0; i < m_images.length(); i++){
            auto img = qvariant_cast<QImage>(m_images[i]);
            painter.drawImage(QRectF(0,0, printer.width(), printer.height()), img);
            if (i < m_images.length() - 1)
                printer.newPage();
        }
        painter.end();
    }
}

void PrinterService::drawLables(QVariantList list)
{
    std::sort(list.begin(), list.end(), [](QVariant &m1, QVariant &m2){
        QString v1, v2;
        v1 = m1.toMap()["result"].toString().split(',')[0].remove("Result");
        v2 = m2.toMap()["result"].toString().split(',')[0].remove("Result");
        return v1.toInt() < v2.toInt();
    });

    m_images.clear();
    //QFile log("log" + QDateTime::currentDateTime().toString("_dd.MM.yyyy_hh.mm.ss") + ".txt");
    //log.open(QIODevice::WriteOnly);
    //QTextStream sstream(&log);

    int currentItem = 0;
    int sheetCount = (int)(list.length()/12);
    for (int k = 0; k <= sheetCount; k++){
        setImageSize(QRectF(0,0,1100 * m_scale, 740 * m_scale));
        QImage image(1100 * m_scale, 740 * m_scale, QImage::Format_ARGB32);
        image.fill(QColor("white"));

        QPainter painter(&image);
        painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
        for(int i = 0; i < 4; ++i){
            for(int j = 0; j < 3; ++j){
                //
                if (currentItem >= list.length()) break;
                QVariantMap part_info = list[currentItem++].toMap();

                int rect_w = 360 * m_scale;
                int rect_h = 170 * m_scale;
                int row_h = rect_h / 8;
                int plus_row = 10;
                int s_pos_x = (rect_w * j) + ( j * 5 * m_scale) + 10;
                int s_pos_y = (rect_h * i) + ( i * 5 * m_scale) - 15;
                // s_pos_y += plus_row;
                row_h += plus_row;
                QRect frame   = QRect(                   s_pos_x,            row_h + s_pos_y,           rect_w,      (rect_h - 5));
                QRect rect1   = QRect(                   s_pos_x,            row_h + s_pos_y,           rect_w,       row_h);
                QRect rect2   = QRect( (rect_w * 0.85) + s_pos_x,      (row_h * 2) + s_pos_y,    rect_w * 0.15,       row_h);
                QRect rect3   = QRect(                   s_pos_x,      (row_h * 2) + s_pos_y,    rect_w * 0.85,       row_h);
                s_pos_y += (plus_row * 4);
                row_h -= plus_row + 4;
                QRect rect4   = QRect(                   s_pos_x,      (row_h * 3) + s_pos_y,     rect_w * 0.5,       row_h);
                QRect rect4Im = QRect( (rect_w * 0.5)  + s_pos_x,      (row_h * 3) + s_pos_y,     rect_w * 0.5,       (row_h + 1) * 6);
                QRect rect5   = QRect(                   s_pos_x,      (row_h * 4) + s_pos_y,     rect_w * 0.3,       row_h);
                QRect rect6   = QRect( (rect_w * 0.3)  + s_pos_x,      (row_h * 4) + s_pos_y,     rect_w * 0.2,       row_h);
                //QRect rect7   = QRect( (rect_w * 0.75) + s_pos_x,      (row_h * 4) + s_pos_y,    rect_w * 0.25,       row_h);
                QRect rect8   = QRect(                   s_pos_x,      (row_h * 5) + s_pos_y,     rect_w * 0.5,       row_h);
                QRect rect9   = QRect(                   s_pos_x,      (row_h * 6) + s_pos_y,     rect_w * 0.5,       row_h);
                //QRect rect10  = QRect( (rect_w * 0.2) +  s_pos_x,      (row_h * 6) + s_pos_y,     rect_w * 0.4,       row_h);
                QRect rect11  = QRect(                   s_pos_x,      (row_h * 7) + s_pos_y,     rect_w * 0.5,       row_h);
                //QRect rect12  = QRect( (rect_w * 0.8) +  s_pos_x,      (row_h * 7) + s_pos_y,       rect_w * 0,       row_h * 0);
                QRect rect13  = QRect(                   s_pos_x,      (row_h * 8) + s_pos_y,     rect_w * 0.5,       row_h);

                //file.remove();
                //qDebug() << rect4Im ;
                QFont font = painter.font();
                QPen pen;
                pen.setWidth(1 * m_scale);
                painter.setPen(pen);
                font.setPixelSize(18 * m_scale);
                painter.setFont(font);
                painter.drawRect(rect1);
                QImage img = encode(part_info["id"].toString(), rect_w, row_h);
                painter.drawImage(rect1, img);
                //painter.drawText(rect1, Qt::AlignCenter,"Штрих код");
                painter.drawRect(rect2);
                painter.drawText(rect2, Qt::AlignCenter,"");
                painter.drawRect(rect3);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(rect3, Qt::AlignCenter, part_info["dec"].toString());
                painter.drawRect(rect4);
                font.setPixelSize(14 * m_scale);
                painter.setFont(font);
                painter.drawText(rect4, Qt::AlignCenter," Габ.: " + part_info["size"].toString());
                font.setBold(false);
                painter.setFont(font);

                //painter.drawText(rect4Im, Qt::AlignCenter, part_info["name"].toString());
                painter.drawRect(rect5);
                painter.drawText(rect5, Qt::AlignCenter, part_info["material"].toString());
                painter.drawRect(rect6);
                painter.drawText(rect6,Qt::AlignLeft | Qt::AlignVCenter," \u2260 ");
                painter.drawText(rect6,Qt::AlignCenter, part_info["thricnes"].toString());
                //painter.drawRect(rect7);
                //painter.drawText(rect7, Qt::AlignCenter,"");
                painter.drawRect(rect8);
                painter.drawText(rect8, Qt::AlignLeft | Qt::AlignVCenter," Листи: " + part_info["result"].toString());
                painter.drawRect(rect9);
                painter.drawText(rect9, Qt::AlignLeft | Qt::AlignVCenter," К-cть: " + part_info["count"].toString());
               // painter.drawRect(rect10);
                //painter.drawText(rect10, Qt::AlignLeft | Qt::AlignVCenter," Габ.: " + part_info["size"].toString());
                painter.drawRect(rect11);
                painter.drawText(rect11, Qt::AlignLeft | Qt::AlignVCenter," № Зам. " + part_info["order_num"].toString());
                // painter.drawRect(rect12);
                // painter.drawText(rect12, Qt::AlignLeft | Qt::AlignVCenter," " + part_info["customer"].toString());
                //painter.drawRect(rect13);
                painter.drawText(rect13, Qt::AlignLeft | Qt::AlignVCenter," Прим.: ");
                pen.setWidth(m_scale * 2);
                painter.setPen(pen);
                painter.drawRect(frame);
                pen.setWidth(m_scale * 1);
                painter.setPen(pen);

                QFile file("tmp.dxf");
                file.open(QIODevice::WriteOnly);

                QTextStream stream(&file);
                QByteArray arr = part_info["file_bit"].toByteArray();
                if (!arr.isEmpty()){
                    //arr = qUncompress(arr);
                    stream << arr;
                    img.fill(QColor(Qt::transparent));
                    file.close();
                    if (!arr.isEmpty()){
                        //qDebug() << "filename" << part_info["dec"].toString();
                        DXFInterface w("tmp.dxf");
                        //qDebug() << "line width" << w.lineWidth();
                        DXFInterface dxf("tmp.dxf", w.lineWidth());
                        img = dxf.getImg();
                        if (!img.isNull()){
                            painter.drawImage(QRect{rect4Im.x() + 2, rect4Im.y() + 2, rect4Im.width() - 2, rect4Im.height() - 2}, img);
                            //img.save(part_info["dec"].toString() + ".png", "PNG");
                        }else {
                            //sstream << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss") << part_info["dec"].toString() << "IMAGE IS NULL\n";
                            painter.drawText(QRect{rect4Im.x() + 2, rect4Im.y() + 2, rect4Im.width() - 2, rect4Im.height() - 2}, Qt::AlignCenter, "ERROR\nIMAGE IS NULL");
                        }
                    }else{
                        painter.drawText(QRect{rect4Im.x() + 2, rect4Im.y() + 2, rect4Im.width() - 2, rect4Im.height() - 2}, Qt::AlignCenter, "ERROR\nDATA CORRUPTED");
                    }
                }else{
                    painter.drawText(QRect{rect4Im.x() + 2, rect4Im.y() + 2, rect4Im.width() - 2, rect4Im.height() - 2}, Qt::AlignCenter, "NONE");
                }
                painter.drawRect(rect4Im);
                file.remove();
                emit progressChanged((double)(currentItem) / (double)(list.count()));
                if (currentItem == list.length()) {
                    m_images.push_back(image);
                    painter.end();
                    break;
                }
            }
        }

        if (currentItem < list.length()) {
            m_images.push_back(image);
            painter.end();
        }
    }
     emit progressChanged(0);
    //log.close();
    imagesChanged(m_images);
}

QImage PrinterService::encode(QString text, double width, double height)
{
    if (text == "") return QImage();
    QZint bc;

    QImage img(width, height, QImage::Format_ARGB32);
    img.fill(QColor("white"));

    bc.setHeight(height);
    bc.setWidth(width);
    bc.setBgColor("white");
    bc.setFgColor("black");
    bc.setHideText(true);
    bc.setBorderType(QZint::NO_BORDER);
    bc.setSymbol(BARCODE_CODE128EXT);

    bc.setText(text);
    QPainter painter(&img);
    bc.render(painter, QRectF(width * 0.05 ,0, width - width * 0.1, height)/*,QZint::AspectRatioMode::CenterBarCode*/);
   // qDebug() << bc.lastError() << text;
    emit codeChanged();
    return img;
}

const QImage &PrinterService::code() const
{
    return m_code;
}

void PrinterService::setCode(const QImage &newCode)
{
    if (m_code == newCode)
        return;
    m_code = newCode;
    emit codeChanged();
}
