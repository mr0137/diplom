import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0

Drawer {
    id: drawer
    interactive: false
    modal: true
    ColumnLayout{
        anchors.fill: parent
        spacing: 0
        clip: true

        Flickable{
            id: resultRect
            clip: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 10
            contentHeight: resultList.height
            contentWidth: resultRect.width
            flickableDirection: Qt.Vertical

            ScrollBar.vertical: ScrollBar {
                orientation: Qt.Vertical
                policy: ScrollBar.AsNeeded
                }


            Flow{
                id: resultList
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                spacing: 10

                Repeater{
                    id: repeater
                    model: AppCore.results
                    delegate: C.BaseBackground{
                        width: 200
                        height: 150
                        elevation: 3
                        backgroundColor: "white"

                        CodeItem{
                            anchors.fill: parent
                            source: modelData.img
                        }

                        Text{
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            text: modelData.name
                            font.bold: true
                            verticalAlignment: Text.AlignVCenter
                            font.pointSize: 25
                        }

                        Text{
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.rightMargin: 3
                            anchors.bottomMargin: 3
                            opacity: modelData.choosed ? 1 : 0
                            Behavior on opacity { NumberAnimation { duration: 200 } }
                            font.bold: true
                            horizontalAlignment: Text.AlignRight
                            text: "\uF058"
                            color: "green"
                            verticalAlignment: Text.AlignVCenter
                            font.family: C.IconHelper.iconsPressed.name
                            font.pointSize: 25
                        }

                        Rectangle{
                            anchors.fill: parent
                            opacity: modelData.choosed ? 0.4 : 0
                            Behavior on opacity { NumberAnimation { duration: 200 } }
                        }

                        C.WavedMouseArea{
                            anchors.fill: parent
                            onReleased: {
                                modelData.choosed = !modelData.choosed
                            }
                        }
                    }
                }
            }
        }

        RowLayout{
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.maximumHeight: 50
            Layout.bottomMargin: 10
            Layout.leftMargin: 10
            spacing: 10
            C.Button{
                Layout.preferredWidth: 200
                Layout.fillHeight: true
                text: "Cancel"
                onReleased: {
                    AppCore.cancelSelection()
                    drawer.close()
                }
            }
            C.Button{
                Layout.preferredWidth: 200
                Layout.fillHeight: true
                text: "Accept"
                onReleased: {
                    drawer.close()
                    AppCore.updateGeneratedLabels()
                }
            }
            Item{ Layout.fillWidth: true }

            C.Button{
                Layout.preferredWidth: 200
                Layout.fillHeight: true
                text: "Select"
                enabled: !toTextField.hasError && !fromtextField.hasError

                onReleased: {
                    AppCore.selectResults(parseInt(fromtextField.text), parseInt(toTextField.text))
                }
            }

            Text{
                text: "From:"
            }

            C.TextField{
                id: fromtextField
                Layout.preferredWidth: 100
                Layout.fillHeight: true
                text: "1"
                validator: RegExpValidator{
                    regExp: /[0-9]{3}/
                }
                showBorder: true
                hasError: parseInt(fromtextField.text) > repeater.count ||
                          parseInt(fromtextField.text) <= 0 ||
                          parseInt(toTextField.text) < parseInt(fromtextField.text) ||
                          fromtextField.text === ""
            }

            Text{
                text: "To:"
            }

            C.TextField{
                id: toTextField
                Layout.preferredWidth: 100
                Layout.fillHeight: true
                text: repeater.count
                validator: RegExpValidator{
                    regExp: /[0-9]{3}/
                }
                showBorder: true
                hasError: parseInt(toTextField.text) > repeater.count ||
                          parseInt(toTextField.text) <= 0 ||
                          parseInt(toTextField.text) < parseInt(fromtextField.text) ||
                          toTextField.text === ""
            }

            C.Button{
                Layout.preferredWidth: 200
                Layout.fillHeight: true
                text: "Desselect all"
                onReleased: AppCore.disselectAll()
            }
        }
    }
}
