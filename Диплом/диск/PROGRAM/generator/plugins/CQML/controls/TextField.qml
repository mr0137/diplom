import QtQuick 2.15
import QtQuick.Controls 2.15 as Q
import QtQuick.Layouts 1.15
import QtQuick.Shapes 1.15

//@disable-check M129
Q.TextField {
    id: control
    color: "green"
    property int mult: 1
    property int characterLimit
    property bool floatingLabel: true
    property bool hasError: characterLimit && length > characterLimit
    property color textColor: hintColor
    property bool showBorder: true
    property bool showFrame: false
    property real frameRadius: 3
    property string helperText: ""
    property string floatingText: ""
    property color errorColor: "red"
    property color hintColor: "green"
    property color frameBorderColor: control.hasError ? control.errorColor
                                                      : fieldPlaceholder.state === "floating" ? control.text !== ""
                                                                                              ? control.color : control.hintColor : "transparent"
    property color frameBackgroundColor: "white"
    hoverEnabled: true
    echoMode: TextInput.Normal
    font.pixelSize: height * 0.5
    selectByMouse: true
    implicitHeight: 30
    placeholderTextColor: "green"
    placeholderText: ""
    selectedTextColor: "white"
    selectionColor: control.hasOwnProperty("color") ? control.color : "blue"

    //textColor: "black"

    background : Item {
        id: background

        property color color: control.color
        property string helperText: control.hasOwnProperty("helperText") ? control.helperText : ""
        property bool floatingLabel: control.hasOwnProperty("floatingLabel") ? control.floatingLabel : ""
        property int characterLimit: control.hasOwnProperty("characterLimit") ? control.characterLimit : 0
        property bool showBorder: control.showBorder

        Rectangle{
            anchors.fill: parent
            color: "transparent"
            border.color: underline.color
            border.width: 1
            radius: control.frameRadius
            visible: control.showFrame
        }

        Rectangle {
            id: underline
            color: control.hasError ? control.errorColor
                                       : control.activeFocus ? background.color
                                                             : control.hintColor

            height: (control.activeFocus ? 2 : 1) * parent.height * 0.05
            visible: background.showBorder

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Behavior on color { ColorAnimation { duration: 100 } }
            Behavior on height { NumberAnimation { duration: 100 } }
        }

        Q.Label {
            id: fieldPlaceholder
            anchors.verticalCenter: parent.verticalCenter
            text: control.floatingText
            font.pixelSize: control.font.pixelSize
            anchors.margins: -font.pointSize * 0.58
            color: control.color
            renderType: Text.QtRendering
            width: paintedWidth + 6
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            height: paintedHeight - 0

            background: Rectangle{
                anchors.topMargin: 2
                anchors.bottomMargin: 0
                radius: 3
                visible: fieldPlaceholder.text !== ""
                color: fieldPlaceholder.state == "floating" ? control.frameBackgroundColor : "transparent"
                border.color: control.frameBorderColor
                border.width: 1
                Behavior on border.color { ColorAnimation { duration: 100 } }
            }

            states: [
                State {
                    name: "floating"
                    when: (control.displayText.length > 0 && background.floatingLabel) || (control.activeFocus && background.floatingLabel)
                    AnchorChanges {
                        target: fieldPlaceholder
                        anchors.verticalCenter: undefined
                        anchors.top: parent.top
                    }
                    PropertyChanges {
                        target: fieldPlaceholder
                        font.pixelSize: control.font.pixelSize * 0.7
                        font.bold: true
                    }
                },
                State {
                    name: "hidden"
                    when: control.displayText.length > 0 && !background.floatingLabel
                    PropertyChanges {
                        target: fieldPlaceholder
                        visible: false
                        font.bold: false
                    }
                }
            ]

            transitions: [
                Transition {
                    id: floatingTransition
                    enabled: false
                    AnchorAnimation {
                        duration: 100
                    }
                    NumberAnimation {
                        duration: 100
                        property: "font.pixelSize"
                    }
                }
            ]

            Component.onCompleted: floatingTransition.enabled = true
        }

        RowLayout {
            anchors {
                left: parent.left
                right: parent.right
                top: underline.top
                topMargin: 4 * mult
            }

            Q.Label {
                id: helperTextLabel
                visible: background.helperText && background.showBorder
                text: background.helperText
                font.pixelSize: 12 * mult
                color: control.hasError ? control.errorColor
                                        : Qt.darker("gray")
                renderType: Text.QtRendering
                Behavior on color {
                    ColorAnimation { duration: 100 }
                }

                property string helperText: control.hasOwnProperty("helperText")
                                            ? control.helperText : ""
            }

            Q.Label {
                id: charLimitLabel
                renderType: Text.QtRendering
                Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                visible: control.characterLimit && control.showBorder
                text: control.length + " / " + control.characterLimit
                font.pixelSize: 12 * mult
                color: control.hasError ? control.errorColor : "black"
                horizontalAlignment: Text.AlignLeft

                Behavior on color {
                    ColorAnimation { duration: 100 }
                }
            }
        }

        function oppositeColor(color){
            color.r = 255 - color.r
            color.g = 255 - color.g
            color.b = 255 - color.b
            return color
        }
    }
}
