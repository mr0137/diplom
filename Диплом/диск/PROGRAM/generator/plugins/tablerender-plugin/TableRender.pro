#=============EDITED_BY_PLUGINHELPER=============#
TEMPLATE = lib
TARGET = TableRender
QT += qml
QT += core
QT += quick
QT += concurrent
QT += svg
CONFIG += plugin
CONFIG += c++17
CONFIG += qt
CONFIG += qmltypes

QML_IMPORT_NAME = TableRender
QML_IMPORT_MAJOR_VERSION = 1.0
DEFINES += TABLERENDER_LIBRARY
include($$PWD/../destidir.pri)

QMLTYPES_FILENAME = $$DESTDIR/TableRender.qmltypes
uri = TableRender

#Inputs
SOURCES += \
        scenegraph/managedtexturenode.cpp \
        scenegraph/paintedrectangleitem.cpp \
        scenegraph/shadowedborderrectanglematerial.cpp \
        scenegraph/shadowedbordertexturematerial.cpp \
        scenegraph/shadowedrectanglematerial.cpp \
        scenegraph/shadowedrectanglenode.cpp \
        scenegraph/shadowedtexturematerial.cpp \
        scenegraph/shadowedtexturenode.cpp \
        styles/cellfontstyle.cpp \
        styles/headerstyle.cpp \
        styles/cellstyle.cpp \
        styles/tablestyle.cpp \
        styles/lazytablestyle.cpp \
        tableprivate.cpp \
        tablerender_plugin.cpp


HEADERS += \
        scenegraph/managedtexturenode.h \
        scenegraph/paintedrectangleitem.h \
        scenegraph/shadowedborderrectanglematerial.h \
        scenegraph/shadowedbordertexturematerial.h \
        scenegraph/shadowedrectanglematerial.h \
        scenegraph/shadowedrectanglenode.h \
        scenegraph/shadowedtexturematerial.h \
        scenegraph/shadowedtexturenode.h \
        styles/cellfontstyle.h \
        styles/headerstyle.h \
        styles/cellstyle.h \
        styles/tablestyle.h \
        styles/lazytablestyle.h \
        tableprivate.h \
        tablerender_global.h \
        tablerender_plugin.h

CONFIG(release, debug|release){
    copy_qmldir.target = $$DESTDIR/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir_qrc
    copy_qmldir.commands = $(COPY_FILE) "$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)" "$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}else{
    default_copy.files = \
        qmldir
    default_copy.path = $$DESTDIR/

    qml_copy.files = \
        qml/TableRender.qml \
        qml/TableScrollBar.qml \

    qml_copy.path = $$DESTDIR/qml/

    COPIES += default_copy qml_copy
}

RESOURCES += \
    resources.qrc \
    scenegraph/shaders/shaders.qrc
