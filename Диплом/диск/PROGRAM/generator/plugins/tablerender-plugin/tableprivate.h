#ifndef TABLEPRIVATE_H
#define TABLEPRIVATE_H

#include <QQuickItem>
#include <itabledataprovider.h>
#include <QImage>
#include <QTextLayout>
#include "tablerender_global.h"
#include <styles/lazytablestyle.h>

class HighlightNode;
class QSGGeometryNode;
class MovingHeaderNode;
class ITableDataProvider;
class ITableColorProvider;

class Table: public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(TableStyle* style            READ style                      WRITE setStyle                      NOTIFY styleChanged)
    Q_PROPERTY(bool pressed                 READ pressed                    WRITE setPressed                    NOTIFY pressedChanged)
    Q_PROPERTY(QRectF viewPos               READ viewPos                    WRITE setViewPos                    NOTIFY viewPosChanged)
    Q_PROPERTY(bool editable                READ editable                                                       NOTIFY editableChanged)
    Q_PROPERTY(bool resizing                READ resizing                   WRITE setResizing                   NOTIFY resizingChanged)
    Q_PROPERTY(QObject* provider            READ provider                   WRITE setProvider                   NOTIFY providerChanged)
    Q_PROPERTY(int rowsCount                READ rowsCount                  WRITE setRowsCount                  NOTIFY rowsCountChanged)
    Q_PROPERTY(int currentRow               READ currentRow                 WRITE setCurrentRow                 NOTIFY currentRowChanged)
    Q_PROPERTY(int pagesCount               READ pagesCount                 WRITE setPagesCount                 NOTIFY pagesCountChanged)
    Q_PROPERTY(int columnCount              READ columnsCount               WRITE setColumnsCount               NOTIFY columnsCountChanged)
    Q_PROPERTY(double contentWidth          READ contentWidth               WRITE setContentWidth               NOTIFY contentWidthChanged)
    Q_PROPERTY(int currentColumn            READ currentColumn              WRITE setCurrentColumn              NOTIFY currentColumnChanged)
    Q_PROPERTY(double contentHeight         READ contentHeight                                                  NOTIFY contentHeightChanged)
    Q_PROPERTY(bool reachedBottom           READ reachedBottom              WRITE setReachedBottom              NOTIFY reachedBottomChanged)
    Q_PROPERTY(bool columnsMoving           READ columnsMoving              WRITE setColumnsMoving              NOTIFY columnsMovingChanged)
    Q_PROPERTY(bool columnsMovable          READ columnsMovable             WRITE setColumnsMovable             NOTIFY columnsMovableChanged)
    Q_PROPERTY(int selectedRowsCount        READ selectedRowsCount          WRITE setSelectedRowsCount          NOTIFY selectedRowsCountChanged)
    Q_PROPERTY(int selectedColumnsCount     READ selectedColumnsCount       WRITE setSelectedColumnsCount       NOTIFY selectedColumnsCountChanged)

public:
    Table(QQuickItem *parent = nullptr);

    bool pressed() const;
    bool editable() const;
    int rowsCount() const;
    bool resizing() const;
    int pagesCount() const;
    int currentRow() const;
    QRectF viewPos() const;
    int columnsCount() const;
    TableStyle *style() const;
    int currentColumn() const;
    QObject* provider() const;
    bool columnsMoving() const;
    bool reachedBottom() const;
    bool columnsMovable() const;
    double contentWidth() const;
    double contentHeight() const;
    int selectedRowsCount() const;
    int selectedColumnsCount() const;

public slots:
    void increaseRow();
    void increaseColumn();
    void decreaseRow();
    void decreaseColumn();
    void moveToPos(double x, double y);
    // [property]
    // [getters]
    void setStyle(TableStyle *newStyle);
    void setProvider(QObject* provider);
    void setColorProvider(QObject *provider);
    void appendColorProvider(QObject *provider);
    void removeColorProvider(QObject *provider);
    void resetSelection();

    // [helpers]
    QRectF cellPos(int row, int column);
    QColor cellBackgroundColor(int row, int column);
    QString cellValue(int row, int column);
    void setCellValue(int row, int column, QVariant value);
    QString headerTitle(int column);
    void setHeaderIcon(int column, QString iconpath);
    void setCurrentCell(int row, int column);
    int headerIndex(QString name);
    // [helpers]

    void pinColumn(int column);
    void unpinColumn(int column);
    QVariantList pinnedIndeces();

    QPoint normalToCell(QPointF pos);
    int getInternalIndex(int index);
    void setColumnsMovable(bool columnsMovable);
    void swapColumns(int index1, int index2);

private:
    // [property]
    // [setters]
    void setPressed(bool pressed);
    void setRowsCount(int rowsCount);
    void setResizing(bool resizing);
    void setViewPos(QRectF viewPos);
    void setPagesCount(int pagesCount);
    void setCurrentRow(int currentRow);
    void setColumnsCount(int columnCount);
    void setCurrentColumn(int currentColumn);
    void setContentWidth(double contentWidth);
    void setReachedBottom(bool reachedBottom);
    void setColumnsMoving(bool columnsMoving);
    void setSelectedRowsCount(int selectedRowsCount);
    void setSelectedColumnsCount(int selectedColumnsCount);
    // [additional]
    void reset(bool resetViewPos = true);
    int rowIndex(double y);
    int columnIndex(double x);
    int pageIndex(int row);
    void updatePage(int ind);
    void updatePageVisibility();
    void updateContentWidth();
    void addPageByRow(int row);
    QVector<Row> loadPageData(int row);
    void updateRows(int startRow, int endRow, ITableDataProvider::Reason r);

    QPair<QImage, QImage> drawPageImage(const QVector<Row>& data);
    QImage drawPinnedImage(int index, const QVector<Row>& data);
    QImage drawHeader(int index);
    QImage drawPageCounter(int from, int count);
    QPointF internalToNormal(QPointF pos);
    void checkSelect(double x, double y);
    bool checkHeaderSwap();
    void checkActuallyPinnedColumns();

    Q_INVOKABLE void updateAllPages();
    Q_INVOKABLE void updateHeaders();

protected:
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void keyReleaseEvent(QKeyEvent *event) override;
    virtual void hoverMoveEvent(QHoverEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    virtual QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;

    QSGNode *drawPages(QSGNode *n);
    QSGNode *drawBackground(QSGNode *n);
    QSGGeometryNode *drawGrid(QSGNode *node);
    QSGGeometryNode *drawColumnResizer(QSGNode *n);

    QSGNode *drawPinnedPages(QSGNode *n);
    QSGNode *drawPinnedHeaders(QSGNode *n);
    QSGNode *drawPinnedHighlight(QSGNode *n, QRectF &res);
    QSGNode *drawPinnedHeaderHighlight(QSGNode *n, const QRectF &res);

    QSGNode *drawVerticalHeader(QSGNode *node);
    QSGNode *drawColorProviderBackground(QSGNode *node);
    QSGGeometryNode *drawFixRectangle(QSGGeometryNode *node);
    QSGGeometryNode *drawSelectedCell(QSGNode *node);
    QSGGeometryNode *drawHorizontalHeader(QSGNode *node);
    MovingHeaderNode *drawMovingHeader(MovingHeaderNode *n);
    HighlightNode *drawVerticalHeaderHightLight(QSGNode *node);
    HighlightNode *drawHorizontalHeaderHightLight(QSGNode *node);

    enum UpdateReason {
        NONE = 0x00,
        SHIFT_X = 0x1,
        SHIFT_Y = 0x02,
        SELECT_CHANGED = 0x04,
        GRID_CHANGED = 0x08,
        RESIZE_COLUMN = 0x0F,
        UPDATE_HEADER = 0x20,
        UPDATE_HEADER_HIGHLIGHT = 0x40,
        UPDATE_COLOR_BACKGROUND = 0x80,
        UPDATE_HEADER_MOVING = 0x44,
        UPDATE_PAGE = 0x10
    };

    /*!
     * \struct PageData
     * \brief Container of page data.
     */
    struct PageData {
        QImage vertHeader;
        QHash<int, QImage> pinnedPages;
        QHash<int, QImage> pinnedHeaders;
        QImage image;
        QImage background;
        double yoffset;
        int firstRow;
        int rowsCount;
        bool needUpdate;
        bool visible;
    };

    /*!
     * \struct ColumunInfo
     * \brief Container for header data.
     */
    struct ColumunInfo{
        QString name;
        double width;
        double offset;
        QColor textColor;
        QColor backgroundColor;
        quint32 headerAlignment;
        quint32 cellAlignment;
        bool enableFilter = false;
        bool enableSort = false;
    };

    /*!
     * \struct PinnedColumnInfo
     * \brief Container for pinned header data.
     */
    struct PinnedColumnInfo{
        double width;
        double offset;
        int visualIndex;
        bool isLeft = false;
        bool operator==(const PinnedColumnInfo&col){
            return this->visualIndex == col.visualIndex;
        }
    };
    Q_DECLARE_FLAGS(UpdateReasons, UpdateReason)
    UpdateReasons updateReasons;

    QVector<ColumunInfo> m_columns;
    QHash<int, PageData> m_pages;

signals:
    // [property]
    void rowsCountChanged(int rowCount);
    void viewPosChanged(QRectF viewPos);
    void resizingChanged(bool resizing);
    void editableChanged(bool editable);
    void styleChanged(TableStyle *style);
    void contentRowChanged(int contentRow);
    void currentRowChanged(int currentRow);
    void pagesCountChanged(int pagesCount);
    void providerChanged(QObject* provider);
    void cacheBufferChanged(int cacheBuffer);
    void cellHeightChanged(double cellheight);
    void columnsCountChanged(int columnsCount);
    void contentColumnChanged(int contentColumn);
    void currentColumnChanged(int currentColumn);
    void headerHeightChanged(double headerHeight);
    void reachedBottomChanged(bool reachedBottom);
    void contentWidthChanged(double contentWidth);
    void contentHeightChanged(double contentHeight);
    void contentRowCountChanged(int contentRowCount);
    void selectedRowsCountChanged(int selectedRowsCount);
    void contentColumnCountChanged(int contentColumnCount);
    void selectedColumnsCountChanged(int selectedColumnsCount);
    // [property]
    void pressedChanged(bool pressed);
    void showContextMenu(QPointF pos);
    void checkboxPressed(int row, int column);
    void columnsMovingChanged(bool columnsMoving);
    void columnsMovableChanged(bool columnsMovable);
    void keyPressed(QVariantMap event, int row, int column);
    void keyReleased(QVariantMap event, int row, int column);
    void doubleClicked(QVariantMap event, int row, int column);
    void clicked(QVariantMap event, int row, int column);
    void headerPressed(int button, int columnIndex, int headerSpace, QRectF geometry);

private:
    // [property]
    int m_rowsCount = 0;
    int m_currentRow = 0;
    int m_columnsCount = 0;
    int m_currentColumn = 0;
    double m_contentWidth = 0;
    int m_selectedRowsCount = 1;
    bool m_reachedBottom = false;
    bool m_columnsMoving = false;
    TableStyle *m_style = nullptr;
    int m_selectedColumnsCount = 1;
    ITableDataProvider* m_provider = nullptr;
    // [property]
    // [resize]
    QRectF m_viewPos = {0, 20, 10, 10};
    bool m_resizing = false;
    int m_resizeColumnIndex = 0;
    double m_resizeStartXPos = 0;
    double m_resizeCurrentXPos = 0;
    double m_moveInternalPosPress = 0;
    double m_moveCurrentXPos = 0;
    // [resize]
    // [internal properties]
    int m_pageRowsCount = 40;
    int m_currentPage = -1;
    int m_pagesCount = 0;
    QString m_appPath = "";
    bool m_selecting = false;
    QPoint m_autoMove = {0,0};
    QPointF m_mousePos = {0,0};
    QVector<int> m_pinnedColumns;
    QVector<PinnedColumnInfo> m_actuallyPinnedColumns;
    QVector<Header> m_prevHeaderData = {};
    QVector<ITableColorProvider*> m_colorProviders;
    QHash<int, int> m_columnIndeces;

    bool m_pinnedResizing = false;
    int m_pinnedResizeColumnIndex = 0;
    bool m_pressed = false;
    bool m_columnsMovable = true;
    int m_movingColumnIndex = -1;

    int m_swaping = false;
    double m_pinnedLeftWidth = 0;
    double m_pinnedRightWidth = 0;
    QHash<int, QString> m_iconsPaths;
    QTimer *m_timer = nullptr;
};

#endif // TABLEPRIVATE_H
