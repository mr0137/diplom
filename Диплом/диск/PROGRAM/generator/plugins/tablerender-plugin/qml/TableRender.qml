import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import TableRender 1.0
import TableRenderPrivate 1.0

// doxygen generated thanks to doxyqml

/*!
    \class TableRender
    \brief A Table that allows the user to view database's content.
    Example of usage:
    \code
    TableRender {
        width: 200
        height: 200
        provider: form.provider
        style: LazyTableStyle.Excel
    }
    \endcode
    \note You can create a custom appearance for a LazyTable by
    assigning a \a {TableStyle}.
*/

Rectangle{
    id: root
    /*! type:ITableDataProvider
        \brief Data provider of table (ITableDataProvider).
    */
    required property var provider

    /*! type:var
    \brief Style could be enum LazyTableStyle::Style or \a TableStyle type.
      Possible enumerations:
      - LazyTableStyle.Default (default)
      - LazyTableStyle.Excel
    */
    //@disable-check M311
    property var style: LazyTableStyle.Default

    /*! type:bool
        \brief HorizontalScrollbarAvailable used to enable or disable horizontal scrollbar.
    */
    property bool horizontalScrollbarAvailable: true
    /*! type:bool
        \brief VerticalScrollbarAvailable enable or disable horizontal scrollbar.
    */
    property bool verticalScrollbarAvailable: true

    /*! type:bool
        \brief moveable enable or disable columns move.
    */
    property bool moveable: true

    /*! type:bool
        \brief Show resizing state of LazyTable.
    */
    readonly property alias resizing: table.resizing

    /*! type:QRectF
        \brief Display geometry of visual rect.
        Internal fields:
        - \c x;
        - \c y;
        - \c width;
        - \c height.
    */
    readonly property alias viewPos: table.viewPos

    /*! type:int
        \brief Show current row position.
    */
    readonly property alias currentRow: table.currentRow
    /*! type:int
        \brief Display current column position.
     */
    readonly property alias currentColumn: table.currentColumn

    /*! type:int
        \brief This property holds the number of selected rows.
        This number can be less than \c 0, that show us that selection move to top of table.
        If number higher than \c 0, that means that the selection moves to bottom of table.
        <br>The range is <b>[-Table::rowsCount, 0) && (0, Table::rowsCount]</b>.
        \sa Table::selectedRowsCount, Table::selectedColumnCount.
     */
    readonly property alias selectedRowsCount: table.selectedRowsCount

    /*! type:int
        \brief This property holds the number of selected columns.
        This number can be less than \c 0, that show us that selection move to top of table.
        If number higher than \c 0, that means that the selection moves to bottom of table.
        The range is <b>[-Table::columnsCount, 0) && (0, Table::columnsCount]</b>.
        \sa Table::selectedColumnCount, Table::selectedRowsCount.
     */
    readonly property alias selectedColumnCount: table.selectedColumnsCount
    /*! type:double
      \brief This property holds the height of whole content.
     */
    readonly property real contentHeight: table.contentHeight + table.style.horizontalHeader.height

    /*! type:double
      \brief This property holds the width of whole content.
     */
    readonly property real contentWidth: table.contentWidth

    /*! type:bool
      \brief This property holds the ediatable state of table.
    */
    readonly property alias editable: table.editable
    /*! type:color
        \brief This property holds color of scrollbar handle.
     */
    property color scrollbarColor: "black"
    /*! type:var
        \brief This proeprty holds list of columns, which user can modify.
        Example:
        \code
        LazyTable{
            ...
            editableColumns: ["col_name1", "col_name2"]
            ...
        }
        \endcode
        \note If \a editableColumns empty - all columns would be editable, but if only ItableDataProvider::isEditable is true.
     */
    property var editableColumns: new Array
    /*! type:color
        \brief This property holds background color of table.
     */
    property color backgroundColor: Qt.darker("gray", 1.1)
    /*!
        \brief Signal is emitted when the user press the header.
        <br><b>Params:</b>
            - \a button - hold pressed mousebutton id;
            - \a column - show where it been pressed;
            - \a headerSpace - display special position of click by enumeration LazyTableStyle::HeaderSpace.
            - \a rect - show geometry of specified header column.
        The corresponding handler is \c onHeaderPressed.
    */
    signal headerPressed(int button, int column, int headerSpace, var rect)
    /*!
        \brief Signal is emitted when user press any key.
        <br><b>Params:</b>
            - \a event - map with params:
                + \a key - keyboard key;
                + \a modifiers - keyboard modifiers (Ctrl, Shift, etc.)
                + \a accepted - modifiable field; if setted true - internal sequance of operation will be stoped.
            - \a row - row, when key has been pressed;
            - \a column - column, when key has been pressed.
    */
    signal keyPressed(var event, int row, int column)
    /*!
        \brief Signal is emitted when user double press the mouse button.
        <br><b>Params:</b>
            - \a event - map with params:
                + \a key - keyboard key;
                + \a modifiers - keyboard modifiers (Ctrl, Shift, etc.)
                + \a event - modifiable field; if setted true - internal sequance of operation will be stoped.
            - \a row - row, when key has been pressed;
            - \a column - column, when key has been pressed.
    */
    signal doubleClicked(var event, int row, int column)
    /*!
        \brief Signal emits when user press right-click or press on column header context menu icon.
        \param pos show internal position of double-click event.
    */
    signal showContextMenu(var pos)

    /*!
        \brief Signal emits when user key released
        <br><b>Params:</b>
            - \a event - map with params:
                + \a key - keyboard key;
                + \a modifiers - keyboard modifiers (Ctrl, Shift, etc.)
                + \a event - modifiable field; if setted true - internal sequance of operation will be stoped.
            - \a row - row, when key has been pressed;
            - \a column - column, when key has been pressed.
    */
    signal keyReleased(var event, int row, int column)

    /*!
        \brief Signal is emitted when user click checkbox on table
        \param row show row position of cell.
        \param column show column position of cell.
    */
    signal checkboxPresed(int row, int column)

    /*!
        \brief Signal is emitted when user click on table
        \brief Signal is emitted when user double press the mouse button.
        <br><b>Params:</b>
            - \a event - map with params:
                + \a key - keyboard key;
                + \a modifiers - keyboard modifiers (Ctrl, Shift, etc.)
                + \a event - modifiable field; if setted true - internal sequance of operation will be stoped.
            - \a row - row, when key has been pressed;
            - \a column - column, when key has been pressed.
    */
    signal clicked(var event, int row, int column)

    /*! type:var
        \brief Lambda that hold actions in response to enter press when editing cell.
    */
    property var editFinishedBehaviour: (text, row, column) => {
                                            table.setCellValue(row, column, text)
                                        }
    /*! type:var
        \brief Component holding popup. By default EditPopup
    */
    property Item popupElement: Item{
        //! [background]
        Rectangle{
            anchors.fill: parent
            anchors.bottomMargin: 5
            anchors.rightMargin: 5
            color: editPopup.opened ? table.cellBackgroundColor(table.currentRow, table.currentColumn) : "transparent"
        }
        Rectangle{
            anchors.fill: parent
            anchors.topMargin: parent.height - 5
            anchors.rightMargin: 5
            color: editPopup.opened ? table.cellBackgroundColor(table.currentRow, table.currentColumn) : "transparent"
        }
        //! [background]

        TextField{
            id: textEdit
            anchors.fill: parent
            selectByMouse: true
            font.family: table.style.cell.font.family
            font.pointSize: table.style.cell.font.pointSize
            horizontalAlignment: table.style.cell.alignment

            Keys.onEnterPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.increaseRow()
            }

            Keys.onReturnPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.increaseRow()
            }

            Keys.onDownPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.increaseRow()
            }

            Keys.onUpPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.decreaseRow()
            }

            Keys.onLeftPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.decreaseColumn()
            }

            Keys.onRightPressed: {
                editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                editPopup.close()
                table.increaseColumn()
            }

            Keys.onEscapePressed: {
                editPopup.close()
            }

            background: Rectangle{
                color: "transparent"
            }
        }
    }

    color: backgroundColor

    onVerticalScrollbarAvailableChanged: {
        verticalScrollBar.visible = verticalScrollbarAvailable
    }

    onHorizontalScrollbarAvailableChanged: {
        horizontalScrollBar.visible = horizontalScrollbarAvailable
    }

    onResizingChanged: {
        table.currentColumnChanged(table.currentColumn)
    }

    Rectangle{
        anchors.fill: parent
        anchors.margins: 1
        color: backgroundColor
    }

    Popup{
        id: editPopup
        enabled: root.editable
        property rect editPos: enabled ? table.cellPos(table.currentRow, getInternalIndex(table.currentColumn)) : Qt.rect(0,0,0,0)
        property int editingRow: 0
        property int editingColumn: 0
        property bool edited: false

        closePolicy: Popup.NoAutoClose | Popup.CloseOnReleaseOutsideParent | Popup.CloseOnEscape
        x: editPos.x + 2 - table.viewPos.x
        y: editPos.y + 2 - table.viewPos.y
        width: editPos.width - 2
        height: editPos.height - 2
        z: 10
        focus: true

        function editWithReplace(firstLetter, row, column){
            editPopup.forceActiveFocus()
            editingColumn = column
            editingRow = row
            textEdit.text = "" + firstLetter
            textEdit.forceActiveFocus()
            editPopup.open()
            edited = true
        }

        function edit(row, column){
            editPopup.forceActiveFocus()
            editingColumn = column
            editingRow = row
            textEdit.text = table.cellValue(row, column)
            textEdit.selectAll()
            textEdit.forceActiveFocus()
            editPopup.open()
            edited = true
        }

        onClosed:{
            editPopup.edited = false
            table.forceActiveFocus()
        }

        background: root.popupElement
    }

    onStyleChanged: {
        if (typeof(style) === 'number'){
            switch(style){
            case 0:
                table.style = LazyTableStyle.getDefaultStyle();
                break;
            case 1:
                table.style = LazyTableStyle.getExcelStyle();
                break;
            }
        }else if (typeof(style) === typeof(TableStyle)){
            table.style = style
        }
    }

    ColumnLayout{
        anchors.fill: root
        anchors.margins: 1
        spacing: 0
        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 0

            TablePrivate {
                id: table
                Layout.fillWidth: true
                Layout.fillHeight: true
                provider: root.provider
                columnsMovable: root.moveable
                property rect prevViewPos: Qt.rect(0,0,0,0)
                onViewPosChanged: {
                    //console.log("viewpos", viewPos.x, viewPos.y)
                    //! не убирать, а то будет вылетать
                    if (prevViewPos.x === viewPos.x && prevViewPos.y === viewPos.y) return;
                    //!
                    if (!horizontalScrollBar.pressed){
                        var horPosition = Math.min(Math.max(0, viewPos.x / (table.contentWidth - table.width)), 1)
                        if (horPosition !== horizontalScrollBar.position && table.rowsCount !== 0){
                            horizontalScrollBar.setPosition(horPosition)
                        }
                        //console.log(horPosition)
                    }
                    if (!verticalScrollBar.pressed){
                        var verPosition = Math.min(Math.max(0, viewPos.y / (table.contentHeight - table.height)), 1)
                        if (verPosition !== verticalScrollBar.position && table.rowsCount !== 0){
                            verticalScrollBar.setPosition(verPosition)
                        }
                    }
                    prevViewPos = viewPos
                }

                Connections {
                    target: table

                    function onHeaderPressed(button, columnIndex, headerSpace, geometry){
                        headerPressed(button, columnIndex, headerSpace, geometry)
                    }

                    function onClicked(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.clicked(ev, row, column)
                        if (!ev["accepted"]){

                        }
                    }

                    function onKeyPressed(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.keyPressed(ev, row, column)

                        if (!ev["accepted"]){
                            if (table.checkForEditable()){
                                if (ev.key === Qt.Key_F2){
                                    editPopup.forceActiveFocus()
                                    editPopup.edit(currentRow, currentColumn)
                                }else if (ev.key >= Qt.Key_A && ev.key <= Qt.Key_Z){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace(String.fromCharCode(ev.key), currentRow, currentColumn)
                                }else if (ev.key >= Qt.Key_0 && ev.key <= Qt.Key_9){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace(String.fromCharCode(ev.key), currentRow, currentColumn)
                                }else if (ev.key === Qt.Key_Space){
                                    editPopup.forceActiveFocus()
                                    editPopup.editWithReplace("", currentRow, currentColumn)
                                }
                            }
                        }
                    }

                    function onShowContextMenu(pos){
                        root.showContextMenu(pos)
                    }

                    function onCheckboxPressed(row, column){
                        root.checkboxPresed(row, column)
                    }

                    function onKeyReleased(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.keyReleased(ev, row, column)

                        if (!ev["accepted"]){

                        }
                    }

                    function onCurrentRowChanged(){
                        if (editPopup.opened){
                            editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                            editPopup.close()
                        }
                    }

                    function onCurrentColumnChanged(){
                        if (editPopup.opened){
                            editFinishedBehaviour(textEdit.text, editPopup.editingRow, editPopup.editingColumn)
                            editPopup.close()
                        }
                    }

                    function onDoubleClicked(event, row, column){
                        var ev = event
                        ev["accepted"] = false
                        root.doubleClicked(ev, row, column)
                        if (!ev["accepted"]){
                            if (table.checkForEditable()){
                                editPopup.forceActiveFocus()
                                editPopup.edit(currentRow, currentColumn)
                            }
                        }
                    }
                }

                function checkForEditable(){
                    let res = false
                    if (!root.editable) {
                        return false
                    }else if (root.editableColumns.length === 0){
                        res = true
                    }else{
                        res = (root.editableColumns.indexOf(table.headerTitle(getInternalIndex(table.currentColumn))) !== -1)
                    }
                    return res
                }
            }

            TableScrollBar{
                id: verticalScrollBar
                orientation: Qt.Vertical
                stepSize: 50 / table.contentHeight
                pageSize: Math.min((table.height) / table.contentHeight, 1)
                visible: pageSize < 1 && root.verticalScrollbarAvailable
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumWidth: 12
                Layout.minimumWidth: 12
                handleColor: scrollbarColor
                backgroundColor: root.backgroundColor
                onPositionChanged: {
                    if (pressed)
                        table.moveToPos(table.viewPos.x, position * (table.contentHeight - table.height))
                }

                onPageSizeChanged: {
                    //console.log("position", position)
                    table.moveToPos(table.viewPos.x, position * (table.contentHeight - table.height))
                }
            }
        }

        TableScrollBar{
            id: horizontalScrollBar
            orientation: Qt.Horizontal
            pageSize: Math.min((table.width - table.style.verticalHeader.width) / table.contentWidth, 1)

            visible: pageSize < 1 && root.horizontalScrollbarAvailable

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumHeight: 12
            Layout.minimumHeight: 12
            Layout.rightMargin: !verticalScrollBar.visible ? 0 : 12
            handleColor: scrollbarColor
            backgroundColor: root.backgroundColor
            onPositionChanged: {
                if (pressed)
                    table.moveToPos(position * (table.contentWidth - table.width + table.style.verticalHeader.height), table.viewPos.y)
            }

            onPageSizeChanged: {
                table.moveToPos(position * (table.contentWidth - table.width + table.style.verticalHeader.height), table.viewPos.y)
            }
        }
    }

    function closePopup(){
        editPopup.close()
    }
    /*!
        \brief Calculate row and column from coords (cells regular coords)
        \param type:Qt.point coords
    */


    function coordsToCellPos(coords){
        var cell = new Map
        var r = table.normalToCell(coords)
        cell["column"] = r.x
        cell["row"] = r.y
        return cell
    }

    /*!
        \brief Append color provider to table (for combining).
        \param type:ITableColorProvider provider
    */
    function appendColorProvider(provider){
        table.appendColorProvider(provider)
    }

    /*!
        \brief Remove color provider
        \param type:ITableColorProvider provider
    */
    function removeColorProvider(provider){
        table.removeColorProvider(provider)
    }

    /*!
        \brief Erase all color provider internal data and set new color provider
        \param type:QObject provider data provider (ItableDataProvider)
     */
    function setColorProvider(provider){
        table.setColorProvider(provider)
    }

    /*!
        \brief Set new value to cell if its possible.
        \param type:int row - row position of cell;
        \param type:int column - column position of cell;
        \param type:string value - new value of cell.
    */
    function setCellValue(row, column, value){
        if (table.checkForEditable()) table.setCellValue(row, column, value)
        else if (value.includes("check:1") || value.includes("check:0")) table.setCellValue(row, column, value)
        else console.log("You have no rights to edit cell")
    }

    /*!
        \brief Return value of cell
        \param type:int row - row position of cell;
        \param type:int column - column position of cell.
    */
    function cellValue(row, column){
        return table.cellValue(row, column)
    }

    /*!
        \brief Used for increasing current row by 1 if it possible.
        \sa LazyTable::decreaseRow, LazyTable::decreaseColumn, LazyTable::increaseColumn.
    */
    function increaseRow(){
        table.increaseRow()
    }

    /*!
        \brief Used for decreasing current row by 1 if it possible.
        \sa LazyTable::increaseRow, LazyTable::increaseColumn, LazyTable::decreaseColumn.
    */
    function decreaseRow(){
        table.decreaseRow()
    }

    /*!
        \brief Used for increasing current column by 1 if it possible.
        \sa LazyTable::decreaseRow, LazyTable::increaseRow, LazyTable::decreaseRow.
    */
    function increaseColumn(){
        table.increaseColumn()
    }

    /*!
        \brief Used for decreasing current column by 1 if it possible.
        \sa LazyTable::increaseColumn, LazyTable::increaseRow, LazyTable::decreaseRow.
    */
    function decreaseColumn(){
        table.decreaseColumn()
    }

    /*!
        \brief Used for setting new current row if it possible.
        \param type:int row - new current row.
    */
    function setCurrentRow(row){
        table.setCurrentCell(row, table.currentColumn)
    }

    /*!
        \brief Used for setting new current column if it possible.
        \param type:int column - new current column.
    */
    function setCurrentColumn(column){
        table.setCurrentCell(table.currentRow, column)
    }

    /*!
        \brief Used as helper for getting column visual index by column's name.
        \param type:string name - column's name.
        \return index.
    */
    function columnIndexByName(name){
        return table.headerIndex(name)
    }

    /*!
        \brief Used as helper for getting real column's index by visual index.
        \param type:int index - visual index.
    */
    function getInternalIndex(index){
        return table.getInternalIndex(index)
    }

    /*!
        \brief Used for pinning column.
        \param type:int column - visual index of column.
    */
    function pinColumn(column){
        table.pinColumn(column)
    }

    /*!
        \brief Used for unpinning column.
        \param type:int column - visual index column.
    */
    function unpinColumn(column){
        table.unpinColumn(column)
    }

    /*!
        \brief Used as helper for getting pinned state of column by visual index.
        \param type:int column - visual index.
    */
    function isPinned(column){
        return table.pinnedIndeces().includes(column)
    }

    /*!
        \brief Used for setting header icon.
        \param type:int column - internal column (non visual)
        \param type:string iconpath - path of the icon.
    */
    function setHeaderIcon(column, iconpath){
        table.setHeaderIcon(column, iconpath)
    }

    /*!
        \brief USed for reseting selection
    */
    function resetSelection(){
        table.resetSelection()
    }
}

