## TableRender 1.0
    TableRender is a plugin that help you to visualize data from anywhere by using lazyloading initialisation. 
## Example of usage
**Project**:<br>
    You need to import plugin to your project via QQmlEngine::addImportPath(<path_to_plugin>). And you need add  this plugin as external library for getting access to *ITableDataProvider* and *ITableColorProvider*. Don't forget to add <b>QML_IMPORT_PATH</b> to  plugin in your *.pro file.

**C++**:<br>
	You need to create your own data provider by inheriting the *ITableDataProvider* interface.
	
**QML**:

    import TableRender 1.0
    ...
    
    Item{
	    ...
	    TableRender{
		    anchors.fill: parent
		    provider: <ITableDataProvider>
		    style: LazyTableStyle.Excel
	    }
	    ... 
    }
    
**Qt Help**:<br>
   Register documentation file *TableRender.qch* in Qt Creator. 
   Tools->Options->Help->Documentation->Add
    
