#ifndef TABLESTYLE_H
#define TABLESTYLE_H

#include <QFont>
#include <QImage>
#include <QObject>
#include <styles/cellstyle.h>
#include <styles/headerstyle.h>

#include "tablerender_global.h"

class TableStyle : public QObject{
    Q_OBJECT
    Q_PROPERTY(CellStyle* cell                  READ cellStyle                                                      NOTIFY cellStyleChanged)
    Q_PROPERTY(HeaderStyle* verticalHeader      READ verticalHeader                                                 NOTIFY verticalHeaderChanged)
    Q_PROPERTY(HeaderStyle* horizontalHeader    READ horizontalHeader                                               NOTIFY horizontalHeaderChanged)

    Q_PROPERTY(QColor gridColor                 READ gridColor                  WRITE setGridColor                  NOTIFY gridColorChanged)
    Q_PROPERTY(QColor iconsColor                READ iconsColor                 WRITE setIconsColor                 NOTIFY iconsColorChanged)
    Q_PROPERTY(double iconsWidth                READ iconsWidth                 WRITE setIconsWidth                 NOTIFY iconsWidthChanged)
    Q_PROPERTY(QColor highlightColor            READ highlightColor             WRITE setHighlightColor             NOTIFY highlightColorChanged)
    Q_PROPERTY(QColor selectionColor            READ selectionColor             WRITE setSelectionColor             NOTIFY selectionColorChanged)
    Q_PROPERTY(double selectionOpacity          READ selectionOpacity           WRITE setSelectionOpacity           NOTIFY selectionOpacityChanged)
    Q_PROPERTY(bool combineColorProviders       READ combineColorProviders      WRITE setCombineColorProviders      NOTIFY combineColorProvidersChanged)
    Q_PROPERTY(bool verticalHeaderAvailable     READ verticalHeaderAvailable    WRITE setVerticalHeaderAvailable    NOTIFY verticalHeaderAvailableChanged)
    Q_PROPERTY(bool numericalHeaderAvailable    READ numericalHeaderAvailable   WRITE setNumericalHeaderAvailable   NOTIFY numericalHeaderAvailableChanged)
    friend class Table;
public:
    TableStyle(QObject *parent = nullptr);

    CellStyle *cellStyle();
    bool verticalHeaderAvailable() const;
    HeaderStyle *verticalHeader();
    HeaderStyle *horizontalHeader();
    void setVerticalHeaderAvailable(bool newVerticalHeaderAvailable);

    const QColor &highlightColor() const;
    void setHighlightColor(const QColor &newHighlightColor);

    void setIconsColor(QColor iconsColor);
    QColor iconsColor() const;

    double iconsWidth() const;
    void setIconsWidth(double iconsWidth);

    double selectionOpacity() const;
    QColor selectionColor() const;
    void setSelectionOpacity(double selectionOpacity);
    void setSelectionColor(QColor selectionColor);

    bool numericalHeaderAvailable() const;
    void setNumericalHeaderAvailable(bool numericalHeaderAvailable);

    bool combineColorProviders() const;
    void setCombineColorProviders(bool newCombineColorProviders);

    QColor gridColor() const;
    void setGridColor(QColor gridColor);

public slots:
    QImage &icon(QString iconName);


signals:
    void verticalHeaderAvailableChanged();
    void updateAllPages();
    void updateHeaders();
//! [unused] (need only to avoid warnings from qml)
    void cellStyleChanged();
    void verticalHeaderChanged();
    void horizontalHeaderChanged();
//! [unused]
    void highlightColorChanged();
    void iconsColorChanged();
    void iconsWidthChanged();

    void selectionOpacityChanged(double selectionOpacity);
    void selectionColorChanged(QColor selectionColor);

    void numericalHeaderAvailableChanged(bool numericalHeaderAvailable);
    void combineColorProvidersChanged();

    void gridColorChanged(QColor gridColor);

private:
    bool m_verticalHeaderAvailable = false;

    CellStyle *m_cellStyle = nullptr;
    HeaderStyle *m_verticalHeader = nullptr;
    HeaderStyle *m_horizontalHeader = nullptr;
    QColor m_highlightColor = "green";
    QImage m_invalid;
    QMap<QString, QImage> m_iconsMap;

    QColor m_iconsColor = "gray";
    double m_iconsWidth = 16;
    double m_selectionOpacity = 0.3;
    QColor m_selectionColor = "black";
    bool m_numericalHeaderAvailable = false;
    bool m_combineColorProviders = true;

    void addImage(QString name, QString path, QColor color = "transparent", double width = 0);
    QColor m_gridColor = "gray";
};
#endif // TABLESTYLE_H
