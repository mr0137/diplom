#include "lazytablestyle.h"

/*!
 * \class LazyTableStyle
 * \brief The LazyTableStyle class is singleton class that allows to use some predifined styles.
 */
class LazyTableStyle;

LazyTableStyle::LazyTableStyle(QObject *parent) : QObject(parent)
{
    m_excelStyle = new TableStyle(this);
    m_excelStyle->setHighlightColor("green");
    m_excelStyle->setVerticalHeaderAvailable(true);
    m_excelStyle->horizontalHeader()->setHeight(30);
    m_excelStyle->horizontalHeader()->setWidth(100);
    m_excelStyle->horizontalHeader()->setTextColor("black");
    m_excelStyle->horizontalHeader()->fontStyle()->setFamily("Arial");
    m_excelStyle->horizontalHeader()->fontStyle()->setPointSize(12);
    m_excelStyle->horizontalHeader()->setBackgroundColor("silver");
    m_excelStyle->horizontalHeader()->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
    m_excelStyle->verticalHeader()->setBackgroundColor("silver");
    m_excelStyle->verticalHeader()->setWidth(10);
    m_excelStyle->verticalHeader()->fontStyle()->setPointSize(12);
    m_excelStyle->setNumericalHeaderAvailable(false);
    m_excelStyle->cellStyle()->setHeight(30);
    m_excelStyle->cellStyle()->setBackgroundColor("white");
    m_excelStyle->cellStyle()->fontStyle()->setPointSize(15);
    m_excelStyle->cellStyle()->fontStyle()->setFamily("Arial");

    m_defaultStyle = new TableStyle(this);
}

LazyTableStyle *LazyTableStyle::instance()
{
    static LazyTableStyle *m_instance;
    if (m_instance == nullptr) {
        m_instance = new LazyTableStyle();
    }
    return m_instance;
}

QObject *LazyTableStyle::qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return instance();
}

/*!
 * \fn TableStyle *LazyTableStyle::getExcelStyle()
 * \brief Predefined ExcelStyle getter.
 */
TableStyle *LazyTableStyle::getExcelStyle()
{
    return m_excelStyle;
}
/*!
 * \fn TableStyle *LazyTableStyle::getDefaultStyle()
 * \brief Predefined DefaultStyle getter.
 */
TableStyle *LazyTableStyle::getDefaultStyle()
{
    return m_defaultStyle;
}
