#ifndef CELLSTYLE_H
#define CELLSTYLE_H

#include <QObject>
#include <qqml.h>
#include <QColor>
#include <styles/cellfontstyle.h>
#include "tablerender_global.h"

class CellStyle : public QObject
{
    Q_OBJECT
    Q_PROPERTY(CellFontStyle* font      READ fontStyle                                      NOTIFY fontStyleChanged)
    Q_PROPERTY(quint8 elide             READ elide              WRITE setElide              NOTIFY elideChanged)
    Q_PROPERTY(double height            READ height             WRITE setHeight             NOTIFY heightChanged)
    Q_PROPERTY(quint32 alignment        READ alignment          WRITE setAlignment          NOTIFY alignmentChanged)
    Q_PROPERTY(QColor textColor         READ textColor          WRITE setTextColor          NOTIFY textColorChanged)
    Q_PROPERTY(int leftMargin           READ leftMargin         WRITE setLeftMargin         NOTIFY leftMarginChanged)
    Q_PROPERTY(int rightMargin          READ rightMargin        WRITE setRightMargin        NOTIFY rightMarginChanged)
    Q_PROPERTY(QColor backgroundColor   READ backgroundColor    WRITE setBackgroundColor    NOTIFY backgroundColorChanged)
    friend class Table;
public:
    explicit CellStyle(QObject *parent = nullptr);

    CellFontStyle *fontStyle();

    int rightMargin() const;
    void setRightMargin(int newRightMargin);

    int leftMargin() const;
    void setLeftMargin(int newLeftMargin);

    const QColor &textColor() const;
    void setTextColor(const QColor &newTextColor);

    const QColor &backgroundColor() const;
    void setBackgroundColor(const QColor &newBackgroundColor);

    double height() const;
    void setHeight(double newHeight);

    quint32 alignment() const;
    void setAlignment(quint32 newAlignment);

    quint8 elide() const;
    void setElide(quint8 newElide);

signals:
    void cellChanged();
    void elideChanged();
    void heightChanged();
    void fontStyleChanged();
    void textColorChanged();
    void alignmentChanged();
    void leftMarginChanged();
    void rightMarginChanged();
    void backgroundColorChanged();

private:
    int m_rightMargin = 5;
    int m_leftMargin = 5;
    CellFontStyle *m_font = nullptr;
    QColor m_textColor = "black";
    QColor m_backgroundColor = "gray";
    double m_height = 20;
    quint32 m_alignment = Qt::AlignRight | Qt::AlignVCenter;
    quint8 m_elide = Qt::ElideLeft;
};

#endif // CELLSTYLE_H
