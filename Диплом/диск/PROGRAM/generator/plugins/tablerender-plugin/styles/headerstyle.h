#ifndef HEADERSTYLE_H
#define HEADERSTYLE_H

#include <QColor>
#include <QObject>
#include <styles/cellfontstyle.h>
#include "tablerender_global.h"

/*!
 * \brief The HeaderStyle class
 */
class HeaderStyle : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double width             READ width              WRITE setWidth              NOTIFY widthChanged)
    Q_PROPERTY(double height            READ height             WRITE setHeight             NOTIFY heightChanged)
    Q_PROPERTY(QColor textColor         READ textColor          WRITE setTextColor          NOTIFY textColorChanged)
    Q_PROPERTY(CellFontStyle* font      READ fontStyle                                      NOTIFY fontStyleChanged)
    Q_PROPERTY(quint32 alignment        READ alignment          WRITE setAlignment          NOTIFY alignmentChanged)
    Q_PROPERTY(HeaderType headerType    READ headerType                                     NOTIFY headerTypeChanged)
    Q_PROPERTY(int leftMargin           READ leftMargin         WRITE setLeftMargin         NOTIFY leftMarginChanged)
    Q_PROPERTY(int rightMargin          READ rightMargin        WRITE setRightMargin        NOTIFY rightMarginChanged)
    Q_PROPERTY(QColor pinnedColor       READ pinnedColor        WRITE setPinnedColor        NOTIFY pinnedColorChanged)
    Q_PROPERTY(QColor backgroundColor   READ backgroundColor    WRITE setBackgroundColor    NOTIFY backgroundColorChanged)
    friend class Table;
    friend class TableStyle;
public:
    enum HeaderType{
        Vertical,
        Horizontal
    };

    Q_ENUM(HeaderType)

public:
    explicit HeaderStyle(HeaderStyle::HeaderType type, QObject *parent = nullptr);

    double height() const;
    void setHeight(double newHeight);

    double width() const;
    void setWidth(double newWidth);

    const QColor backgroundColor() const;
    void setBackgroundColor(const QColor &newBackgroundColor);

    const QColor textColor() const;
    void setTextColor(const QColor &newTextColor);

    int leftMargin() const;
    void setLeftMargin(int newLeftMargin);

    int rightMargin() const;
    void setRightMargin(int newRightMargin);

    CellFontStyle *fontStyle();

    HeaderType headerType() const;

    quint32 alignment() const;
    void setAlignment(quint32 newAlignment);

    QColor pinnedColor() const;
    void setPinnedColor(QColor pinnedColor);

public slots:

signals:
    void styleChanged();
    void heightChanged();
    void widthChanged();
    void backgroundColorChanged();
    void textColorChanged();
    void leftMarginChanged();
    void rightMarginChanged();
    void fontStyleChanged();
    void headerTypeChanged();
    void headerChanged();
    void alignmentChanged();
    void iconsWidthChanged();
    void pinnedColorChanged();


private:
    double m_height = 20;
    double m_width = 20;
    QColor m_backgroundColor = "gray";
    QColor m_textColor = "black";
    int m_leftMargin = 2;
    int m_rightMargin = 2;
    CellFontStyle *m_font = nullptr;
    HeaderType m_headerType;
    quint32 m_alignment = Qt::AlignHCenter | Qt::AlignVCenter;
    bool m_widthChangeable = true;
    QColor m_pinnedColor = "lightblue";
};

#endif // HEADERSTYLE_H
