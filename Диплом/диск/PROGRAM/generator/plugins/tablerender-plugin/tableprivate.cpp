#include "tableprivate.h"

#include <QSGFlatColorMaterial>
#include <QSGNode>
#include <QDebug>
#include <QSGSimpleTextureNode>
#include <QQuickWindow>
#include <QPainter>
#include <itablecolorprovider.h>
#include <QTimer>
#include <QSGGeometryNode>
#include <scenegraph/shadowedrectanglenode.h>

/*!
 *   \class Table
 *   \since TableRender 1.0
 *   \brief The Table class allows the user to view database's content.
 *
 * */
class Table;

/*!
 * \class HighlightNode
 * \brief The HighlightNode class is only used for getting easy access to childs of higlight side
 */
class HighlightNode : public QSGNode
{
public:
    HighlightNode() {}
    //! childs
    QSGGeometryNode *highlight = nullptr;
    QSGGeometryNode *background = nullptr;

    //! material
    QSGFlatColorMaterial *bgMaterial = nullptr;
    QSGFlatColorMaterial *highlightMaterial = nullptr;
};
/*!
 * \class MovingHeaderNode
 * \brief The MovingHeaderNode class is only used for getting easy access to childs of moving header side
 */
class MovingHeaderNode : public QSGGeometryNode
{
public:
    MovingHeaderNode() {}
    QSGSimpleTextureNode *texture = nullptr;
    ShadowedRectangleNode *shadowNode = nullptr;
    int prevColumnIndex = -1;
    QImage img;
};

/*!
 * \class MainNode
 * \brief The MainNode class is the main visual object, which creating in Table::updatePaintNode method. Its used only for simple access to QSGNode and all his childs
 */
class MainNode : public QSGNode
{
public:
    MainNode() {};
    //! subchilds
    QSGNode *pages = nullptr;
    QSGGeometryNode *grid = nullptr;
    QSGGeometryNode *selectedCell = nullptr;
    QSGGeometryNode *columnResizer = nullptr;
    QSGNode *verticalHeader = nullptr;
    QSGNode *horizontalHeader = nullptr;
    QSGNode *styleCellBackground = nullptr;
    QSGNode *colorProviderBackground = nullptr;
    HighlightNode *verticalHeaderHighlight = nullptr;
    HighlightNode *horizontalHeaderHighlight = nullptr;
    QSGGeometryNode *fixRectNode = nullptr;

    QSGNode * pinnedPagesNode = nullptr;
    QSGNode *pinnedHeaderNode = nullptr;
    QSGNode *pinnedSelectionNode = nullptr;
    MovingHeaderNode *movingHeader = nullptr;
    QSGNode *pinnedSelectionHeaderNode = nullptr;
    //! childs
    QSGTransformNode *shiftXTransformNode = nullptr;
    QSGTransformNode *shiftYTransformNode = nullptr;
    QSGTransformNode *shiftXYTransformNode = nullptr;
};

/*!
  \fn Table::Table(QQuickItem *parent)
  \brief Constructor Table
 */
Table::Table(QQuickItem *parent) : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setFlag(QQuickItem::ItemAcceptsInputMethod, true);
    setFlag(QQuickItem::ItemIsFocusScope, true);
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton | Qt::MiddleButton);
    setClip(true);
    setSmooth(true);
    setAntialiasing(true);
    setAcceptHoverEvents(true);
    forceActiveFocus();

    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);

    connect(m_timer, &QTimer::timeout, this, [this](){
        if (pressed()){
            m_moveInternalPosPress = m_mousePos.x() - m_columns[m_columnIndeces[m_movingColumnIndex]].offset - (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
            m_moveCurrentXPos = m_mousePos.x() - m_moveInternalPosPress;
            setColumnsMoving(true);
            setCursor(Qt::DragMoveCursor);
            updateReasons.setFlag(UPDATE_HEADER_MOVING);
            updateReasons.setFlag(UPDATE_HEADER);
            update();
        }
    });

    m_style = LazyTableStyle::instance()->getDefaultStyle();

    connect(this, &Table::widthChanged, this, [this](){
        setViewPos(QRectF{m_viewPos.x(), m_viewPos.y(), width() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_viewPos.height()});
    });

    connect(this, &Table::heightChanged, this, [this](){
        setViewPos(QRectF{m_viewPos.x(), m_viewPos.y(), m_viewPos.width(), height()});
    });

    updateHeaders();
}

/*!
 * \fn void Table::increaseRow()
 * \brief Used for increasing current row by 1 if it possible.
 */
void Table::increaseRow()
{
    if (m_currentRow < (m_rowsCount-1)) {
        setCurrentRow(m_currentRow + 1);
    }
}

/*!
 * \fn void Table::increaseColumn()
 * \brief Used for increasing current column by 1 if it possible.
 */
void Table::increaseColumn()
{
    if (m_currentColumn < (m_columns.size() - 1)) {
        setCurrentColumn(m_currentColumn + 1);
    }
}

/*!
 * \fn void Table::decreaseRow()
 * \brief Used only for decreasing current row by 1 if it possible.
 */
void Table::decreaseRow()
{
    if (m_currentRow > 0) {
        setCurrentRow(m_currentRow - 1);
    }
}

/*!
 * \fn void Table::decreaseColumn()
 * \brief Usedonly for decresing current column by 1 if it possible.
 */
void Table::decreaseColumn()
{
    if (m_currentColumn > 0) {
        setCurrentColumn(m_currentColumn - 1);
    }
}

/*!
 * \fn void Table::moveToPos(double x, double y)
 * \brief Used for change visual rect position (Table::viewPos).
 */
void Table::moveToPos(double x, double y)
{
    //x side
    if (x < 0 || contentWidth() - viewPos().width() < 0){
        x = 0;
    }else if (x > contentWidth() - viewPos().width()){
        x = contentWidth() - viewPos().width();
    }

    if (y < 0){
        y = 0;
    }else if (y > contentHeight() - viewPos().height()){
        // y = contentHeight() - viewPos().height();
    }
    if (viewPos().x() != x)
        updateReasons.setFlag(SHIFT_X);
    if (viewPos().y() != y)
        updateReasons.setFlag(SHIFT_Y);

    if (viewPos().x() == x && viewPos().y() == y){
        return;
    }
    setViewPos({x, y, m_viewPos.width(), m_viewPos.height()});
    update();
}

/*!
 * \fn void Table::reset(bool resetViewPos)
 * \brief Used for reseting all internal variables when reloading ro changing provider.
 */
void Table::reset(bool resetViewPos)
{
    if (resetViewPos){
        m_pages.clear();
        setRowsCount(0);
        setPagesCount(0);
        setCurrentColumn(0);
        setCurrentRow(0);
        setReachedBottom(false);
        moveToPos(0, 0);
    }

    auto h = m_provider->headerData();
    if (h.size() != m_prevHeaderData.size() && h != m_prevHeaderData){
        setContentWidth(0);
        m_columnIndeces.clear();
        m_columns.clear();
        m_columns.resize(m_provider->columnCount());
        double r = 0;
        for (int  i=0, l = m_provider->columnCount(); i <l; ++i) {
            m_columns[i].width = h.length() - 1 < i || h[i].width == 0 ? m_style->horizontalHeader()->width() : h[i].width;
            m_columns[i].name = h[i].text;
            m_columns[i].backgroundColor = h[i].backgroundColor;
            m_columns[i].textColor = h[i].textColor;
            m_columns[i].offset = r;
            m_columns[i].cellAlignment = h[i].cellAlignment;
            m_columns[i].headerAlignment = h[i].headerAlignment;
            m_columnIndeces.insert(i,i);
            r+= m_columns[i].width;
        }
        setContentWidth(r);
        setColumnsCount(m_columns.size());
    }
    if (rowsCount() == 0) {
        setCurrentRow(-1);
    }

    m_prevHeaderData = h;
    updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    updateReasons.setFlag(RESIZE_COLUMN);

    setRowsCount(m_provider->rowCount());
    updatePageVisibility();
    updateAllPages();
    updateHeaders();
}

/*!
 * \fn Table::updateRows(int startRow, int endRow, ITableDataProvider::Reason r)
 * \brief Used for updating internal cache of pages. When \a startRow and \a endRow are positions of updating.
 */
void Table::updateRows(int startRow, int endRow, ITableDataProvider::Reason r)
{
    Q_UNUSED(r)
    setRowsCount(m_provider->rowCount());
    setReachedBottom(m_provider->isFullLoaded());
    // поиск страниц, где происходит редактирование
    int startPage = pageIndex(startRow), endPage = pageIndex(endRow);
    for (int i = startPage; i <= endPage; i++){
        // проверка на наличие страницы в кэше
        updatePage(i);
    }

    if (rowsCount() == 0){
        //setCurrentColumn(-1);
        setCurrentRow(-1);
    }

    updateHeaders();
    updatePageVisibility();
}

/*!
 * \fn Table::updateContentWidth
 * \brief Used only for \a contentWidth update.
 */
void Table::updateContentWidth()
{
    double r = 0;
    for (const auto &c : qAsConst(m_columns)){
        r += c.width;
    }
    setContentWidth(r);
}

/*!
 * \fn int Table::rowIndex(double y)
 * \brief Used only as helper which takes \a y and returns row.
 */
int Table::rowIndex(double y)
{
    return (y - m_style->horizontalHeader()->height()) / m_style->cellStyle()->height();
}

int Table::columnIndex(double x)
{
    int result = -1;
    for (int i = 0; i < m_columns.size(); i++){
        if (m_columns[m_columnIndeces[i]].offset  <= x && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= x ){
            result = i;
            break;
        }
    }
    return result;
}

/*!
 * \fn int Table::pageIndex(int row)
 * \brief Used for getting index of page by \a row.
 */
int Table::pageIndex(int row)
{
    return row / m_pageRowsCount;
}

/*!
 * \fn QSGNode *Table::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
 * \brief main visual layer method which called from another thread.
 * All visual classes are creating here.
 */
QSGNode *Table::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    MainNode* node = static_cast<MainNode*>(oldNode);
    if (m_columns.size() == 0) return node;
    if (node == nullptr){
        QRectF r;
        node = new MainNode();
        node->shiftXTransformNode = new QSGTransformNode();
        node->shiftYTransformNode = new QSGTransformNode();
        node->shiftXYTransformNode = new QSGTransformNode();
        node->appendChildNode(node->shiftXYTransformNode);
        node->appendChildNode(node->shiftYTransformNode);
        node->appendChildNode(node->shiftXTransformNode);

        node->grid =                            drawGrid(nullptr);
        node->pages =                           drawPages(nullptr);
        node->selectedCell =                    drawSelectedCell(nullptr);
        node->columnResizer =                   drawColumnResizer(nullptr);
        node->verticalHeader =                  drawVerticalHeader(nullptr);
        node->horizontalHeader =                drawHorizontalHeader(nullptr);
        node->verticalHeaderHighlight =         drawVerticalHeaderHightLight(nullptr);
        node->horizontalHeaderHighlight =       drawHorizontalHeaderHightLight(nullptr);
        node->styleCellBackground =             drawBackground(nullptr);
        node->colorProviderBackground =         drawColorProviderBackground(nullptr);
        node->movingHeader =                    drawMovingHeader(nullptr);
        node->pinnedPagesNode =                 drawPinnedPages(nullptr);
        node->pinnedHeaderNode =                drawPinnedHeaders(nullptr);
        node->pinnedSelectionNode =             drawPinnedHighlight(nullptr, r);
        node->pinnedSelectionHeaderNode =       drawPinnedHeaderHighlight(nullptr, r);
        node->fixRectNode =                     drawFixRectangle(nullptr);

        node->appendChildNode(node->fixRectNode);

        node->shiftXTransformNode->appendChildNode(node->horizontalHeader);
        node->shiftXTransformNode->appendChildNode(node->horizontalHeaderHighlight);
        node->shiftXTransformNode->appendChildNode(node->movingHeader);
        node->shiftXTransformNode->appendChildNode(node->pinnedHeaderNode);
        node->shiftXTransformNode->appendChildNode(node->pinnedSelectionHeaderNode);
        node->shiftXTransformNode->appendChildNode(node->columnResizer);
        node->shiftYTransformNode->appendChildNode(node->verticalHeader);
        node->shiftYTransformNode->appendChildNode(node->verticalHeaderHighlight);
        node->shiftYTransformNode->appendChildNode(node->pinnedPagesNode);
        node->shiftYTransformNode->appendChildNode(node->pinnedSelectionNode);
        node->shiftXYTransformNode->appendChildNode(node->styleCellBackground);
        node->shiftXYTransformNode->appendChildNode(node->colorProviderBackground);
        node->shiftXYTransformNode->appendChildNode(node->pages);
        node->shiftXYTransformNode->appendChildNode(node->grid);
        node->shiftXYTransformNode->appendChildNode(node->selectedCell);
    }

    if (m_columns.size() == 0) return node;

    if (updateReasons.testFlag(SHIFT_X)) {
        node->shiftXTransformNode->setMatrix(QTransform().translate(-viewPos().x(),0));
        node->shiftXTransformNode->markDirty(QSGNode::DirtyMatrix);
    }
    if (updateReasons.testFlag(SHIFT_Y)) {
        node->shiftYTransformNode->setMatrix(QTransform().translate(0, -viewPos().y()));
        node->shiftYTransformNode->markDirty(QSGNode::DirtyMatrix);
    }
    if (updateReasons.testFlag(SHIFT_X) || updateReasons.testFlag(SHIFT_Y)) {
        node->shiftXYTransformNode->setMatrix(QTransform().translate(-viewPos().x(), -viewPos().y()));
        node->shiftXYTransformNode->markDirty(QSGNode::DirtyMatrix);
    }

    drawVerticalHeaderHightLight(node->verticalHeaderHighlight);
    drawHorizontalHeaderHightLight(node->horizontalHeaderHighlight);
    drawMovingHeader(node->movingHeader);
    drawPinnedPages(node->pinnedPagesNode);
    drawPinnedHeaders(node->pinnedHeaderNode);
    QRectF r;
    drawPinnedHighlight(node->pinnedSelectionNode, r);
    drawPinnedHeaderHighlight(node->pinnedSelectionHeaderNode, r);

    drawHorizontalHeader(node->horizontalHeader);

    drawColumnResizer(node->columnResizer);

    node->verticalHeader = drawVerticalHeader(node->verticalHeader);
    drawFixRectangle(node->fixRectNode);
    drawGrid(node->grid);

    drawBackground(node->styleCellBackground);
    node->colorProviderBackground = drawColorProviderBackground(node->colorProviderBackground);
    node->pages = drawPages(node->pages);
    node->selectedCell = drawSelectedCell(node->selectedCell);

    if (updateReasons.testFlag(SELECT_CHANGED)) {
        node->shiftXYTransformNode->markDirty(QSGNode::DirtySubtreeBlocked);
    }

    if (updateReasons.testFlag(RESIZE_COLUMN)) {
        node->shiftXTransformNode->markDirty(QSGNode::DirtySubtreeBlocked);
    }

    node->markDirty(QSGNode::DirtySubtreeBlocked);
    updateReasons = NONE;

    if (m_autoMove != QPoint{0, 0}){
        updateReasons.setFlag(SELECT_CHANGED);
        if (viewPos().y() + viewPos().height() < contentHeight()){
            moveToPos(viewPos().x() + m_autoMove.x(), viewPos().y() + m_autoMove.y());
        }else{
            moveToPos(viewPos().x() + m_autoMove.x(), viewPos().y());
        }
    }
    return node;
}

/*!
 * \fn QSGNode *Table::drawPinnedHighlight(QSGNode *n, QRectF &res)
 * \brief Used for drawing pinned columns hightlight(without header).
 */
QSGNode *Table::drawPinnedHighlight(QSGNode *n, QRectF &res)
{
    if (n == nullptr){
        n = new QSGNode();
    }

    QSGNode *l = nullptr;
    QSGNode *r = nullptr;
    QSGNode *frame = nullptr;

    if (n->childCount() != 3){
        l = new QSGNode();
        n->appendChildNode(l);

        r = new QSGNode();
        n->appendChildNode(r);

        frame = new QSGNode();
        n->appendChildNode(frame);
    }else{
        l = n->childAtIndex(0);
        r = n->childAtIndex(1);
        frame = n->childAtIndex(2);
    }

    // pinned selection scheme
    //              tlf                        trf
    //               |                          |
    //               V                          V
    //         |=============............============|   <-y1
    //         ||           |            |          ||
    //         ||    lr1    |            |    rr1   ||
    //         ||           | <-rlf      |          ||
    //   llf-> |=============            ============|   <-rrf
    //         ||           |      lrf-> |          ||
    //         ||    lr2    |            |    rr2   ||
    //         ||           |            |          ||
    //         |=============............============|   <-y2
    //              ^                           ^
    //              |                           |
    //              blf                         brf
    //
    // =============
    // lr1 => left rect 1 -> small rect
    // lr2 => left rect 2 -> huge rect
    // rr1 => right rect 1
    // rr2 => right rect 2

    QSGGeometryNode *lr1 = nullptr;
    QSGGeometryNode *lr2 = nullptr;
    QSGGeometryNode *rr1 = nullptr;
    QSGGeometryNode *rr2 = nullptr;

    QSGGeometryNode *llf = nullptr;
    QSGGeometryNode *rlf = nullptr;
    QSGGeometryNode *tlf = nullptr;
    QSGGeometryNode *blf = nullptr;
    QSGGeometryNode *lrf = nullptr;
    QSGGeometryNode *rrf = nullptr;
    QSGGeometryNode *trf = nullptr;
    QSGGeometryNode *brf = nullptr;

    QSGFlatColorMaterial *lbgMaterial = nullptr;
    QSGFlatColorMaterial *rbgMaterial = nullptr;
    QSGFlatColorMaterial *frameMaterial = nullptr;

    int startLColumn = -1;
    int endLColumn = -1;
    double cl_x1 = (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    double cl_x2 = 0, cl_x3 = 0;
    double lwidth = 0;

    int startRColumn = -1;
    int endRColumn = -1;

    double cr_x1 = (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    double cr_x2 = 0, cr_x3 = 0;
    double rwidth = 0;
    double absoluteRWidth = 0;

    bool currentLAvailable = false, currentRAvailable = false;

    //searching for selected pinned columns
    for (const auto &col : qAsConst(m_actuallyPinnedColumns)){
        if (col.isLeft){
            bool sel = false;
            if (m_selectedColumnsCount > 0){
                if (col.visualIndex >= m_currentColumn && col.visualIndex < m_currentColumn + m_selectedColumnsCount){
                    sel = true;
                }
            }else{
                if (col.visualIndex >= m_currentColumn + m_selectedColumnsCount + 1 && col.visualIndex <= m_currentColumn){
                    sel = true;
                }
            }

            if (sel){
                if (startLColumn == -1){
                    startLColumn = col.visualIndex;
                }
                if (col.visualIndex == m_currentColumn)
                    currentLAvailable = true;

                lwidth += col.width;
                endLColumn = col.visualIndex;
            }

            if (startLColumn == -1)
                cl_x1 += col.width;
        }else{
            bool sel = false;
            if (m_selectedColumnsCount > 0){
                if (col.visualIndex >= m_currentColumn && col.visualIndex < m_currentColumn + m_selectedColumnsCount){
                    sel = true;
                }
            }else{
                if (col.visualIndex >= m_currentColumn + m_selectedColumnsCount + 1 && col.visualIndex <= m_currentColumn){
                    sel = true;
                }
            }

            if (sel){
                if (startRColumn == -1){
                    startRColumn = col.visualIndex;
                }
                if (col.visualIndex == m_currentColumn)
                    currentRAvailable = true;

                rwidth += col.width;
                endRColumn = col.visualIndex;
            }

            absoluteRWidth += col.width;

            if (startRColumn == -1)
                cr_x1 += col.width;
        }
    }

    if (l->childCount() != 2){
        lbgMaterial = new QSGFlatColorMaterial();

        lr1 = new QSGGeometryNode();
        lr1->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        lr1->setMaterial(lbgMaterial);
        lr1->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        lr1->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        lr2 = new QSGGeometryNode();
        lr2->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        lr2->setMaterial(lbgMaterial);
        lr2->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        lr2->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        l->appendChildNode(lr1);
        l->appendChildNode(lr2);
    }else{
        lr1 = static_cast<QSGGeometryNode*>(l->childAtIndex(0));
        lr2 = static_cast<QSGGeometryNode*>(l->childAtIndex(1));
        lbgMaterial = static_cast<QSGFlatColorMaterial*>(lr1->material());
    }

    if (r->childCount() != 2){
        rbgMaterial = new QSGFlatColorMaterial();

        rr1 = new QSGGeometryNode();
        rr1->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rr1->setMaterial(rbgMaterial);
        rr1->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rr1->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        rr2 = new QSGGeometryNode();
        rr2->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rr2->setMaterial(rbgMaterial);
        rr2->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rr2->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        r->appendChildNode(rr1);
        r->appendChildNode(rr2);

    }else{
        rr1 = static_cast<QSGGeometryNode*>(r->childAtIndex(0));
        rr2 = static_cast<QSGGeometryNode*>(r->childAtIndex(1));
        rbgMaterial = static_cast<QSGFlatColorMaterial*>(rr1->material());
    }

    if (frame->childCount() != 8){
        frameMaterial = new QSGFlatColorMaterial();

        llf = new QSGGeometryNode();
        llf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        llf->setMaterial(frameMaterial);
        llf->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        llf->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        rlf = new QSGGeometryNode();
        rlf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rlf->setMaterial(frameMaterial);
        rlf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        tlf = new QSGGeometryNode();
        tlf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        tlf->setMaterial(frameMaterial);
        tlf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        blf = new QSGGeometryNode();
        blf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        blf->setMaterial(frameMaterial);
        blf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        lrf = new QSGGeometryNode();
        lrf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        lrf->setMaterial(frameMaterial);
        lrf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        rrf = new QSGGeometryNode();
        rrf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rrf->setMaterial(frameMaterial);
        rrf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        trf = new QSGGeometryNode();
        trf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        trf->setMaterial(frameMaterial);
        trf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        brf = new QSGGeometryNode();
        brf->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        brf->setMaterial(frameMaterial);
        brf->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        frame->appendChildNode(llf);
        frame->appendChildNode(rlf);
        frame->appendChildNode(tlf);
        frame->appendChildNode(blf);
        frame->appendChildNode(lrf);
        frame->appendChildNode(rrf);
        frame->appendChildNode(trf);
        frame->appendChildNode(brf);
    }else{

        llf = static_cast<QSGGeometryNode*>(frame->childAtIndex(0));
        rlf = static_cast<QSGGeometryNode*>(frame->childAtIndex(1));
        tlf = static_cast<QSGGeometryNode*>(frame->childAtIndex(2));
        blf = static_cast<QSGGeometryNode*>(frame->childAtIndex(3));
        lrf = static_cast<QSGGeometryNode*>(frame->childAtIndex(4));
        rrf = static_cast<QSGGeometryNode*>(frame->childAtIndex(5));
        trf = static_cast<QSGGeometryNode*>(frame->childAtIndex(6));
        brf = static_cast<QSGGeometryNode*>(frame->childAtIndex(7));

        frameMaterial = static_cast<QSGFlatColorMaterial*>(llf->material());
    }

    //top y pos and bottom y pos
    double y1 = 0.0, y2 = 0.0;
    if (m_currentColumn >= 0 && m_currentRow >= 0){
        if (m_selectedRowsCount > 0){
            y1 = m_currentRow*m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            y2 = y1 + m_style->cellStyle()->height() * selectedRowsCount();
        }else{
            y2 = m_currentRow * m_style->cellStyle()->height() + m_style->cellStyle()->height() * 2;
            y1 = y2 - qAbs(m_selectedRowsCount) * m_style->cellStyle()->height();
        }
    }

    if (endLColumn >= 0 && startLColumn >= 0){
        cl_x3 = cl_x1 + lwidth;
        cl_x2 = m_selectedColumnsCount > 0 ? (cl_x1 + (currentLAvailable ? m_columns[m_columnIndeces[startLColumn]].width : 0)) : cl_x3 - (currentLAvailable ? m_columns[m_columnIndeces[endLColumn]].width : 0);

        //small rect
        lr1->geometry()->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? cl_x1 : cl_x2, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
        lr1->geometry()->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? cl_x2 : cl_x3, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
        lr1->geometry()->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? cl_x2 : cl_x3, m_selectedRowsCount > 0 ? y2 : y2 - m_style->cellStyle()->height());
        lr1->geometry()->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? cl_x1 : cl_x2, m_selectedRowsCount > 0 ? y2 : y2 - m_style->cellStyle()->height());
        //huge rect (working)
        lr2->geometry()->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? cl_x2 : cl_x1, y1);
        lr2->geometry()->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? cl_x3 : cl_x2, y1);
        lr2->geometry()->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? cl_x3 : cl_x2, y2);
        lr2->geometry()->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? cl_x2 : cl_x1, y2);

        if (m_selectedColumnsCount > 0){
            llf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn == startLColumn ? cl_x1 - 1 : cl_x1, y1);
            llf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn == startLColumn ? cl_x1 + 1 : cl_x1, y1);
            llf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn == startLColumn ? cl_x1 + 1 : cl_x1, y2);
            llf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn == startLColumn ? cl_x1 - 1 : cl_x1, y2);

            rlf->geometry()->vertexDataAsPoint2D()[0].set(endLColumn == startLColumn && m_selectedColumnsCount == 1 ? cl_x3 + 1 : endLColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cl_x3 + 1 : m_currentColumn == startLColumn ? cl_x3 : cl_x3, y1);
            rlf->geometry()->vertexDataAsPoint2D()[1].set(endLColumn == startLColumn && m_selectedColumnsCount == 1 ? cl_x3 - 1 : endLColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cl_x3 - 1 : m_currentColumn == startLColumn ? cl_x3 : cl_x3, y1);
            rlf->geometry()->vertexDataAsPoint2D()[2].set(endLColumn == startLColumn && m_selectedColumnsCount == 1 ? cl_x3 + 1 : endLColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cl_x3 + 1 : m_currentColumn == startLColumn ? cl_x3 : cl_x3, y2);
            rlf->geometry()->vertexDataAsPoint2D()[3].set(endLColumn == startLColumn && m_selectedColumnsCount == 1 ? cl_x3 - 1 : endLColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cl_x3 - 1 : m_currentColumn == startLColumn ? cl_x3 : cl_x3, y2);
        }else{
            llf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn + m_selectedColumnsCount + 1 == startLColumn ? cl_x1 - 1 : cl_x1, y1);
            llf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn + m_selectedColumnsCount + 1 == startLColumn ? cl_x1 + 1 : cl_x1, y1);
            llf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn + m_selectedColumnsCount + 1 == startLColumn ? cl_x1 + 1 : cl_x1, y2);
            llf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn + m_selectedColumnsCount + 1 == startLColumn ? cl_x1 - 1 : cl_x1, y2);

            rlf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn == endLColumn ? cl_x3 + 1 : cl_x3, y1);
            rlf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn == endLColumn ? cl_x3 - 1 : cl_x3, y1);
            rlf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn == endLColumn ? cl_x3 + 1 : cl_x3, y2);
            rlf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn == endLColumn ? cl_x3 - 1 : cl_x3, y2);
        }

        tlf->geometry()->vertexDataAsPoint2D()[0].set(cl_x3, y1 - 1);
        tlf->geometry()->vertexDataAsPoint2D()[1].set(cl_x1, y1 - 1);
        tlf->geometry()->vertexDataAsPoint2D()[2].set(cl_x3, y1 + 1);
        tlf->geometry()->vertexDataAsPoint2D()[3].set(cl_x1, y1 + 1);

        blf->geometry()->vertexDataAsPoint2D()[0].set(cl_x3, y2 - 1);
        blf->geometry()->vertexDataAsPoint2D()[1].set(cl_x1, y2 - 1);
        blf->geometry()->vertexDataAsPoint2D()[2].set(cl_x3, y2 + 1);
        blf->geometry()->vertexDataAsPoint2D()[3].set(cl_x1, y2 + 1);

    }else{
        //small
        lr1->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        lr1->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        lr1->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        lr1->geometry()->vertexDataAsPoint2D()[3].set(0,0);
        //huge rect
        lr2->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        lr2->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        lr2->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        lr2->geometry()->vertexDataAsPoint2D()[3].set(0,0);
        //left frame of left rect
        llf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        llf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        llf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        llf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        rlf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rlf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rlf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rlf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        tlf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        tlf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        tlf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        tlf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        blf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        blf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        blf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        blf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        llf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        rlf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        tlf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        blf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    }

    if (endRColumn >= 0 && startRColumn >= 0){
        cr_x1 = viewPos().width() - absoluteRWidth + cr_x1;
        cr_x3 = cr_x1 + rwidth;
        cr_x2 = m_selectedColumnsCount > 0 ? (cr_x1 + (currentRAvailable ? m_columns[m_columnIndeces[startRColumn]].width : 0)) : cr_x3 - (currentRAvailable ? m_columns[m_columnIndeces[endRColumn]].width : 0);

        //small rect
        rr1->geometry()->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? cr_x1 : cr_x2, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
        rr1->geometry()->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? cr_x2 : cr_x3, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
        rr1->geometry()->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? cr_x2 : cr_x3, m_selectedRowsCount > 0 ? y2 : y2 - m_style->cellStyle()->height());
        rr1->geometry()->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? cr_x1 : cr_x2, m_selectedRowsCount > 0 ? y2 : y2 - m_style->cellStyle()->height());
        //huge rect (working)
        rr2->geometry()->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? cr_x2 : cr_x1, y1);
        rr2->geometry()->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? cr_x3 : cr_x2, y1);
        rr2->geometry()->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? cr_x3 : cr_x2, y2);
        rr2->geometry()->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? cr_x2 : cr_x1, y2);

        if (m_selectedColumnsCount > 0){
            lrf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn == startRColumn ? cr_x1 + 1 : cr_x1, y1);
            lrf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn == startRColumn ? cr_x1 - 1 : cr_x1, y1);
            lrf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn == startRColumn ? cr_x1 + 1 : cr_x1, y2);
            lrf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn == startRColumn ? cr_x1 - 1 : cr_x1, y2);

            rrf->geometry()->vertexDataAsPoint2D()[0].set(endRColumn == startRColumn && m_selectedColumnsCount == 1 ? cr_x3 + 2 : endRColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cr_x3 + 2    : m_currentColumn == startRColumn ? cr_x3 : cr_x3, y1);
            rrf->geometry()->vertexDataAsPoint2D()[1].set(endRColumn == startRColumn && m_selectedColumnsCount == 1 ? cr_x3     : endRColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cr_x3        : m_currentColumn == startRColumn ? cr_x3 : cr_x3, y1);
            rrf->geometry()->vertexDataAsPoint2D()[2].set(endRColumn == startRColumn && m_selectedColumnsCount == 1 ? cr_x3 + 2 : endRColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cr_x3 + 2    : m_currentColumn == startRColumn ? cr_x3 : cr_x3, y2);
            rrf->geometry()->vertexDataAsPoint2D()[3].set(endRColumn == startRColumn && m_selectedColumnsCount == 1 ? cr_x3     : endRColumn == m_currentColumn + m_selectedColumnsCount - 1 ? cr_x3        : m_currentColumn == startRColumn ? cr_x3 : cr_x3, y2);
        }else{
            lrf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn + m_selectedColumnsCount + 1 == startRColumn ? cr_x1 + 2 : cl_x1, y1);
            lrf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn + m_selectedColumnsCount + 1 == startRColumn ? cr_x1     : cl_x1, y1);
            lrf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn + m_selectedColumnsCount + 1 == startRColumn ? cr_x1 + 2 : cl_x1, y2);
            lrf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn + m_selectedColumnsCount + 1 == startRColumn ? cr_x1     : cl_x1, y2);

            rrf->geometry()->vertexDataAsPoint2D()[0].set(m_currentColumn == endRColumn ? cr_x3 + 2 : cr_x3, y1);
            rrf->geometry()->vertexDataAsPoint2D()[1].set(m_currentColumn == endRColumn ? cr_x3     : cr_x3, y1);
            rrf->geometry()->vertexDataAsPoint2D()[2].set(m_currentColumn == endRColumn ? cr_x3 + 2 : cr_x3, y2);
            rrf->geometry()->vertexDataAsPoint2D()[3].set(m_currentColumn == endRColumn ? cr_x3     : cr_x3, y2);
        }

        trf->geometry()->vertexDataAsPoint2D()[0].set(cr_x3, y1 - 1);
        trf->geometry()->vertexDataAsPoint2D()[1].set(cr_x1, y1 - 1);
        trf->geometry()->vertexDataAsPoint2D()[2].set(cr_x3, y1 + 1);
        trf->geometry()->vertexDataAsPoint2D()[3].set(cr_x1, y1 + 1);

        brf->geometry()->vertexDataAsPoint2D()[0].set(cr_x3, y2 - 1);
        brf->geometry()->vertexDataAsPoint2D()[1].set(cr_x1, y2 - 1);
        brf->geometry()->vertexDataAsPoint2D()[2].set(cr_x3, y2 + 1);
        brf->geometry()->vertexDataAsPoint2D()[3].set(cr_x1, y2 + 1);

    }else{
        rr1->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rr1->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rr1->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rr1->geometry()->vertexDataAsPoint2D()[3].set(0,0);
        //huge rect
        rr2->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rr2->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rr2->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rr2->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        lrf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        lrf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        lrf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        lrf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        rrf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rrf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rrf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rrf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        trf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        trf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        trf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        trf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        brf->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        brf->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        brf->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        brf->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        lrf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        rrf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        trf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
        brf->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    }

    res.setX(cl_x1);
    res.setY(cl_x3);
    res.setWidth(cr_x1);
    res.setHeight(cr_x3);

    QColor c = m_style->selectionColor();
    c.setAlphaF(m_style->selectionOpacity());
    lbgMaterial->setColor(c);
    rbgMaterial->setColor(c);
    frameMaterial->setColor(m_style->highlightColor());
    lr2->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    lr1->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    rr1->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    rr2->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    n->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);

    return n;
}

/*!
 * \fn QSGNode *Table::drawPinnedHeaderHighlight(QSGNode *n, const QRectF &res)
 * \brief Used for drawing pinned header highlight only.
 */
QSGNode *Table::drawPinnedHeaderHighlight(QSGNode *n, const QRectF &res)
{
    if (n == nullptr){
        n = new QSGNode();
    }

    QSGFlatColorMaterial *selectionMaterial = nullptr;
    QSGFlatColorMaterial *highlightMaterial = nullptr;
    QSGGeometryNode *lg1 = nullptr;
    QSGGeometryNode *lg2 = nullptr;
    QSGGeometryNode *rg1 = nullptr;
    QSGGeometryNode *rg2 = nullptr;

    if (n->childCount() != 4){
        selectionMaterial = new QSGFlatColorMaterial();
        highlightMaterial = new QSGFlatColorMaterial();

        lg1 = new QSGGeometryNode();
        lg1->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        lg1->setMaterial(selectionMaterial);
        lg1->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        lg1->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        lg2 = new QSGGeometryNode();
        lg2->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        lg2->setMaterial(highlightMaterial);
        lg2->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        lg2->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        rg1 = new QSGGeometryNode();
        rg1->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rg1->setMaterial(selectionMaterial);
        rg1->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rg1->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        rg2 = new QSGGeometryNode();
        rg2->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        rg2->setMaterial(highlightMaterial);
        rg2->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rg2->setFlags(QSGNode::OwnsGeometry /*| QSGNode::OwnsMaterial*/);

        n->appendChildNode(lg1);
        n->appendChildNode(lg2);
        n->appendChildNode(rg1);
        n->appendChildNode(rg2);
    }else{
        lg1 = static_cast<QSGGeometryNode*>(n->childAtIndex(0));
        lg2 = static_cast<QSGGeometryNode*>(n->childAtIndex(1));
        rg1 = static_cast<QSGGeometryNode*>(n->childAtIndex(2));
        rg2 = static_cast<QSGGeometryNode*>(n->childAtIndex(3));

        selectionMaterial = static_cast<QSGFlatColorMaterial*>(lg1->material());
        highlightMaterial = static_cast<QSGFlatColorMaterial*>(lg2->material());
    }

    double cl_x1 = res.x(), cl_x2 = res.y(), cr_x1 = res.width(), cr_x2 = res.height();

    if (cl_x1 != 0 && cl_x2 != 0 && m_currentColumn != -1 && m_currentRow != -1){
        lg1->geometry()->vertexDataAsPoint2D()[0].set(viewPos().x() + cl_x1, 0);
        lg1->geometry()->vertexDataAsPoint2D()[1].set(viewPos().x() + cl_x2, 0);
        lg1->geometry()->vertexDataAsPoint2D()[2].set(viewPos().x() + cl_x2, m_style->horizontalHeader()->height() - 1);
        lg1->geometry()->vertexDataAsPoint2D()[3].set(viewPos().x() + cl_x1, m_style->horizontalHeader()->height() - 1);

        lg2->geometry()->vertexDataAsPoint2D()[0].set(viewPos().x() + cl_x1, m_style->horizontalHeader()->height() - 3);
        lg2->geometry()->vertexDataAsPoint2D()[1].set(viewPos().x() + cl_x2, m_style->horizontalHeader()->height() - 3);
        lg2->geometry()->vertexDataAsPoint2D()[2].set(viewPos().x() + cl_x2, m_style->horizontalHeader()->height() - 1);
        lg2->geometry()->vertexDataAsPoint2D()[3].set(viewPos().x() + cl_x1, m_style->horizontalHeader()->height() - 1);
    }else{
        lg1->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        lg1->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        lg1->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        lg1->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        lg2->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        lg2->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        lg2->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        lg2->geometry()->vertexDataAsPoint2D()[3].set(0,0);
    }

    if (cr_x1 != 0 && cr_x2 != 0 && m_currentColumn != -1 && m_currentRow != -1){
        rg1->geometry()->vertexDataAsPoint2D()[0].set(viewPos().x() + cr_x1, 0);
        rg1->geometry()->vertexDataAsPoint2D()[1].set(viewPos().x() + cr_x2, 0);
        rg1->geometry()->vertexDataAsPoint2D()[2].set(viewPos().x() + cr_x2, m_style->horizontalHeader()->height() - 1);
        rg1->geometry()->vertexDataAsPoint2D()[3].set(viewPos().x() + cr_x1, m_style->horizontalHeader()->height() - 1);

        rg2->geometry()->vertexDataAsPoint2D()[0].set(viewPos().x() + cr_x1, m_style->horizontalHeader()->height() - 3);
        rg2->geometry()->vertexDataAsPoint2D()[1].set(viewPos().x() + cr_x2, m_style->horizontalHeader()->height() - 3);
        rg2->geometry()->vertexDataAsPoint2D()[2].set(viewPos().x() + cr_x2, m_style->horizontalHeader()->height() - 1);
        rg2->geometry()->vertexDataAsPoint2D()[3].set(viewPos().x() + cr_x1, m_style->horizontalHeader()->height() - 1);
    }else{
        rg1->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rg1->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rg1->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rg1->geometry()->vertexDataAsPoint2D()[3].set(0,0);

        rg2->geometry()->vertexDataAsPoint2D()[0].set(0,0);
        rg2->geometry()->vertexDataAsPoint2D()[1].set(0,0);
        rg2->geometry()->vertexDataAsPoint2D()[2].set(0,0);
        rg2->geometry()->vertexDataAsPoint2D()[3].set(0,0);
    }

    QColor selectionColor = m_style->selectionColor();
    selectionColor.setAlphaF(m_style->selectionOpacity() * 0.8);
    selectionMaterial->setColor(selectionColor);

    highlightMaterial->setColor(m_style->highlightColor());

    n->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

    return n;
}

/*!
 * \fn QSGNode *Table::drawPinnedPages(QSGNode *n)
 * \brief Used for drawing pinned pages (column by column).
 */
QSGNode *Table::drawPinnedPages(QSGNode *n)
{
    if (n == nullptr){
        n = new QSGNode();
    }

    while(n->childCount() != 0){
        delete n->firstChild();
    }

    for (const auto &col : qAsConst(m_actuallyPinnedColumns)){
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto img = page.pinnedPages[col.visualIndex];
                QPainter painter(&img);
                painter.setRenderHints(QPainter::Antialiasing);
                painter.setPen(m_style->gridColor());
                painter.drawLine(col.isLeft ? col.width - 1 : 0, 0, col.isLeft ? col.width - 1 : 0, page.rowsCount * m_style->cellStyle()->height() - 1);

                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(col.offset, page.yoffset, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }

    n->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    return n;
}

/*!
 * \fn QSGNode *Table::drawPinnedHeaders(QSGNode *n)
 * \brief Used for drawing pinned headers only.
 */
QSGNode *Table::drawPinnedHeaders(QSGNode *n)
{
    if (n == nullptr){
        n = new QSGNode();
    }

    while(n->childCount() != 0){
        delete n->firstChild();
    }

    for (const auto &col : qAsConst(m_actuallyPinnedColumns)){
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto img = page.pinnedHeaders[col.visualIndex];
                QPainter painter(&img);
                painter.setRenderHints(QPainter::Antialiasing);
                painter.setPen(m_style->gridColor());
                painter.drawLine(col.isLeft ? col.width - 1 : 0, 0, col.isLeft ? col.width - 1 : 0, img.height() - 1);
                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(viewPos().x() + col.offset-1, 0, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }

    return n;
}

/*!
 * \fn void Table::mousePressEvent(QMouseEvent *event)
 * \brief Mouse hendler which proceed mouse press event.
 */
void Table::mousePressEvent(QMouseEvent *event)
{
    event->setAccepted(true);
    forceActiveFocus();
    m_mousePos = event->pos();

    if (m_columns.size() == 0) return;
    if (m_provider->rowCount() == 0) return;
    auto visualX = event->x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    setPressed(true);
    int currentColumn = 0;
    // [resizing]
    if (event->x() > m_pinnedLeftWidth + 5 + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && event->x() < viewPos().width() - m_pinnedRightWidth){
        if(event->y() < m_style->horizontalHeader()->height()){
            double iconWidth = m_style->iconsWidth();
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < 5 ){ // time to resize
                    m_resizeColumnIndex = i;
                    m_resizeCurrentXPos = x;
                    setResizing(true);
                    break;
                }else if (qAbs(delta) < iconWidth + 5){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                            return;
                    }
                }else if (m_columns[m_columnIndeces[i]].offset  <= visualX && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX ){
                    if (event->button() == Qt::RightButton){
                        return;
                    }
                    currentColumn = i;
                }
            }
            // last column
            auto ll = m_columns[m_columnIndeces[m_columns.size() - 1]];
            if (qAbs(visualX -  ll.offset - ll.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                m_resizeColumnIndex = m_columns.size() - 1;
                m_resizeCurrentXPos = visualX;
                setResizing(true);
            }
            if (resizing()) return;

            if (m_timer->isActive()){
                m_timer->stop();
            }

            if (columnsMovable()){
                m_movingColumnIndex = currentColumn;

                m_timer->setSingleShot(true);
                m_timer->start(120);
            }

            return;
        }
        if (event->button() == Qt::RightButton){
            int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
            if (row >= m_rowsCount) row = m_rowsCount - 1;
            int column = 0;
            for (int i =0, l = m_columns.size(); i< l; ++i) {
                if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                    column = i;
                    break;
                }
            }

            int startColumn = selectedColumnsCount() > 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() + 1;
            int endColumn = selectedColumnsCount() < 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() - 1;
            int startRow = selectedRowsCount() > 0 ? m_currentRow : m_currentRow + selectedRowsCount() + 1;
            int endRow = selectedRowsCount() < 0 ? m_currentRow : m_currentRow + selectedRowsCount() - 1;
            if (row == -1) {
                emit showContextMenu(event->pos());
                return;
            }
            if (m_provider->getRow(row).cells[m_columnIndeces[column]].isCheckBox){
                int nindex = columnIndex(visualX);
                auto pc = internalToNormal(QPointF{m_columns[m_columnIndeces[nindex]].offset + m_columns[m_columnIndeces[nindex]].width / 2 - 5,
                                                   event->y() + viewPos().y()});
                if (pc.x() - 5 <= event->pos().x() && pc.x() + 15 >= event->pos().x()){
                    emit checkboxPressed(row, m_columnIndeces[column]);
                    return;
                }
            }

            if (column >= startColumn && column <= endColumn){
                if (row >= startRow && row <= endRow){}
                else{
                    setCurrentCell(row, column);
                }
            }else{
                setCurrentCell(row, column);
            }

            emit showContextMenu(event->pos());
        }else{
            // [resizing]
            int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
            if (row >= m_rowsCount) row = m_rowsCount - 1;

            int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : 0);
            if (index < 0) index = 0;

            int nindex = columnIndex(visualX);
            if (row == -1) return;
            if (m_provider->getRow(row).cells[m_columnIndeces[nindex]].isCheckBox){
                auto pc = internalToNormal(QPointF{m_columns[m_columnIndeces[nindex]].offset + m_columns[m_columnIndeces[nindex]].width / 2 - 5,
                                                   event->y() + viewPos().y()});
                if (pc.x() - 5 <= event->pos().x() && pc.x() + 15 >= event->pos().x()){
                    emit checkboxPressed(row, m_columnIndeces[nindex]);
                    return;
                }
            }

            auto p  = internalToNormal(QPointF{m_columns[m_columnIndeces[index]].offset + m_columns[m_columnIndeces[index]].width + 5,
                                               (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + 5});
            auto pos = event->pos();

            if (p.x() >= pos.x() && p.x() <= pos.x() + 10
                    && p.y() >= pos.y() && p.y() <= pos.y() + 10) {
                m_selecting = true;
                updateReasons.setFlag(SELECT_CHANGED);
                update();
                return;
            }

            // [selection]
            setCurrentRow(row);
            m_selecting = true;
            for (int i =0, l = m_columns.size(); i< l; ++i) {
                if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                    setCurrentColumn(i);
                    break;
                }
            }

        }
    }else{
        double x = event->pos().x();
        for (auto &col : m_actuallyPinnedColumns){
            //! right side
            if (x >= viewPos().width() - m_pinnedRightWidth - 5 && m_pinnedRightWidth != 0){
                if (col.isLeft) continue;
                double delta = x - col.offset;
                if (qAbs(delta) < 5 && event->y() < m_style->horizontalHeader()->height()){ // time to resize
                    m_pinnedResizeColumnIndex = m_actuallyPinnedColumns.indexOf(col);
                    m_resizeCurrentXPos = event->pos().x();
                    m_pinnedResizing = true;
                    setResizing(true);
                    return;
                }else if (x > col.offset && x < col.offset + col.width){
                    if (col.offset + col.width / 2 <= x &&
                            col.offset + col.width / 2 + 17 >= x && m_provider->getRow(rowIndex(event->y() + viewPos().y())).cells[m_columnIndeces[col.visualIndex]].isCheckBox){
                        emit checkboxPressed(rowIndex(event->y() + viewPos().y()), m_columnIndeces[col.visualIndex]);
                        return;
                    }
                }
            }else if (x <= m_pinnedLeftWidth + 5 + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && m_pinnedLeftWidth != 0){
                if (!col.isLeft) continue;
                double delta = x - col.offset - col.width;
                if (qAbs(delta) < 5 && event->y() < m_style->horizontalHeader()->height()){ // time to resize
                    m_pinnedResizeColumnIndex = m_actuallyPinnedColumns.indexOf(col);
                    m_resizeCurrentXPos = event->pos().x();
                    m_pinnedResizing = true;
                    setResizing(true);
                    break;
                }else if (x > col.offset && x < col.offset + col.width){
                    if (col.offset + col.width / 2 - 5 <= x   &&
                            col.offset + col.width / 2 + 12 >= x && m_provider->getRow(rowIndex(event->y() + viewPos().y())).cells[m_columnIndeces[col.visualIndex]].isCheckBox){
                        emit checkboxPressed(rowIndex(event->y() + viewPos().y()), m_columnIndeces[col.visualIndex]);
                        return;
                    }
                }
            }
        }
        //part of rightside
        if (m_actuallyPinnedColumns.size() != 0 && (m_pinnedLeftWidth !=0 || m_pinnedRightWidth != 0)){
            auto &col = m_actuallyPinnedColumns.last();
            if (!col.isLeft){
                if (qAbs(x - col.offset - col.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                    m_pinnedResizeColumnIndex = m_actuallyPinnedColumns.indexOf(col);
                    m_resizeCurrentXPos = event->pos().x();
                    m_pinnedResizing = true;
                    setResizing(true);
                }
            }
        }
    }    
}

/*!
 * \fn void Table::mouseMoveEvent(QMouseEvent *event)
 * \brief Mouse handler which proceed mouse move event.
 */
void Table::mouseMoveEvent(QMouseEvent *event)
{
    if (m_provider->rowCount() == 0) return;
    auto x = event->x();
    auto y = event->y();
    if (!m_pressed) {
        m_selecting = false;
        m_resizing = false;
        return;
    }
    if (resizing()){
        m_resizeCurrentXPos = x;
        updateReasons.setFlag(RESIZE_COLUMN);
        update();
    }else if (m_selecting){
        checkSelect(x, y);
    }else if (columnsMoving() && columnsMovable()){
        m_moveCurrentXPos = x - m_moveInternalPosPress;
        if (!checkHeaderSwap()){
            updateReasons.setFlag(UPDATE_HEADER_MOVING);
            update();
        }
    }
}

/*!
 * \fn void Table::mouseReleaseEvent(QMouseEvent *event)
 * \brief Mouse handler which proceed mouse release event.
 */
void Table::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_columns.size() == 0) return;
    if (m_provider->rowCount() == 0) return;

    auto visualX = event->x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    if (event->x() > m_pinnedLeftWidth + 5 + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && event->x() < viewPos().width() - m_pinnedRightWidth){
        if(event->y() < m_style->horizontalHeader()->height() && !resizing() && !columnsMoving()){
            double iconWidth = m_style->iconsWidth();
            for (int i = 0, l = m_columns.size(); i < l; ++i){
                auto x = visualX - m_columns[m_columnIndeces[i]].width;
                double delta = x - m_columns[m_columnIndeces[i]].offset;
                if (qAbs(delta) < iconWidth + 5){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        emit headerPressed(event->button(),i, m_provider->headersWithButton().contains(m_columnIndeces[i]) ? LazyTableStyle::Button : LazyTableStyle::Free, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), 0, m_columns[m_columnIndeces[i]].width, m_style->horizontalHeader()->height()});
                        break;
                    }
                }else if (m_columns[m_columnIndeces[i]].offset  <= visualX && m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX ){
                    emit headerPressed(event->button(), i, LazyTableStyle::Free, {m_columns[m_columnIndeces[i]].offset - viewPos().x() + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                                                                                  0,
                                                                                  m_columns[m_columnIndeces[i]].width,
                                                                                  m_style->horizontalHeader()->height()});
                    break;
                }
            }
            setPressed(false);
            return;
        }
    }else if (!m_pinnedResizing && !m_resizing && !columnsMoving() && event->x() > (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)){
        double iconWidth = m_style->iconsWidth();
        double x = event->pos().x();
        for (auto &col : m_actuallyPinnedColumns){
            //! right side
            if (x >= viewPos().width() - m_pinnedRightWidth - 5 && m_pinnedRightWidth != 0){
                if (col.isLeft) continue;
                double delta = x - col.offset;
                if (qAbs(delta - col.width) < iconWidth + 5 && event->y() < m_style->horizontalHeader()->height() && qAbs(delta - col.width) > 5){
                    if (event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        emit headerPressed(event->button(),
                                           col.visualIndex,
                                           m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex]) ?
                                    LazyTableStyle::Button :
                                    LazyTableStyle::Free,
                        {col.offset,
                                    0,
                                    col.width,
                                    m_style->horizontalHeader()->height()
                        });

                        return;
                    }
                    break;
                }else if (col.offset  <= x && col.offset + col.width - 5 >= x){
                    if (event->y() < m_style->horizontalHeader()->height()){
                        emit headerPressed(event->button(), col.visualIndex, LazyTableStyle::Free, {col.offset,
                                                                                                    0,
                                                                                                    col.width,
                                                                                                    m_style->horizontalHeader()->height()});
                        return;
                    }
                    int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
                    if (row >= m_rowsCount) row = m_rowsCount - 1;
                    int column = col.visualIndex;

                    int startColumn = selectedColumnsCount() > 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() + 1;
                    int endColumn = selectedColumnsCount() < 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() - 1;
                    int startRow = selectedRowsCount() > 0 ? m_currentRow : m_currentRow + selectedRowsCount() + 1;
                    int endRow = selectedRowsCount() < 0 ? m_currentRow : m_currentRow + selectedRowsCount() - 1;

                    if (event->button() == Qt::LeftButton && m_provider->getRow(row).cells[m_columnIndeces[col.visualIndex]].isCheckBox){
                        if (col.offset + col.width / 2 <= x &&
                                col.offset + col.width / 2 + 17 >= x){
                            return;
                        }
                    }

                    if (column >= startColumn && column <= endColumn && !m_provider->getRow(row).cells[m_columnIndeces[column]].isCheckBox){
                        if (row >= startRow && row <= endRow){}
                        else{
                            setCurrentCell(row, column);
                        }
                    }else{
                        setCurrentCell(row, column);
                    }
                    if (event->button() == Qt::RightButton)
                        emit showContextMenu(event->pos());

                }
                //! left side
            }else if (x <= m_pinnedLeftWidth + 5 + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && m_pinnedLeftWidth != 0){
                if (!col.isLeft) continue;
                double delta = x - col.offset - col.width;
                if (qAbs(delta) < 5 && event->y() < m_style->horizontalHeader()->height()){ // time to resize
                    m_pinnedResizeColumnIndex = m_actuallyPinnedColumns.indexOf(col);
                    m_resizeCurrentXPos = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                    m_pinnedResizing = true;
                    setResizing(true);
                    break;
                }else if (qAbs(delta) < iconWidth + 5 && event->y() < m_style->horizontalHeader()->height()){
                    if (delta < 0
                            && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                            && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){
                        emit headerPressed(event->button(), col.visualIndex, m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex]) ? LazyTableStyle::Button : LazyTableStyle::Free, {col.offset,
                                    0,
                                    col.width,
                                    m_style->horizontalHeader()->height()});
                        return;
                    }
                }else if (col.offset  <= x && col.offset + col.width >= x){
                    if (event->y() < m_style->horizontalHeader()->height()){

                        emit headerPressed(event->button(), col.visualIndex, LazyTableStyle::Free, {col.offset,
                                                                                                    0,
                                                                                                    col.width,
                                                                                                    m_style->horizontalHeader()->height()});
                        return;
                    }
                    int row = (event->y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
                    if (row >= m_rowsCount) row = m_rowsCount - 1;
                    int column = col.visualIndex;

                    int startColumn = selectedColumnsCount() > 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() + 1;
                    int endColumn = selectedColumnsCount() < 0 ? m_currentColumn : m_currentColumn + selectedColumnsCount() - 1;
                    int startRow = selectedRowsCount() > 0 ? m_currentRow : m_currentRow + selectedRowsCount() + 1;
                    int endRow = selectedRowsCount() < 0 ? m_currentRow : m_currentRow + selectedRowsCount() - 1;

                    if (event->button() == Qt::LeftButton && m_provider->getRow(row).cells[m_columnIndeces[col.visualIndex]].isCheckBox){
                        if (col.offset + col.width / 2 <= x &&
                                col.offset + col.width / 2 + 17 >= x){
                            return;
                        }
                    }

                    if (event->button() == Qt::LeftButton){
                        setCurrentCell(row, column);
                        return;
                    }

                    if (column >= startColumn && column <= endColumn){
                        if (row >= startRow && row <= endRow){}
                        else{
                            setCurrentCell(row, column);
                        }
                    }else{
                        setCurrentCell(row, column);
                    }
                    if (event->button() == Qt::RightButton)
                        emit showContextMenu(event->pos());

                }
            }
        }

        //part of rightside
        if (m_actuallyPinnedColumns.size() != 0 && (m_pinnedLeftWidth !=0 || m_pinnedRightWidth != 0)){
            auto &col = m_actuallyPinnedColumns.last();
            if (!col.isLeft){
                if (qAbs(x - col.offset - col.width) < iconWidth + 5 && qAbs(x - col.offset - col.width) > 5 &&  event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                        && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2){

                    emit headerPressed(event->button(), col.visualIndex, m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex]) ? LazyTableStyle::Button : LazyTableStyle::Free, {col.offset,
                                0,
                                col.width,
                                m_style->horizontalHeader()->height()});
                    return;
                }else if (qAbs(x - col.offset - col.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                    m_pinnedResizeColumnIndex = m_actuallyPinnedColumns.indexOf(col);
                    m_resizeCurrentXPos = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                    m_pinnedResizing = true;
                    setResizing(true);
                }
            }
        }
    }
    else if ((m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) > event->x() && event->y() < m_style->horizontalHeader()->height()) {
        setCurrentCell(0,0);
        setSelectedColumnsCount(m_columnsCount);
        setSelectedRowsCount(rowsCount());
    }



    // event->setAccepted(true);
    m_selecting = false;
    if (columnsMoving()){
        updateReasons.setFlag(UPDATE_HEADER_MOVING);
        updateAllPages();
    }
    setColumnsMoving(false);
    m_autoMove = QPoint{0,0};
    double d = 0;
    if (m_pinnedResizing){
        if (m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].isLeft){
            d = m_resizeCurrentXPos - m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].offset;
        }else{
            d = m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].width - (m_resizeCurrentXPos - m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].offset);
        }

    }else{
        d = m_resizeCurrentXPos - m_columns[m_columnIndeces[m_resizeColumnIndex]].offset + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    }
    emit clicked(QVariantMap{{"key",  event->button()},{"modifiers", int(event->modifiers())}}, m_currentRow, m_currentColumn);
    //фикс от изменения размера к минимальному при случайном нажатии на позицию резайза в хедере
    if (d < 5 && resizing() && !m_pinnedResizing){
        setResizing(false);
        updateReasons.setFlag(UPDATE_HEADER);
        update();
        setPressed(false);
        setCursor(Qt::ArrowCursor);
        return;
    }else if (m_pinnedResizing && qAbs(d - m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].offset - m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].width) < 5){
        setResizing(false);
        updateReasons.setFlag(UPDATE_HEADER);
        update();
        setPressed(false);
        setCursor(Qt::ArrowCursor);
        return;
    }

    if (m_pinnedResizing){
        int idx = m_actuallyPinnedColumns[m_pinnedResizeColumnIndex].visualIndex;
        auto&c = m_columns[m_columnIndeces[idx]];
        c.width = d;
        for (int i= idx + 1, l = m_columns.size();i<l;++i){
            m_columns[m_columnIndeces[i]].offset = m_columns[m_columnIndeces[i-1]].offset + m_columns[m_columnIndeces[i-1]].width;
        }
        m_pinnedResizing = false;
        setResizing(false);
        updateAllPages();
        checkActuallyPinnedColumns();
        updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        updateReasons.setFlag(UPDATE_HEADER);
        update();
    }

    if (resizing()) {
        auto&c = m_columns[m_columnIndeces[m_resizeColumnIndex]];
        c.width = d;
        for (int i= m_resizeColumnIndex + 1, l = m_columns.size();i<l;++i){
            m_columns[m_columnIndeces[i]].offset = m_columns[m_columnIndeces[i-1]].offset + m_columns[m_columnIndeces[i-1]].width;
        }
        setResizing(false);
        updateAllPages();
        updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        updateReasons.setFlag(UPDATE_HEADER);
        update();
    }
    setCursor(Qt::ArrowCursor);
    setPressed(false);
}

/*!
 * \fn void Table::mouseReleaseEvent(QMouseEvent *event)
 * \brief Mouse handler which proceed mouse double click event.
 */
void Table::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->setAccepted(true);
    if (event->button() == Qt::LeftButton){
        m_selecting = false;
        auto x = event->pos().x();
        auto y = event->pos().y();
        if (y > m_style->horizontalHeader()->height()){
            if (m_rowsCount == 0) return;
            if (x > m_pinnedLeftWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && x < viewPos().width() - m_pinnedRightWidth){
                auto visualX = x + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                int row = (y - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
                if (row >= m_rowsCount) row = m_rowsCount - 1;
                int column = 0;
                for (int i =0, l = m_columns.size(); i< l; ++i) {
                    if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width > visualX) {
                        column = i;
                        break;
                    }
                }
                if (m_provider->getRow(row).cells[column].isCheckBox) return;

                setCurrentColumn(column);
                setCurrentRow(row);
            }
            QVariantMap eventObject;
            eventObject["key"] = event->button();
            eventObject["modifiers"] = int(event->modifiers());
            emit doubleClicked(eventObject, m_currentRow, m_currentColumn);
        }
    }
}

/*!
 * \fn void Table::keyPressEvent(QKeyEvent *event)
 * \brief Keyboard handler which proceed keyboard key press event.
 */
void Table::keyPressEvent(QKeyEvent *event)
{
    if (event->modifiers() & Qt::ShiftModifier){
        bool needUpdate = false;
        if (event->key() == Qt::Key_Up){
            if (m_selectedRowsCount - 1 == 0 && m_currentRow != 0){
                setSelectedRowsCount(-2);
            }else if (m_selectedRowsCount - 1 < -m_currentRow && m_selectedRowsCount - 1 != 0){
                setSelectedRowsCount(-m_currentRow - 1);
            }else if (m_selectedRowsCount - 1 != 0){
                setSelectedRowsCount(m_selectedRowsCount - 1);
            }
            needUpdate = true;

        }
        if (event->key() == Qt::Key_Down){
            if (m_selectedRowsCount + 1 == -1 ){
                setSelectedRowsCount(1);
            }else if (m_selectedRowsCount + 1 <= m_rowsCount - m_currentRow){
                setSelectedRowsCount(m_selectedRowsCount + 1);
            }
            needUpdate = true;
        }

        if (event->key() == Qt::Key_Left){
            if (m_selectedColumnsCount - 1 == 0 && m_currentColumn != 0){
                setSelectedColumnsCount(-2);
            }else if (m_selectedColumnsCount - 1 < -m_currentColumn && m_selectedColumnsCount - 1 != 0){
                setSelectedColumnsCount(-m_currentColumn - 1);
            }else if (m_selectedColumnsCount - 1 != 0){
                setSelectedColumnsCount(m_selectedColumnsCount - 1);
            }
            needUpdate = true;
        }

        if (event->key() == Qt::Key_Right){
            if (m_selectedColumnsCount + 1 == -1){
                setSelectedColumnsCount(1);
            }else if (m_selectedColumnsCount + 1 <= m_columnsCount - m_currentColumn){
                setSelectedColumnsCount(m_selectedColumnsCount + 1);
            }
            needUpdate = true;
        }

        if (needUpdate){
            updateReasons.setFlag(SELECT_CHANGED);
            QPointF newPos = {viewPos().x(), viewPos().y()};
            int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : -1);
            if (index < 0) index = 0;
            auto x = m_columns[m_columnIndeces[index]].offset + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
            auto y = (m_currentRow + m_selectedRowsCount - (m_selectedRowsCount > 0 ? 1 : -1)) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            auto w1 = m_columns[m_columnIndeces[index]].width;
            auto w2 = viewPos().height() - m_style->horizontalHeader()->height();

            if ((y-w2) > viewPos().y()) {
                newPos.setY(y-w2);
            }else if (y - m_style->horizontalHeader()->height() < viewPos().y()){
                newPos.setY(y - m_style->horizontalHeader()->height());
            }

            if (x > (viewPos().x() + viewPos().width() - w1 - m_pinnedRightWidth)) {
                newPos.setX(-(viewPos().width() - m_pinnedRightWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) - x - w1));
            } else if (x < viewPos().x() + m_pinnedLeftWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) {
                newPos.setX(x - m_pinnedLeftWidth - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0));
            }

            moveToPos(newPos.x(), newPos.y());
            event->setAccepted(true);
        }else{
            QVariantMap eventObject;
            eventObject["key"] = event->key();
            eventObject["modifiers"] = int(event->modifiers());
            eventObject["nativeVirtualKey"] = event->nativeVirtualKey();
            eventObject["nativeModifiers"] = event->nativeModifiers();
            emit keyPressed(eventObject, m_currentRow, m_currentColumn);
        }
    }else{
        if (event->key() == Qt::Key_Up) {
            decreaseRow();
        }else if (event->key() == Qt::Key_Down) {
            increaseRow();
        }else if (event->key() == Qt::Key_Left) {
            decreaseColumn();
        }else if (event->key() == Qt::Key_Right) {
            increaseColumn();
        }else{
            QVariantMap eventObject;
            eventObject["key"] = event->key();
            eventObject["modifiers"] = int(event->modifiers());
            eventObject["nativeVirtualKey"] = event->nativeVirtualKey();
            eventObject["nativeModifiers"] = event->nativeModifiers();
            emit keyPressed(eventObject, m_currentRow, m_currentColumn);
        }
    }
}

/*!
 * \fn void Table::keyReleaseEvent(QKeyEvent *event)
 * \brief Keyboard handler which proceed keyboard key release event.
 */
void Table::keyReleaseEvent(QKeyEvent *event)
{
    QVariantMap eventObject;
    eventObject["key"] = event->key();
    eventObject["modifiers"] = int(event->modifiers());
    eventObject["nativeVirtualKey"] = event->nativeVirtualKey();
    eventObject["nativeModifiers"] = event->nativeModifiers();
    emit keyReleased(eventObject, m_currentRow, m_currentColumn);
}

/*!
 * \fn void Table::hoverMoveEvent(QHoverEvent *event)
 * \brief Mouse handler which proceed mouse movement event.
 */
void Table::hoverMoveEvent(QHoverEvent *event)
{
    event->setAccepted(true);
    double x = event->posF().x() + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
    if (!resizing() && m_columns.size() != 0){
        bool res = false;
        if(event->pos().y() < m_style->horizontalHeader()->height() && m_style->horizontalHeader()->height() != 0){
            double iconWidth = m_style->iconsWidth();
            if (event->pos().x() > m_pinnedLeftWidth + 5 + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)
                    && event->pos().x() < viewPos().width() - m_pinnedRightWidth){
                auto visualX = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);

                for (int i = 0, l = m_columns.size(); i < l; ++i){
                    auto x = visualX - m_columns[m_columnIndeces[i]].width;
                    double delta = x - m_columns[m_columnIndeces[i]].offset;
                    if (qAbs(delta) < 5 ){ // time to resize
                        m_resizeColumnIndex = i;
                        m_resizeCurrentXPos = x;
                        res = true;
                        break;
                    }else if (qAbs(delta) < iconWidth + 5){
                        if (delta < 0
                                && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                && m_provider->headersWithButton().contains(m_columnIndeces[i])){
                            setCursor(Qt::PointingHandCursor);
                            return;
                        }
                    }
                }
                //! last column
                auto ll = m_columns[m_columnIndeces[m_columns.size() - 1]];
                if (m_columns.size() != 0)
                    if (qAbs(visualX -  ll.offset - ll.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                        m_resizeColumnIndex = m_columns.size() - 1;
                        m_resizeCurrentXPos = visualX;
                        res = true;
                    }
            }else{
                double x = event->pos().x();
                for (const auto &col : qAsConst(m_actuallyPinnedColumns)){
                    if (x >= viewPos().width() - m_pinnedRightWidth - 5 && m_pinnedRightWidth != 0){
                        if (col.isLeft) continue;
                        double delta = x - col.offset;
                        if (qAbs(delta) < 5 ){ // time to resize
                            m_resizeColumnIndex = col.visualIndex;
                            m_resizeCurrentXPos = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                            res = true;
                            break;
                        }else if (qAbs(delta) < iconWidth + 5){
                            if (delta < 0
                                    && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                    && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                    && m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex])){
                                setCursor(Qt::PointingHandCursor);
                                return;
                            }
                        }

                    }else if (x <= m_pinnedLeftWidth + 5 +  viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) && m_pinnedLeftWidth != 0){
                        if (!col.isLeft) continue;
                        double delta = x - col.offset - col.width;
                        if (qAbs(delta) < 5){ // time to resize
                            m_resizeColumnIndex = col.visualIndex;
                            m_resizeCurrentXPos = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                            res = true;
                            break;
                        }else if (qAbs(delta) < iconWidth + 5){
                            if (delta < 0
                                    && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                    && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                    && m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex])){
                                setCursor(Qt::PointingHandCursor);
                                return;
                            }
                        }
                    }
                }

                if (m_actuallyPinnedColumns.size() != 0 && (m_pinnedLeftWidth !=0 || m_pinnedRightWidth != 0)){
                    const auto &col = m_actuallyPinnedColumns.last();
                    if (!col.isLeft){
                        if (qAbs(x - col.offset - col.width) < iconWidth + 5 && event->pos().y() >= (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                && event->pos().y() <= m_style->horizontalHeader()->height() - (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2
                                && m_provider->headersWithButton().contains(m_columnIndeces[col.visualIndex])){
                            setCursor(Qt::PointingHandCursor);
                            return;
                        }else if (qAbs(x - col.offset - col.width - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) < 10){
                            m_resizeColumnIndex = col.visualIndex;
                            m_resizeCurrentXPos = event->pos().x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                            res = true;
                        }
                    }
                }


            }
            if (res){
                setCursor(Qt::SizeHorCursor);
            }else{
                setCursor(Qt::ArrowCursor);
            }
        }else if (m_columns.size() > 0){
            //! selection
            int index = m_currentColumn + selectedColumnsCount() - (selectedColumnsCount() > 0 ? 1 : 0);
            if (index < 0) index = 0;
            auto p = internalToNormal(QPointF{m_columns[m_columnIndeces[index]].offset + m_columns[m_columnIndeces[index]].width + 5,
                                              (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + 5});
            auto pos = event->posF();
            bool checkbox = false;
            auto row = m_provider->getRow(rowIndex(event->pos().y() + viewPos().y()));
            if (x > m_pinnedLeftWidth + 5 + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)
                    && event->pos().x() < viewPos().width() - m_pinnedRightWidth){
                int nindex = columnIndex(viewPos().x() + x);
                auto cell = row.cells.size() != 0 && nindex != -1 ? row.cells[m_columnIndeces[nindex]] : Cell();
                if (cell.isCheckBox){
                    auto pc = internalToNormal(QPointF{m_columns[m_columnIndeces[nindex]].offset + m_columns[m_columnIndeces[nindex]].width / 2 - 5,
                                                       event->posF().y() + viewPos().y()});

                    if (pc.x() - 5 <= x - (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0) &&
                            pc.x() + 15 >= x - (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0)){
                        setCursor(Qt::PointingHandCursor);
                    }else{
                        setCursor(Qt::ArrowCursor);
                    }
                    checkbox = true;
                }
            }else if (x < m_pinnedLeftWidth){
                for (int i = 0; i < m_actuallyPinnedColumns.size(); i++){
                    const auto &col = m_actuallyPinnedColumns[i];
                    if (col.isLeft){
                        auto row = m_provider->getRow(rowIndex(event->pos().y() + viewPos().y()));
                        auto cell = row.cells.size() != 0 && col.visualIndex != -1 ? row.cells[m_columnIndeces[col.visualIndex]] : Cell();
                        if (x > col.offset && x < col.offset + col.width){
                            if (col.offset + col.width / 2 <= x && col.offset + col.width / 2 + 17 >= x && cell.isCheckBox){
                                setCursor(Qt::PointingHandCursor);
                                return;
                            }else{
                                setCursor(Qt::ArrowCursor);
                                checkbox = true;
                            }
                        }else{
                            setCursor(Qt::ArrowCursor);
                            checkbox = true;
                        }
                    }
                }
            }else if (event->pos().x() > m_pinnedRightWidth && event->pos().x() < viewPos().width()){
                for (int i = 0; i < m_actuallyPinnedColumns.size(); i++){
                    const auto &col = m_actuallyPinnedColumns[i];
                    if (!col.isLeft){
                        auto row = m_provider->getRow(rowIndex(event->pos().y() + viewPos().y()));
                        auto cell = row.cells.size() != 0 && col.visualIndex != -1 ? row.cells[m_columnIndeces[col.visualIndex]] : Cell();
                        if (x > col.offset && x < col.offset + col.width){
                            if (col.offset + col.width / 2 <= x && col.offset + col.width / 2 + 17 >= x && cell.isCheckBox){
                                setCursor(Qt::PointingHandCursor);
                                return;
                            }else{
                                setCursor(Qt::ArrowCursor);
                                checkbox = true;
                            }
                        }else{
                            setCursor(Qt::ArrowCursor);
                            checkbox = true;
                        }
                    }
                }
            }

            if (p.x() >= pos.x() && p.x() <= pos.x() + 10
                    && p.y() >= pos.y() && p.y() <= pos.y() + 10 && !checkbox) {
                setCursor(Qt::PointingHandCursor);
            }else if (!checkbox){
                setCursor(Qt::ArrowCursor);
            }

            //if (m_selecting){
            //    checkSelect(event->posF().x(), event->posF().y());
            //}
        }
    }
}

/*!
 * \fn void Table::wheelEvent(QWheelEvent *event)
 * \brief Wheel handler which proceed mouse wheel event.
 */
void Table::wheelEvent(QWheelEvent *event)
{
    double delta = event->angleDelta().y() / 120.0;
    if (delta > 0){
        if (viewPos().y() - 50 < 0){
            moveToPos(viewPos().x(), 0);
        }else{
            moveToPos(viewPos().x(), viewPos().y() - 50);
        }
        //scrollTop();
    }else{
        if (viewPos().y() + viewPos().height() + 50 >= contentHeight() + (reachedBottom() ? 0 : 50)){
            moveToPos(viewPos().x(), contentHeight() - viewPos().height());
        }else{
            moveToPos(viewPos().x(), viewPos().y() + 50);
        }
    }

    //! scale (doesn't work)
    //if (event->modifiers() & Qt::ControlModifier){
    //    double zoomFactor = 0.9;
    //    if (delta > 0)
    //    {
    //        zoomFactor = 1.0/zoomFactor;
    //    }
    //    double z = this->scale() * zoomFactor;
    //    if (z < 3 && z > 0.01)
    //       setScale(z);
    //}

    event->setAccepted(true);
}

/*!
 * \fn QSGGeometryNode *Table::drawGrid(QSGNode *node)
 * \brief Used for drawing grid.
 */
QSGGeometryNode *Table::drawGrid(QSGNode *node)
{
    QSGGeometryNode *gridNode;
    if (node == nullptr) {
        gridNode = new QSGGeometryNode;
        gridNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->gridColor());
        gridNode->setMaterial(m);
        auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        gridNode->setGeometry(g);
    } else {
        gridNode = static_cast<QSGGeometryNode*>(node);
    }

/*! oldstyle
    int l = 2*(m_columns.size()) + 2*(m_rowsCount+2); //points two for each vertical and horizontal lines
    auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), l);
    g->setDrawingMode(QSGGeometry::DrawLines);

    double h = m_style->cellStyle()->height() * (m_rowsCount) + m_style->horizontalHeader()->height();

    //column lines
    int vi =0;
    for (int i =0, l= m_columns.size(); i <l; ++i) {
        auto offset = m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        g->vertexDataAsPoint2D()[vi++].set(offset, 0);
        g->vertexDataAsPoint2D()[vi++].set(offset, h);
    }
    //last column
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, 0);
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, h);
    //row lines
    auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
    for (int i =0, l= m_rowsCount; i < l; ++i) {
        auto y = m_style->horizontalHeader()->height() + i * m_style->cellStyle()->height();
        g->vertexDataAsPoint2D()[vi++].set(0, y);
        g->vertexDataAsPoint2D()[vi++].set(w, y);
    }
    //last row
    auto y = m_style->horizontalHeader()->height() + m_rowsCount * m_style->cellStyle()->height();
    g->vertexDataAsPoint2D()[vi++].set(0, y);
    g->vertexDataAsPoint2D()[vi++].set(w, y);
*/
    //column lines
    int gy = viewPos().y() - 1;
    double h = (gy + viewPos().height() > cellPos(m_rowsCount, m_columnsCount).y() + m_style->cellStyle()->height()) ?
                ((m_rowsCount) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height()) :
                (gy + viewPos().height() + 1);
    int startCell = gy / m_style->cellStyle()->height();
    int endCell = (h - m_style->horizontalHeader()->height()) / m_style->cellStyle()->height();
    int vi  = 0;
    if (endCell - startCell < 0) endCell = 0;
    int l  = 2*(m_columns.size()) + (endCell - startCell)*2 + 2; //points two for each vertical and horizontal lines
    auto g = gridNode->geometry();
    g->allocate(l);
    g->setDrawingMode(QSGGeometry::DrawLines);

    for (int i =0, l= m_columns.size(); i <l; ++i) {
        auto offset = m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        g->vertexDataAsPoint2D()[vi++].set(offset, gy);
        g->vertexDataAsPoint2D()[vi++].set(offset, h);
    }
    //last column
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, gy);
    g->vertexDataAsPoint2D()[vi++].set(m_columns[m_columnIndeces[m_columns.size()-1]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[m_columns.size()-1]].width, h);

    auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
    for (; startCell <= endCell; startCell++){
        auto y = m_style->horizontalHeader()->height() + startCell * m_style->cellStyle()->height();
        g->vertexDataAsPoint2D()[vi++].set(0, y);
        g->vertexDataAsPoint2D()[vi++].set(w, y);
    }
    qDebug() << l << endCell - startCell << vi;
    //row lines
    //auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    //double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0);
    //for (int i =0, l= m_rowsCount; i < l; ++i) {
    //    auto y = m_style->horizontalHeader()->height() + i * m_style->cellStyle()->height();
    //    g->vertexDataAsPoint2D()[vi++].set(0, y);
    //    g->vertexDataAsPoint2D()[vi++].set(w, y);
    //}
    ////last row
    //auto y = m_style->horizontalHeader()->height() + m_rowsCount * m_style->cellStyle()->height();
    //g->vertexDataAsPoint2D()[vi++].set(0, y);
    //g->vertexDataAsPoint2D()[vi++].set(w, y);

    gridNode->setGeometry(g);
    gridNode->markDirty(QSGNode::DirtyGeometry);

    return gridNode;
}

/*!
 * \fn QSGGeometryNode *Table::drawHorizontalHeader(QSGNode *node)
 * \brief Used for drawing horizontal header.
 */
QSGGeometryNode *Table::drawHorizontalHeader(QSGNode *node)
{
    QSGGeometryNode *rectNode;
    QSGGeometry *g;
    if (node == nullptr) {
        rectNode = new QSGGeometryNode;
        // geometry
        g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rectNode->setGeometry(g);
        // rect color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->horizontalHeader()->backgroundColor());
        rectNode->setMaterial(m);
    } else {
        rectNode = static_cast<QSGGeometryNode*>(node);
        g = rectNode->geometry();
    }

    if (updateReasons.testFlag(UPDATE_HEADER)){
        //update rect geom
        auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
        double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        g->vertexDataAsPoint2D()[0].set(0, 0);
        g->vertexDataAsPoint2D()[1].set(w, 0);
        g->vertexDataAsPoint2D()[2].set(w, m_style->horizontalHeader()->height());
        g->vertexDataAsPoint2D()[3].set(0, m_style->horizontalHeader()->height());
        rectNode->markDirty(QSGNode::DirtyGeometry);
        //header text node
        // setup text node
        QSGSimpleTextureNode *textNode = nullptr;
        if (rectNode->childCount()>0) {
            textNode = static_cast<QSGSimpleTextureNode *>(rectNode->childAtIndex(0));
        } else {
            textNode = new QSGSimpleTextureNode();
            textNode->setOwnsTexture(true);
            rectNode->appendChildNode(textNode);
        }
        //draw image with texts
        QImage img(w, m_style->horizontalHeader()->height(), QImage::Format_ARGB32);

        img.fill(QColor(0,0,0,0));
        QPainter painter(&img);
        painter.setRenderHints(QPainter::Antialiasing);
        painter.setFont(m_style->horizontalHeader()->fontStyle()->font());
        QPen p(m_style->gridColor());
        p.setWidthF(0.8);
        painter.setPen(p);
        painter.drawLine((m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() - 1 : 0), 0, (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() - 1 : 0), m_style->horizontalHeader()->height());
        auto buttonIndeces = m_provider->headersWithButton();
        for (int i = 0, l = m_columns.size(); i < l; ++i) {
            painter.setPen(m_style->horizontalHeader()->textColor());
            if (columnsMoving() && i == m_movingColumnIndex) continue;
            QRectF cellRect = {
                m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                0,
                m_columns[m_columnIndeces[i]].width,
                m_style->horizontalHeader()->height()
            };
            //filling bg
            QBrush brush;
            brush.setColor(m_pinnedColumns.contains(i) ? m_style->horizontalHeader()->pinnedColor() :
                                                         m_columns[m_columnIndeces[i]].backgroundColor == "transparent" ?  m_style->horizontalHeader()->backgroundColor() :
                                                                                                                           m_columns[m_columnIndeces[i]].backgroundColor);
            brush.setStyle(Qt::SolidPattern);
            painter.fillRect(cellRect, brush);
            //drawing text
            cellRect -= QMarginsF{2.5, 2.5 , (m_provider->headersWithButton().contains(m_columnIndeces[i]) ? m_style->iconsWidth() : 0) + 6, 0};
            painter.setPen(m_columns[m_columnIndeces[i]].textColor == "black" ? m_style->horizontalHeader()->textColor() : m_columns[m_columnIndeces[i]].textColor);

            painter.drawText(cellRect,
                             m_columns[m_columnIndeces[i]].headerAlignment == 0 ?
                                m_style->horizontalHeader()->alignment() :
                                m_columns[m_columnIndeces[i]].headerAlignment,
                             m_columns[m_columnIndeces[i]].name);
            //drawing icon
            painter.setPen(m_style->iconsColor());
            if (buttonIndeces.contains(m_columnIndeces[i])){
                QRectF iconRect = {
                    m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[i]].width - 5 - m_style->iconsWidth(),
                    (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2,
                    m_style->iconsWidth(),
                    m_style->iconsWidth()
                };
                if (m_iconsPaths.contains(m_columnIndeces[i])){
                    painter.drawRect(iconRect);
                    iconRect -= QMarginsF{1,1,1,1};
                    auto img = QImage(m_iconsPaths[m_columnIndeces[i]]);
                    painter.drawImage(iconRect, img);
                }
            }
            double x = m_columns[m_columnIndeces[i]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_columns[m_columnIndeces[i]].width;

            painter.setPen(m_style->gridColor());
            painter.drawLine(x - 1, 0, x - 1, m_style->horizontalHeader()->height());
        }
        painter.drawLine(0, m_style->horizontalHeader()->height() - 1, m_contentWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_style->horizontalHeader()->height() - 1);
        auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
        textNode->setRect(QRectF(0,0,w, m_style->horizontalHeader()->height()));
        textNode->setTexture(t);
    }
    return rectNode;
}

/*!
 * \fn HighlightNode *Table::drawVerticalHeaderHightLight(QSGNode *node)
 * \brief Used for drawing vertical header highlight.
 */
HighlightNode *Table::drawVerticalHeaderHightLight(QSGNode *node)
{
    auto hnode = static_cast<HighlightNode*>(node);

    if (hnode == nullptr){
        hnode = new HighlightNode();
        hnode->highlight = new QSGGeometryNode();
        hnode->background = new QSGGeometryNode();

        hnode->highlight->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        hnode->background->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));

        hnode->bgMaterial = new QSGFlatColorMaterial();
        hnode->highlightMaterial = new QSGFlatColorMaterial();

        hnode->background->setMaterial(hnode->bgMaterial);
        hnode->highlight->setMaterial(hnode->highlightMaterial);

        hnode->appendChildNode(hnode->background);
        hnode->appendChildNode(hnode->highlight);

        hnode->background->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
        hnode->highlight->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_HIGHLIGHT)){
        double y1 = 0.0, y2 = 0.0;
        if (m_currentColumn >= 0 && m_currentRow >= 0){
            if (m_selectedRowsCount > 0){
                y1 = m_currentRow*m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
                y2 = y1 + m_style->cellStyle()->height() * selectedRowsCount();
            }else{
                y2 = m_currentRow * m_style->cellStyle()->height() + m_style->cellStyle()->height() * 2;
                y1 = y2 - qAbs(m_selectedRowsCount) * m_style->cellStyle()->height();
            }
        }

        hnode->highlight->geometry()->vertexDataAsPoint2D()[0].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y1);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[1].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ) -3, y1);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[2].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y2);
        hnode->highlight->geometry()->vertexDataAsPoint2D()[3].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ) -3, y2);

        hnode->background->geometry()->vertexDataAsPoint2D()[0].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y1);
        hnode->background->geometry()->vertexDataAsPoint2D()[1].set(0, y1);
        hnode->background->geometry()->vertexDataAsPoint2D()[2].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ), y2);
        hnode->background->geometry()->vertexDataAsPoint2D()[3].set(0, y2);

        QColor bgColor = m_style->selectionColor();
        bgColor.setAlphaF(m_style->selectionOpacity() * 0.8);

        hnode->bgMaterial->setColor(bgColor);
        hnode->highlightMaterial->setColor(m_style->highlightColor());

        hnode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return hnode;
}

/*!
 * \fn HighlightNode *Table::drawHorizontalHeaderHightLight(QSGNode *node)
 * \brief Used for drawing horizontal header highlight.
 */
HighlightNode *Table::drawHorizontalHeaderHightLight(QSGNode *node)
{
    auto hnode = static_cast<HighlightNode*>(node);

    if (hnode == nullptr){
        hnode = new HighlightNode();
        hnode->highlight = new QSGGeometryNode();
        hnode->background = new QSGGeometryNode();

        hnode->highlight->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        hnode->background->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));

        hnode->bgMaterial = new QSGFlatColorMaterial();
        hnode->highlightMaterial = new QSGFlatColorMaterial();

        hnode->background->setMaterial(hnode->bgMaterial);
        hnode->highlight->setMaterial(hnode->highlightMaterial);

        hnode->appendChildNode(hnode->background);
        hnode->appendChildNode(hnode->highlight);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_HIGHLIGHT)){

        double x1 = 0.0, x2 = 0.0;
        int xIndex1 = m_currentColumn + selectedColumnsCount() - 1;
        int xIndex2 = m_currentColumn + selectedColumnsCount() + 1;
        if (xIndex1 < 0) xIndex1 = 0;
        if (xIndex2 > m_columns.size() - 1) xIndex2 = m_columns.size() - 1;
        if (m_currentColumn >= 0 && m_currentRow >= 0){
            if (m_selectedColumnsCount >= 0){
                x1 = m_columns[m_columnIndeces[m_currentColumn]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                x2 = m_columns[m_columnIndeces[xIndex1]].offset + m_columns[m_columnIndeces[m_currentColumn + selectedColumnsCount() - 1]].width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            }else{
                x1 = m_columns[m_columnIndeces[xIndex2]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
                x2 = m_columns[m_columnIndeces[m_currentColumn]].offset + m_columns[m_columnIndeces[m_currentColumn]].width+ (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            }
        }

        auto g1 = hnode->highlight->geometry();

        g1->vertexDataAsPoint2D()[0].set(x2, m_style->horizontalHeader()->height() - 2);
        g1->vertexDataAsPoint2D()[1].set(x1, m_style->horizontalHeader()->height() - 2);
        g1->vertexDataAsPoint2D()[2].set(x2, m_style->horizontalHeader()->height());
        g1->vertexDataAsPoint2D()[3].set(x1, m_style->horizontalHeader()->height());

        auto g2 = hnode->background->geometry();

        g2->vertexDataAsPoint2D()[0].set(x2, 0);
        g2->vertexDataAsPoint2D()[1].set(x1, 0);
        g2->vertexDataAsPoint2D()[2].set(x2, m_style->horizontalHeader()->height());
        g2->vertexDataAsPoint2D()[3].set(x1, m_style->horizontalHeader()->height());

        hnode->highlight->setGeometry(g1);

        hnode->background->setGeometry(g2);

        QColor bgColor = m_style->selectionColor();
        bgColor.setAlphaF(m_style->selectionOpacity() * 0.8);

        hnode->bgMaterial->setColor(bgColor);
        hnode->highlightMaterial->setColor(m_style->highlightColor());

        // hnode->background->setMaterial(hnode->bgMaterial);
        // hnode->highlight->setMaterial(hnode->highlightMaterial);

        hnode->background->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
        hnode->highlight->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);

        hnode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return hnode;
}

/*!
 * \fn QSGNode *Table::drawVerticalHeader(QSGNode *node)
 * \brief Used for drawing vertical header.
 */
QSGNode *Table::drawVerticalHeader(QSGNode *node)
{
    if (updateReasons.testFlag(UPDATE_HEADER)){
        if (m_style->numericalHeaderAvailable()){
            QSGNode *rectNode = static_cast<QSGNode*>(node);


            if (rectNode == nullptr) {
                if (node != nullptr) delete rectNode;
                rectNode = new QSGNode;
                node = rectNode;
            }

            if (updateReasons.testFlag(UPDATE_PAGE)) {
                while(rectNode->childCount() != 0){
                    delete rectNode->firstChild();
                }
                for (const auto &page : qAsConst(m_pages)){
                    if (page.visible){
                        auto textNode = new QSGSimpleTextureNode;
                        textNode->setOwnsTexture(true);
                        auto& img = page.vertHeader;
                        auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                        textNode->setRect(QRectF(0,page.yoffset, img.width(), img.height()));
                        textNode->setTexture(t);
                        rectNode->appendChildNode(textNode);
                    }
                }
                node->markDirty(QSGNode::DirtyNodeAdded);
            }

        }else{
            QSGGeometryNode *rectNode = static_cast<QSGGeometryNode*>(node);;
            if (rectNode == nullptr) {
                if (node != nullptr) delete node;
                rectNode = new QSGGeometryNode;
                node = rectNode;
                // geometry
                auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
                g->setDrawingMode(QSGGeometry::DrawTriangleFan);
                rectNode->setGeometry(g);
                // rect color
                auto m = new QSGFlatColorMaterial();
                m->setColor(m_style->verticalHeader()->backgroundColor());
                rectNode->setMaterial(m);
                rectNode->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
            }
            if (m_style->verticalHeaderAvailable()){
                rectNode->geometry()->vertexDataAsPoint2D()[0].set(0, 0);
                rectNode->geometry()->vertexDataAsPoint2D()[1].set(m_style->verticalHeader()->width(), 0);
                rectNode->geometry()->vertexDataAsPoint2D()[2].set(m_style->verticalHeader()->width(), m_rowsCount*m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
                rectNode->geometry()->vertexDataAsPoint2D()[3].set(0, m_rowsCount*m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
            }else{
                rectNode->geometry()->vertexDataAsPoint2D()[0].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[1].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[2].set(0,0);
                rectNode->geometry()->vertexDataAsPoint2D()[3].set(0,0);

            }

            node->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

        }
    }
    return node;
}

/*!
 * \fn QSGNode *Table::drawColorProviderBackground(QSGNode *node)
 * \brief Used for drawing color provider colored background.
 */
QSGNode *Table::drawColorProviderBackground(QSGNode *node)
{
    if (node == nullptr) {
        node = new QSGNode();
    }

    if (updateReasons.testFlag(UPDATE_COLOR_BACKGROUND)){
        while(node->childCount() != 0){
            delete node->firstChild();
        }

        QMap<int, QVector<QColor>> rowsScheme;
        //create scheme
        for (auto p : qAsConst(m_colorProviders)){
            auto id = p->selectedID();
            //Drawing bg
            for (const auto &i : id){
                int key = m_provider->rowById(i);
                QVector<QColor> v = rowsScheme.value(key, QVector<QColor>());
                v.append(p->selectionColor());
                rowsScheme.insert(key, v);
            }
        }

        for (auto it = rowsScheme.begin(), end = rowsScheme.end(); it != end; it++){
            const QVector<QColor> &colors = it.value();
            for (int i = 0; i < colors.length(); i++){
                QSGGeometryNode *n = new QSGGeometryNode();
                QSGGeometry *g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
                QSGFlatColorMaterial *m = new QSGFlatColorMaterial();

                m->setColor(colors[i]);
                g->setDrawingMode(QSGGeometry::DrawTriangleFan);
                int r = it.key();
                double y1 = r * m_style->cellStyle()->height() + m_style->horizontalHeader()->height() + (double)(i / colors.length()) * m_style->cellStyle()->height();
                double y2 = (r + 1) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height() - (i /(double) colors.length()) * m_style->cellStyle()->height();
                g->vertexDataAsPoint2D()[0].set(m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0, y1);
                g->vertexDataAsPoint2D()[1].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0) + m_contentWidth, y1);
                g->vertexDataAsPoint2D()[2].set((m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0) + m_contentWidth, y2);
                g->vertexDataAsPoint2D()[3].set(m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0, y2);

                n->setGeometry(g);
                n->setMaterial(m);
                node->appendChildNode(n);
                n->setFlags(QSGNode::OwnsMaterial | QSGNode::OwnsGeometry);
                //n->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
            }
        }
        node->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    }
    return node;
}

/*!
 * \fn QSGGeometryNode *Table::drawFixRectangle(QSGGeometryNode *node)
 * \brief Used for drawing "fix rectangle" on top left position to hide a bug.
 */
QSGGeometryNode *Table::drawFixRectangle(QSGGeometryNode *node)
{
    QSGFlatColorMaterial *material = nullptr;
    QSGFlatColorMaterial *frameMaterial = nullptr;
    QSGGeometryNode *frameNode = nullptr;
    if (node == nullptr){
        node = new QSGGeometryNode();
        node->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        material = new QSGFlatColorMaterial;
        node->setMaterial(material);
        node->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);
        node->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
    }else{
        material = static_cast<QSGFlatColorMaterial*>(node->material());
    }

    if (node->childCount() != 1){
        frameNode = new QSGGeometryNode();
        frameMaterial = new QSGFlatColorMaterial;
        frameNode->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        frameNode->setMaterial(frameMaterial);
        frameNode->geometry()->setLineWidth(1);
        frameNode->geometry()->setDrawingMode(QSGGeometry::DrawLines);
        frameNode->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        node->appendChildNode(frameNode);

    }else{
        frameNode = static_cast<QSGGeometryNode*>(node->childAtIndex(0));
        frameMaterial = static_cast<QSGFlatColorMaterial*>(frameNode->material());
    }

    node->geometry()->vertexDataAsPoint2D()[0].set(0, 0);
    node->geometry()->vertexDataAsPoint2D()[1].set(m_style->verticalHeader()->width(), 0);
    node->geometry()->vertexDataAsPoint2D()[2].set(m_style->verticalHeader()->width(), m_style->horizontalHeader()->height());
    node->geometry()->vertexDataAsPoint2D()[3].set(0, m_style->horizontalHeader()->height());

    frameNode->geometry()->vertexDataAsPoint2D()[0].set(m_style->verticalHeader()->width(), 0);
    frameNode->geometry()->vertexDataAsPoint2D()[1].set(m_style->verticalHeader()->width(), m_style->horizontalHeader()->height());
    frameNode->geometry()->vertexDataAsPoint2D()[2].set(0, m_style->horizontalHeader()->height() - 1);
    frameNode->geometry()->vertexDataAsPoint2D()[3].set(m_style->verticalHeader()->width(), m_style->horizontalHeader()->height() - 1/*contentWidth() + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0) , m_style->horizontalHeader()->height()*/);

    material->setColor(m_style->horizontalHeader()->backgroundColor());
    frameMaterial->setColor(m_style->gridColor());

    node->markDirty(QSGNode::DirtyMaterial | QSGNode::DirtyGeometry);
    return node;
}

/*!
 * \fn QSGGeometryNode *Table::drawSelectedCell(QSGNode *node)
 * \brief Used for drawing background color from provider.
 */
QSGGeometryNode *Table::drawSelectedCell(QSGNode *node)
{
    QSGGeometryNode *selectNode;
    if (node == nullptr) {
        selectNode = new QSGGeometryNode();
        selectNode->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        selectNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->highlightColor());
        selectNode->setMaterial(m);
    } else {
        selectNode = static_cast<QSGGeometryNode*>(node);
    }

    auto g = selectNode->geometry();
    if (!g){
        selectNode->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        g = selectNode->geometry();
    }
    g->setLineWidth(2);
    g->setDrawingMode(QSGGeometry::DrawLineLoop);

    // draw selection
    double x1 = 0.0, y1 = 0.0, x2 = 0.0, y2 = 0.0;
    int xIndex1 = m_currentColumn + selectedColumnsCount() - 1;
    int xIndex2 = m_currentColumn + selectedColumnsCount() + 1;
    if (xIndex1 < 0) xIndex1 = 0;
    if (xIndex2 > m_columns.size() - 1) xIndex2 = m_columns.size() - 1;
    if (m_currentColumn >= 0 && m_currentRow >= 0){
        if (m_selectedColumnsCount >= 0){
            x1 = m_columns[m_columnIndeces[m_currentColumn]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            x2 = m_columns[m_columnIndeces[xIndex1]].offset + m_columns[m_columnIndeces[m_currentColumn + selectedColumnsCount() - 1]].width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        }else{
            x1 = m_columns[m_columnIndeces[xIndex2]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
            x2 = m_columns[m_columnIndeces[m_currentColumn]].offset + m_columns[m_columnIndeces[m_currentColumn]].width+ (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
        }

        if (m_selectedRowsCount > 0){
            y1 = m_currentRow*m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            y2 = y1 + m_style->cellStyle()->height() * selectedRowsCount();
        }else{
            y2 = m_currentRow * m_style->cellStyle()->height() + m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
            y1 = y2 - qAbs(m_selectedRowsCount) * m_style->cellStyle()->height();
        }
    }
    g->vertexDataAsPoint2D()[0].set(x1, y1);
    g->vertexDataAsPoint2D()[1].set(x2, y1);
    g->vertexDataAsPoint2D()[2].set(x2, y2);
    g->vertexDataAsPoint2D()[3].set(x1, y2);

    // draw selection background
    // =========================
    // |current|               |
    // =========               |
    // |       |               |
    // |       |     huge      |
    // | small |               |
    // |       |               |
    // |       |               |
    // =========================

    QSGGeometryNode *rect1 = nullptr;
    QSGGeometry *rg1 = nullptr;
    QSGFlatColorMaterial *rm1 = nullptr;

    QSGGeometryNode *rect2 = nullptr;
    QSGGeometry *rg2 = nullptr;
    QSGFlatColorMaterial *rm2 = nullptr;

    if (selectNode->childCount() > 2) {
        rect1 = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(0));
        rm1 = static_cast<QSGFlatColorMaterial *>(rect1->material());
        rg1 = rect1->geometry();

        rect2 = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(1));
        rm2 = static_cast<QSGFlatColorMaterial *>(rect2->material());
        rg2 = rect2->geometry();
    } else {
        rect1 = new QSGGeometryNode();
        rg1 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        rg1->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rect1->setGeometry(rg1);
        rect1->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        rm1 = new QSGFlatColorMaterial();
        rect1->setMaterial(rm1);
        selectNode->appendChildNode(rect1);

        rect2 = new QSGGeometryNode();
        rg2 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        rg2->setDrawingMode(QSGGeometry::DrawTriangleFan);
        rect2->setGeometry(rg2);
        rect2->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        rm2 = new QSGFlatColorMaterial();
        rect2->setMaterial(rm2);
        selectNode->appendChildNode(rect2);
    }

    double c_x1 = 0, c_x2 = 0, c_y2 = 0;

    c_x1 = (m_selectedColumnsCount > 0 ? x1 : x2 - m_columns[m_columnIndeces[m_currentColumn]].width);
    c_x2 = (m_selectedColumnsCount > 0 ? x1 + m_columns[m_columnIndeces[m_currentColumn]].width : x2);
    c_y2 = (m_selectedRowsCount > 0 ? m_style->cellStyle()->height() : y2);

    //small rect
    rg1->vertexDataAsPoint2D()[0].set(c_x1, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
    rg1->vertexDataAsPoint2D()[1].set(c_x2, m_selectedRowsCount > 0 ? y1 + m_style->cellStyle()->height() : y1);
    rg1->vertexDataAsPoint2D()[2].set(c_x2, m_selectedRowsCount > 0 ? y2 : c_y2 - m_style->cellStyle()->height());
    rg1->vertexDataAsPoint2D()[3].set(c_x1, m_selectedRowsCount > 0 ? y2 : c_y2 - m_style->cellStyle()->height());
    //huge rect
    rg2->vertexDataAsPoint2D()[0].set(m_selectedColumnsCount > 0 ? c_x2 : x1,   y1);
    rg2->vertexDataAsPoint2D()[1].set(m_selectedColumnsCount > 0 ? x2 : c_x1,   y1);
    rg2->vertexDataAsPoint2D()[2].set(m_selectedColumnsCount > 0 ? x2 : c_x1,   y2);
    rg2->vertexDataAsPoint2D()[3].set(m_selectedColumnsCount > 0 ? c_x2 : x1, y2);

    // color of selection bg
    QColor c = m_style->selectionColor();
    c.setAlphaF(m_style->selectionOpacity());
    rm1->setColor(c);
    rect1->setGeometry(rg1);

    rm2->setColor(c);
    rect2->setGeometry(rg2);

    // handle
    QSGGeometryNode *handleNode = nullptr;
    QSGGeometry *g2 = nullptr;
    QSGFlatColorMaterial *m2 = nullptr;
    if (selectNode->childCount() > 2) {
        handleNode = static_cast<QSGGeometryNode *>(selectNode->childAtIndex(2));
        m2 = static_cast<QSGFlatColorMaterial *>(handleNode->material());
        g2 = handleNode->geometry();
    } else {
        handleNode = new QSGGeometryNode();
        m2 = new QSGFlatColorMaterial();
        g2 = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4);
        g2->setDrawingMode(QSGGeometry::DrawTriangleFan);
        handleNode->setGeometry(g2);
        handleNode->setMaterial(m2);
        handleNode->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);
        selectNode->appendChildNode(handleNode);
    }
    // handle geometry
    m2->setColor(m_style->highlightColor());
    g2->vertexDataAsPoint2D()[0].set((m_selectedColumnsCount > 0 ? x2 : x1) - 5, (m_selectedRowsCount > 0 ? y2 : y1) - 5);
    g2->vertexDataAsPoint2D()[1].set((m_selectedColumnsCount > 0 ? x2 : x1) - 5, (m_selectedRowsCount > 0 ? y2 : y1) + 5);
    g2->vertexDataAsPoint2D()[2].set((m_selectedColumnsCount > 0 ? x2 : x1) + 5, (m_selectedRowsCount > 0 ? y2 : y1) + 5);
    g2->vertexDataAsPoint2D()[3].set((m_selectedColumnsCount > 0 ? x2 : x1) + 5, (m_selectedRowsCount > 0 ? y2 : y1) - 5);
    handleNode->setGeometry(g2);
    handleNode->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

    selectNode->setGeometry(g);
    selectNode->markDirty(QSGNode::DirtyGeometry);

    return selectNode;
}

/*!
 * \fn QSGNode *Table::drawPages(QSGNode *n)
 * \brief Used for drawing pages.
 */
QSGNode *Table::drawPages(QSGNode *n)
{
    if (n == nullptr) {
        n = new QSGNode;
    }

    if (updateReasons.testFlag(UPDATE_PAGE)) {
        while(n->childCount() != 0){
            delete n->firstChild();
        }
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto& img = page.image;
                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(0, page.yoffset, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }
    return n;
}

/*!
 * \fn MovingHeaderNode *Table::drawMovingHeader(MovingHeaderNode *n)
 * \brief Used for drawing currently moving header.
 */
MovingHeaderNode *Table::drawMovingHeader(MovingHeaderNode *n)
{
    if (n == nullptr){
        n = new MovingHeaderNode();
        n->setGeometry(new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), 4));
        n->geometry()->setDrawingMode(QSGGeometry::DrawTriangleFan);

        auto m = new QSGFlatColorMaterial();
        m->setColor("transparent");
        n->setMaterial(m);
        n->setFlags(QSGNode::OwnsGeometry | QSGNode::OwnsMaterial);

        n->texture = new QSGSimpleTextureNode();
        n->texture->setOwnsTexture(true);
        n->appendChildNode(n->texture);

        n->shadowNode = new ShadowedRectangleNode();

        //n->appendChildNode(n->shadowNode);
    }

    if (updateReasons.testFlag(UPDATE_HEADER_MOVING)){
        if (columnsMoving()){
            auto col = m_columns[m_columnIndeces[m_movingColumnIndex]];
            if (n->prevColumnIndex != m_movingColumnIndex || n->img.width() != col.width){
                n->img = drawHeader(m_movingColumnIndex);
            }
            auto t = window()->createTextureFromImage(n->img, QQuickWindow::TextureHasAlphaChannel);
            n->texture->setTexture(t);
            n->texture->setRect(QRectF(m_moveCurrentXPos, 0, col.width, m_style->horizontalHeader()->height()));

            n->prevColumnIndex = m_movingColumnIndex;

            n->shadowNode->setBorderEnabled(true);
            //auto col = m_columns[m_columnIndeces[m_movingColumnIndex]];
            n->shadowNode->setRect({m_moveCurrentXPos, 0, col.width, m_style->horizontalHeader()->height()});
            n->shadowNode->setRadius(QVector4D(0, 0, 0, 0));
            n->shadowNode->setOffset(QVector2D{2, 2});
            n->shadowNode->setColor("transparent");
            n->shadowNode->setShadowColor("black");
            n->shadowNode->setSize(4);
            n->shadowNode->setBorderWidth(0.5);
            n->shadowNode->setBorderColor("black");

            n->shadowNode->updateGeometry();
        }else{
            n->shadowNode->setBorderEnabled(true);
            n->shadowNode->setRect({0,0,1,1});
            n->shadowNode->setSize(0);
            n->shadowNode->setRadius(QVector4D(0, 0, 0, 0));
            n->shadowNode->setOffset(QVector2D{0, 0});
            n->shadowNode->setColor("transparent");
            n->shadowNode->setShadowColor("transparent");
            n->shadowNode->setBorderWidth(1);
            n->shadowNode->setBorderColor("transparent");
            n->shadowNode->updateGeometry();

            QImage img(1, 1, QImage::Format_ARGB32);
            n->texture->setRect(QRectF(0,0,0,0));
            n->texture->setTexture(window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel));
        }

        if (n->childCount() == 1)
            n->appendChildNode(n->shadowNode);

        n->markDirty(QSGNode::DirtyGeometry);
    }
    return n;
}

/*!
 * \fn QSGNode *Table::drawBackground(QSGNode *n)
 * \brief Used for background drawing.
 */
QSGNode *Table::drawBackground(QSGNode *n)
{
    if (n == nullptr) {
        n = new QSGNode;
    }

    if (updateReasons.testFlag(UPDATE_PAGE)) {
        while(n->childCount() != 0){
            delete n->firstChild();
        }
        for (const auto &page : qAsConst(m_pages)){
            if (page.visible){
                auto textNode = new QSGSimpleTextureNode;
                textNode->setOwnsTexture(true);
                auto& img = page.background;
                auto t = window()->createTextureFromImage(img, QQuickWindow::TextureHasAlphaChannel);
                textNode->setRect(QRectF(0,page.yoffset, img.width(), img.height()));
                textNode->setTexture(t);
                n->appendChildNode(textNode);
            }
        }
    }
    return n;
}

/*!
 * \fn QSGGeometryNode *Table::drawColumnResizer(QSGNode *n)
 * \brief Used for background drawing.
 */
QSGGeometryNode *Table::drawColumnResizer(QSGNode *n)
{
    QSGGeometryNode *resizerNode;
    if (n == nullptr) {
        resizerNode = new QSGGeometryNode;
        resizerNode->setFlag(QSGNode::OwnsGeometry);
        // color
        auto m = new QSGFlatColorMaterial();
        m->setColor(m_style->highlightColor());
        resizerNode->setMaterial(m);
    } else {
        resizerNode = static_cast<QSGGeometryNode*>(n);
    }

    auto g = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), resizing() ? 2 : 0);
    g->setDrawingMode(QSGGeometry::DrawLines);

    if (resizing()) {
        double h = m_style->cellStyle()->height() * m_rowsCount + m_style->horizontalHeader()->height();
        g->vertexDataAsPoint2D()[0].set(m_resizeCurrentXPos + viewPos().x(), 0);
        g->vertexDataAsPoint2D()[1].set(m_resizeCurrentXPos + viewPos().x(), h);
    }

    resizerNode->setGeometry(g);
    resizerNode->markDirty(QSGNode::DirtyGeometry);

    return resizerNode;
}

/*!
 * \fn QPair<QImage, QImage> Table::drawPageImage(const QVector<Row> &data)
 * \brief Used for page drawing. Return pair of QImages. First of pair is page, second is background.
 */
QPair<QImage, QImage> Table::drawPageImage(const QVector<Row> &data)
{
    if (m_columns.size() == 0) return {};
    auto& a = m_columns[m_columnIndeces[m_columns.size()-1]];
    double w = a.offset + a.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    QImage img(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    QImage bgimg(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    img.fill(QColor(0,0,0,0));
    bgimg.fill(QColor(0,0,0,0));
    QTextLayout textlayout;


    QPainter painter(&img);
    painter.setRenderHints(QPainter::Antialiasing);
    QPainter bgPainter(&bgimg);
    bgPainter.setRenderHints(QPainter::Antialiasing);
    for (int i = 0; i < data.size(); ++i) {
        const auto &row = data[i];
        auto yoffset = i * m_style->cellStyle()->height();
        //const QFontMetrics fontMetrics = painter.fontMetrics();
        for (int j = 0; j < row.cells.size(); j++) {
            const auto &cell = row.cells[m_columnIndeces[j]];
            QRectF cellRect = {
                m_columns[m_columnIndeces[j]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0),
                yoffset ,
                m_columns[m_columnIndeces[j]].width,
                m_style->cellStyle()->height()
            };

            QRectF cellMarginedRect = {
                m_columns[m_columnIndeces[j]].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_style->cellStyle()->leftMargin(),
                yoffset,
                m_columns[m_columnIndeces[j]].width - m_style->cellStyle()->rightMargin() - m_style->cellStyle()->leftMargin(),
                m_style->cellStyle()->height()
            };
            QBrush brush;
            brush.setColor(cell.flags.testFlag(Cell::OWN_BGCOLOR) ? cell.backgroundColor : m_style->cellStyle()->backgroundColor());
            brush.setStyle(Qt::SolidPattern);
            bgPainter.fillRect(cellRect, brush);
            painter.setFont(m_style->cellStyle()->fontStyle()->font());
            painter.setPen(cell.flags.testFlag(Cell::OWN_TXTCOLOR) ? cell.textColor : m_style->cellStyle()->textColor());
            QString text = cell.displayText;
            quint32 alignment = m_columns[m_columnIndeces[j]].cellAlignment == 0 ? m_style->cellStyle()->alignment() : m_columns[m_columnIndeces[j]].cellAlignment;
            if (text == "1" && cell.isCheckBox){
                alignment = Qt::AlignHCenter | Qt::AlignVCenter;
                QFont f;
                f.setPixelSize(m_style->cellStyle()->height() - 5);
                painter.setFont(f);
                text = "☑";
            }else if (text == "0" && cell.isCheckBox){
                alignment = Qt::AlignHCenter | Qt::AlignVCenter;
                text = "☐";
                QFont f;
                f.setPixelSize(m_style->cellStyle()->height() - 5);
                painter.setFont(f);
            }
            painter.drawText(cellMarginedRect, alignment, text);
        }
    }
    QPen pen;
    pen.setColor("red");
    pen.setWidth(4);
    painter.setPen(pen);
    //painter.drawRect(0, 0, w, m_style->cellStyle()->height() * data.size());
    return {img, bgimg};
}

/*!
 * \fn QImage Table::drawPinnedImage(int index, const QVector<Row> &data)
 * \brief Used for drawing specific(pinned) colmn.
 */
QImage Table::drawPinnedImage(int index, const QVector<Row> &data)
{
    if (m_columns.size() == 0) return {};
    if (!m_pinnedColumns.contains(index)) return {};
    auto& a = m_columns[m_columnIndeces[index]];
    double w = a.width;
    QImage img(w, m_style->cellStyle()->height() * m_pageRowsCount, QImage::Format_ARGB32);
    img.fill(QColor(0,0,0,0));

    QPainter painter(&img);
    painter.setRenderHints(QPainter::Antialiasing);
    for (int i = 0; i < data.size(); ++i) {
        const auto &row = data[i];
        auto yoffset = i * m_style->cellStyle()->height();
        const auto &cell = row.cells[m_columnIndeces[index]];
        QRectF cellRect = {
            0,
            yoffset ,
            m_columns[m_columnIndeces[index]].width,
            m_style->cellStyle()->height()
        };

        QRectF cellMarginedRect = {
            static_cast<double>(m_style->cellStyle()->leftMargin()),
            yoffset,
            m_columns[m_columnIndeces[index]].width - m_style->cellStyle()->rightMargin() - m_style->cellStyle()->leftMargin(),
            m_style->cellStyle()->height()
        };

        QColor cc = "transparent";
        if (m_colorProviders.length() > 0){
            if (m_colorProviders.at(0)->hasSelection(row)){
                cc = m_colorProviders.at(0)->selectionColor();
            }
        }

        if (cc == "transparent"){
            if (cell.flags.testFlag(Cell::OWN_BGCOLOR)){
                cc = cell.backgroundColor;
            }
            else{
                cc = m_style->cellStyle()->backgroundColor();
            }
        }
        QBrush brush;
        brush.setColor(cc);
        brush.setStyle(Qt::SolidPattern);
        painter.fillRect(cellRect, brush);
        painter.setFont(m_style->cellStyle()->fontStyle()->font());
        painter.setPen(cell.flags.testFlag(Cell::OWN_TXTCOLOR) ? cell.textColor : m_style->cellStyle()->textColor());
        QString text = cell.displayText;
        quint32 alignment = m_columns[m_columnIndeces[index]].cellAlignment == 0 ? m_style->cellStyle()->alignment() : m_columns[m_columnIndeces[index]].cellAlignment;
        if (text == "1" && cell.isCheckBox){
            alignment = Qt::AlignHCenter | Qt::AlignVCenter;
            QFont f;
            f.setPixelSize(m_style->cellStyle()->height() - 5);
            painter.setFont(f);
            text = "☑";
        }else if (text == "0" && cell.isCheckBox){
            alignment = Qt::AlignHCenter | Qt::AlignVCenter;
            text = "☐";
            QFont f;
            f.setPixelSize(m_style->cellStyle()->height() - 5);
            painter.setFont(f);
        }
        painter.drawText(cellMarginedRect, alignment, text);
        painter.setPen(m_style->gridColor());
        painter.drawLine(0, yoffset , cellRect.width(), yoffset );
    }
    //painter.drawLine(img.width() - 1, 0, img.width() - 1,img.height());
    //painter.drawLine(0, 0, 0, img.height());
    return img;
}

/*!
 * \fn QImage Table::drawHeader(int index)
 * \brief Used for drawing separate header.
 */
QImage Table::drawHeader(int index)
{
    if (index < 0 || index >= m_columns.size()) return {};
    auto col = m_columns[m_columnIndeces[index]];
    //draw image with texts

    QImage img(col.width, m_style->horizontalHeader()->height(), QImage::Format_ARGB32);
    img.fill(QColor(0,0,0,0));

    QPainter painter(&img);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setFont(m_style->horizontalHeader()->fontStyle()->font());

    QRectF cellRect = {
        0,
        0,
        col.width,
        m_style->horizontalHeader()->height()
    };
    //filling bg
    QBrush brush;
    brush.setColor(m_pinnedColumns.contains(index) ? m_style->horizontalHeader()->pinnedColor() : m_columns[m_columnIndeces[index]].backgroundColor == "transparent" ? m_style->horizontalHeader()->backgroundColor()
                                                                                                                                                  : m_columns[m_columnIndeces[index]].backgroundColor);
    brush.setStyle(Qt::SolidPattern);
    painter.fillRect(cellRect, brush);
    //drawing text
    cellRect -= QMarginsF{2.5, 2.5 , (m_provider->headersWithButton().contains(m_columnIndeces[m_columnIndeces[index]]) ? m_style->iconsWidth() : 0) + 6, 0};
    painter.setPen(m_columns[m_columnIndeces[index]].textColor == "black" ? m_style->horizontalHeader()->textColor() : m_columns[m_columnIndeces[index]].textColor);

    painter.drawText(cellRect,
                     m_columns[m_columnIndeces[index]].headerAlignment == 0 ?
                        m_style->horizontalHeader()->alignment() :
                        m_columns[m_columnIndeces[index]].headerAlignment,
                     col.name);
    //drawing icon
    painter.setPen(m_style->iconsColor());
    if (m_provider->headersWithButton().contains(m_columnIndeces[index])){
        QRectF iconRect = QRectF{
                col.width - 5 - m_style->iconsWidth(),
                (m_style->horizontalHeader()->height() - m_style->iconsWidth()) / 2,
                m_style->iconsWidth(),
                m_style->iconsWidth()
    };
        if (m_iconsPaths.contains(m_columnIndeces[index])){
            painter.drawRect(iconRect);
            iconRect -= QMarginsF{1,1,1,1};
            painter.drawImage(iconRect, QImage(m_iconsPaths[m_columnIndeces[index]]));
        }
    }
    painter.setPen(m_style->gridColor());
    painter.drawLine(0, m_style->horizontalHeader()->height() - 1, col.width, m_style->horizontalHeader()->height() - 1);

    return img;
}

/*!
 * \fn QImage Table::drawPageCounter(int from, int count)
 * \brief Used for paint page counter (specific vertical header at the left side).
 */
QImage Table::drawPageCounter(int from, int count)
{
    if (m_style->numericalHeaderAvailable()){
        const QFontMetrics fontMetrics(m_style->verticalHeader()->fontStyle()->font());
        double w = fontMetrics.horizontalAdvance(QString::number(from + count + 1)) + 5;
        QImage img(w, m_style->cellStyle()->height() * count, QImage::Format_ARGB32);
        QPainter painter(&img);
        painter.setRenderHints(QPainter::Antialiasing);
        img.fill(m_style->verticalHeader()->backgroundColor());
        for (int i = 0; i < count; i++){
            auto yoffset = i * m_style->cellStyle()->height();
            //painter.setPen(m_currentRow == from + i ? m_style->highlightColor(): m_style->verticalHeader()->textColor());
            painter.setPen(m_style->verticalHeader()->textColor());
            painter.setFont(m_style->verticalHeader()->fontStyle()->font());
            QRectF cellRect = {
                0,
                yoffset ,
                w - 2,
                m_style->cellStyle()->height()
            };
            painter.drawText(cellRect, Qt::AlignHCenter | Qt::AlignVCenter, QString::number(from + i + 1));
            painter.setPen("black");
            painter.drawLine(0, yoffset + m_style->cellStyle()->height() - 1, w, yoffset + m_style->cellStyle()->height() - 1);
        }
        return img;
    }
    return {};
}

/*!
 * \fn QPointF Table::internalToNormal(QPointF pos)
 * \brief Used for calculating internal position to visual.
 */
QPointF Table::internalToNormal(QPointF pos)
{
    QPointF result;
    result.setX(pos.x() - viewPos().x() + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0 ));
    result.setY(pos.y() - viewPos().y() + m_style->horizontalHeader()->height());
    return result;
}

/*!
 * \fn void Table::checkSelect(double x, double y)
 * \brief Used for updating selected rows/columns by current mouse position \a x and \a y.
 */
void Table::checkSelect(double x, double y)
{
    if (viewPos().width() - 20 - m_pinnedRightWidth < x) {
        m_autoMove.setX(qAbs(x - viewPos().width() - m_pinnedRightWidth + 20) / 10);
    }else if (x < 20 + m_pinnedLeftWidth){
        m_autoMove.setX(-qAbs(viewPos().width() - x - m_pinnedLeftWidth - 20) / 15);
    }else {
        m_autoMove.setX(0);
    }

    if (viewPos().height() - 20 < y) {
        m_autoMove.setY(qAbs(y - viewPos().height() + 20) / 5);
    }else if (y < 20){
        m_autoMove.setY(-qAbs(viewPos().height() - y  - 20) / 15);
    }else {
        m_autoMove.setY(0);
    }
    if (m_autoMove != QPoint{0,0}){
        updateReasons.setFlag(SELECT_CHANGED);
        update();
    }

    bool needBreakX = false, needBreakY = false;

    for (int i = 0; i < m_columns.size(); i++){
        auto col = m_columns[m_columnIndeces[i]];
        auto topPos = internalToNormal(QPointF {col.offset + col.width,
                                                (m_currentRow + selectedRowsCount() - (selectedRowsCount() > 0 ? 1 : 0)) * m_style->cellStyle()->height() + m_style->cellStyle()->height() + m_style->horizontalHeader()->height()});

        if (topPos.x() - col.width / 2 >= x && topPos.x() >= x){
            if (i - m_currentColumn > 0){
                setSelectedColumnsCount(i - m_currentColumn);
            }else if (i - m_currentColumn < 0){
                setSelectedColumnsCount(i - m_currentColumn - 1);
            }else setSelectedColumnsCount(1);

            needBreakX = true;
        }
        if (m_columns.size() - 1 == i && !needBreakX ){
            setSelectedColumnsCount(i - m_currentColumn + 1);
            needBreakX = true;
        }
        double res = (viewPos().y() + y
                      - (m_currentRow * m_style->cellStyle()->height()))
                / m_style->cellStyle()->height() - m_style->horizontalHeader()->height() / m_style->cellStyle()->height();

        int rowsCount = res - (m_selectedRowsCount < 0 ? 0.5 : -0.5);
        if (rowsCount > 0){
            if (m_currentRow + rowsCount - 1 < m_rowsCount)
                setSelectedRowsCount(rowsCount);
            needBreakY = true;
        }else if (rowsCount < 0){
            if (m_currentRow >= qAbs(rowsCount)){
                setSelectedRowsCount(rowsCount - 1);
            }else{
                setSelectedRowsCount(-m_currentRow - 1);
            }
            needBreakY = true;
        }else{
            if (res > 0){
                setSelectedRowsCount(1);
            }else if (res <= -0.5){
                setSelectedRowsCount(-2);
            }

            needBreakY = true;
        }

        if (needBreakY && needBreakX) {
            updateReasons.setFlag(SELECT_CHANGED);
            update();
            return;
        }
    }
    if (needBreakY || needBreakX) {
        updateReasons.setFlag(SELECT_CHANGED);
        update();
    }
}

/*!
 * \fn bool Table::checkHeaderSwap()
 * \brief Used for checking positions and other internal variables.
 * Retutn true if columns were swapped.
 */
bool Table::checkHeaderSwap()
{
    //! automove with headers swap doesn't work
    //! TODO: fix it.
    auto currcol = m_columns[m_columnIndeces[m_movingColumnIndex]];
    double lx = m_moveCurrentXPos, rx = lx + currcol.width;
    //if (viewPos().width() - 20 < rx) {
    //    m_autoMove.setX(qAbs(rx - viewPos().width() + 20) / 10);
    //}else if (lx < 20){
    //    m_autoMove.setX(-qAbs(viewPos().width() - lx  - 20) / 15);
    //}else {
    //    m_autoMove.setX(0);
    //}
    for (int i = 0; i < m_columns.size(); i++){
        auto col = m_columns[m_columnIndeces[i]];

        if (i < m_movingColumnIndex){
            if (lx < col.offset + col.width / 2){
                swapColumns(i, m_movingColumnIndex);
                m_movingColumnIndex = i;
                updateReasons.setFlag(UPDATE_HEADER);
                updateAllPages();
                return true;
            }
        }else if (i > m_movingColumnIndex){
            if (rx > col.offset + col.width / 2){
                swapColumns(i, m_movingColumnIndex);
                m_movingColumnIndex = i;
                updateReasons.setFlag(UPDATE_HEADER);
                updateAllPages();
                return true;
            }
        }
    }
    return false;
}

/*!
 * \fn void Table::checkActuallyPinnedColumns()
 * \brief Used for checking is any column by visual index are actually pinned.
 */
void Table::checkActuallyPinnedColumns()
{
    m_actuallyPinnedColumns.clear();
    m_pinnedLeftWidth = 0;
    m_pinnedRightWidth = 0;
    for (const auto &index : qAsConst(m_pinnedColumns)){
        if (m_columnIndeces[index] >= m_columns.size()) continue;
        auto col = m_columns[m_columnIndeces[index]];
        if (col.offset < viewPos().x() + m_pinnedLeftWidth){
            m_actuallyPinnedColumns.push_back({col.width, (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) + m_pinnedLeftWidth, index, true});
            m_pinnedLeftWidth += col.width;
        }
    }

    QVector<PinnedColumnInfo> tmp;

    for (int index = m_pinnedColumns.size() - 1; index >= 0; index--){
        if (m_columnIndeces[index] >= m_columns.size()) continue;
        auto col = m_columns[m_columnIndeces[m_pinnedColumns[index]]];
        if (col.offset + col.width > viewPos().x() + viewPos().width() - m_pinnedRightWidth){
            tmp.push_front({col.width, viewPos().width() - m_pinnedRightWidth - col.width + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_pinnedColumns[index], false});
            m_pinnedRightWidth += col.width;
        }
    }

    m_actuallyPinnedColumns.append(tmp);
}

/*!
 * \fn void Table::swapColumns(int index1, int index2)
 * \brief Used for swapping columns between each other
 */
void Table::swapColumns(int index1, int index2)
{
    if (index1 > -1 && index1 < m_columns.size() && index2 > -1 && index2 < m_columns.size()){
        auto& col1 = m_columns[m_columnIndeces[index1]];
        auto& col2 = m_columns[m_columnIndeces[index2]];

        if (col1.offset > col2.offset){
            col1.offset = col2.offset;
            col2.offset = col1.offset + col1.width;
        }else{
            col2.offset = col1.offset;
            col1.offset = col2.offset + col2.width;
        }

        int index11 = m_columnIndeces[index1];
        m_columnIndeces[index1] = m_columnIndeces[index2];
        m_columnIndeces[index2] = index11;

        ////bug fix
        for (int i= 1, l = m_columns.size();i<l;++i){
            m_columns[m_columnIndeces[i]].offset = m_columns[m_columnIndeces[i-1]].offset + m_columns[m_columnIndeces[i-1]].width;
        }

        if (m_pinnedColumns.contains(index1) && m_pinnedColumns.contains(index2)){
            int idx2 = m_pinnedColumns.indexOf(index1);
            m_pinnedColumns[m_pinnedColumns.indexOf(index1)] =  index2;
            m_pinnedColumns[idx2] = index1;
            checkActuallyPinnedColumns();
        }else if (m_pinnedColumns.contains(index2)){
            m_pinnedColumns[m_pinnedColumns.indexOf(index2)] =  index1;
            checkActuallyPinnedColumns();
        }else if (m_pinnedColumns.contains(index1)){
            m_pinnedColumns[m_pinnedColumns.indexOf(index1)] =  index2;
            checkActuallyPinnedColumns();
        }
    }
}

/*!
 * \fn void Table::updatePage(int ind)
 * \brief Table::updatePage used for updating cache of page's image by \a ind.
 */
void Table::updatePage(int ind)
{
    if (!m_pages.contains(ind)) return;
    auto& p = m_pages[ind];
    auto data = loadPageData(ind * m_pageRowsCount);
    auto pair = drawPageImage(data);
    p.image = pair.first;
    p.background = pair.second;
    p.vertHeader = drawPageCounter(ind * m_pageRowsCount, m_pageRowsCount);
    p.rowsCount = data.size();
    p.pinnedPages.clear();
    p.pinnedHeaders.clear();
    for (const int &i : qAsConst(m_pinnedColumns)){
        p.pinnedPages.insert(i, drawPinnedImage(i, data));
        p.pinnedHeaders.insert(i, drawHeader(i));
    }
    updateReasons.setFlag(UPDATE_PAGE);
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

/*!
 * \fn void Table::updateAllPages()
 * \brief Table::updateAllPages used for updating cache for all pages.
 */
void Table::updateAllPages()
{
    for (auto it = m_pages.begin(); it != m_pages.end(); it++) {
        auto& p = it.value();
        auto data = loadPageData(it.value().firstRow);
        p.yoffset = p.firstRow * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
        auto pair = drawPageImage(data);
        p.pinnedPages.clear();
        p.pinnedHeaders.clear();
        for (const int &i : qAsConst(m_pinnedColumns)){
            p.pinnedPages.insert(i, drawPinnedImage(i, data));
            p.pinnedHeaders.insert(i, drawHeader(i));
        }
        p.image = pair.first;
        p.background = pair.second;
        p.rowsCount = data.size();
        p.vertHeader = drawPageCounter(p.firstRow * m_style->cellStyle()->height() ,data.size());
    }
    updateReasons.setFlag(UPDATE_PAGE);
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

/*!
 * \fn void Table::updateHeaders()
 * \brief Table::updateHeaders used for updating headers.
 */
void Table::updateHeaders()
{
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

/*!
 * \fn void Table::addPageByRow(int row)
 * \brief Add page to cache by row. If page already exist - return immediately.
 */
void Table::addPageByRow(int row)
{
    QVector<Row> pageRows = loadPageData(row);
    if (!pageRows.isEmpty()){
        int index = pageIndex(row);
        //! new (or start) row index of page
        if (!m_pages.contains(index)){
            int nrow = index * m_pageRowsCount;
            PageData pd;
            pd.firstRow = nrow;
            auto pair = drawPageImage(pageRows);
            pd.image = pair.first;
            pd.background = pair.second;
            pd.pinnedPages.clear();
            pd.pinnedHeaders.clear();
            for (const int &i : qAsConst(m_pinnedColumns)){
                pd.pinnedPages.insert(i, drawPinnedImage(i, pageRows));
                pd.pinnedHeaders.insert(i, drawHeader(i));
            }
            pd.vertHeader = drawPageCounter(nrow, m_pageRowsCount);
            pd.yoffset = index * m_style->cellStyle()->height() * m_pageRowsCount + m_style->horizontalHeader()->height();
            pd.rowsCount = pageRows.length();
            pd.visible = true;
            m_pages.insert(index, pd);
            setPagesCount(m_pages.count());
            updateReasons.setFlag(UPDATE_PAGE);
            update();
        }
    }
}

/*!
 * \fn void Table::updatePageVisibility()
 * \brief used for update internal fields.
 */
void Table::updatePageVisibility()
{
    //! можно оптимизировать
    int startRow = rowIndex(viewPos().y());
    int endRow = rowIndex(viewPos().y() + viewPos().height());
    int startPageIndex = pageIndex(startRow);
    int endPageIndex = pageIndex(endRow);

    if (startPageIndex > 0 && (startRow - startPageIndex * m_pageRowsCount) < 10) {
        startPageIndex -= 1;
    }

    auto pages = m_pages.keys();
    bool needUpd = false;

    for (int  i=startPageIndex; i <=endPageIndex; ++i) {
        pages.removeAll(i);
        if (!m_pages.contains(i)) {
            addPageByRow(i*m_pageRowsCount);
            needUpd = true;
        }
    }

    for (auto p: pages) {
        m_pages.remove(p);
    }

    if (m_style->numericalHeaderAvailable()){
        //m_style->verticalHeader()->m_width = m_pages[endPageIndex].vertHeader.width();
        //emit m_style->verticalHeader()->widthChanged();
        //updateReasons.setFlag(UPDATE_HEADER);
        //updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
        //update();
    }

    if (needUpd) {
        updateReasons.setFlag(UPDATE_PAGE);
        update();
    }
    return;

}

/*!
 * \fn QVector<Row> Table::loadPageData(int row)
 * \brief load data from ITableDataProvider by row.
 */
QVector<Row> Table::loadPageData(int row)
{
    if (m_provider == nullptr) return {};

    int index = pageIndex(row);
    //! new (or start) row index of page
    int nrow = index * m_pageRowsCount;

    QVector<Row> pageRows;
    for (int i = 0; i < m_pageRowsCount; i++){
        auto row = m_provider->getRow(nrow + i);
        if (row.id != -1){
            pageRows.push_back(row);
        }else{
            break;
        }
    }
    return pageRows;
}

/*!
 * \fn void Table::setStyle(TableStyle *newStyle)
 * \brief Table's style setter.
 */
void Table::setStyle(TableStyle *newStyle)
{
    if (m_style == newStyle && newStyle == nullptr)
        return;
    if (m_style != nullptr){
        if (m_style->parent() == this){
            m_style->deleteLater();
        }else{
            disconnect(m_style, nullptr, this, nullptr);
        }
    }

    m_style = newStyle;

    connect(m_style, &TableStyle::updateAllPages, this, &Table::updateAllPages);
    connect(m_style, &TableStyle::updateHeaders, this, &Table::updateHeaders);

    double r = 0;
    if (!provider()) return;
    for (int  i=0, l= m_provider->columnCount(); i <l; ++i) {
        if (m_columns[m_columnIndeces[i]].width == 0){
            m_columns[m_columnIndeces[i]].width = m_style->horizontalHeader()->width();
        }
        m_columns[m_columnIndeces[i]].offset = r;
        r+= m_columns[m_columnIndeces[i]].width;
    }
    setContentWidth(r);

    updateAllPages();
    updatePageVisibility();
    updateHeaders();
    emit styleChanged(m_style);
}

/*!
 * \fn void Table::setCurrentRow(int currentRow)
 * \brief Set current row position if it possible and update internal fields after.
 */
void Table::setCurrentRow(int currentRow)
{
    if (m_currentRow == currentRow)
        return;
    if (currentRow == -1){
        updateReasons.setFlag(SELECT_CHANGED);
        updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
        emit currentRowChanged(m_currentRow);
    }


    m_currentRow = currentRow;
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
    auto y = (m_currentRow == m_rowsCount ? m_currentRow - 1 : m_currentRow) * m_style->cellStyle()->height() + m_style->horizontalHeader()->height();
    auto w = viewPos().height();
    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    if ((y-w) > viewPos().y() - m_style->cellStyle()->height()) {
        moveToPos(m_viewPos.x() , y-w + m_style->cellStyle()->height());
    }else if (y - m_style->horizontalHeader()->height() < viewPos().y()){
        moveToPos(m_viewPos.x() , y - m_style->horizontalHeader()->height());
    }
    update();

    emit currentRowChanged(m_currentRow);
}

/*!
 * \fn void Table::setCurrentColumn(int currentColumn)
 * \brief Set current column position if it possible and update internal fields after.
 */
void Table::setCurrentColumn(int currentColumn)
{
    if (m_currentColumn == currentColumn)
        return;

    m_currentColumn = currentColumn;
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
    auto x = m_columns[m_currentColumn].offset + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);
    auto w = m_columns[m_currentColumn].width;
    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    bool pinnedContains = false;
    for (const auto &col : qAsConst(m_actuallyPinnedColumns)){
        if (col.visualIndex == currentColumn) {
            pinnedContains = true;
            break;
        }
    }

    if (!pinnedContains){
        if (x > (viewPos().x() + viewPos().width() - w - m_pinnedRightWidth)) {
            moveToPos(-(viewPos().width() - m_pinnedRightWidth +(m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0) - x - w), m_viewPos.y());
        } else if (x < viewPos().x() + m_pinnedLeftWidth + (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0)) {
            moveToPos(x - m_pinnedLeftWidth - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0), m_viewPos.y());
        }
    }
    update();

    emit currentColumnChanged(m_currentColumn);
}

/*!
 * \fn void Table::setSelectedColumnsCount(int selectedColumnsCount)
 * \brief Set new selected columns count.
 */
void Table::setSelectedColumnsCount(int selectedColumnsCount)
{
    if (m_selectedColumnsCount == selectedColumnsCount)
        return;

    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    m_selectedColumnsCount = selectedColumnsCount;
    emit selectedColumnsCountChanged(m_selectedColumnsCount);
    update();
}

/*!
 * \fn void Table::setSelectedRowsCount(int selectedRowsCount)
 * \brief Set new selected rows count.
 */
void Table::setSelectedRowsCount(int selectedRowsCount)
{
    if (m_selectedRowsCount == selectedRowsCount)
        return;

    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    m_selectedRowsCount = selectedRowsCount;
    emit selectedRowsCountChanged(m_selectedRowsCount);
    update();
}

/*!
 * \fn void Table::setProvider(QObject *provider)
 * \brief Set provider (ITableDataProvider).
 */
void Table::setProvider(QObject *provider)
{
    if ((QObject*)m_provider == provider)
        return;

    if (m_provider != nullptr) {
        disconnect(m_provider, nullptr, this, nullptr);
    }

    m_provider = qobject_cast<ITableDataProvider*>(provider);
    auto h = m_provider->headerData();
    m_columns.resize(m_provider->columnCount());

    connect(m_provider, &ITableDataProvider::reseted, this, &Table::reset);
    connect(m_provider, &ITableDataProvider::rowsUpdated, this, &Table::updateRows);
    connect(m_provider, &ITableDataProvider::sortChanged, this, &Table::setHeaderIcon);
    connect(m_provider, &ITableDataProvider::filterChanged, this, &Table::setHeaderIcon);
    connect(m_provider, &ITableDataProvider::fullLoaded, this, [this](){
        setRowsCount(m_provider->rowCount());
        setReachedBottom(true);
    });

    double r = 0;
    for (int  i=0, l = m_provider->columnCount(); i <l; ++i) {
        m_columns[m_columnIndeces[i]].name = h[i].text;
        m_columns[m_columnIndeces[i]].cellAlignment = h[i].cellAlignment;
        m_columns[m_columnIndeces[i]].headerAlignment = h[i].headerAlignment;
        if (h.length() > i){
            m_columns[m_columnIndeces[i]].width = h[i].width;
        }
        r += m_columns[m_columnIndeces[i]].width;
    }
    setContentWidth(r);
    reset();
    //! [fix]
    setRowsCount(m_provider->rowCount());
    setColumnsCount(m_provider->columnCount());
    //! [fix]
    emit editableChanged(m_provider->isEditable());
    emit providerChanged((QObject*)m_provider);
}

/*!
 * \fn void Table::setColorProvider(QObject *provider)
 * \brief Set color provider (ITableColorProvider).
 */
void Table::setColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (!m_colorProviders.contains(pprovider) && pprovider){

        for (int i = m_colorProviders.size() - 1; i >= 0; i--){
            removeColorProvider(m_colorProviders[i]);
        }

        m_colorProviders.append(pprovider);
        updateAllPages();

        connect(pprovider, &ITableColorProvider::schemeChanged, this, [this](){
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        connect(provider, &ITableColorProvider::destroyed, this, [this, pprovider](){
            m_colorProviders.removeAll(pprovider);
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

/*!
 * \fn void Table::appendColorProvider(QObject *provider)
 * \brief Append another color provider to internal list.
 */
void Table::appendColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (!m_colorProviders.contains(pprovider) && pprovider){
        m_colorProviders.append(pprovider);

        connect(pprovider, &ITableColorProvider::schemeChanged, this, [this](){
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        connect(provider, &ITableColorProvider::destroyed, this, [this, pprovider](){
            m_colorProviders.removeAll(pprovider);
            updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
            update();
        });

        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

/*!
 * \fn void Table::removeColorProvider(QObject *provider)
 * \brief Table::removeColorProvider
 */
void Table::removeColorProvider(QObject *provider)
{
    ITableColorProvider * pprovider = qobject_cast<ITableColorProvider*>(provider);
    if (m_colorProviders.contains(pprovider) && pprovider){
        disconnect(pprovider, nullptr, this, nullptr);
        m_colorProviders.removeAll(pprovider);
        updateReasons.setFlag(UPDATE_COLOR_BACKGROUND);
        update();
    }
}

/*!
 * \fn void Table::resetSelection()
 * \brief Used for resetting selection.
 */
void Table::resetSelection()
{
    setCurrentCell(-1, 0);
    setCurrentRow(-1);
    setSelectedColumnsCount(1);
    setSelectedRowsCount(1);
}

/*!
 * \fn QRectF Table::cellPos(int row, int column)
 * \brief Return cell's geometry by \c row and \c column.
 */
QRectF Table::cellPos(int row, int column)
{
    QRectF result {0,0,0,0};
    if (column < m_columns.size() && column >= 0 &&
            row < m_rowsCount && row >= 0){
        result.setX(m_columns[column].offset + (m_style->verticalHeaderAvailable() ? m_style->verticalHeader()->width() : 0));
        result.setY(row * m_style->cellStyle()->height() + m_style->horizontalHeader()->height());
        result.setWidth(m_columns[column].width);
        result.setHeight(m_style->cellStyle()->height());
    }
    return result;
}

/*!
 * \fn QColor Table::cellBackgroundColor(int row, int column)
 * \brief Used for getting color for specific cell.
 */
QColor Table::cellBackgroundColor(int row, int column)
{
    if (m_provider){
        auto r = m_provider->getRow(row);
        if (m_colorProviders.length() != 0){
            for (const auto &p : qAsConst(m_colorProviders)){
                if (p->hasSelection(r.id)){
                    return p->selectionColor();
                }
            }
        }

        if (r.cells.size() > column && column >= 0){
            auto c = r.cells[column];
            if (c.flags.testFlag(Cell::OWN_BGCOLOR)){
                return c.backgroundColor;
            }
        }
    }
    return m_style->cellStyle()->backgroundColor();
}

/*!
 * \fn QString Table::cellValue(int row, int column)
 * \brief Return value of cell by specific \c row and \c column.
 */
QString Table::cellValue(int row, int column)
{
    if (column < m_columns.size() && column >= 0 &&
            row < m_rowsCount && row >= 0){
        return m_provider->getRow(row).cells[column].displayText;
    }
    return "";
}

/*!
 * \fn void Table::setCellValue(int row, int column, QVariant value)
 * \brief Cell's value setter by \c row and \c column.
 */
void Table::setCellValue(int row, int column, QVariant value)
{
    if (m_provider) m_provider->setCellData(row, column, value);
}

/*!
 * \fn QString Table::headerTitle(int column)
 * \brief Return header text by column.
 */
QString Table::headerTitle(int column)
{
    if (m_columnsCount > column && column >= 0)
        return m_columns[column].name;
    return "";
}

/*!
 * \fn void Table::setHeaderIcon(int column, QString iconpath)
 * \brief Setter for icon button.
 */
void Table::setHeaderIcon(int column, QString iconpath)
{
    m_iconsPaths[column] = iconpath;
    updateReasons.setFlag(UPDATE_HEADER);
    update();
}

/*!
 * \fn void Table::setCurrentCell(int row, int column)
 * \brief Method set new current cell by \c row and \c column if it possible.
 */
void Table::setCurrentCell(int row, int column)
{
    if (row >=0 && row < rowsCount())
        setCurrentRow(row);
    if (column >= 0 && column < columnsCount())
        setCurrentColumn(column);

    updateReasons.setFlag(SELECT_CHANGED);
    updateReasons.setFlag(UPDATE_HEADER_HIGHLIGHT);
    update();
}

/*!
 * \fn int Table::headerIndex(QString name)
 * \brief Return column index by header text(title).
 */
int Table::headerIndex(QString name)
{
    for (int i = 0; i < m_columns.size(); i++){
        if (m_columns[m_columnIndeces[i]].name == name) return i;
    }
    return -1;
}

/*!
 * \fn void Table::pinColumn(int column)
 * \brief method used for adding visual column index to pin sequance.
 */
void Table::pinColumn(int column)
{
    if (m_pinnedColumns.contains(column)) return;
    m_pinnedColumns.push_back(column);

    std::sort(m_pinnedColumns.begin(), m_pinnedColumns.end());
    checkActuallyPinnedColumns();
    updateAllPages();
    update();
}

/*!
 * \fn void Table::unpinColumn(int column)
 * \brief method used for removing visual column index to pin sequance.
 */
void Table::unpinColumn(int column)
{
    if (!m_pinnedColumns.contains(column)) return;
    m_pinnedColumns.removeAll(column);
    std::sort(m_pinnedColumns.begin(), m_pinnedColumns.end());
    checkActuallyPinnedColumns();
    updateAllPages();
    update();
}

/*!
 * \fn QVariantList Table::pinnedIndeces()
 * \brief Return QVariantList of indeces.
 */
QVariantList Table::pinnedIndeces()
{
    QVariantList list;
    for (int i = 0; i < m_pinnedColumns.size(); i++){
        list.push_back(m_pinnedColumns[i]);
    }
    return list;
}

/*!
 * \fn QPoint Table::normalToCell(QPointF pos)
 * \brief Return row and column (x and y) by mouse position. Used only as internal helper.
 */
QPoint Table::normalToCell(QPointF pos)
{
    QPoint res{-1, -1};
    auto visualX = pos.x() + viewPos().x() - (m_style->verticalHeaderAvailable() ?  m_style->verticalHeader()->width() : 0);

    if(pos.y() > m_style->horizontalHeader()->height()){
        for (int i = 0, l = m_columns.size(); i < l; ++i){
            if (m_columns[m_columnIndeces[i]].offset + m_columns[m_columnIndeces[i]].width >= visualX){
                res.setX(i);
                break;
            }
        }
    }

    int row = (pos.y() - m_style->horizontalHeader()->height() + viewPos().y()) / m_style->cellStyle()->height();
    if (row >= m_rowsCount) row = m_rowsCount - 1;

    res.setY(row);

    return res;
}

/*!
 * \fn int Table::getInternalIndex(int index)
 * \brief Return column index by visual index.
 */
int Table::getInternalIndex(int index)
{
    if (m_columnIndeces.contains(index)) return m_columnIndeces[index];
    return -1;
}

void Table::setColumnsMovable(bool columnsMovable)
{
    if (m_columnsMovable == columnsMovable)
        return;

    m_columnsMovable = columnsMovable;
    emit columnsMovableChanged(m_columnsMovable);
}

void Table::setPressed(bool pressed)
{
    if (m_pressed == pressed)
        return;

    m_pressed = pressed;
    emit pressedChanged(m_pressed);
}

void Table::setColumnsMoving(bool columnsMoving)
{
    if (m_columnsMoving == columnsMoving)
        return;

    m_columnsMoving = columnsMoving;
    emit columnsMovingChanged(m_columnsMoving);
}

void Table::setReachedBottom(bool reachedBottom)
{
    if (m_reachedBottom == reachedBottom)
        return;

    m_reachedBottom = reachedBottom;
    emit reachedBottomChanged(m_reachedBottom);
}

void Table::setPagesCount(int pagesCount)
{
    if (m_pagesCount == pagesCount)
        return;

    m_pagesCount = pagesCount;
    emit pagesCountChanged(m_pagesCount);
}

void Table::setViewPos(QRectF viewPos)
{
    if (m_viewPos == viewPos)
        return;
    m_viewPos = viewPos;
    updatePageVisibility();
    checkActuallyPinnedColumns();
    emit viewPosChanged(m_viewPos);
}

/*!
 * \fn void Table::setContentWidth(double contentWidth)
 * \internal
 * \brief column width setter.
 */
void Table::setContentWidth(double contentWidth)
{
    if (qFuzzyCompare(m_contentWidth, contentWidth))
        return;

    m_contentWidth = contentWidth;
    emit contentWidthChanged(m_contentWidth);
}

/*!
 * \fn void Table::setRowsCount(int rowsCount)
 * \brief Table::setRowsCount
 */
void Table::setRowsCount(int rowsCount)
{
    if (m_rowsCount == rowsCount)
        return;
    m_rowsCount = rowsCount;
    emit contentHeightChanged(contentHeight());
    emit rowsCountChanged(m_rowsCount);
}

/*!
 * \fn void Table::setColumnsCount(int columnCount)
 * \brief Columns count setter.
 */
void Table::setColumnsCount(int columnCount)
{
    if (m_columnsCount == columnCount)
        return;

    m_columnsCount = columnCount;
    emit columnsCountChanged(m_columnsCount);
}

/*!
 * \fn void Table::setResizing(bool resizing)
 * \brief setter of property \property Table::resizing
 */
void Table::setResizing(bool resizing)
{
    if (m_resizing == resizing)
        return;

    m_resizing = resizing;
    if (!m_resizing){
        updateContentWidth();
    }
    emit resizingChanged(m_resizing);
}

/*!
    \property TableStyle *Table::style
    \brief This property holds the visual style (TableStyle) of the Table.
*/
TableStyle *Table::style() const { return m_style; }

/*!
    \property QRectF Table::viewPos
    \brief This property holds the visual rectF of the Table.
*/
QRectF Table::viewPos()                  const { return m_viewPos; }

/*!
    \property bool Table::resizing
    \brief This property holds the state of flag. Changes its state to \c true when colmn resizing in process.
*/
bool Table::resizing()                   const { return m_resizing; }

/*!
    \property QObject* Table::provider
    \brief This property holds the ITableDataProvider.
*/
QObject *Table::provider()               const { return m_provider; }

/*!
    \property int Table::rowsCount
    \brief This property holds the number of rows (which had been loaded in a while ago).
*/
int Table::rowsCount()                   const { return m_rowsCount; }

/*!
    \property int Table::rowsCount
    \brief This property holds currentRow's index.
*/
int Table::currentRow()                  const { return m_currentRow; }

/*!
    \property int Table::pagesCount
    \brief This property holds number of pages.
*/
int Table::pagesCount()                  const { return m_pagesCount; }

/*!
    \property int Table::columnCount
    \brief This property holds number of columns.
*/
int Table::columnsCount()                 const { return m_columnsCount; }

/*!
    \property double Table::contentWidth
    \brief This property holds the width of whole content.
*/
double Table::contentWidth()             const { return m_contentWidth; }

/*!
    \property int Table::currentColumn
    \brief This property holds the currentColumn's index.
*/
int Table::currentColumn()               const { return m_currentColumn; }

/*!
    \property bool Table::reachedBottom
    \brief This property holds the flag which display state of provider's full filling.
*/
bool Table::reachedBottom()              const { return m_reachedBottom; }

/*!
    \property int Table::selectedRowsCount
    \brief This property holds the number of selected rows.
    This number can be less than \c 0, that show us that selection move to top of table.
    If number higher than \c 0, that means that the selection moves to bottom of table.
    <br>The range is <b>[-Table::rowsCount, 0) && (0, Table::rowsCount]</b>.
*/
int Table::selectedRowsCount()           const { return m_selectedRowsCount; }

/*!
    \property int Table::selectedColumnsCount
    \brief This property holds the number of selected columns.
    This number can be less than \c 0, that show us that selection move to top of table.
    If number higher than \c 0, that means that the selection moves to bottom of table.
    The range is <b>[-Table::columnsCount, 0) && (0, Table::columnsCount]</b>.
*/
int Table::selectedColumnsCount()        const { return m_selectedColumnsCount; }

/*!
 * \property bool Table::columnsMovable
 * \brief This property holds the ability to columns moving(swaping).
 */
bool Table::columnsMovable()             const { return m_columnsMovable; }

/*!
 * \brief Table::pressed
 * \return
 */
bool Table::pressed()                    const { return m_pressed; }

/*!
 * \property bool Table::columnsMoving
 * \brief This property holds the state of moving columns.
 * \remark This property is read-only
 */
bool Table::columnsMoving()              const { return m_columnsMoving; }

/*!
    \property double Table::contentHeight
    \brief This property holds the height of whole content.
    \remark This property is read-only
*/
double Table::contentHeight()            const { return m_rowsCount * m_style->cellStyle()->height() + m_style->horizontalHeader()->height(); }

/*!
    \property bool Table::editable
    \brief This property holds the ediatable state of table and sets from ITableDataProvider::isEditable().
    \remark This property is read-only
*/
bool Table::editable()                   const { return m_provider ? m_provider->isEditable() : false; }
