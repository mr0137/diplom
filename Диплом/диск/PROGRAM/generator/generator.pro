TEMPLATE = subdirs

SUBDIRS = \
    LablePrint \
    SymbolsGenerator \
    libs \
    plugins \
    #main

libs.subdir = libs
plugins.subdir = plugins
#main.subdir = main
Matrix.subdir = Matrix
plugins.depends = libs

#main.depends = libs plugins
Matrix.depends = libs plugins
