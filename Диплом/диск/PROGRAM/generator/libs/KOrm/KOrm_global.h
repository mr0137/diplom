#ifndef KORM_GLOBAL_H
#define KORM_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(KORM_LIBRARY)
#  define KORM_EXPORT Q_DECL_EXPORT
#else
#  define KORM_EXPORT Q_DECL_IMPORT
#endif

#endif // KORM_GLOBAL_H
