#include "customproxymodel.h"
#include "klist.h"

#include <QDebug>

CustomProxyModel::CustomProxyModel(QObject *parent):QSortFilterProxyModel(parent)
{

}

void CustomProxyModel::setFilter(std::function<bool (QObject *)> filter)
{
    m_filter = filter;
    invalidate();
}

QAbstractItemModel *CustomProxyModel::source() const
{
    return sourceModel();
}

void CustomProxyModel::setSource(QAbstractItemModel *source)
{
    setSourceModel(source);
}

void CustomProxyModel::setFilterRow(const int source_row)
{
    auto proxy = static_cast<QAbstractProxyModel*>(sourceModel());
    auto const proxyIndex = proxy->index(source_row, 0);
    filterAcceptsRow(source_row,proxyIndex);
}

QObject *CustomProxyModel::getObject(const int row)
{
    auto shellModel = qobject_cast<KShellModel*>(sourceModel());
    if (shellModel)
        return shellModel->getObject(row);

    return nullptr;
}

void CustomProxyModel::sortModelOnColumn(const int role, const int column)
{
    qDebug()<<role<<"   ==   "<<column;
    static bool sortFlag;
    Qt::SortOrder order = !sortFlag ? Qt::AscendingOrder : Qt::DescendingOrder;
    setSortRole(role);
    sort(column,order);
    sortFlag = sortFlag ? false : true;
}

void CustomProxyModel::shovActiveComponent(const int role, QString )
{
    setFilterRole(role);
    setFilterRegularExpression("^true$");
}

void CustomProxyModel::setFilterOnRole(const int role, const QString value)
{
    if(role <= 0 || value.isEmpty()){
        return ;
    }
    QRegExp reg(value, Qt::CaseInsensitive);
//    QRegularExpression reg(value,Qt::CaseInsensitive);
    setFilterRole(role);
    setFilterRegExp(reg);
}

void CustomProxyModel::resetModel()
{
    QRegExp reg;
    setFilterRegExp(reg);
}

int CustomProxyModel::roleKey(const QByteArray &role) const
{
    return -1;
}

bool CustomProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    auto model = sourceModel();

    QModelIndex sourceIndex = model->index(source_row, 0, source_parent);

    if (m_filter){
        auto ob = model->data(sourceIndex, Qt::UserRole+1);
        auto obj = qvariant_cast<QObject*>(model->data(sourceIndex, Qt::UserRole+1));
        return m_filter(obj);
    }
    return true;
}

//bool CustomProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
//{
//    QVariant leftData = sourceModel()->data(left);
//    QVariant rightData = sourceModel()->data(right);

//    if (leftData.type() == QVariant::DateTime) {
//        //return leftData.toDateTime() < rightData.toDateTime();
//    } else {
//        static QRegExp emailPattern("[\\w\\.]*@[\\w\\.]*)");

//        QString leftString = leftData.toString();
//        if(left.column() == 1 && emailPattern.indexIn(leftString) != -1)
//            leftString = emailPattern.cap(1);

//        QString rightString = rightData.toString();
//        if(right.column() == 1 && emailPattern.indexIn(rightString) != -1)
//            rightString = emailPattern.cap(1);

//        return QString::localeAwareCompare(leftString, rightString) < 0;
//    }

//    return false;
//}


