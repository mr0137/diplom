#include "klist.h"


#include "kpoco.h"

QVariant KShellModel::data(const QModelIndex &index, int role) const {
    auto object = qobject_cast<KPoco*>(m_getter(index.row()));
    if (role == (Qt::UserRole+1)) {
        return QVariant::fromValue(object);
    }

    auto search = m_roles.find(role);
    if (search != m_roles.end()) {
        return object->getField(search.value());
    }

    return QVariant::fromValue<QObject*>(nullptr);
}

QObject *KShellModel::getObject(int index)
{
    if (index < 0 || index >= rowCount({})) return nullptr;
    return m_getter(index);
}

void KShellModel::_buildIndex()
{
    auto getIndexes = [this](QByteArray role){
        QVector<int> res;
        for (auto it = m_roles.begin(), e = m_roles.end(); it != e; it++) {
            if (it.value().startsWith(role)) res << it.key();
        }
        return res;
    };

    for (const auto &r : qAsConst(m_roles)) {
        m_rolesName2indexes[r] = getIndexes(r);
    }
}

QMap<QString, int> KShellModel::getFieldToRoleMap() const
{
    return fieldToRoleMap;
}

int KShellModel::getIndexRoleOnName(QString nameRole) const
{
    return fieldToRoleMap[nameRole];
}

void KShellModel::setFieldToRoleMap(const QMap<QString, int> &value)
{
    fieldToRoleMap = value;
}


