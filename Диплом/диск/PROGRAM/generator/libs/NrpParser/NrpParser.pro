QT -= gui
QT += gui-private
QT += core
QT += xml

TEMPLATE = lib
DEFINES += NRPPARSER_LIBRARY

CONFIG += c++17
CONFIG(release, debug|release){
DESTDIR = $$PWD/../../bin/libs
}
SOURCES += \
    nrpparser.cpp

HEADERS += \
    NrpParser_global.h \
    nrpparser.h #\
#    qzipreader_p.h \
#    qzipwriter_p.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../../bin/libs/ -lQXlsx
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QXlsx/debug/ -lQXlsx
else:unix: LIBS += -L$$OUT_PWD/../QXlsx/ -lQXlsx

INCLUDEPATH += $$PWD/../QXlsx
DEPENDPATH += $$PWD/../QXlsx

