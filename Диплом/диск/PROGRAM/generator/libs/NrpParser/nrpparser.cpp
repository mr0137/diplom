#include "nrpparser.h"

#include <QBuffer>
#include <QFile>
#include <QDebug>
#include <QRegularExpression>
#include <QDir>
#include <QDirIterator>
#include <QtMath>

namespace NrpParser {

QVector<ObjectFile> NrpParser::getObjectFiles() const
{
    return m_objectFiles;
}

void NrpParser::init(const QByteArray &data)
{
    auto copy = std::move(data);
    QBuffer s(&copy);
    QZipReader r(&s);
    QVector<QPair<QString, QString>> nrpPaths;

    for(auto & entry : r.fileInfoList()) {
        if(entry.size){
            auto str = entry.filePath;
            int l_index_e = 0;
            ObjectFile file;
            QString name;
            QString ext;
            QString path;

            l_index_e = str.contains('.') ? str.lastIndexOf('.') : str.count();

            if(str.contains('/')){
                int l_index_n = str.lastIndexOf('/');
                file.name = str.mid(l_index_n + 1, l_index_e - l_index_n - 1);
                file.ext = str.mid(l_index_e + 1, str.count() - 1);
                file.path = str.mid(0,l_index_n + 1);
            }else{
                file.name = str.mid(0, l_index_e);
                file.ext = str.mid(l_index_e + 1, str.count() - 1);
            }

            if(file.ext == "xml"){
                QString tmp = r.fileData(entry.filePath);
                tmp.remove("\t");
                tmp.remove("\n");
                tmp.remove("\r");
                tmp.remove("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                file.xml = tmp;
            }else{
                file.bytes = r.fileData(entry.filePath);
            }

            //qDebug()<<file.path <<" - "<<file.name<<" - "<<file.ext;

            m_objectFiles.push_back(file);

            //qDebug()<<path<<" - "<<name<<" - "<<ext;
            //qDebug()<<r.fileData(entry.filePath);
        }

        // auto nrpFile = QDir().absoluteFilePath(fileNames.last());
        // auto rexp1 = QRegularExpression("Nest2D/Results/content.xml");
        // auto rexp2 = QRegularExpression("Nest2D/Results/\\d+/content.xml");
        // if(entry.filePath == "Nest2D/Parts/content.xml") {
        //     partsData.append(r.fileData(entry.filePath));
        //     partsCatalogueMapping[partsData.size() - 1] = nrpFile;
        // }else if(entry.filePath == "Nest2D/Plates/content.xml") {
        //     plateData.append(r.fileData(entry.filePath));
        //     platesCatalogueMapping[plateData.size() - 1] = nrpFile;
        // }else if (rexp1.match(entry.filePath).hasMatch()) {
        //     parseResultStats(r.fileData(entry.filePath));
        // }else if(rexp2.match(entry.filePath).hasMatch()) {
        //     nrpPaths.push_back({entry.filePath, nrpFile});
        // }
    }





    //for (const auto &p : nrpPaths){
    //    resultsData.push_back(r.fileData(p.first));
    //    resultCatalogueMapping[resultsData.size() - 1] = p.second;
    //}

    r.close();
}

QByteArray NrpParser::openFile(const QString &fileName)
{
    assert(fileName.endsWith(".nrp2") || fileName.endsWith(".lxds"));
    assert(std::find_if(fileNames.begin(), fileNames.end(), [&](const QString & name){return name == fileName;}) == fileNames.end());

    fileNames.append(fileName);
    QFile file(fileName);
    file.open(QFile::ReadOnly);
    auto data = file.readAll();
    file.close();
    return data;
}

NrpParser::NrpParser()
{
}

NrpParser::~NrpParser()
{
    clearAll();
}

NrpParser::NrpParser(const QString & folder)
{
    loadFolder(folder);
}

NrpParser::NrpParser(const char * path) : NrpParser(QVector<QString>{QString(path)})
{

}

void NrpParser::loadXlsx(const QString &xlsxName)
{
    QString nameCopy = xlsxName;
    if(nameCopy.startsWith("file:///")) {
        nameCopy = nameCopy.mid(8);
    }
    QXlsx::Document doc(nameCopy);
    if(doc.isLoadPackage() && !xlsxFileNames.contains(nameCopy)) {
        xlsxFileNames.append(nameCopy);
        int i = 2;
        for(int k = 1; k < doc.dimension().columnCount(); k++) {
            //qDebug() << k;
            QString hLetter((char)(64 + k));
            auto header = doc.cellAt(hLetter + "1")->value().toString();
            if(header == "Позначення" || header == "Позн." || header == "Поз.") codeColIndex = hLetter;
            if(header == "Матеріал" || header == "Мат.") metalTypeColIndex = hLetter;
            if(header == "Товщина" || header == "Товщ.") thicknessColIndex = hLetter;
            if(header == "Кількість" || header == "К-сть") countColIndex = hLetter;
        }

        auto cell = doc.cellAt(codeColIndex + QString::number(i));
        if(cell) fromExcel.push_back({});
        while(cell) {

            auto name = cell->value().toString();
            if(!name.isEmpty()) {
                ExcelPart p;
                p.name = name;
                cell = doc.cellAt(metalTypeColIndex + QString::number(i));
                if(cell) p.metalType = cell->value().toString();
                cell = doc.cellAt(thicknessColIndex + QString::number(i));
                if(cell) p.thickness = cell->value().toInt();
                cell = doc.cellAt(countColIndex + QString::number(i));
                if(cell) p.count = cell->value().toInt();
                p.position = i;
                p.file = nameCopy;

                fromExcel.last().push_back(p);
            }

            i++;
            cell = doc.cellAt(codeColIndex + QString::number(i));
        }

        //qDebug() << fromExcel;
    }
    else {
        //qDebug() << xlsxName;
    }
}

void NrpParser::loadXlsx(int i)
{
    auto fileName = xlsxFileNames[i];
    auto filters = columns[i];
    auto codeColIndex = filters["name"].toString();
    auto metalTypeColIndex = filters["metalType"].toString();
    auto thicknessColIndex = filters["thickness"].toString();
    auto countColIndex = filters["count"].toString();
    QXlsx::Document doc(fileName);
    int j = 2;
    auto cell = doc.cellAt( codeColIndex + QString::number(j));
    if(cell && fromExcel.size() < xlsxFileNames.size()) fromExcel.push_back({});
    while(cell) {

        auto name = cell->value().toString();
        if(!name.isEmpty()) {
            ExcelPart p;
            p.name = name;
            cell = doc.cellAt(metalTypeColIndex + QString::number(j));
            if(cell) p.metalType = cell->value().toString();
            cell = doc.cellAt(thicknessColIndex + QString::number(j));
            if(cell) p.thickness = cell->value().toInt();
            cell = doc.cellAt(countColIndex + QString::number(j));
            if(cell) p.count = cell->value().toInt();
            p.position = j;
            p.file = fileName;

            fromExcel[i].push_back(p);
        }

        j++;
        cell = doc.cellAt(codeColIndex + QString::number(j));
    }
}

void NrpParser::loadXlsx()
{
    loadXlsx(xlsxFileNames.size() - 1);
}

void NrpParser::reloadXlsx(int i)
{
    fromExcel[i].clear();

    loadXlsx(i);

}

QVariantList NrpParser::addXlsx(const QString &xlsxName)
{
    QString nameCopy = xlsxName;
    if(nameCopy.startsWith("file:///")) {
        nameCopy = nameCopy.mid(8);
    }
    QXlsx::Document doc(nameCopy);
    if(doc.isLoadPackage() && !xlsxFileNames.contains(nameCopy)) {
        xlsxFileNames.append(nameCopy);
        QVariantList columns;
        for(int k = 1; k < doc.dimension().columnCount(); k++) {
            //qDebug() << k;
            QString hLetter((char)(64 + k));
            columns.append(doc.cellAt(hLetter + "1")->value());
        }
        allColumns.append(columns);


        return columns;
    }
    else {
        //qDebug() << xlsxName;
    }

    return {};
}

void NrpParser::setFilters(const QVariantMap &filters)
{
    if(filters.isEmpty())
        columns.append({{"name", "Позначення"}, {"metalType", "Матеріал"}, {"thickness", "Товщ."}, {"count", "К-сть"}});
    else
        columns.append(filters);
    //qDebug() << columns.last();
}

void NrpParser::setFilters(const QVariantMap &filters, int i)
{
    assert(i < columns.size());
    columns[i] = filters;
}

void NrpParser::setFilter(const QPair<QString, QVariant> &filter, int i)
{
    assert(i < columns.size());
    columns[i][filter.first] = filter.second;
}

QVariantList NrpParser::checkXlsx()
{
    QVariantList checkResult;
    if(!results.isEmpty()) {
        compareResults.clear();

        QVector<Part> allParts;
        for(auto & result : results) {
            QVector<Part> copy;
            for(auto & part : result->parts) {
                Part newpart;
                newpart.amount = part->amount;
                newpart.name = part->name;
                newpart.thickness = result->thikness.midRef(1).toInt();
                copy.push_back(newpart);
            }
            std::for_each(copy.begin(), copy.end(), [result](Part & p){if(result->plateAmount > 0)p.amount *= result->plateAmount;});
            allParts.append(copy);
        }


        while (!allParts.isEmpty()) {
            auto part = allParts.begin();
            auto partSearch = std::partition(part, allParts.end(), [&part](const Part & p) {
                return (*part).name == p.name;
            });

            auto desiredSum = std::accumulate(part, partSearch, 0, [](int & sum, Part & nextPart){return sum + nextPart.amount;});

            bool foundInExcel = false;
            for(int i = 0; i < fromExcel.size(); i++) {
                //                for(int j = 0; j < fromExcel[i].size(); i++) {
                QVector<ExcelPart> excelCopy;
                std::copy(fromExcel[i].begin(), fromExcel[i].end(), std::back_inserter(excelCopy));


                auto excelSearch = std::partition(excelCopy.begin(), excelCopy.end(), [this, &part](const ExcelPart & expart){
                    return convertToCyrillic((*part).name.split(" ")[0]) == expart.name && !expart.name.isEmpty();
                });

                if(excelSearch != excelCopy.begin()) {
                    foundInExcel = true;

                    int factSum = std::accumulate(excelCopy.begin(), excelSearch, 0, [](int & sum, const ExcelPart & expart) {return sum + expart.count;});
                    if(compareBy[i]["count"]) {
                        if(desiredSum != factSum) {
                            compareResults.append(CompareResult{.partName = (*part).name, .type = ErrorType::Mismatch, .nestCount = desiredSum, .excelSum = factSum});
                            checkResult << QVariantMap{{"text", ("ПОМИЛКА: деталь з позначенням " + (*part).name.split(" ")[0] + " не збігається з EXCEL!!! (РОЗКРОЙ - "  + QString::number(desiredSum)  + "деталей, EXCEL - " + QString::number(factSum) + ")")}, {"color", "red"}, {"type", "error"}};
                    QXlsx::Document doc((*part).file);
                    if(doc.isLoadPackage()) {
                        auto it = excelCopy.begin();
                        while(it != excelSearch) {
                            doc.cellAt(this->codeColIndex + QString::number((*it).position))->format().setPatternBackgroundColor("red");
                            doc.cellAt(this->countColIndex + QString::number((*it).position))->format().setPatternBackgroundColor("red");
                            it++;
                        }
                    }

                    //qDebug() << checkResult.last();

                }
                else {
                    compareResults.append(CompareResult{.partName = (*part).name, .type = ErrorType::Equal, .nestCount = desiredSum, .excelSum = factSum});
                    checkResult << QVariantMap{{"text", ("УСПІШНО: деталь " + (*part).name.split(" ")[0] + " збігається з EXCEL ( "  + QString::number(desiredSum)  + " == " + QString::number(factSum) + ")")}, {"color", "green"}, {"type", "success"}};
            //qDebug() << checkResult.last();
        }
    }
    if(compareBy[i]["thickness"]) {
        if(excelCopy.begin()->thickness != part->thickness) {
            compareResults.append(CompareResult{.partName = (*part).name, .type = ErrorType::Mismatch, .nestCount = desiredSum, .excelSum = factSum});
            checkResult << QVariantMap{{"text", ("ПОМИЛКА(товщина): деталь з позначенням " + (*part).name.split(" ")[0] + " не збігається з EXCEL!!! (РОЗКРОЙ - "  + QString::number((*part).thickness)  + "мм, EXCEL - " + QString::number(excelCopy.begin()->thickness) + "мм)")}, {"color", "red"}, {"type", "error"}};

}
else {
compareResults.append(CompareResult{.partName = (*part).name, .type = ErrorType::Mismatch, .nestCount = desiredSum, .excelSum = factSum});
checkResult << QVariantMap{{"text", ("УСПІШНО(товщина): деталь з позначенням " + (*part).name.split(" ")[0] + " збігається з EXCEL (РОЗКРОЙ - "  + QString::number((*part).thickness)  + "мм, EXCEL - " + QString::number(excelCopy.begin()->thickness) + "мм)")}, {"color", "green"}, {"type", "success"}};
}
}


}
}

if(!foundInExcel) {
    compareResults.append(CompareResult{.partName = (*part).name, .type = ErrorType::ExcelNotFound, .nestCount = desiredSum, .excelSum = 0});
    checkResult << QVariantMap{{"text", ("ПОМИЛКА: деталь з позначенням " + (*part).name.split(" ")[0] + " не знайдено в Excel, КІЛЬКІСТЬ НА РОЗКРОЇ - " + QString::number(desiredSum))},{ "color", "red"}, {"type", "error"}};
}
allParts.erase(part, partSearch);
}

}

return checkResult;

}

void NrpParser::setCompareBy(const QString &name, bool check)
{
    if(compareBy.size() < xlsxFileNames.size()) {
        compareBy.append(QMap<QString, bool>{});
    }
    setCompareBy(name, check, compareBy.size() - 1);
}

void NrpParser::setCompareBy(const QString &name, bool check, int i)
{
    assert(i < compareBy.size());
    compareBy[i][name] = check;
}

void NrpParser::setCompareBy(const QMap<QString, bool> &rules)
{
    if(compareBy.size() < xlsxFileNames.size()) {
        compareBy.append(QMap<QString, bool>{});
    }
    setCompareBy(rules, compareBy.size() - 1);
}

void NrpParser::setCompareBy(const QMap<QString, bool> &rules, int i)
{
    assert(i < compareBy.size());
    compareBy[i] = rules;
}

NrpParser::NrpParser(const QVector<QString> &fileName)
{
    loadFiles(fileName);
}

NrpParser::NrpParser(const QByteArray &binary)
{
    init(binary);
}

void NrpParser::load(const QString &fileName)
{
    clearAll();
    load(openFile(fileName));
}

void NrpParser::load(const QByteArray &binary)
{
    init(binary);
}

void NrpParser::loadFiles(const QVector<QString> &files)
{
    clearAll();
    for(auto & file : files) {
        init(openFile(file));
    }
}

void NrpParser::loadFolder(const QString &path)
{
    clearAll();
    QString dirCopy = path;
    if(path.startsWith("file:///"))
        dirCopy = dirCopy.mid(8);
    QDir dir(dirCopy);
    dir.setNameFilters(QStringList() << "*.nrp2" << "*.lxds");
    dir.setFilter(QDir::Files);
    QDirIterator it(dir, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        //qDebug() << "start reading"<< it.next();
        load(openFile(it.filePath()));
        //qDebug() << "end reading" << it.filePath();
    }
}

void NrpParser::parse(const QString & what, int i)
{
    if(!reader.atEnd()) {
        //        qDebug() << "start to read";
        auto tokeType = reader.readNext();
        switch(tokeType) {
        case QXmlStreamReader::StartDocument:
            //            qDebug() << "start documeent";
            parse(what, i);
            break;
        case QXmlStreamReader::StartElement:
            //            qDebug() << "start element" << reader.name();
            if(reader.name() == what) {
                //                qDebug() << "parsing " + what + " section now";
                if(what == "Parts")
                    parseParts(i);
                else if(what == "Plates")
                    parsePlates(0);
            }
            //            qDebug() << "after parsing " << reader.name();
            parse(what, i);
            break;
        case QXmlStreamReader::EndElement:
            //            qDebug() << "end element" << reader.name();
            if(reader.name() == what) {
                //                qDebug() << "End of" + what + "section";
                return;
            }
            parse(what, i);
            break;
        case QXmlStreamReader::Characters:
            parse(what, i);
            break;
        default:
            parse(what, i);
            break;
        }
    }
    //qDebug() << "end to read" <<  reader.atEnd();
}

void NrpParser::parse()
{
    for(int i = 0; i < partsData.size(); i++) {

        reader.clear();
        reader.addData(partsData[i]);
        parse("Parts", i);

    }

    for(int i = 0; i < plateData.size(); i++) {
        reader.clear();
        reader.addData(plateData[i]);
        parse("Plates", i);
    }

    parseResults();
}

QList<Plate *> NrpParser::getPlatesAsList() const
{
    QList<Plate *> parts;
    for(auto & part : handlePlateMapping) {
        parts.append(part);
    }
    return parts;
}

void NrpParser::clearAll()
{
    if(handlePartMapping.count()) qDeleteAll(handlePartMapping);
    handlePartMapping.clear();
    if(handlePlateMapping.count()) qDeleteAll(handlePlateMapping);
    handlePlateMapping.clear();
    if(results.count()) qDeleteAll(results);
    partsCatalogueMapping.clear();
    platesCatalogueMapping.clear();
    results.clear();
    if (resultStats.count()) qDeleteAll(resultStats);
    resultStats.clear();
    //    fromExcel.clear();
    fileNames.clear();
    //    xlsxFileNames.clear();
    partsData.clear();
    plateData.clear();
    resultsData.clear();
    resultCatalogueMapping.clear();
    //    compareResults.clear();
    //    columns.clear();
    clearXlsx();
}

void NrpParser::clearXlsx()
{
    fromExcel.clear();
    xlsxFileNames.clear();
    compareResults.clear();
    columns.clear();
    compareBy.clear();
}

void NrpParser::removeXlsxDocument(int index)
{
    fromExcel.remove(index);
    xlsxFileNames.remove(index);
    columns.remove(index);
    compareBy.remove(index);
    allColumns.removeAt(index);
}

QVariantList NrpParser::getAllColumns(int index)
{
    return allColumns[index];
}

QVariantMap NrpParser::getFilters(int index)
{
    return columns[index];
}

QMap<QString, bool> NrpParser::getCompareBy(int index)
{
    return compareBy[index];
}

void NrpParser::parseParts(int i)
{

    auto tokenType = reader.readNext();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestPart") {


            parseNestPart(i);
            //            parseParts();
        }
        parseParts(i);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "Parts") {
            // qDebug() << "End of parts section";
            return;
        }
        parseParts(i);
        break;
    default:
        parseParts(i);
        break;
    }
}

void NrpParser::parseNestPart(int i)
{
    auto tokenType = reader.tokenType();

    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestPart") {

            auto attributes = reader.attributes();
            Part * p = new Part;
            p->amount = attributes.value("Amount").toInt();
            p->amountUsed = attributes.value("AmountUsed").toInt();
            p->name = attributes.value("Name").toString();
            p->portionName = attributes.value("PortionName").toString();
            p->handle = attributes.value("Handle").toInt();
            p->uid = attributes.value("ID").toString();
            p->file = partsCatalogueMapping[i];

            handlePartMapping[p->name] = p;


            //            parseParts();
        }
        reader.readNext();
        parseNestPart(i);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "NestPart") {
            //            qDebug() << "End of nestpart section";
            return;
        }
        reader.readNext();
        parseNestPart(i);
        break;
    default:
        reader.readNext();
        parseNestPart(i);
        break;
    }

}

void NrpParser::parsePlates(int i)
{
    auto tokenType = reader.readNext();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestPlate") {


            parseNestPlate(i);
            //            parseParts();
        }
        parsePlates(i);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "Plates") {
            //            qDebug() << "End of plates section";
            return;
        }
        parsePlates(i);
        break;
    default:
        parsePlates(i);
        break;
    }
}

void NrpParser::parseNestPlate(int i)
{
    auto tokenType = reader.tokenType();

    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestPlate") {

            auto attributes = reader.attributes();
            Plate * p = new Plate;
            p->amount = attributes.value("Amount").toInt();
            p->amountUsed = attributes.value("AmountUsed").toInt();
            p->name = attributes.value("Name").toString();
            p->portionName = attributes.value("PortionName").toString();
            p->handle = attributes.value("Handle").toInt();
            p->uid = attributes.value("ID").toString();
            p->Class = attributes.value("Class").toString();
            p->outlineID = attributes.value("OutlineID").toInt();
            p->plateType = attributes.value("PlateType").toInt();
            p->file = platesCatalogueMapping[i];

            handlePlateMapping[p->name] = p;


            //            parseParts();
        }
        reader.readNext();
        parseNestPlate(i);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "NestPlate") {
            //            qDebug() << "End of nestplate section";
            return;
        }
        reader.readNext();
        parseNestPlate(i);
        break;
    default:
        reader.readNext();
        parseNestPlate(i);
        break;
    }
}

void NrpParser::parseResults()
{
    for(int i = 0; i < resultsData.size(); i++) {
        reader.clear();
        reader.addData(resultsData[i]);
        parseResult(i);
        reader.clear();
    }
}

void NrpParser::parseResult(int i)
{
    auto tokenType = reader.readNext();
    //    qDebug() << reader.atEnd() << reader.errorString();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestResult") {

            auto attr = reader.attributes();

            Result * r = new Result;
            r->handle = attr.value("Handle").toInt();
            r->name = attr.value("Name").toString();
            if (resultStats.contains(r->name)){
                r->cutLength = resultStats[r->name]->lengthOfCurve;
                r->pierceCount = QString::number(resultStats[r->name]->pierceCount);
            }
            r->catalogueName = resultCatalogueMapping[i];
            r->uid = attr.value("ID").toString();
            r->portionName = attr.value("PortionName").toString();
            r->thikness = attr.value("Thickness").toString();
            r->utilization = attr.value("Utilization").toDouble();
            auto searchHandle = attr.value("PlateHandle").toInt();
            auto searchResult = std::find_if(handlePlateMapping.begin(), handlePlateMapping.end(), [&searchHandle, &r](const Plate * p){
                return p->handle == searchHandle || p->file == r->catalogueName;
            });

            r->plate = *searchResult;
            r->plateGap = attr.value("PlateGap").toInt();
            r->plateAmount = attr.value("PlateAmount").toInt();
            parseExtMinMax(r);
            //            parseShapes(r);
            parseNestedParts(r);

            results.push_back(r);
        }
        parseResult(i);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "NestResult") {
            //            qDebug() << "End of nestresult section";
            return;
        }
        parseResult(i);
        break;
    default:
        parseResult(i);
        break;
    }
}

void NrpParser::parseResultStats(QString data)
{
    reader.clear();
    reader.addData(data);

    QString currentName;
    while (!reader.hasError()){
        auto tokenType = reader.readNext();
        switch (tokenType) {
        case QXmlStreamReader::StartElement:{
            auto attr = reader.attributes();
            if (reader.name() == "NestResult"){
                currentName = attr.value("Name").toString();
            }else if (reader.name() == "Statistics"){
                attr = reader.attributes();
                auto r = new ResultStat;
                r->lengthOfCurve = QString::number(round(attr.value("LengthOfCurve").toDouble() * 100) / 100.00, 'f', 2);
                r->lengthOfMove = QString::number(round(attr.value("LengthOfMove").toDouble() * 100) / 100.00, 'f', 2);
                r->pierceCount = attr.value("PierceCount").toInt();
                r->cutTime = attr.value("CutTime").toString();
                r->moveTime = attr.value("MoveTime").toString();
                r->pierceTime = attr.value("PierceTime").toString();
                r->delayTime = attr.value("DelayTime").toString();
                r->totalTime = attr.value("TotalTime").toString();
                r->name = currentName;
                r->id = attr.value("ID").toString();
                resultStats.insert(r->name, r);
            }
            break;
        }
        case QXmlStreamReader::EndElement:
            break;
        default: break;
        }
    }
}

void NrpParser::parseNestedPart(Result *r)
{
    auto tokenType = reader.tokenType();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestedParts") {
            auto attr = reader.attributes();
            int partHandle = attr.value("RefNestPartHandle").toInt();
            auto part = std::find_if(r->parts.begin(), r->parts.end(), [partHandle](const Part * part){
                return part->handle == partHandle;
            });
            auto globalPart = std::find_if(handlePartMapping.begin(), handlePartMapping.end(), [&r, &partHandle](const Part * p){
                auto result = p->handle == partHandle && r->catalogueName == p->file;
                return result;
            });
            if(globalPart != handlePartMapping.end()) {
                if(part != r->parts.end()) {
                    (*part)->amount++;
                    (*part)->amountUsed++;
                }
                else {
                    auto newPart = new Part;
                    newPart->amount = 1;
                    newPart->amountUsed = 1;
                    newPart->handle = (*globalPart)->handle;
                    newPart->name = (*globalPart)->name;
                    newPart->portionName = (*globalPart)->portionName;
                    newPart->uid = (*globalPart)->uid;
                    r->parts.push_back(newPart);
                }
            }
            else {
                auto pp = r->parts.last();
                pp->amount++;
                pp->amountUsed++;
                //                auto newPart = new Part;
                //                newPart->handle = partHandle;
                //                newPart->amount = 1;
                //                newPart->amountUsed = 1;
                //                newPart->done = 0;
                //                newPart->name = "Unknown" + QString::number(partHandle);
                //                handlePartMapping[newPart->name] = newPart;
                //                r->parts.push_back(newPart);
            }
        }
        reader.readNext();
        parseNestedPart(r);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "NestedParts") {
            //            qDebug() << "End of shapes section";
            return;
        }
        reader.readNext();
        parseNestedPart(r);
        break;
    default:
        reader.readNext();
        parseNestedPart(r);
        break;
    }
}

QString &NrpParser::convertToCyrillic(QString &name)
{
    auto code = name.left(3);
    auto search = codeMapping.find(code);
    if(search != codeMapping.end())
        return name.replace(code, *search);
    else return name;
}

//void NrpParser::parseShape(Result *r)
//{

//    auto tokenType = reader.tokenType();
////    if(tokenType == QXmlStreamReader::StartElement && tokenType)
//    switch (tokenType) {
//    case QXmlStreamReader::StartElement:
//        qDebug() << "ParseShape: start element" << reader.name();
//        if(reader.name() == "Shape") {
//            auto attr = reader.attributes();
//            Shape s;
//            s.Class = attr.value("Class").toString();
//            s.handle = attr.value("Handle").toInt();
//            parseNestShapePart(&s);
//            parseBlockName(&s);
//            r->shapes.push_back(s);
//        }
//        tokenType = reader.readNext();
//        parseShape(r);
//        break;
//    case QXmlStreamReader::EndElement:
//        qDebug() << "ParseShape: end element" << reader.name();
//        if(reader.name() == "Shape") {
//            qDebug() << "ParseShape: End of shape section";
//            return;
//        }
//        tokenType = reader.readNext();
//        parseShape(r);
//        break;
//    default:
//        reader.readNext();
//        parseShape(r);
//        break;
//    }
//}

//void NrpParser::parseNestShapePart(Shape *s)
//{
//    while(reader.readNext() != QXmlStreamReader::StartElement);
//    if(reader.name() == "BlockName") {/*qDebug() << reader.name();*/ return;};
//    auto tokenType = reader.tokenType();
//    switch (tokenType) {
//    case QXmlStreamReader::StartElement:
////        qDebug() << "start element" << reader.name();
//        if(reader.name() == "NestedPart" || reader.name() == "NestedParts") {
//            if(reader.name() != "NestedParts") while(reader.readNext() != QXmlStreamReader::StartElement);
//            auto attr = reader.attributes();
//            s->part.push_back(this->handlePartMapping[attr.value("RefNestPartHandle").toInt()]);
//        }
//        else return;
//        parseNestShapePart(s);
//        break;
//    case QXmlStreamReader::EndElement:
////        qDebug() << "end element" << reader.name();
//        if(reader.name() == "NestedPart") {
////            qDebug() << "End of nestedParts section";
//            return;
//        }
//        parseNestShapePart(s);
//        break;
//    default:
//        parseNestShapePart(s);
//        break;
//    }
//}

void NrpParser::parseBlockName(Shape *s)
{
    auto tokenType = reader.tokenType();
    if(reader.name() != "BlockName") return;
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "BlockName") {
            s->blockName = reader.readElementText();
            //            r->extMin = {attr.value("X").toDouble(), attr.value("Y").toDouble()};
        }
        parseBlockName(s);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "BlockName" or reader.name() == "Shape") {
            //            qDebug() << "End of blockname section";
            return;
        }
        parseBlockName(s);
        break;
    default:
        parseBlockName(s);
        break;
    }
}

void NrpParser::parseNestedParts(Result *r)
{
    auto tokenType = reader.readNext();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "NestedParts" || reader.name() == "NestedPart") {
            parseNestedPart(r);
        }
        parseNestedParts(r);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "Shapes") {
            //            qDebug() << "End of nestedparts section";
            return;
        }
        parseNestedParts(r);
        break;
    default:
        parseNestedParts(r);
        break;
    }
}

void NrpParser::parseExtMinMax(Result *r)
{
    auto tokenType = reader.readNext();
    switch (tokenType) {
    case QXmlStreamReader::StartElement:
        //        qDebug() << "start element" << reader.name();
        if(reader.name() == "ExtMin") {
            auto attr = reader.attributes();
            r->extMin = {attr.value("X").toDouble(), attr.value("Y").toDouble()};
        }
        else if (reader.name() == "ExtMax") {
            auto attr = reader.attributes();
            r->extMax = {attr.value("X").toDouble(), attr.value("Y").toDouble()};
        }
        parseExtMinMax(r);
        break;
    case QXmlStreamReader::EndElement:
        //        qDebug() << "end element" << reader.name();
        if(reader.name() == "ExtMax") {
            //            qDebug() << "End of ext(min, max) section";
            return;
        }
        parseExtMinMax(r);
        break;
    default:
        parseExtMinMax(r);
        break;
    }
}

void NrpParser::printResultList()
{
    printf("%10s %10s %40s %20s %10s %10s\n", "List", "ListAmount",  "ListNo", "Part", "PartAmount", "ListUtilization");
    for(auto & result: results) {
        for(auto &part : result->parts) {
            //            for(auto & part : shape.part)
            if(part)
                printf("%10s %10d %40s %20s %10d %10f\n",
                       result->name.toStdString().c_str(),
                       result->plateAmount,
                       result->uid.toStdString().c_str(),
                       part->name.toStdString().c_str(),
                       part->amount,
                       result->utilization);
        }
    }

}

void NrpParser::saveToFile(QString fileName, QString format = "")
{

}

QList<Part *> NrpParser::getPartsAsList() const
{
    QList<Part *> parts;
    for(auto & part : handlePartMapping) {
        parts.append(part);
    }
    return parts;
}
}

//void SplitToVector(QVector<QString> &v, const QString & dlm, const QString & src){
//    QString::size_type p, start=0, len=src.length();
//    v.clear();
//    start = src.find_first_not_of(dlm);
//    p = src.find_first_of(dlm, start);
//    while(p != string::npos){
//        v.push_back(src.substr(start, p-start));
//        start = src.find_first_not_of(dlm, p);
//        p = src.find_first_of(dlm, start);
//    }
//    if(len>start)//rest
//        v.push_back(src.substr(start, len - start));
//}

