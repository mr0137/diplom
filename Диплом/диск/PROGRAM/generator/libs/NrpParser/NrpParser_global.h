#ifndef XMLPARSER_GLOBAL_H
#define XMLPARSER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NRPPARSER_LIBRARY)
#  define XMLPARSER_EXPORT Q_DECL_EXPORT
#else
#  define XMLPARSER_EXPORT Q_DECL_IMPORT
#endif

#endif // XMLPARSER_GLOBAL_H
