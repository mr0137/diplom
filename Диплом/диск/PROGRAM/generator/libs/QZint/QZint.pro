QT += gui
QT += core
TEMPLATE = lib
DEFINES += QZINT_LIBRARY NO_PNG NO_QR
CONFIG += shared dll
CONFIG += c++17

include($$PWD/../destidir.pri)

SOURCES += \
    2of5.cpp \
    auspost.cpp \
    aztec.cpp \
    code.cpp \
    code1.cpp \
    code128.cpp \
    code128ext.cpp \
    code16k.cpp \
    code49.cpp \
    common.cpp \
    composite.cpp \
    dllversion.cpp \
    dmatrix.cpp \
    gridmtx.cpp \
    gs1.cpp \
    imail.cpp \
    large.cpp \
    library.cpp \
    maxicode.cpp \
    medical.cpp \
    pdf417.cpp \
    plessey.cpp \
    png.cpp \
    postal.cpp \
    ps.cpp \
    qr.cpp \
    qzint.cpp \
    reedsol.cpp \
    render.cpp \
    rss.cpp \
    smaz.cpp \
    svg.cpp \
    telepen.cpp \
    upcean.cpp

HEADERS += \
    QZint_global.h \
    aztec.h \
    code1.h \
    code49.h \
    common.h \
    composite.h \
    dmatrix.h \
    font.h \
    gb2312.h \
    gridmtx.h \
    gs1.h \
    large.h \
    maxicode.h \
    maxipng.h \
    pdf417.h \
    qr.h \
    qzint.h \
    reedsol.h \
    rss.h \
    sjis.h \
    smaz.h \
    zint.h
