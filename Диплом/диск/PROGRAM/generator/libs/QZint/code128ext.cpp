/* code128.c - Handles Code 128 and derivatives */

/*
    libzint - the open source barcode library
    Copyright (C) 2008 Robin Stuart <robin@zint.org.uk>
    Bugfixes thanks to Christian Sakowski and BogDan Vatra

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include "common.h"
#include "gs1.h"
#include <QDebug>

#define TRUE 1
#define FALSE 0
#define SHIFTA 90
#define LATCHA 91
#define SHIFTB 92
#define LATCHB 93
#define SHIFTC 94
#define LATCHC 95
#define AORB 96
#define ABORC 97

#define DPDSET	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ*"

int listext[2][170];

/* Code 128 tables checked against ISO/IEC 15417:2007 */
//або чи не був пропущеним якийсь рядок

const char *symbols[95] ={
    " ",
    "!",
    "\"",
    "#",
    "$",
    "%",
    "&",
    "'",
    "(",
    ")",
    "*",
    "+",
    ",",
    "-",
    ".",
    "/",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    ":",
    ";",
    "<",
    "=",
    ">",
    "?",
    "@",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "[",
    "\\",
    "]",
    "^",
    "_",
    "`",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "{",
    "|",
    "}",
    "~"
};

const char *C128TableEXT[107] = {
    "2122222",  // space
    "2221222",  // !
    "2222212",  // "
    "1212232",  // #
    "1213222",  // $
    "1312222",  // %
    "1222132",  // &
    "1223122",  // '
    "1322122",  // (
    "2212132",  // )
    "2213122",  // *
    "2312122",  // +
    "1122322",  // ,
    "1221322",  // -
    "1222312",  // .
    "1132222",  // /
    "1231222",  // 0
    "1232212",  // 1
    "2232112",  // 2
    "2211322",  // 3
    "2212312",  // 4
    "2132122",  // 5
    "2231122",  // 6
    "3121312",  // 7
    "3112222",  // 8
    "3211222",  // 9
    "3212212",  // :
    "3122122",  // ;
    "3221122",  // <
    "3222112",  // =
    "2121232",  // >
    "2123212",  // ?
    "2321212",  // @
    "1113232",  // A
    "1311232",  // B
    "1313212",  // C
    "1123132",  // D
    "1321132",  // E
    "1323112",  // F
    "2113132",  // G
    "2311132",  // H
    "2313112",  // I
    "1121332",  // J
    "1123312",  // K
    "1321312",  // L
    "1131232",  // M
    "1133212",  // N
    "1331212",  // O
    "3131212",  // P
    "2113312",  // Q
    "2311312",  // R
    "2131132",  // S
    "2133112",  // T
    "2131312",  // U
    "3111232",  // V
    "3113212",  // W
    "3311212",  // X
    "3121132",  // Y
    "3123112",  // Z
    "3321112",  // [
    "3141112",  /* \ */
    "2214112",  // ]
    "4311112",  // ^
    "1112242",  // _
    "1114222",  // `
    "1211242",  // a
    "1214212",  // b
    "1411222",  // c
    "1412212",  // d
    "1122142",  // e
    "1124122",  // f
    "1221142",  // g
    "1224112",  // h
    "1421122",  // i
    "1422112",  // j
    "2412112",  // k
    "2211142",  // l
    "4131112",  // m
    "2411122",  // n
    "1341112",  // o
    "1112422",  // p
    "1211422",  // q
    "1212412",  // r
    "1142122",  // s
    "1241122",  // t
    "1242112",  // u
    "4112122",  // v
    "4211122",  // w
    "4212112",  // x
    "2121412",  // y
    "2141212",  // z
    "4121212",  // {
    "1111432",  // |
    "1113412",  // }
    "1311412",  // ~
    "1141132",  // DEL Ã
    "1143112",  //
    "4111132",  //
    "4113112",  //
    "1131412",  //
    "1141312",  //
    "3111412",  //
    "4111312",  //
    "2114122",  //
    "2112142",  //
    "2112322",  //
    "2331121"}; //
/* Code 128 character encodation - Table 1 */

int parunmoddext(uint8_t llyth)
{
    int modd = SHIFTB;

    if (llyth <= 31)
        modd = SHIFTA;
    else if ((llyth >= 48) && (llyth <= 57))
        modd = ABORC;
    else if (llyth <= 95)
        modd = AORB;
    else if (llyth <= 127)
        modd = SHIFTB;
    else if (llyth <= 159)
        modd = SHIFTA;
    else if (llyth <= 223)
        modd = AORB;

    return modd;
}

/**
 * bring together same type blocks
 */
void grwpext(int *indexlistexte)
{
    if (*indexlistexte <= 1)
        return;

    for (int i = 1; i < *indexlistexte; i++) {
        if (listext[1][i - 1] == listext[1][i]) {
            /* bring together */
            listext[0][i - 1] = listext[0][i - 1] + listext[0][i];

            /* decreace the listext */
            for (int j = i + 1; j < *(indexlistexte); j++) {
                listext[0][j - 1] = listext[0][j];
                listext[1][j - 1] = listext[1][j];
            }
            (*indexlistexte)--;
            i--;
        }
    }
}

/**
 * Implements rules from ISO 15417 Annex E
 */
void dxsmoothext(int *indexlistexte)
{
    int current, length, last, next;

    for (int i = 0; i < *indexlistexte; i++) {
        current = listext[1][i];
        length = listext[0][i];

        if (i != 0)
            last = listext[1][i - 1];
        else
            last = FALSE;

        if (i != *indexlistexte - 1)
            next = listext[1][i + 1];
        else
            next = FALSE;

        if (i == 0) {
            /* first block */
            if (*indexlistexte == 1 && length == 2 && current == ABORC)
                /* Rule 1a */
                listext[1][i] = LATCHC;

            if (current == ABORC) {
                if (length >= 4)
                    /* Rule 1b */
                    listext[1][i] = LATCHC;
                else
                    listext[1][i] = current = AORB;
            }

            if (current == SHIFTA)
                /* Rule 1c */
                listext[1][i] = LATCHA;
            if (current == AORB && next == SHIFTA)
                /* Rule 1c */
                listext[1][i] = current = LATCHA;
            if (current == AORB)
                /* Rule 1d */
                listext[1][i] = LATCHB;
        } else {
            if (current == ABORC && length >= 4)
                /* Rule 3 */
                listext[1][i] = current = LATCHC;
            if (current == ABORC)
                listext[1][i] = current = AORB;
            if (current == AORB && last == LATCHA)
                listext[1][i] = current = LATCHA;
            if (current == AORB && last == LATCHB)
                listext[1][i] = current = LATCHB;
            if (current == AORB && next == SHIFTA)
                listext[1][i] = current = LATCHA;
            if (current == AORB && next == SHIFTB)
                listext[1][i] = current = LATCHB;
            if (current == AORB)
                listext[1][i] = current = LATCHB;
            if (current == SHIFTA && length > 1)
                /* Rule 4 */
                listext[1][i] = current = LATCHA;
            if (current == SHIFTB && length > 1)
                /* Rule 5 */
                listext[1][i] = current = LATCHB;
            if (current == SHIFTA && last == LATCHA)
                listext[1][i] = current = LATCHA;
            if (current == SHIFTB && last == LATCHB)
                listext[1][i] = current = LATCHB;
            if (current == SHIFTA && last == LATCHC)
                listext[1][i] = current = LATCHA;
            if (current == SHIFTB && last == LATCHC)
                listext[1][i] = current = LATCHB;
        } /* Rule 2 is implimented elsewhere, Rule 6 is implied */
    }
    grwpext(indexlistexte);
}

/**
 * Translate Code 128 Set A characters into barcodes.
 * This set handles all control characters NULL to US.
 */
void c128_set_a_ext(uint8_t source, char dest[], int values[], int *bar_chars)
{
    /* limit the range to 0-127 */
    source &= 127;

    if (source < 32)
        source += 64;
    else
        source -= 32;

    concat(dest, C128TableEXT[source]);
    values[(*bar_chars)++] = source;
}

/**
 * Translate Code 128 Set B characters into barcodes.
 * This set handles all characters which are not part of long numbers and not
 * control characters.
 */
void c128_set_b_ext(uint8_t source, char dest[], int values[], int *bar_chars)
{
    /* limit the range to 0-127 */
    source &= 127;
    source -= 32;

    concat(dest, C128TableEXT[source]);
    values[(*bar_chars)++] = source;
}

/**
 * Translate Code 128 Set C characters into barcodes.
 * This set handles numbers in a compressed form.
 */
void c128_set_c_ext(uint8_t source_a, uint8_t source_b, char dest[], int values[], int *bar_chars)
{
    int weight;

    weight = 10 * ctoi(source_a) + ctoi(source_b);
    concat(dest, C128TableEXT[weight]);
    values[(*bar_chars)++] = weight;
}

/**
 * Handle Code 128 and NVE-18.
 */
int code_128ext(struct zint_symbol *symbol, uint8_t source[], int length){

    int error_number = 0;
    std::string str;
    int symbol_per_line = -1;
    int max_rows = 50;

    std::function times_cal = [&symbol_per_line](int length){
        return (length%symbol_per_line==0 ? length/symbol_per_line : length/symbol_per_line + 1);
    };

    std::function length_per_line_cal = [&length, &str](){
        while (1){
            for (int i = 16; i >= 10; i--){
                if (length%i==0){
                    return i;
                }
            }
            length++;
            str+= "~";
        }
        return -1;
    };

    for (int i = 0; i<length; i++) {
        str += (char)source[i];
    }
    //lookng for correct len
    symbol_per_line = length_per_line_cal() + 2;

    int times = times_cal(str.length());
    int j = 0, t = 0;
    //adding row metadata(row number)
    do{
        //qDebug() << i << t << str.length() << times << times-t;
        if ((size_t)j >= str.length()) break;
        //int row = times_cal(str.length()+2)-t;
        str.insert(j + (bool)t, "~`");
        j+=symbol_per_line - !(bool)t++;
        times = times_cal(str.length());
    }while (t < times);

    //qDebug() << "i" << i << "length" << str.length();
    // while(str.length() < (size_t)times*symbol_per_line) str.append("");

    //std::string sbstr = str.substr((times-1)*symbol_per_line, symbol_per_line);
    //if (sbstr[i] == '|') str.erase(times*symbol_per_line-1, symbol_per_line);
    int pos = str.find("~`");
    t++;
    int rows = t-1, r = 0;
    do{
        str.erase(pos,2);
        t--;
        str.insert(pos,  QString(symbols[r]).toLatin1() + QString(symbols[t]).toLatin1()/*(r>=10?"":"0") + QString::number(r).toLatin1() + (t>=10?"":"0") + QString::number(t).toLatin1()*/);
        r++;
        pos = str.find("~`");
    }while(pos != -1);
    //qDebug() << str.c_str();
    source = reinterpret_cast<uint8_t*>(&str[0]);
    //qDebug() << "=================" << times;
    for (int kj = 0; kj < times; kj++){
        //qDebug() << kj << str.substr(kj*symbol_per_line, symbol_per_line).c_str() << str.substr(kj*symbol_per_line, symbol_per_line).length();
        int values[270] = { 0 }, bar_characters, read, total_sum = 0;
        int indexchaine, indexlistexte, sourcelen, f_state;
        char set[270] = { ' ' }, fset[270] = { ' ' }, mode, last_set, current_set = ' ';
        float glyph_count;
        char dest[1000];

        strcpy(dest, "");
        sourcelen = symbol_per_line;

        bar_characters = 0;
        f_state = 0;

        if(sourcelen > 260 || times >= max_rows) {
            /* This only blocks rediculously long input - the actual length of the
           resulting barcode depends on the type of data, so this is trapped later */
            strcpy(symbol->errtxt, "Input too long");
            return ZERROR_TOO_LONG;
        }

        /* Detect extended ASCII characters */
        for(int i = 0; i < sourcelen; i++) {
            if(source[i+kj*symbol_per_line] >= 128)
                fset[i] = 'f';
        }
        fset[sourcelen] = '\0';

        indexlistexte = 0;
        indexchaine = 0;
        mode = parunmoddext(source[indexchaine+kj*symbol_per_line]);

        memset(listext[0], 0, 270 * sizeof(listext[0][0]));

        do {
            listext[1][indexlistexte] = mode;
            while ((listext[1][indexlistexte] == mode) && (indexchaine < sourcelen)) {
                listext[0][indexlistexte]++;
                indexchaine++;
                mode = parunmoddext(source[indexchaine+kj*symbol_per_line]);
                if((symbol->symbology == BARCODE_CODE128B) && (mode == ABORC)) {
                    mode = AORB;
                }
            }
            indexlistexte++;
        } while (indexchaine < sourcelen);

        dxsmoothext(&indexlistexte);

        /* Resolve odd length LATCHC blocks */
        if((listext[1][0] == LATCHC) && (listext[0][0] & 1)) {
            /* Rule 2 */
            listext[0][1]++;
            listext[0][0]--;
            if(indexlistexte == 1) {
                listext[0][1] = 1;
                listext[1][1] = LATCHB;
                indexlistexte = 2;
            }
        }
        if (indexlistexte > 1) {
            for(int i = 1; i < indexlistexte; i++) {
                if((listext[1][i] == LATCHC) && (listext[0][i] & 1)) {
                    /* Rule 3b */
                    listext[0][i - 1]++;
                    listext[0][i]--;
                }
            }
        }

        /* Put set data into set[] */

        read = 0;
        for(int i = 0; i < indexlistexte; i++) {
            for(int j = 0; j < listext[0][i]; j++) {
                set[read] = 'B';
                read++;
            }
        }

        /* Now we can calculate how long the barcode is going to be - and stop it from
       being too long */
        last_set = ' ';
        glyph_count = 0.0;
        for (int i = 0; i < sourcelen; i++) {
            if((set[i] == 'a') || (set[i] == 'b')) {
                glyph_count = glyph_count + 1.0;
            }
            if((fset[i] == 'f') || (fset[i] == 'n')) {
                glyph_count = glyph_count + 1.0;
            }
            if(((set[i] == 'A') || (set[i] == 'B')) || (set[i] == 'C')) {
                if(set[i] != last_set) {
                    last_set = set[i];
                    glyph_count = glyph_count + 1.0;
                }
            }
            if(i == 0) {
                if(fset[i] == 'F') {
                    glyph_count = glyph_count + 2.0;
                }
            } else {
                if((fset[i] == 'F') && (fset[i - 1] != 'F')) {
                    glyph_count = glyph_count + 2.0;
                }
                if((fset[i] != 'F') && (fset[i - 1] == 'F')) {
                    glyph_count = glyph_count + 2.0;
                }
            }

            if(set[i] == 'C') {
                glyph_count = glyph_count + 0.5;
            } else {
                glyph_count = glyph_count + 1.0;
            }
        }
        if(glyph_count > 80.0) {
            strcpy(symbol->errtxt, "Input too long");
            return ZERROR_TOO_LONG;
        }

        /* So now we know what start character to use - we can get on with it! */
        if(symbol->output_options & READER_INIT) {
            /* Reader Initialisation mode */
            switch(set[0]) {
            case 'A': /* Start A */
                concat(dest, C128TableEXT[103]);
                values[0] = 103;
                current_set = 'A';
                concat(dest, C128TableEXT[96]); /* FNC3 */
                values[1] = 96;
                bar_characters++;
                break;
            case 'B': /* Start B */
                concat(dest, C128TableEXT[104]);
                values[0] = 104;
                current_set = 'B';
                concat(dest, C128TableEXT[96]); /* FNC3 */
                values[1] = 96;
                bar_characters++;
                break;
            case 'C': /* Start C */
                concat(dest, C128TableEXT[104]); /* Start B */
                values[0] = 105;
                concat(dest, C128TableEXT[96]); /* FNC3 */
                values[1] = 96;
                concat(dest, C128TableEXT[99]); /* Code C */
                values[2] = 99;
                bar_characters += 2;
                current_set = 'C';
                break;
            }
        } else {
            /* Normal mode */
            switch(set[0]) {
            case 'A': /* Start A */
                concat(dest, C128TableEXT[103]);
                values[0] = 103;
                current_set = 'A';
                break;
            case 'B': /* Start B */
                concat(dest, C128TableEXT[104]);
                values[0] = 104;
                current_set = 'B';
                break;
            case 'C': /* Start C */
                concat(dest, C128TableEXT[105]);
                values[0] = 105;
                current_set = 'C';
                break;
            }
        }
        bar_characters++;
        last_set = set[0];

        if(fset[0] == 'F') {
            switch(current_set) {
            case 'A':
                concat(dest, C128TableEXT[101]);
                concat(dest, C128TableEXT[101]);
                values[bar_characters] = 101;
                values[bar_characters + 1] = 101;
                break;
            case 'B':
                concat(dest, C128TableEXT[100]);
                concat(dest, C128TableEXT[100]);
                values[bar_characters] = 100;
                values[bar_characters + 1] = 100;
                break;
            }
            bar_characters += 2;
            f_state = 1;
        }

        /* Encode the data */
        read = 0;
        do {
            if((read != 0) && (set[read] != current_set))
            { /* Latch different code set */
                switch(set[read])
                {
                case 'A': concat(dest, C128TableEXT[101]);
                    values[bar_characters] = 101;
                    bar_characters++;
                    current_set = 'A';
                    break;
                case 'B': concat(dest, C128TableEXT[100]);
                    values[bar_characters] = 100;
                    bar_characters++;
                    current_set = 'B';
                    break;
                case 'C': concat(dest, C128TableEXT[99]);
                    values[bar_characters] = 99;
                    bar_characters++;
                    current_set = 'C';
                    break;
                }
            }

            if(read != 0) {
                if((fset[read] == 'F') && (f_state == 0)) {
                    /* Latch beginning of extended mode */
                    switch(current_set) {
                    case 'A':
                        concat(dest, C128TableEXT[101]);
                        concat(dest, C128TableEXT[101]);
                        values[bar_characters] = 101;
                        values[bar_characters + 1] = 101;
                        break;
                    case 'B':
                        concat(dest, C128TableEXT[100]);
                        concat(dest, C128TableEXT[100]);
                        values[bar_characters] = 100;
                        values[bar_characters + 1] = 100;
                        break;
                    }
                    bar_characters += 2;
                    f_state = 1;
                }
                if((fset[read] == ' ') && (f_state == 1)) {
                    /* Latch end of extended mode */
                    switch(current_set) {
                    case 'A':
                        concat(dest, C128TableEXT[101]);
                        concat(dest, C128TableEXT[101]);
                        values[bar_characters] = 101;
                        values[bar_characters + 1] = 101;
                        break;
                    case 'B':
                        concat(dest, C128TableEXT[100]);
                        concat(dest, C128TableEXT[100]);
                        values[bar_characters] = 100;
                        values[bar_characters + 1] = 100;
                        break;
                    }
                    bar_characters += 2;
                    f_state = 0;
                }
            }

            if((fset[read] == 'f') || (fset[read] == 'n')) {
                /* Shift to or from extended mode */
                switch(current_set) {
                case 'A':
                    concat(dest, C128TableEXT[101]); /* FNC 4 */
                    values[bar_characters] = 101;
                    break;
                case 'B':
                    concat(dest, C128TableEXT[100]); /* FNC 4 */
                    values[bar_characters] = 100;
                    break;
                }
                bar_characters++;
            }

            if((set[read] == 'a') || (set[read] == 'b')) {
                /* Insert shift character */
                concat(dest, C128TableEXT[98]);
                values[bar_characters] = 98;
                bar_characters++;
            }

            switch(set[read])
            { /* Encode data characters */
            case 'a':
            case 'A': c128_set_a_ext(source[read+kj*symbol_per_line], dest, values, &bar_characters);
                read++;
                break;
            case 'b':
            case 'B': c128_set_b_ext(source[read+kj*symbol_per_line], dest, values, &bar_characters);
                read++;
                break;
            case 'C': c128_set_c_ext(source[read+kj*symbol_per_line], source[read+kj*symbol_per_line + 1], dest, values, &bar_characters);
                read += 2;
                break;
            }

        } while (read < sourcelen);

        /* check digit calculation */
        /*total_sum = 0;
        for(int i = 0; i < bar_characters; i++) {
            char c = values[i];
            printf("%c", c);
            printf("\n");
        }*/

        for (int i = 0; i < bar_characters; i++) {
            if(i > 0)
                values[i] *= i;
            total_sum += values[i];
        }
        concat(dest, C128TableEXT[total_sum % 103]);

        /* Stop character */
        concat(dest, C128TableEXT[106]);
        expand(symbol, dest);
    }
    return error_number;
}
