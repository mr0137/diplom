TEMPLATE     = lib
TARGET       = QEMF
QT += widgets
QT += core
QT += printsupport

include($$PWD/../destidir.pri)

CONFIG  += dll shared
DEFINES += QEMF_DLL QEMF_DLL_BUILD

INCLUDEPATH += .

HEADERS = Bitmap.h\
	BitmapHeader.h\
	EmfEnums.h\
	EmfHeader.h\
	EmfLogger.h\
	EmfObjects.h\
	EmfOutput.h\
	EmfParser.h\
	EmfRecords.h\
	QEmfRenderer.h

SOURCES = Bitmap.cpp\
	BitmapHeader.cpp\
	EmfHeader.cpp\
	EmfLogger.cpp\
	EmfObjects.cpp\
	EmfOutput.cpp\
	EmfParser.cpp\
	EmfRecords.cpp\
        QEmfRenderer.cpp
