QT -= gui
QT += core
QT += quick
QT += sql
QT += concurrent
TEMPLATE = lib
CONFIG += shared dll
CONFIG += c++17
DEFINES += EWAREHOUSEBASE_LIBRARY
CONFIG(release, debug|release): DEFINES += DEPLOY_MODE
include($$PWD/../destidir.pri)

SOURCES += \
    blockdata.cpp \
    calc.cpp \
    crcchecksum.cpp \
    databaseservice.cpp \
    fileservice.cpp \
    quicksort.cpp

HEADERS += \
    blockdata.h \
    calc.h \
    crcchecksum.h \
    databaseservice.h \
    ewarehouseBase_global.h \
    fileservice.h \
    interfaces/ipageplugin.h \
    pluginLoader.h \
    quicksort.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../bin/libs/ -lTableRenderBase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../TableRenderBase/debug/ -lTableRenderBase
else:unix: LIBS += -L$$OUT_PWD/../TableRenderBase/ -lTableRenderBase

INCLUDEPATH += $$PWD/../TableRenderBase
DEPENDPATH += $$PWD/../TableRenderBase

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../klibcorelite/release/ -lklibcorelite
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../klibcorelite/debug/ -lklibcorelite
else:unix: LIBS += -L$$OUT_PWD/../klibcorelite/ -lklibcorelite

INCLUDEPATH += $$PWD/../klibcorelite
DEPENDPATH += $$PWD/../klibcorelite
