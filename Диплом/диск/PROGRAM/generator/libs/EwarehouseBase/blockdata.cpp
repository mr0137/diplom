#include "blockdata.h"

BlockData::BlockData(QObject *parent) : QObject(parent)
{

}

void BlockData::init(QVariantMap params)
{
    Q_UNUSED(params)
}

QString BlockData::listingName() const
{
    return m_listingName;
}

void BlockData::setListingName(const QString &newListingName)
{
    if (m_listingName == newListingName)
        return;
    m_listingName = newListingName;
    emit listingNameChanged();
}

bool BlockData::isEdited() const
{
    return m_isEdited;
}

void BlockData::setIsEdited(bool newIsEdited)
{
    if (m_isEdited == newIsEdited)
        return;
    m_isEdited = newIsEdited;
    emit isEditedChanged();
}

bool BlockData::isCached() const
{
    return m_isCached;
}

void BlockData::setIsCached(bool newIsCached)
{
    if (m_isCached == newIsCached)
        return;
    m_isCached = newIsCached;
    emit isCachedChanged();
}

