#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <stdlib.h>
#include <stdio.h>
#include <functional>

namespace QuickSort{

template <class Obj>
void swap(Obj* a, Obj* b)
{
    Obj t = *a;
    *a = *b;
    *b = t;
}

template <class Container, class Obj>
int partition (Container arr, int low, int high, std::function<bool(Obj& elem1, Obj &elem2)> func)
{
    int pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (func(arr[j], pivot))
        {
            i++;    // increment index of smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

template <class Container, class Obj>
void quickSort(Container arr, int low, int high, std::function<bool(Obj& elem1, Obj &elem2)> func)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1, func);
        quickSort(arr, pi + 1, high, func);
    }
}
template <class Container, class Obj>
void sort(Container &list, std::function<bool(Obj& elem1, Obj &elem2)> func){
    quickSort(list, 0, list.length()-1, func);
}
}


#endif // QUICKSORT_H
