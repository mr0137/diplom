#ifndef DATABASESERVICE_H
#define DATABASESERVICE_H

#include <QObject>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDatabase>
#include <QThreadStorage>
#include <QString>
#include "ewarehouseBase_global.h"

class EWAREHOUSEBASE_EXPORT DatabaseService: public QObject
{
    Q_OBJECT
public:
    DatabaseService(QObject * parent = nullptr);
    QString getLastProduceOrder(QVariantMap &map, QString strQuery);
    bool isNumberFree(QString strQuery);

    static QSqlDatabase createConnection(QString connectionName);
    static void closeConnection(QString connectionName);
    static void closeAllConnections(QString connectionName);
    static QSqlDatabase createLocalConnection();
    static bool isDatabaseAvailable();
    static QString currentConnection();
private:
    void connectDB();
    static void init(QSqlDatabase db);
};

#endif // DATABASESERVICE_H
