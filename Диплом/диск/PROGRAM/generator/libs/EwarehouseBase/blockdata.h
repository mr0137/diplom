#ifndef BLOCKDATA_H
#define BLOCKDATA_H

#include <QObject>
#include <QVariantList>
#include <QtQml>
#include "ewarehouseBase_global.h"

class ITableDataProvider;
class EWAREHOUSEBASE_EXPORT BlockData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isEdited READ isEdited WRITE setIsEdited NOTIFY isEditedChanged)
    Q_PROPERTY(bool isCached READ isCached WRITE setIsCached NOTIFY isCachedChanged)
    Q_PROPERTY(QString listingName READ listingName WRITE setListingName NOTIFY listingNameChanged)
public:
    explicit BlockData(QObject *parent = nullptr);
    virtual void init(QVariantMap params);

    QString listingName() const;
    bool isEdited() const;
    void setIsEdited(bool newIsEdited);
    bool isCached() const;
    void setIsCached(bool newIsCached);

protected:
    void setListingName(const QString &newListingName);
public slots:
    virtual void proceedDataFromBlock(QVariantMap ) {}
    virtual bool save(QVariantList result = QVariantList()){
        Q_UNUSED(result)
        return true;
    }
    virtual QVariant serialize() {return {};}
    virtual void deserialize(const QVariant &data, ITableDataProvider* listing = nullptr) {
        Q_UNUSED(data)
        Q_UNUSED(listing)
    }

signals:
    void listingNameChanged();
    void isEditedChanged();
    void tryLoadByName(QString blockName, QVariant data);
    void loaded();
    void isCachedChanged();

protected:
    QString m_listingName = "";
    bool m_isEdited = false;
    bool m_isCached = false;
};

#endif // BLOCKDATA_H
