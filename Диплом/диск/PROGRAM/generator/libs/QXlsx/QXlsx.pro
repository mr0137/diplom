TEMPLATE = lib
QT += gui-private
QT += core
CONFIG += c++17
CONFIG += shared dll

DEFINES += QXLSX_LIBRARY

CONFIG(release, debug|release){
DESTDIR = $$PWD/../../bin/libs
}

SOURCES += \
    qxlsx.cpp \
    xlsxabstractooxmlfile.cpp \
    xlsxabstractsheet.cpp \
    xlsxcell.cpp \
    xlsxcellformula.cpp \
    xlsxcelllocation.cpp \
    xlsxcellrange.cpp \
    xlsxcellreference.cpp \
    xlsxchart.cpp \
    xlsxchartsheet.cpp \
    xlsxcolor.cpp \
    xlsxconditionalformatting.cpp \
    xlsxcontenttypes.cpp \
    xlsxdatavalidation.cpp \
    xlsxdatetype.cpp \
    xlsxdocpropsapp.cpp \
    xlsxdocpropscore.cpp \
    xlsxdocument.cpp \
    xlsxdrawing.cpp \
    xlsxdrawinganchor.cpp \
    xlsxformat.cpp \
    xlsxmediafile.cpp \
    xlsxnumformatparser.cpp \
    xlsxrelationships.cpp \
    xlsxrichstring.cpp \
    xlsxsharedstrings.cpp \
    xlsxsimpleooxmlfile.cpp \
    xlsxstyles.cpp \
    xlsxtheme.cpp \
    xlsxutility.cpp \
    xlsxworkbook.cpp \
    xlsxworksheet.cpp \
    xlsxzipreader.cpp \
    xlsxzipwriter.cpp

HEADERS += \
    QXlsx_global.h \
    qxlsx.h \
    xlsxabstractooxmlfile.h \
    xlsxabstractooxmlfile_p.h \
    xlsxabstractsheet.h \
    xlsxabstractsheet_p.h \
    xlsxcell.h \
    xlsxcell_p.h \
    xlsxcellformula.h \
    xlsxcellformula_p.h \
    xlsxcelllocation.h \
    xlsxcellrange.h \
    xlsxcellreference.h \
    xlsxchart.h \
    xlsxchart_p.h \
    xlsxchartsheet.h \
    xlsxchartsheet_p.h \
    xlsxcolor_p.h \
    xlsxconditionalformatting.h \
    xlsxconditionalformatting_p.h \
    xlsxcontenttypes_p.h \
    xlsxdatavalidation.h \
    xlsxdatavalidation_p.h \
    xlsxdatetype.h \
    xlsxdocpropsapp_p.h \
    xlsxdocpropscore_p.h \
    xlsxdocument.h \
    xlsxdocument_p.h \
    xlsxdrawing_p.h \
    xlsxdrawinganchor_p.h \
    xlsxformat.h \
    xlsxformat_p.h \
    xlsxglobal.h \
    xlsxmediafile_p.h \
    xlsxnumformatparser_p.h \
    xlsxrelationships_p.h \
    xlsxrichstring.h \
    xlsxrichstring_p.h \
    xlsxsharedstrings_p.h \
    xlsxsimpleooxmlfile_p.h \
    xlsxstyles_p.h \
    xlsxtheme_p.h \
    xlsxutility_p.h \
    xlsxworkbook.h \
    xlsxworkbook_p.h \
    xlsxworksheet.h \
    xlsxworksheet_p.h \
    xlsxzipreader_p.h \
    xlsxzipwriter_p.h

INCLUDEPATH += $$PWD

