#ifndef ITABLECOLORPROVIDER_H
#define ITABLECOLORPROVIDER_H

#include <QColor>
#include <QObject>
#include <QMap>
#include "tablerenderbase_global.h"

#define ColorScheme QMap<int, QColor>

class Row;
/*!
 * \interface ITableColorProvider
 * \brief The ITableColorProvider class used as interface for interraction with Table 's internal color provider.
 */
class TABLERENDERBASE_EXPORT ITableColorProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor selectionColor READ selectionColor WRITE setSelectionColor NOTIFY selectionColorChanged)
public:
    /*!
     * \fn explicit ITableColorProvider(QObject *parent = nullptr)
     * \brief Default constructor.
     */
    explicit ITableColorProvider(QObject *parent = nullptr) : QObject(parent) {}
    /*!
     * \fn virtual void disselect(int id) = 0
     * \brief Disselect row with Row::id
     */
    virtual void disselect(int id) = 0;
    /*!
     * \fn virtual void disselect(const QVariantList &id) = 0
     * \brief Disselect row sequance with Row::id
     */
    virtual void disselect(const QVariantList &id) = 0;
    /*!
     * \fn virtual void select(int id) = 0
     * \brief Select row with Row::id
     */
    virtual void select(int id) = 0;
    /*!
     * \fn virtual void select(const QVariantList &id) = 0
     * \brief Select row sequance with Row::id
     */
    virtual void select(const QVariantList &id) = 0;
    /*!
     * \fn virtual bool hasSelection(const Row &row) = 0
     * \brief Method return true if Row selected
     */
    virtual bool hasSelection(const Row &row) = 0;
    /*!
     * \fn virtual bool hasSelection(const int &id) = 0
     * \brief Method return true if Row::id selected
     */
    virtual bool hasSelection(const int &id) = 0;
    /*!
     * \fn virtual QVector<int> selectedID() = 0
     * \brief Method return selected ID
     */
    virtual QVector<int> selectedID() = 0;
    /*!
     * \fn void setSelectionColor(QColor color)
     * \brief Method set color of selection
     */
    void setSelectionColor(QColor color) {
        if (m_selectionColor == color) return;

        m_selectionColor = color;
        emit schemeChanged();
        emit selectionColorChanged();
    };
    /*!
     * \fn QColor selectionColor()
     * \brief Return selection color. Default color \c black.
     */
    QColor selectionColor() { return m_selectionColor; };
signals:
    void schemeChanged();
    void selectionColorChanged();
protected:
    QColor m_selectionColor = "black";
};

#endif // ITABLECOLORPROVIDER_H
