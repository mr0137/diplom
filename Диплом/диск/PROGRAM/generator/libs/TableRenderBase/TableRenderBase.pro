#=============EDITED_BY_PLUGINHELPER=============#
TEMPLATE = lib
TARGET = TableRenderBase
QT += qml
QT += core
QT += quick
QT += svg
CONFIG += shared dll
CONFIG += c++17
CONFIG += qt
include($$PWD/../destidir.pri)

DEFINES += TABLERENDERBASE_LIBRARY
#Inputs
SOURCES += \
        itablecolorprovider.cpp \
        itabledataprovider.cpp


HEADERS += \
        itablecolorprovider.h \
        itabledataprovider.h \
        tablerender_global.h 

