import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Private 1.0

/**
    \class TableScrollBar
    \internal
    \brief Simple scrollbar for LazyTable.
*/
Item {
    id: scrollBar

    // The properties that define the scrollbar's state.
    // position and pageSize are in the range 0.0 - 1.0.  They are relative to the
    // height of the page, i.e. a pageSize of 0.5 means that you can see 50%
    // of the height of the view.
    // orientation can be either Qt.Vertical or Qt.Horizontal
    readonly property alias position: internal.position
    property real pageSize
    property real minimumSize: 15
    property real stepSize: 0.2
    property variant orientation : Qt.Vertical
    property color handleColor: "black"
    property color backgroundColor: "white"
    readonly property bool pressed: dragArea.drag.active || innerMouseArea.down || wheelHandler.down

    onPositionChanged: {
        fakeHandle.x = (orientation == Qt.Vertical ? 0 : (scrollBar.position * (scrollBar.width - visualHandle.width)))
        fakeHandle.y = (orientation == Qt.Vertical ? (scrollBar.position * (scrollBar.height - visualHandle.height)) : 0)
    }

    QtObject{
        id: internal
        property real position: 0
    }

    // A light, semi-transparent background
    DropArea {
        anchors.fill: parent
        Rectangle {
            id: background
            anchors.fill: parent
            //radius: orientation == Qt.Vertical ? (width/2 - 1) : (height/2 - 1)
            color: scrollBar.backgroundColor
            //opacity: 0.3
        }

    }
    // Size the bar to the required size, depending upon the orientation.
    Rectangle {
        id: visualHandle
        x: orientation == Qt.Vertical ? scrollBar.width / 2 - width / 2 : (scrollBar.position * (scrollBar.width - visualHandle.width))
        y: orientation == Qt.Vertical ? (scrollBar.position * (scrollBar.height - visualHandle.height)) : scrollBar.height / 2 - height / 2
        width:  orientation == Qt.Vertical ? (parent.width-2) : Math.max((scrollBar.pageSize * (scrollBar.width-2))   , scrollBar.minimumSize)
        height: orientation == Qt.Vertical ? Math.max((scrollBar.pageSize * (scrollBar.height-2)), scrollBar.minimumSize) : (parent.height-2)
        radius: orientation == Qt.Vertical ? (width/2 - 1) : (height/2 - 1)
        color: handleColor
        opacity: 0.7
    }

    MouseArea{
        id: innerMouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton
        property bool down: false
        onPressed: {
            down = true
            if (orientation == Qt.Vertical){
                scrollBar.setPosition(mouseY/scrollBar.height)
            }else{
                scrollBar.setPosition(mouseX/scrollBar.width)
            }
        }
        onReleased: {
            down = false
        }
    }

    Item{
        id: fakeHandle
        width:  orientation == Qt.Vertical ? (parent.width) : Math.max((scrollBar.pageSize * (scrollBar.width)) , scrollBar.minimumSize)
        height: orientation == Qt.Vertical ? Math.max((scrollBar.pageSize * (scrollBar.height-2)), scrollBar.minimumSize) : (parent.height)
        Drag.active: dragArea.drag.active
        Drag.hotSpot.x: dragArea.pos.x
        Drag.hotSpot.y: dragArea.pos.y

        MouseArea {
            id: dragArea
            anchors.fill: parent
            preventStealing: true
            drag.minimumX: 0
            drag.maximumX: scrollBar.orientation == Qt.Vertical ? width : scrollBar.width - width
            drag.minimumY: 0
            drag.maximumY: scrollBar.orientation == Qt.Vertical ? scrollBar.height - height : height
            drag.axis: scrollBar.orientation == Qt.Vertical ? Drag.YAxis : Drag.XAxis
            drag.target: parent
            property point pos: Qt.point(0,0)
            property int clickOffset: 0

            onMouseXChanged: {
                if (dragArea.drag.active && scrollBar.orientation != Qt.Vertical){
                    dragArea.pos = Qt.point(mouseX, pos.y)
                    scrollBar.setPosition(fakeHandle.x/(scrollBar.width - width))
                }
            }

            onMouseYChanged: {
                if (dragArea.drag.active && scrollBar.orientation == Qt.Vertical){
                    dragArea.pos = Qt.point(pos.x, mouseY)
                    scrollBar.setPosition(fakeHandle.y/(scrollBar.height - height))
                }
            }
        }
    }

    function increase(){
        scrollBar.setPosition(scrollBar.position + stepSize)

    }

    function decrease(){
        scrollBar.setPosition(scrollBar.position - stepSize)
    }

    function setPosition(pos){
        if (pos < 0 ) internal.position = 0
        else if (pos > 1) internal.position = 1
        else internal.position = pos
    }

    WheelHandler{
        id: wheelHandler
        property bool down: false
        onWheel: {
            if (event.angleDelta.y > 0){
                down = true
                scrollBar.decrease()
                down = false
            }else{
                down = true
                scrollBar.increase()
                down = false
            }
        }
    }
}
