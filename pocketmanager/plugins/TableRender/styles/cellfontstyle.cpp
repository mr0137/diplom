#include "cellfontstyle.h"

/*!
 * \class CellFontStyle
 * \brief The CellFontStyle class used as wrapper for default QFont.
 */
class CellFontStyle;

/*!
 * \fn explicit CellFontStyle::CellFontStyle(QObject *parent = nullptr)
 * \brief Default contructor.
 */
CellFontStyle::CellFontStyle(QObject *parent) : QObject(parent)
{
    m_font.setBold(false);
    m_font.setFamily("Arial");
    m_font.setOverline(false);
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, 0);
    m_font.setPointSizeF(9);
    m_font.setWordSpacing(4);
    m_font.setUnderline(false);
}

/*!
 * \property QFont CellFontStyle::font()
 * \brief Default QFont object.
 */
QFont CellFontStyle::font()
{
    return m_font;
}

/*!
 * \property QString CellFontStyle::family
 * \brief This property holds Font family string.
 */
const QString CellFontStyle::family() const
{
    return m_font.family();
}

/*!
 * \fn void CellFontStyle::setFamily(const QString &newFamily)
 * \brief Font family setter.
 */
void CellFontStyle::setFamily(const QString &newFamily)
{
    if (m_font.family() == newFamily)
        return;
    m_font.setFamily(newFamily);
    emit fontChanged();
    emit familyChanged();
}

/*!
 * \property qreal CellFontStyle::pointSize
 * \brief This property holds Font's point size.
 */
qreal CellFontStyle::pointSize() const
{
    return m_font.pointSizeF();
}

/*!
 * \fn void CellFontStyle::setPointSize(qreal newPointSize)
 * \brief Font's point size setter.
 */
void CellFontStyle::setPointSize(qreal newPointSize)
{
    if (qFuzzyCompare(m_font.pointSizeF(), newPointSize))
        return;

    m_font.setPointSizeF(newPointSize);
        emit fontChanged();
    emit pointSizeChanged();
}

/*!
 * \property qreal CellFontStyle::letterSpacing
 * \brief This property holds value of letter's spacing.
 */
qreal CellFontStyle::letterSpacing() const
{
    return m_font.letterSpacing();
}

/*!
 * \fn void CellFontStyle::setLetterSpacing(qreal newLetterSpacing)
 * \brief Font's letter spacing setter.
 */
void CellFontStyle::setLetterSpacing(qreal newLetterSpacing)
{
    if (qFuzzyCompare(m_font.letterSpacing(), newLetterSpacing))
        return;
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, newLetterSpacing);
        emit fontChanged();
    emit letterSpacingChanged();
}

/*!
 * \property bool CellFontStyle::overline
 * \brief This property holds is Font overlined or not.
 */
bool CellFontStyle::overline() const
{
    return m_font.overline();
}

/*!
 * \fn void CellFontStyle::setOverline(bool newOverline)
 * \brief Font's overline option setter.
 */
void CellFontStyle::setOverline(bool newOverline)
{
    if (m_font.overline() == newOverline)
        return;
    m_font.setOverline(newOverline);
        emit fontChanged();
    emit overlineChanged();
}

/*!
 * \property bool CellFontStyle::bold
 * \brief This property holds is Font bold or not.
 */
bool CellFontStyle::bold() const
{
    return m_font.bold();
}

/*!
 * \fn void CellFontStyle::setBold(bool newBold)
 * \brief Font's bold option setter.
 */
void CellFontStyle::setBold(bool newBold)
{
    if (m_font.bold() == newBold)
        return;
    m_font.setBold(newBold);
    emit boldChanged();
}
/*!
 * \property bool CellFontStyle::underline
 * \brief This property holds is Font underlined or not.
 */
bool CellFontStyle::underline() const
{
    return m_font.underline();
}

/*!
 * \fn void CellFontStyle::setUnderline(bool newUnderline)
 * \brief Font's underline option setter.
 */
void CellFontStyle::setUnderline(bool newUnderline)
{
    if (m_font.underline() == newUnderline)
        return;
    m_font.setUnderline(newUnderline);
    emit underlineChanged();
}

/*!
 * \property qreal CellFontStyle::wordSpacing
 * \brief This property holds value of word spacing.
 */
qreal CellFontStyle::wordSpacing() const
{
    return m_font.wordSpacing();
}

/*!
 * \fn void CellFontStyle::setWordSpacing(bool newWordSpacing)
 * \brief Font's word spacing setter.
 */
void CellFontStyle::setWordSpacing(qreal newWordSpacing)
{
    if (qFuzzyCompare(m_font.wordSpacing(), newWordSpacing))
        return;
    m_font.setWordSpacing(newWordSpacing);
    emit wordSpacingChanged();
}

/*!
 * \fn void CellFontStyle::reset()
 * \brief Reset font variables to default.
 */
void CellFontStyle::reset()
{
    m_font.setBold(false);
    m_font.setFamily("Arial");
    m_font.setOverline(false);
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, 0);
    m_font.setPointSizeF(9);
    m_font.setWordSpacing(10);
    m_font.setUnderline(false);
    emit fontChanged();
}
