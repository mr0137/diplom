#ifndef TABLERENDER_PLUGIN_H
#define TABLERENDER_PLUGIN_H

#include <QQmlExtensionPlugin>

/*!
 * \class LightTablePlugin
 * \brief The LightTablePlugin class is Qt plugin interface.
 * Used only for registering plugin's types.
 */
class LightTablePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // TABLERENDER_PLUGIN_H
