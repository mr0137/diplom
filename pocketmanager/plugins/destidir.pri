DESTDIR = $$PWD/../bin/plugins/$$TARGET

!contains(TARGET, BasePlugin|CQML) {

android:{
LIBS += -L$$OUT_PWD/../../libs/tablerenderbase/ -lTableRenderBase_armeabi-v7a
}else{
CONFIG(release, debug|release){
LIBS += -L$$PWD/../bin/libs -lTableRenderBase
}else{
win32: LIBS += -L$$OUT_PWD/../../libs/tablerenderbase/debug/ -lTableRenderBase
else:unix: LIBS += -L$$OUT_PWD/../../libs/tablerenderbase/ -lTableRenderBase
}
}
INCLUDEPATH += $$PWD/../libs/tablerenderbase
DEPENDPATH += $$PWD/../libs/tablerenderbase
}
