#include "scanner.h"

#include <QQuickItem>
#include <QtConcurrent>

Scanner::Scanner(QObject *parent) : QObject(parent)
{
    m_scanThread = new QThread(this);
    m_zxing = new QZXing(QZXing::DecoderFormat_PDF_417, nullptr);
    m_zxing->moveToThread(m_scanThread);

    connect(m_zxing, &QZXing::tagFound, this, [this](QString str){
        qDebug() << "Tag" << str;
        emit scanned(str);
        qDebug() << "TIME" << m_elapsedTimer.elapsed();
        setReadingTime(m_elapsedTimer.elapsed());
        setLastReadedCode(str);
        setReadyForScan(true);
    });

    //connect(m_zxing, &QZXing::decodingFinished, this, [this](bool state){
    //    setReadyForScan(true);
    //});

    connect(m_zxing, &QZXing::error, this, [this](QString err){
        emit error(err);
        qDebug() << "error" << err;

        setReadingTime(m_elapsedTimer.elapsed());
        setReadyForScan(true);
    });

    connect(this, &Scanner::decodeImage, m_zxing, [this](QImage img){
        m_zxing->decodeImage(img);
    });

    connect(m_grabResult.data(), &QQuickItemGrabResult::ready, [this]() {
        qDebug() << "ready";
        QImage img(m_grabResult->image());
        if (img.isNull()) qDebug() << "invalid QML Image";
        else emit decodeImage(img);
        m_grabResult.clear();
    });

    m_scanThread->start();
}

Scanner::~Scanner()
{
    m_scanThread->terminate();
    delete m_zxing;
}

const QString &Scanner::lastReadedCode() const
{
    return m_lastReadedCode;
}

void Scanner::setLastReadedCode(const QString &newLastReadedCode)
{
    if (m_lastReadedCode == newLastReadedCode)
        return;
    m_lastReadedCode = newLastReadedCode;
    emit lastReadedCodeChanged();
}

void Scanner::decode(QObject *imageObj)
{
    QQuickItemGrabResult *item = 0;
    item = qobject_cast<QQuickItemGrabResult*>(imageObj);

    QImage item_image(item->image());
    if (item_image.isNull())
        qDebug() << "invalid QML Image";
    else{
        m_elapsedTimer.start();
        m_img = item_image;
        qDebug() << "decoding start";
        m_zxing->decodeImage(item_image, item_image.width(), item_image.height(), true);
        //setReadingTime(m_elapsedTimer.elapsed());
    }
}

void Scanner::decode(QImage img)
{
    //QElapsedTimer t;
    m_elapsedTimer.start();
    m_zxing->decodeImage(img, img.width(), img.height(), true);
    //setReadingTime(t.elapsed());
}

void Scanner::scan(QQuickItem *item)
{
    if (!readyForScan()) return;
    setReadyForScan(false);

    m_grabResult = item->grabToImage();
qDebug() << "scan";

}

const QImage &Scanner::img() const
{
    return m_img;
}

bool Scanner::readyForScan() const
{
    return m_readyForScan;
}

void Scanner::setReadyForScan(bool newReadyForScan)
{
    if (m_readyForScan == newReadyForScan)
        return;
    m_readyForScan = newReadyForScan;
    emit readyForScanChanged();
}

int Scanner::readingTime() const
{
    return m_readingTime;
}

void Scanner::setReadingTime(int newReadingTime)
{
    if (m_readingTime == newReadingTime)
        return;
    m_readingTime = newReadingTime;
    emit readingTimeChanged();
}
