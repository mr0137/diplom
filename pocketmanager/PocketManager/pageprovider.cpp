#include "pageprovider.h"

#include <QQmlComponent>

PageProvider::PageProvider(QObject *parent) : QObject(parent)
{
}

void PageProvider::setEngine(QQmlEngine *engine)
{
    m_engine = engine;
    if (engine != nullptr){
        m_components.insert("initial", QVariant::fromValue<QQmlComponent*>(new QQmlComponent(m_engine, "qrc:/manager/qml/InitialPage.qml", this)));
        m_components.insert("scan", QVariant::fromValue<QQmlComponent*>(new QQmlComponent(m_engine, "qrc:/manager/qml/ScanPage.qml", this)));
        m_components.insert("view", QVariant::fromValue<QQmlComponent*>(new QQmlComponent(m_engine, "qrc:/manager/qml/ViewDataPage.qml", this)));
    }
}

QQmlComponent *PageProvider::getComponent(QString componentName)
{
    if (m_components.contains(componentName)){
        return qvariant_cast<QQmlComponent *>(m_components[componentName]);
    }
    return nullptr;
}

void PageProvider::initialState()
{
    setCurrentState("initial");
}

void PageProvider::scanState()
{
    setCurrentState("scan");
}

Label::Label(QObject *parent): QObject(parent)
{
    connect(this, &Label::countChanged, this, [this](){
        if (!m_des)
            setCountUpdated(true);
    });
    connect(this, &Label::failedCountChanged, this, [this](){
        if (!m_des)
            setCountUpdated(true);
    });
}

void Label::deserialize(QVariantMap map)
{
    //qDebug() << map;
    m_des = true;
    QString n1 = map.value("Name", "").toString();
    if (n1.contains('(')){
        setName1(n1.split('(').first());
        setName2(n1.split('(').last().remove(")"));
    }else{
        setName1(n1);
    }
    setCount(map.value("PlanCount", "0").toInt());
    setPart_path(map.value("part_path", "").toString());
    setThickness(map.value("Thickness", "").toDouble());
    setDimension(map.value("ExtMax_X", "").toString() + "x" + map.value("ExtMax_Y", "").toString());
    setId(map.value("barcode_id", "0").toInt());
    setOrderName(map.value("f_name").toString());
    setmaterial(map.value("MaterialName", "").toString());
    setNote(map.value("note", "").toString());
    setFailesCount(map.value("failedCount", "0").toInt());
    setResult(map.value("results", "").toString());
    setMaxImage(qvariant_cast<QImage>(map.value("imageMax", "")));
    setMinImage(qvariant_cast<QImage>(map.value("imageMin", "")));
    m_des = false;
    setCountUpdated(false);
}

QVariantMap Label::serialize()
{
    return QVariantMap{
        { "part_id", id() },
        { "PlanCount", count() },
        { "Thickness", thickness() },
        { "dimension", dimension() },
        { "f_name", orderName() },
        { "MaterialName", material() },
        { "note", note() },
        { "failedCount", failedCount() },
        { "Name", QString(name1())}
    };
}
