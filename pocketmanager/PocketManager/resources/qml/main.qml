import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0

ApplicationWindow {
    id: window
    width:  728  / 2
    height: 1280 / 2
    visible: true
    title: qsTr("Pocket Manager")

    Item{
        id: contentItem
        anchors.fill: parent

        C.BaseBackground{
            id: connectionLostRect
            anchors{
                top: parent.top
                right: parent.right
                left: parent.left
                topMargin: AppCore.client.isConnected && AppCore.client.isPermitted ? -connectionLostRect.height: 0
            }
            z: 1
            backgroundColor: AppCore.palette.app.errorColor
            elevation: 3
            height: AppCore.mult * parent.height * 0.04
            Behavior on anchors.topMargin { NumberAnimation { duration: 200 } }

            Text {
                anchors.fill: parent
                text: !AppCore.client.isConnected ? (qsTr("Server doesn't connected") + AppCore.trFix) : (qsTr("Connected. Permission DENIED") + AppCore.trFix)
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: connectionLostRect.height / 2
            }
        }

        StackView{
            id: stackView
            anchors.fill: parent
            initialItem: AppCore.pageProvider.getComponent("initial")

            Connections{
                target: AppCore.pageProvider
                function onCurrentStateChanged(){
                    switch(AppCore.pageProvider.currentState){
                    case "initial":
                        if (stackView.depth === 1){
                            stackView.push(AppCore.pageProvider.getComponent("initial"))
                        }else{
                            stackView.pop(null)
                        }
                        break;
                    default:
                        if (stackView.depth === 1){
                            stackView.push(AppCore.pageProvider.getComponent(AppCore.pageProvider.currentState))
                        }else{
                            stackView.replace(stackView.currentItem, AppCore.pageProvider.getComponent(AppCore.pageProvider.currentState))
                        }
                        break;
                    }
                }
            }
        }
    }
    DebugPopup{
        id: debugPopup
        width: parent.width
        height: parent.height
    }

    C.Button{
        visible: AppCore.debugMode
        enabled: AppCore.debugMode
        anchors.right: parent.right
        anchors.top: parent.top
        width: parent.height * 0.1
        height: parent.height * 0.05
        buttonRadius: 5
        text: "debug"
        buttonBackground: AppCore.palette.button.backgroundColor
        onPressed: {
            debugPopup.open()
        }
    }

    //Item{
    //    anchors.fill: parent
    //    visible: AppCore.debugMode
    //    z: 2
    //    ColumnLayout{
    //        anchors.fill: parent
    //
    //        Item{
    //            Layout.fillHeight: true
    //        }
    //
    //        Text{
    //            font.pointSize: 20
    //            text: "own ip: " + AppCore.client.ip
    //        }
    //        Text{
    //            font.pointSize: 20
    //            text: "server ip: " + AppCore.client.serverIP
    //        }
    //
    //
    //    }
    //}
}
