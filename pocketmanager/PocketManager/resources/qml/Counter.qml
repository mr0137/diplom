import QtQuick 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0

RowLayout{
    id: counter
    required property var prop
    Item{
        Layout.fillWidth: true
    }
    C.Button{
        id: minusButton
        Layout.fillHeight: true
        Layout.preferredWidth: height
        text: "-"
        textColor: AppCore.palette.button.textColor
        buttonBackground: AppCore.palette.button.backgroundColor
        buttonRadius: height / 2
        onReleased: {
            if (AppCore.pageProvider.label.count === 0) return
            if (!holdTimer.running){
                AppCore.pageProvider.label.count--
            }else{
                holdTimer.stop()
            }

            countTextField.text = AppCore.pageProvider.label.count
        }
        onPressAndHold: {
            holdTimer.restart()
        }
    }

    C.BaseBackground{
        backgroundColor: AppCore.palette.button.backgroundColor
        Layout.preferredWidth: parent.width * 0.38
        Layout.fillHeight: true
        radius: height / 2
        elevation: 2

        C.TextField{
            id: countTextField
            anchors.leftMargin: parent.height / 2
            anchors.rightMargin: parent.height / 2
            anchors.fill: parent
            floatingLabel: false
            frameBackgroundColor: AppCore.palette.app.mainColor
            color: AppCore.palette.button.textColor
            hintColor: "transparent"

            horizontalAlignment: Text.AlignHCenter

            validator: RegExpValidator{
                regExp: /\d+/
            }

            onTextChanged: {
                if (parseInt(countTextField.text) !== AppCore.pageProvider.label.count){
                    if (countTextField.text === "") AppCore.pageProvider.label.count = 0
                    else AppCore.pageProvider.label.count = parseInt(countTextField.text)
                }
            }

            Connections{
                target: AppCore.pageProvider.label
                function onCountChanged() {
                    if (parseInt(countTextField.text) !== AppCore.pageProvider.label.count)
                        countTextField.text = AppCore.pageProvider.label.count
                }
            }
            Component.onCompleted: countTextField.text = AppCore.pageProvider.label.count
        }
    }

    C.Button{
        id: plusButton
        Layout.fillHeight: true
        Layout.preferredWidth: height
        text: "+"
        textColor: AppCore.palette.button.textColor
        buttonBackground: AppCore.palette.button.backgroundColor
        buttonRadius: height / 2

        onReleased: {
            if (!holdTimer.running){
                AppCore.pageProvider.label.count++
            }else{
                holdTimer.stop()
            }
            countTextField.text = AppCore.pageProvider.label.count

        }

        onPressAndHold: {
            holdTimer.restart()
        }
    }

    Item{
        Layout.fillWidth: true
    }

    Timer{
        id: holdTimer
        repeat: true
        interval: 150
        running: false
        onTriggered: {
            if (plusButton.down){
                AppCore.pageProvider.label.count++
            }else if (minusButton.down && AppCore.pageProvider.label.count !== 0){
                AppCore.pageProvider.label.count--
            }
            countTextField.text = AppCore.pageProvider.label.count
        }
    }
}

