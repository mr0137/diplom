import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import CQML 1.0 as C
import App 1.0

C.BaseBackground {
    id: label
    backgroundColor: Qt.darker("white", 1.1)
    elevation: 4
    border.width: 0.6
    border.color: "black"
    //clipContent: true
    property int rowCount: 8

    ColumnLayout{
        anchors.fill: parent
        spacing: 0
        Rectangle{
            color: "transparent"
            Layout.fillWidth: true
            Layout.preferredHeight: label.height / rowCount
            Text{
                anchors.fill: parent
                text: "id: " + AppCore.pageProvider.label.id
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: parent.height / 2
            }
        }

        Rectangle{
            Layout.fillWidth: true
            Layout.preferredHeight: 1
            color: "black"
        }

        RowLayout{
            Layout.fillWidth: true
            Layout.preferredHeight: label.height / rowCount
            spacing: -1
            Rectangle{
                color: "transparent"
                Layout.fillWidth: true
                Layout.fillHeight: true
                Text{
                    anchors.fill: parent
                    text: AppCore.pageProvider.label.name1
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: parent.height / 1.9
                }
            }

            Rectangle{
                Layout.fillHeight: true
                Layout.preferredWidth: 1
                color: "black"
            }

            Rectangle{
                color: "transparent"
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.maximumWidth: parent.width * 0.15
            }
        }

        Rectangle{
            Layout.fillWidth: true
            Layout.preferredHeight: 2
            color: "black"
        }

        RowLayout{
            Layout.fillWidth: true
            spacing: -1
            Layout.preferredHeight: label.height * 6

            ColumnLayout{
                Layout.fillHeight: true
                Layout.fillWidth: true
                spacing: -1

                Rectangle{
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: label.height / rowCount
                    Text{
                        anchors.fill: parent
                        text: " Габ.: " + AppCore.pageProvider.label.dimension
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: parent.height / 2
                    }
                }

                Rectangle{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "black"
                }

                RowLayout{
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.maximumHeight: label.height / rowCount
                    spacing: -1
                    Rectangle{
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Text{
                            anchors.fill: parent
                            text: AppCore.pageProvider.label.material
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: parent.height / 2
                        }
                    }

                    Rectangle{
                        Layout.fillHeight: true
                        Layout.preferredWidth: 1
                        color: "black"
                    }

                    Rectangle{
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.maximumWidth: label.width * 0.25
                        Text{
                            anchors.fill: parent
                            text: "≠ " + AppCore.pageProvider.label.thickness
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: parent.height / 2
                        }
                    }
                }

                Rectangle{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "black"
                }

                Rectangle{
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.preferredHeight: label.height / rowCount
                    Text{
                        anchors.fill: parent
                        text: " Result: " + AppCore.pageProvider.label.result
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: parent.height / 2
                    }
                }

                Rectangle{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "black"
                }

                RowLayout{
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.maximumHeight: label.height / rowCount
                    spacing: -1
                    Rectangle{
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.maximumWidth: parent.width * 0.4
                        Text{
                            anchors.fill: parent
                            text: " К-сть: " + AppCore.pageProvider.label.count
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: parent.height / 2
                        }
                    }

                    Rectangle{
                        Layout.fillHeight: true
                        Layout.preferredWidth: 1
                        color: "black"
                    }

                    Rectangle{
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Text{
                            anchors.fill: parent
                            //text: " Габ.: " + AppCore.pageProvider.label.dimension
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: parent.height / 2
                        }
                    }
                }

                Rectangle{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "black"
                }

                RowLayout{
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.maximumHeight: label.height / rowCount
                    spacing: -1
                    Rectangle{
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Text{
                            anchors.fill: parent
                            text: " № Зам. " + AppCore.pageProvider.label.orderName
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: parent.height / 2
                        }
                    }
                }

                Rectangle{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "black"
                }

                Rectangle{
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.preferredHeight: label.height / rowCount
                    Text{
                        anchors.fill: parent
                        text: " Прим.: " + AppCore.pageProvider.label.note
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: parent.height / 2
                    }
                }
            }

            Rectangle{
                Layout.fillHeight: true
                Layout.preferredWidth: 1
                color: "black"
            }

            Rectangle{
                id: imageRect
                Layout.fillHeight: true
                Layout.margins: 2
                Layout.topMargin: 0
                Layout.preferredWidth: label.width * 0.4

                CodeItem{
                    id: codeItem
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height

                    Behavior on height { NumberAnimation{ easing.type: Easing.InCubic; duration: 100 } }
                    Behavior on width { NumberAnimation{ easing.type: Easing.InCubic; duration: 100 } }

                    source: AppCore.pageProvider.label.minImage
                    state: "min"
                    states:[
                        State {
                            name: "min"
                            PropertyChanges { target: codeItem; source: AppCore.pageProvider.label.minImage }
                            ParentChange { target: codeItem; parent: imageRect; }
                        },
                        State {
                            name: "max"
                            PropertyChanges { target: codeItem; source: AppCore.pageProvider.label.maxImage }
                            ParentChange { target: codeItem; parent: maxImage; }
                        }
                    ]

                    MouseArea{
                        anchors.fill: parent
                        onPressed: {
                            if (codeItem.state === "min"){
                                codeItem.state = "max"
                            }else{
                                codeItem.state = "min"
                            }
                        }
                    }
                }
            }
        }
    }
}
