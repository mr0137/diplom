#ifndef CODEITEM_H
#define CODEITEM_H

#include <QImage>
#include <QQuickItem>

class CodeItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QImage source READ source WRITE setSource NOTIFY sourceChanged)
public:
    CodeItem(QQuickItem *parent = nullptr);

    const QImage &source() const;
    void setSource(const QImage &newSource);

signals:
    void sourceChanged();

protected:
    virtual QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;
private:
    QImage m_source;
};

#endif // CODEITEM_H
