#include <QAbstractItemModel>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <appcore.h>
#include <codeitem.h>

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0);

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString & msg)
{
#ifdef QT_NO_DEBUG
    //DevLogger::instance()->addLog(context.category, context.function, msg);
#endif
    if (!msg.contains("QQuickWidget does not support") && !msg.contains("If you wish to create your root"))
        (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, msg);
}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qSetMessagePattern("%{if-category}\033[32m%{category}:%{endif}%{if-warning}\033[37m%{file}:%{line}: %{endif}\033\[31m%{if-debug}\033\[36m%{endif}[%{function}]\033[0m %{message}");
    qInstallMessageHandler(customMessageHandler);

    app.setOrganizationName("TK");
    app.setOrganizationDomain("TK");

    AppCore::instance()->setRootPath(argv[0]);

    QQmlApplicationEngine engine;
    qmlRegisterSingletonInstance("App", 1, 0, "AppCore", AppCore::instance());
    qmlRegisterAnonymousType<QAbstractListModel>("App", 1);
    qmlRegisterType<Scanner>("App", 1, 0, "Scanner");
    qmlRegisterType<PageProvider>("App", 1, 0, "PageProvider");
    qmlRegisterType<ClientService>("App", 1, 0, "ClientService");
    qmlRegisterType<CodeItem>("App", 1, 0, "CodeItem");

#ifdef Q_OS_WINDOWS
    //qDebug() << AppCore::instance()->rootDir() + "\\bin\\plugins";
    engine.addImportPath(AppCore::instance()->rootDir() + "\\bin\\plugins");
#else
    qDebug() << AppCore::instance()->rootDir() + "/bin/plugins";
    engine.addImportPath(AppCore::instance()->rootDir() + "/bin/plugins");
#endif
    qDebug() << engine.importPathList();

    //для генерации
    AppCore::instance()->pageProvider()->setEngine(&engine);

    const QUrl url(QStringLiteral("qrc:/manager/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
