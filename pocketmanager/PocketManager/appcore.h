#ifndef APPCORE_H
#define APPCORE_H


#include <QObject>
#include <QDebug>
#include <QVariantMap>
#include <QVariantList>
#include <QVariantMap>
#include <QTimer>
#include <QFuture>
#include <pageprovider.h>
#include <clientservice.h>
#include <scanner.h>
//#include <databaseservice.h>
#include <qqml.h>

//! ядро программы
class DatabaseService;
class AppCore: public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(AppCore)
    QML_SINGLETON
    //не использвуется
    Q_PROPERTY(qreal mult READ mult WRITE setMult NOTIFY multChanged)
    Q_PROPERTY(ClientService* client READ client NOTIFY clientChanged)
    // фикс для перевода (не думаю, что будет нужен)
    Q_PROPERTY(QString trFix READ trFix WRITE setTrFix NOTIFY trFixChanged)
    //путь к главному каталогу
    Q_PROPERTY(QString rootDir READ rootDir WRITE setRootDir NOTIFY rootDirChanged)
    Q_PROPERTY(Scanner* scanner READ scanner WRITE setScanner NOTIFY scannerChanged)
    Q_PROPERTY(PageProvider* pageProvider READ pageProvider NOTIFY pageProviderChanged)
    //палитра
    Q_PROPERTY(QVariantMap palette READ palette WRITE setPalette NOTIFY paletteChanged)
    //режим дебага
    Q_PROPERTY(bool debugMode READ debugMode WRITE setDebugMode NOTIFY debugModeChanged)
    //статистика
    Q_PROPERTY(QVariantList debugStat READ debugStat WRITE setDebugStat NOTIFY debugStatChanged)
    AppCore(QObject * parent = nullptr);
signals:
    void paletteChanged();
    void rootDirChanged();
    void debugModeChanged();
    void message(QString key,QVariant var);
    void printerServiceChanged();
    void trFixChanged();
    void multChanged();
    void pageProviderChanged();
    void clientChanged();
    void scannerChanged();
    void debugStatChanged();

public:
    static AppCore* instance();
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);
    // [property]
    qreal mult() const;
    bool debugMode() const;
    QString rootDir() const;
    Scanner *scanner() const;
    QVariantMap palette() const;
    QString trFix() const;
    ClientService *client() const;
    QVariantList debugStat() const;
    bool isDatabaseAvailable() const;
    PageProvider *pageProvider() const;

    void setMult(qreal newMult);
    void setRootPath(QString path);
    void setScanner(Scanner *newScanner);
    void setDebugMode(bool newDebugMode);
    void setTrFix(const QString &newTrFix);
    void setRootDir(const QString &newRootDir);
    void setPalette(const QVariantMap &newPalette);
    void setDebugStat(const QVariantList &newDebugStat);
    void setIsDatabaseAvailable(bool newIsDatabaseAvailable);
    // [property]

public slots:
    void updateTranslation();
    void updateLabel();

private:
    QTimer *m_timer = nullptr;
    bool m_debugMode = true;
    QVariantMap m_palette;
    QFuture<void> m_future;

    QVariantMap tabOrderN;
    QString m_rootDir = "";

    qreal m_mult = 1;
    QString m_trFix;
    QVariantList m_debugStat;
    QString m_loadingListingMessage;
    bool m_isDatabaseAvailable = false;

    Scanner *m_scanner = nullptr;
    ClientService *m_client = nullptr;
    PageProvider *m_pageProvider = nullptr;

    void initDebug();
};

#endif // APPCORE_H
