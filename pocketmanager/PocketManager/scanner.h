#ifndef SCANNER_H
#define SCANNER_H

#include <QElapsedTimer>
#include <QFuture>
#include <QImage>
#include <QObject>
#include <QQuickItemGrabResult>
#include <QZXing.h>
#include <qqml.h>

//мультипоточный парсинг картинки на строчку, работает асинхронно
//тайминги перепроверки сведены вручную в qml
class Scanner : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QString lastReadedCode READ lastReadedCode WRITE setLastReadedCode NOTIFY lastReadedCodeChanged)
    Q_PROPERTY(int readingTime READ readingTime WRITE setReadingTime NOTIFY readingTimeChanged)
    Q_PROPERTY(bool readyForScan READ readyForScan WRITE setReadyForScan NOTIFY readyForScanChanged)

public:
    explicit Scanner(QObject *parent = nullptr);
    ~Scanner();

    const QString &lastReadedCode() const;
    void setLastReadedCode(const QString &newLastReadedCode);
    bool readyForScan() const;
    void setReadyForScan(bool newReadyForScan);

    int readingTime() const;
    void setReadingTime(int newReadingTime);

    const QImage &img() const;

public slots:
    void decode(QObject *imageObj);
    void decode(QImage img);
    void scan(QQuickItem* item);

signals:
    void lastReadedCodeChanged();
    void scanned(QString code);
    void error(QString error);
    void decodeImage(QImage img);
    void readyForScanChanged();

    void readingTimeChanged();

private:
    QString m_lastReadedCode;
    QZXing *m_zxing = nullptr;
    bool m_readyForScan = true;
    QImage m_img;
    QThread *m_scanThread = nullptr;
    int m_readingTime = 0;
    QElapsedTimer m_elapsedTimer;
    QSharedPointer<const QQuickItemGrabResult> m_grabResult;
};


#endif // SCANNER_H
