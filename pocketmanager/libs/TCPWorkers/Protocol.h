#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <QByteArray>
#include <mutex>
#include "TCPWorkers_global.h"

//для удобного понимания типа пакета
static constexpr auto CONNECTION_TYPE               = uint8_t(1);
static constexpr auto CONNECTION_PERMISSION         = uint8_t(2);
static constexpr auto DEVICE_NAME                   = uint8_t(3);
static constexpr auto SCANNED_CODE                  = uint8_t(4);
static constexpr auto UPDATE_COUNT                  = uint8_t(5);
static constexpr auto DEBUG_IMAGE                   = uint8_t(6);

static constexpr auto ACCESS_ACCEPTED = uint8_t(1);
static constexpr auto ACCESS_DENIED = uint8_t(0);

//!
//! \brief The SendType enum : \c P2P, or \c Broadcast
//!
enum class SendType {P2P, Broadcast};
//!
//! \brief The Frame struct
//!
struct Frame {
    //!
    //! \brief Socket descriptor
    //!
    int socketDescriptor;
    //!
    //! \brief Data which incapsulates \c this frame
    //!
    QByteArray data;
    //!
    //! \brief type of sending
    //!
    SendType type;
};

constexpr auto ADD_STREAM = uint8_t(30);

#endif // PROTOCOL_H
