#=============EDITED_BY_PLUGINHELPER=============#
TEMPLATE = lib
TARGET = TCPWorkers
QT -= gui
QT += network
QT += concurrent
CONFIG += shared dll
CONFIG += c++17

QML_IMPORT_NAME = TCPWorkers
include($$PWD/../destidir.pri)
#Inputs
DEFINES += TCPWORKERS_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += $$PWD/
INCLUDEPATH += $$PWD/../..

SOURCES += \
    streamconnector.cpp \
    streamconnectorclientside.cpp \
    streamconnectorv2.cpp \
    tcpclientworker.cpp \
    tcpserverworker.cpp

HEADERS += \
    Protocol.h \
    TCPWorkers_global.h \
    streamconnector.h \
    streamconnectorclientside.h \
    streamconnectorv2.h \
    tcpclientworker.h \
    tcpserverworker.h
