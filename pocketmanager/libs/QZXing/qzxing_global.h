#ifndef QZXING_GLOBAL_H
#define QZXING_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QZXING_LIBRARY)
#  define QZXING_EXPORT Q_DECL_EXPORT
#else
#  define QZXING_EXPORT Q_DECL_IMPORT
#endif


#endif // QZXING_GLOBAL_H
