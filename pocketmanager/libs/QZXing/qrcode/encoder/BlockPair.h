#ifndef BLOCKPAIR_H
#define BLOCKPAIR_H

#include <vector>
#include <common/Array.h>
#include <common/GreyscaleLuminanceSource.h>

namespace zxing {
namespace qrcode {

class BlockPair
{
private:
    ArrayRef<bbyte> data_;
    ArrayRef<bbyte> errorCorrection_;

public:
    BlockPair(ArrayRef<bbyte> data, ArrayRef<bbyte> errorCorrection) :
      data_(data), errorCorrection_(errorCorrection)  {}

    BlockPair(const BlockPair& other) : data_(other.data_), errorCorrection_(other.errorCorrection_) {}

    ArrayRef<bbyte> getDataBytes() { return data_; }

    ArrayRef<bbyte> getErrorCorrectionBytes() { return errorCorrection_; }
};

}
}

#endif //BLOCKPAIR_H
