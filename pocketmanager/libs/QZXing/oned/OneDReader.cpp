// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ZXing.h>
#include <oned/OneDReader.h>
#include <ReaderException.h>
#include <oned/OneDResultPoint.h>
#include <NotFoundException.h>
#include <math.h>
#include <limits.h>
#include <algorithm>


#include <typeinfo>
#include <QString>
#include <QDebug>

using std::vector;
using zxing::Ref;
using zxing::Result;
using zxing::NotFoundException;
using zxing::oned::OneDReader;

// VC++
using zxing::BinaryBitmap;
using zxing::BitArray;
using zxing::DecodeHints;

OneDReader::OneDReader() {}

Ref<Result> OneDReader::decode(Ref<BinaryBitmap> image, DecodeHints hints) {
    try {
        return doDecode(image, hints);
    } catch (NotFoundException const& nfe) {
        // std::cerr << "trying harder" << std::endl;
        bool tryHarder = hints.getTryHarder();
        if (tryHarder && image->isRotateSupported()) {
            // std::cerr << "v rotate" << std::endl;
            Ref<BinaryBitmap> rotatedImage(image->rotateCounterClockwise());
            // std::cerr << "^ rotate" << std::endl;
            Ref<Result> result = doDecode(rotatedImage, hints);
            // Doesn't have java metadata stuff
            ArrayRef< Ref<ResultPoint> >& points (result->getResultPoints());
            if (points && !points->empty()) {
                int height = rotatedImage->getHeight();
                for (int i = 0; i < points->size(); i++) {
                    points[i].reset(new OneDResultPoint(height - points[i]->getY() - 1, points[i]->getX()));
                }
            }
            // std::cerr << "tried harder" << std::endl;
            return result;
        } else {
            // std::cerr << "tried harder nfe" << std::endl;
            throw nfe;
        }
    }
}

Ref<Result> OneDReader::doDecode(Ref<BinaryBitmap> image, DecodeHints hints) {
    int width = image->getWidth();
    int height = image->getHeight();
    Ref<BitArray> row(new BitArray(width));

    int middle = height >> 1;
    bool tryHarder = hints.getTryHarder();
    int rowStep = std::max(1, height >> (tryHarder ? 8 : 5));
    using namespace std;
    // cerr << "rS " << rowStep << " " << height << " " << tryHarder << endl;
    int maxLines;
    if (tryHarder) {
        maxLines = height; // Look at the whole image, not just the center
    } else {
        maxLines = 15; // 15 rows spaced 1/32 apart is roughly the middle half of the image
    }

    if (hints.containsFormat(BarcodeFormat::CODE_128_EXT)){
        //qDebug() << "ext";
        std::string result;
        int length = -1;

        for (int y = 0; y < height; y+=1) {
            int rowNumber = y;
            if (false) {
                std::cerr << "rN "
                          << rowNumber << " "
                          << height << " "
                          << middle << " "
                          << rowStep << " "
                          << std::endl;
            }
            if (rowNumber < 0 || rowNumber >= height) {
                break;
            }
            try {
                row = image->getBlackRow(rowNumber, row);
            } catch (NotFoundException const& ignored) {
                (void)ignored;
                continue;
            }

            for (int attempt = 0; attempt < 2; attempt++) {
                if (attempt == 1) {
                    row->reverse(); // reverse the row and continue
                }
                try {
                    Ref<Result> res = decodeRow(rowNumber, row);
                    // We found our barcode
                    if (attempt == 1) {
                        // it was upside down, so note that
                        // flip the result points horizontally.
                        ArrayRef< Ref<ResultPoint> > points(res->getResultPoints());
                        if (points) {
                            points[0] = Ref<ResultPoint>(new OneDResultPoint(width - points[0]->getX() - 1,
                                                         points[0]->getY()));
                            points[1] = Ref<ResultPoint>(new OneDResultPoint(width - points[1]->getX() - 1,
                                                         points[1]->getY()));

                        }
                    }
                    if (result.find(res->getText()->getText()) == std::string::npos){
                        std::string t = res->getText()->getText();
                        if (length == -1) length= t.length();
                        result.append(t);
                        attempt = 2;
                        //if ((int)t.at(0) + 1 == (int)t.at(1)) y = height;
                    }else{
                        //delete res;
                        break;
                    }
                    //delete res;

                } catch (ReaderException const& re) {
                    (void)re;
                    continue;
                }
            }
        }

        //removing metadata
        //finding length per line and rows count
        if (result.empty()) throw NotFoundException();
        int foundedRows = 0;
        int rows = int(result.at(1))-32;
        int pos = int(result.at(0))-32;
        qDebug() << rows;

        if (pos != 0) throw NotFoundException();

        for (size_t i = 0; i < result.length(); i+=length){
            if ((int)result.at(i)-32 == foundedRows){
                foundedRows++;
            }else{
                throw NotFoundException();
            }
        }

        //qDebug() << "do erase" ;
        for (int i = rows-1; i >= 0; i--){
            result.erase(i*length,2);
        }

        //qDebug() << "do clear from ||";
        for (int i = result.length()-1; i>= 0; i--){
            if (result[i] == '~') result.erase(i, 1);
            else break;
        }

        vector<bbyte> rawCodes(20, 0);
        ArrayRef<bbyte> rawBytes (20);
        //qDebug() << "raw vector";
        for (int i = 0; i < 20; i++) {
            rawBytes[i] = rawCodes[i];
        }

        ArrayRef< Ref<ResultPoint> > resultPoints(2);
        resultPoints[0] =
                Ref<OneDResultPoint>(new OneDResultPoint(1., (float) 1.));
        resultPoints[1] =
                Ref<OneDResultPoint>(new OneDResultPoint(1., (float) 1.));
       // qDebug() << "return" << result.c_str();
        return Ref<Result>(new Result(Ref<String>(new String(result)), rawBytes, resultPoints, BarcodeFormat::CODE_128_EXT));
    }else{
       // qDebug() << "else";
        for (int x = 0; x < maxLines; x++) {

            // Scanning from the middle out. Determine which row we're looking at next:
            int rowStepsAboveOrBelow = (x + 1) >> 1;
            bool isAbove = (x & 0x01) == 0; // i.e. is x even?
            int rowNumber = middle + rowStep * (isAbove ? rowStepsAboveOrBelow : -rowStepsAboveOrBelow);
            if (false) {
                std::cerr << "rN "
                          << rowNumber << " "
                          << height << " "
                          << middle << " "
                          << rowStep << " "
                          << isAbove << " "
                          << rowStepsAboveOrBelow
                          << std::endl;
            }
            if (rowNumber < 0 || rowNumber >= height) {
                // Oops, if we run off the top or bottom, stop
                break;
            }

            // Estimate black point for this row and load it:
            try {
                row = image->getBlackRow(rowNumber, row);
            } catch (NotFoundException const& ignored) {
                (void)ignored;
                continue;
            }

            // While we have the image data in a BitArray, it's fairly cheap to reverse it in place to
            // handle decoding upside down barcodes.
            for (int attempt = 0; attempt < 2; attempt++) {
                if (attempt == 1) {
                    row->reverse(); // reverse the row and continue
                }
                try {
                    // Look for a barcode
                    // std::cerr << "rn " << rowNumber << " " << typeid(*this).name() << std::endl;
                    Ref<Result> result = decodeRow(rowNumber, row);
                    // We found our barcode
                    if (attempt == 1) {
                        // But it was upside down, so note that
                        // result.putMetadata(ResultMetadataType.ORIENTATION, new Integer(180));
                        // And remember to flip the result points horizontally.
                        ArrayRef< Ref<ResultPoint> > points(result->getResultPoints());
                        if (points) {
                            points[0] = Ref<ResultPoint>(new OneDResultPoint(width - points[0]->getX() - 1,
                                                         points[0]->getY()));
                            points[1] = Ref<ResultPoint>(new OneDResultPoint(width - points[1]->getX() - 1,
                                                         points[1]->getY()));

                        }
                    }
                    return result;
                } catch (ReaderException const& re) {
                    (void)re;
                    continue;
                }
            }
        }
    }
    throw NotFoundException();
}

int OneDReader::patternMatchVariance(vector<int>& counters,
                                     vector<int> const& pattern,
                                     int maxIndividualVariance) {
    return patternMatchVariance(counters, &pattern[0], maxIndividualVariance);
}

int OneDReader::patternMatchVariance(vector<int>& counters,
                                     int const pattern[],
                                     int maxIndividualVariance) {
    int numCounters = counters.size();
    unsigned int total = 0;
    unsigned int patternLength = 0;
    for (int i = 0; i < numCounters; i++) {
        total += counters[i];
        patternLength += pattern[i];
    }
    if (total < patternLength) {
        // If we don't even have one pixel per unit of bar width, assume this is too small
        // to reliably match, so fail:
        return INT_MAX;
    }
    // We're going to fake floating-point math in integers. We just need to use more bits.
    // Scale up patternLength so that intermediate values below like scaledCounter will have
    // more "significant digits"
    int unitBarWidth = (total << INTEGER_MATH_SHIFT) / patternLength;
    maxIndividualVariance = (maxIndividualVariance * unitBarWidth) >> INTEGER_MATH_SHIFT;

    int totalVariance = 0;
    for (int x = 0; x < numCounters; x++) {
        int counter = counters[x] << INTEGER_MATH_SHIFT;
        int scaledPattern = pattern[x] * unitBarWidth;
        int variance = counter > scaledPattern ? counter - scaledPattern : scaledPattern - counter;
        if (variance > maxIndividualVariance) {
            return INT_MAX;
        }
        totalVariance += variance;
    }
    return totalVariance / total;
}

void OneDReader::recordPattern(Ref<BitArray> row,
                               int start,
                               vector<int>& counters) {
    int numCounters = counters.size();
    for (int i = 0; i < numCounters; i++) {
        counters[i] = 0;
    }
    int end = row->getSize();
    if (start >= end) {
        throw NotFoundException();
    }
    bool isWhite = !row->get(start);
    int counterPosition = 0;
    int i = start;
    while (i < end) {
        if (row->get(i) ^ isWhite) { // that is, exactly one is true
            counters[counterPosition]++;
        } else {
            counterPosition++;
            if (counterPosition == numCounters) {
                break;
            } else {
                counters[counterPosition] = 1;
                isWhite = !isWhite;
            }
        }
        i++;
    }
    // If we read fully the last section of pixels and filled up our counters -- or filled
    // the last counter but ran off the side of the image, OK. Otherwise, a problem.
    if (!(counterPosition == numCounters || (counterPosition == numCounters - 1 && i == end))) {
        throw NotFoundException();
    }
}

OneDReader::~OneDReader() {}
