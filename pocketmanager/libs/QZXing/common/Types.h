// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
#ifndef __TYPES_H__
#define __TYPES_H__

#include <string>
#include <iostream>
#include <common/Counted.h>

namespace zxing {

typedef unsigned char bbyte;
typedef bool boolean;

}

#endif // __COMMON__TYPES_H__
