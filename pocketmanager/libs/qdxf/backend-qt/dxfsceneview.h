#ifndef DXFSCENEVIEW_H
#define DXFSCENEVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

class DXFSceneView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit DXFSceneView(QWidget *parent = 0);

private:
    QPoint mouseLast;
    bool mouseClicked;
signals:

public slots:

protected:
    void wheelEvent(QWheelEvent* event);
 /*   void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);*/

};

#endif // DXFSCENEVIEW_H
