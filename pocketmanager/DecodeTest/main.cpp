#include <QCoreApplication>
#include <QGuiApplication>
#include <appcore.h>

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    AppCore::instance();
    AppCore::instance()->autoTest();

    return a.exec();
}
