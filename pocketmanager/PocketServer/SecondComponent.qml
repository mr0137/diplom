import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0
import TableRender 1.0

ColumnLayout{
    spacing: 0
    TableRender{
        id: tableRender
        Layout.fillHeight: true
        Layout.fillWidth: true
        provider: AppCore.logTable
        backgroundColor: AppCore.palette.app.firstBackgroundColor

        onResizingChanged:{
            tableRender.closePopup()
        }

        style: TableStyle{
            verticalHeaderAvailable: true
            iconsColor: AppCore.palette.app.darkBackgroundColor
            iconsWidth: 1
            highlightColor: AppCore.palette.text.textColor
            selectionColor: "black"
            selectionOpacity: 0.3
            cell{
                height: 35
                backgroundColor: AppCore.palette.app.firstBackgroundColor
                textColor: AppCore.palette.text.textColor
                font{
                    pointSize: 15
                    family: "Arial"
                    letterSpacing: 0.1
                    wordSpacing: 0.1
                }
            }
            horizontalHeader{
                height: 27
                width: 240
                backgroundColor: AppCore.palette.app.lightBackgroundColor
                textColor: AppCore.palette.app.mainColor
                font{
                    pointSize: 16
                    family: "Arial"
                    letterSpacing: 0.1
                    wordSpacing: 0.1
                }
            }
            verticalHeader{
                width: 10
                backgroundColor: AppCore.palette.app.lightBackgroundColor
            }
        }

        Component.onCompleted: setCurrentRow(0)
    }

    C.BaseBackground{
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.maximumHeight: 50
        backgroundColor: AppCore.palette.app.firstBackgroundColor
        elevation: 3
        C.Button{
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            elevation: 3
            width: 90
            text: "back"
            buttonBackground: AppCore.palette.app.lightBackgroundColor
            textColor: AppCore.palette.app.mainColor
            onReleased: {
                stackView.pop()
            }
        }
    }
}
