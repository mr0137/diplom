#include "dbservice.h"

#include <Protocol.h>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QThread>
#include <client.h>
#include <QDateTime>
#include <QDir>


//!============================== CONNECTIONS WINDOWS =======================================
//!
//!    "Driver={SQL Server};Server=192.168.81.142\\SQL142;Database=Produce;Integrated Security = true"
//!
//!    "Driver={SQL Server};Server=192.168.81.190\\NEWSQL;Database=Produce;Integrated Security = true"
//!
//!
//!=============================== CONNECTIONS LINUX =======================================
//!
//!    "Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.142\\SQL142;Database=Produce;Integrated Security = true"
//!
//!    "Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.190\\NEWSQL;Database=Produce;Integrated Security = true"
//!

static int no = 0;
static QMap<QString, QSqlDatabase> storage;
static QString connection = "";

void DBService::Local::connect()
{
    m_sql = QSqlDatabase::addDatabase("QSQLITE", QString::number(no++));
    m_sql.setDatabaseName("config.sqlite");

    if (!m_sql.open()) {
        qWarning() << "MSqlS error: " << m_sql.lastError();
    }

    QSqlQuery q(m_sql);
    QString query = QString(
                "CREATE TABLE IF NOT EXISTS ClientsList ("
                "ID INTEGER PRIMARY KEY,"
                "Device TEXT,"
                "UID TEXT,"
                "Permission BOOL"
                ");");
    q.prepare(query);
    q.exec(query);

    query = QString(
                    "CREATE TABLE IF NOT EXISTS Log ("
                    "ID INTEGER PRIMARY KEY,"
                    "Time TEXT,"
                    "UID TEXT,"
                    "DeviceName TEXT,"
                    "Message TEXT"
                    ");");
        q.prepare(query);
        q.exec(query);
        addLog("|----------Logging Start----------|");
}

QVariantList DBService::Local::getClients()
{
    QSqlQuery q(m_sql);
    QString query = QString(
                "SELECT * FROM ClientsList;"
                );
    q.exec(query);
    QVariantList result;
    while (q.next()) {
        QVariantList list;
        auto rec = q.record();
        for (int i = 0; i < rec.count(); i++){
            list.push_back(rec.value(i));
        }
        result.push_back(QVariant(list));
    }
    return result;
}

void DBService::Local::changeAccess(QString uid, bool access)
{
    QSqlQuery q(m_sql);
    QString query = QString(
                "SELECT * FROM ClientsList WHERE UID=\"" + uid + "\""
                );
    q.exec(query);
    if (q.next()){
        query = "UPDATE ClientsList SET Permission=" + QString::number(access) + " WHERE UID=\"" + uid + "\";";
        q.exec(query);
    }
}

void DBService::Local::addClientToDB(Client *client)
{
    QSqlQuery q(m_sql);
    QString query = QString(
                "SELECT * FROM ClientsList WHERE UID=\"" + client->uid() + "\""
                );
    q.exec(query);
    if (!q.next()){
        query = "INSERT INTO ClientsList (UID, Device, Permission) VALUES (\"" + client->uid() + "\", \"" + client->deviceName() + "\", false);";
    }
    q.exec(query);
}

void DBService::Local::updateDeviceName(QString uid, QString deviceName)
{
    QSqlQuery q(m_sql);
    QString query = QString(
                "SELECT * FROM ClientsList WHERE UID=\"" + uid + "\""
                );
    q.exec(query);
    if (q.next()){
        query = "UPDATE ClientsList SET Device=\"" + QString(deviceName) + "\" WHERE UID=\"" + uid + "\";";
        q.exec(query);
    }
}

QString DBService::Local::addLog(QString message, QString uid, QString deviceName)
{
    QSqlQuery q(m_sql);
    auto t = currentTime();
    QString query = "INSERT INTO Log (Time, Message, UID, DeviceName) VALUES (\"" + t + "\",\"" + message + "\", \"" + uid + "\", \"" + deviceName + "\");";
    q.exec(query);
    if (m_handler){
        m_handler(t, uid, deviceName, message);
    }
    return t;
}

void DBService::Local::setAddLogHandler(std::function<void (QString, QString, QString, QString)> handler)
{
    m_handler = handler;
}

DBService::Local::~Local()
{
    addLog("|----------Logging End----------|");
    m_sql.close();
}

QString DBService::Local::currentTime()
{
    return QDateTime::currentDateTime().toString("dd.MM.yy HH:mm:ss");
}

DBService::Local::Local()
{
    connect();
}

QVariantMap DBService::Global::selectPartInfo(const int id)
{
    QSqlQuery q(createConnection("barcode"));
    q.prepare("SELECT * , ("
     " select"
     " STRING_AGG( tmp_list.[Result],',') as res"
     " FROM [Test_View_info_for_part_lable] as tmp_list where tmp_list.part_id = [Test_View_info_for_part_lable].part_id"
     " ) AS results FROM [Produce].[dbo].[Test_View_info_for_part_lable] WHERE [barcode_id] = :id AND file_ext = 'nrp2'");
    q.bindValue(":id",id);

    if(!q.exec()){
        qDebug()<<"error select [View_part_on_barcode] !!!";
        if (m_handler) m_handler();
        return {};
    };
    QVariantMap map;
    while(q.next()){
        for (int i = 0; i < q.record().count(); ++i){
            if(q.record().fieldName(i) == "results"){
                auto ll = q.record().value(i).toString().split(',');
                ll.removeDuplicates();
                std::sort(ll.begin(), ll.end(), [](const QString &str1, const QString &str2){ return str1.toInt() < str2.toInt(); });
                auto str = ll.join(',');
                map[q.record().fieldName(i)] = str;
                //qDebug() << "str" << str;
                continue;
            }

            map[q.record().fieldName(i)] = q.record().value(i).toString();
            qDebug()<<q.record().fieldName(i)<< "===" << q.record().value(i).toString();
        }
    }

    q.prepare("SELECT top 1 * FROM [Produce].[dbo].[View_dxf_file_to_xml_file] WHERE [barcode_id] = :id");
    q.bindValue(":id",id);
    if(!q.exec()){
        qDebug() << "error SELECT [Produce].[dbo].[View_dxf_file_to_xml_file] !!!";
        qDebug() << q.lastError().text();
        return {};
    }

    q.next();

    if(q.record().value(0).isNull()){
        QString path = q.record().value(2).toString();
        QDir dir(path);
        dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
        QFile file(dir.filePath(path));
        qDebug()<<path;
        if (!file.open(QIODevice::ReadOnly)){
            qDebug() << file.errorString();
            return {};
        }
        map["file_bit"] = file.readAll();
        file.close();
    }else{
        map["file_bit"] = qUncompress(q.record().value(0).toByteArray());
    }

    return map;
}

DBService::Global::Global()
{

}

bool DBService::Global::isDatabaseAvailable()
{
    return !createConnection("testConnection").isOpenError();
}

void DBService::Global::closeAllConnections(QString connectionName)
{
    for (const auto &k : storage.keys()){
        if (k.contains(connectionName)){
            storage.remove(k);
        }
    }
}

void DBService::Global::closeConnection(QString connectionName)
{
    QString key = connectionName + "_" + QString::number((quint64)QThread::currentThread(), 16);
    if (storage.contains(key)){
        storage[key].close();
        storage.remove(key);
    }
}

QSqlDatabase DBService::Global::createConnection(QString connectionName)
{
    qDebug()<< connectionName;

    QSqlDatabase msqls;

    auto f = [](){
        QSqlDatabase msqls = QSqlDatabase::addDatabase("QSQLITE", QString::number(no++));
#ifdef Q_OS_LINUX
        msqls.setDatabaseName("Driver={/opt/microsoft/msodbcsql17/lib64/libmsodbcsql-17.7.so.2.1};Server=192.168.71.142\\SQL142;Database=Produce;Integrated Security = true");
        msqls.setUserName("User123");
        msqls.setPassword("User_123");
#else
        msqls.setDatabaseName("Driver={SQL Server};Server=192.168.71.142\\SQL142;Database=Produce;Integrated Security = true");
        msqls.setUserName("User123");
        msqls.setPassword("User_123");
#endif
        return msqls;
    };

    QString key = connectionName + "_" + QString::number((quint64)QThread::currentThread(), 16);
    if (!storage.contains(key)){
        msqls = f();
        if (!msqls.open()) {
            qWarning() << "MSqlS error: " << msqls.lastError();
            if (m_handler) m_handler();
        }
        storage.insert({{key, msqls}});
    }else{
        msqls = storage[key];
    }

    return msqls;
}

void DBService::Global::setErrorHandler(std::function<void ()> handler)
{
    m_handler = handler;
}

DBService::Global::~Global()
{
    closeAllConnections("barcode");
    closeAllConnections("testConnection");
}

