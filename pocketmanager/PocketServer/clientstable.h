#ifndef CLIENTSTABLE_H
#define CLIENTSTABLE_H

#include <itabledataprovider.h>

class ServerService;

class ClientsTable : public ITableDataProvider
{
    Q_OBJECT
public:
    explicit ClientsTable(QObject *parent = nullptr);

    virtual int rowCount() const override;
    virtual int columnCount() const override;
    virtual QVector<Header> headerData() const override;
    virtual Row getRow(int row) override;
    virtual void sort(int column) override;
    virtual bool isFullLoaded() override;
    virtual int rowById(int id) override;
    virtual bool isEditable() override;
public slots:
    bool isPermitted(int row);
private:
    QMap<int, Row*> m_rowsCache;
    ServerService *m_server;
    QVector<int> m_filteredIndeces;
};

#endif // CLIENTSTABLE_H
