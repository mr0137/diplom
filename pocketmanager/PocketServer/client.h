#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <kmacro.h>
#include <itabledataprovider.h>

class Client : public QObject
{
    Q_OBJECT
    K_QML_TYPE(Client)
    K_AUTO_PROPERTY(int, socketDescriptor, socketDescriptor, setSocketDescriptor, socketDescriptorChanged, 0)
    K_AUTO_PROPERTY(int, port, port, setPort, portChanged, 0)
    K_CONST_PROPERTY(Row*, row, new Row())

    Q_PROPERTY(int absoluteId READ absoluteId WRITE setAbsoluteId NOTIFY absoluteIdChanged)
    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
    Q_PROPERTY(bool isPermitted READ isPermitted WRITE setIsPermitted NOTIFY isPermittedChanged)
    Q_PROPERTY(QString uid READ uid WRITE setUid NOTIFY uidChanged)
    Q_PROPERTY(bool connected READ connected WRITE setConnected NOTIFY connectedChanged)
    Q_PROPERTY(QString addr READ addr WRITE setAddr NOTIFY addrChanged)
public:
    explicit Client(QObject *parent = nullptr);
    ~Client();
    const QString &addr() const;
    void setAddr(const QString &newAddr);

    bool connected() const;
    void setConnected(bool newConnected);

    const QString &uid() const;
    void setUid(const QString &newUid);

    bool isPermitted() const;
    void setIsPermitted(bool newIsPermitted);

    const QString &deviceName() const;
    void setDeviceName(const QString &newDeviceName);

    const int &absoluteId() const;
    void setAbsoluteId(const int &newAbsoluteId);

signals:
    void uidChanged();
    void addrChanged();
    void connectedChanged();
    void deviceNameChanged();
    void isPermittedChanged();
    void absoluteIdChanged();
    void updated();

private:
    QString m_addr;
    QString m_uid;
    bool m_connected = false;
    bool m_isPermitted = false;
    QString m_deviceName;
    int m_absoluteId;
};

#endif // CLIENT_H
