QT += quick
QT += core
QT += gui
QT += concurrent
QT += multimedia
QT += quickwidgets
QT += network
QT += qml
QT += sql
QT += widgets

CONFIG += c++17

HEADERS += \
    appcore.h \
    client.h \
    clientstable.h \
    dbservice.h \
    log.h \
    mainwindow.h \
    serverservice.h

SOURCES += \
        appcore.cpp \
        client.cpp \
        clientstable.cpp \
        dbservice.cpp \
        log.cpp \
        main.cpp \
        mainwindow.cpp \
        serverservice.cpp

RESOURCES += qml.qrc

QML_IMPORT_NAME = App
QML_IMPORT_MAJOR_VERSION = 1
uri = App
QML_IMPORT_PATH = $$PWD/../plugins

include($$PWD/../config.pri)

android :{
LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/tablerenderbase/ -lTableRenderBase_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/TCPWorkers/ -lTCPWorkers_armeabi-v7a
LIBS += -L$$OUT_PWD/../libs/qdxf/ -lqdxf_armeabi-v7a
}else:CONFIG(debug, debug|release){
win32: LIBS += -L$$OUT_PWD/../libs/klibcorelite/debug/ -lklibcorelite
else:unix: LIBS += -L$$OUT_PWD/../libs/klibcorelite/ -lklibcorelite

win32: LIBS += -L$$OUT_PWD/../libs/TCPWorkers/debug/ -lTCPWorkers
else:unix: LIBS += -L$$OUT_PWD/../libs/TCPWorkers/ -lTCPWorkers

win32: LIBS += -L$$OUT_PWD/../libs/tablerenderbase/debug/ -lTableRenderBase
else:unix: LIBS += -L$$OUT_PWD/../libs/tablerenderbase/ -lTableRenderBase

win32: LIBS += -L$$OUT_PWD/../libs/qdxf/debug/ -lqdxf
else:unix: LIBS += -L$$OUT_PWD/../libs/qdxf/ -lqdxf
}else{
android :{
}else{
LIBS += -L$$PWD/../bin/libs -lklibcorelite
LIBS += -L$$PWD/../bin/libs -lTableRenderBase
LIBS += -L$$PWD/../bin/libs -lTCPWorkers
LIBS += -L$$PWD/../bin/libs -lqdxf
}
}

INCLUDEPATH += $$PWD/../libs/klibcorelite
DEPENDPATH += $$PWD/../libs/klibcorelite

INCLUDEPATH += $$PWD/../libs/TCPWorkers
DEPENDPATH += $$PWD/../libs/TCPWorkers

INCLUDEPATH += $$PWD/../libs/tablerenderbase
DEPENDPATH += $$PWD/../libs/tablerenderbase

INCLUDEPATH += $$PWD/../libs/qdxf
DEPENDPATH += $$PWD/../libs/qdxf

FORMS += \
    mainwindow.ui
