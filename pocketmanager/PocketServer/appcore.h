#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>
#include <qqml.h>
#include <tcpserverworker.h>
#include <kmacro.h>
#include <utility/kobservablelist.h>
#include <client.h>
#include <clientstable.h>
#include <log.h>
#include <QThread>
#include <serverservice.h>

class AppCore : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(AppCore)
    QML_SINGLETON
    Q_PROPERTY(ServerService* server READ server WRITE setServer NOTIFY serverChanged)
    Q_PROPERTY(bool isDBAvailable READ isDBAvailable WRITE setIsDBAvailable NOTIFY isDBAvailableChanged)
    Q_PROPERTY(QString rootDir READ rootDir WRITE setRootDir NOTIFY rootDirChanged)
    Q_PROPERTY(QVariantMap palette READ palette WRITE setPalette NOTIFY paletteChanged)
    Q_PROPERTY(ClientsTable* table READ table NOTIFY tableChanged)
    Q_PROPERTY(Log* logTable READ logTable WRITE setLogTable NOTIFY logTableChanged)
    explicit AppCore(QObject *parent = nullptr);
public:
    static AppCore* instance();
    ~AppCore();
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);
    void setRootPath(QString path);

    const QString &rootDir() const;
    void setRootDir(const QString &newRootDir);

    ServerService *server() const;
    void setServer(ServerService *newServer);

    ClientsTable *table() const;
    void setTable(ClientsTable *newTable);

    const QVariantMap &palette() const;
    void setPalette(const QVariantMap &newPalette);

    bool isDBAvailable() const;
    void setIsDBAvailable(bool newIsDBAvailable);

    Log *logTable() const;
    void setLogTable(Log *newLogTable);

signals:
    void rootDirChanged();

    void serverChanged();

    void tableChanged();

    void paletteChanged();

    void isDBAvailableChanged();

    void logTableChanged();

private:
    QString m_ip;
    int m_port = 45454;
    QString m_rootDir;
    QTimer *m_timer = nullptr;
    QThread *m_thread = nullptr;
    ServerService *m_server;
    ClientsTable *m_table;
    QVariantMap m_palette;
    bool m_isDBAvailable;
    Log *m_logTable;
};

#endif // APPCORE_H
