import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import CQML 1.0 as C
import App 1.0
import TableRender 1.0

ColumnLayout{
    spacing: 0

    Rectangle{
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.maximumHeight: AppCore.isDBAvailable ? 0 : 50
        color: "red"
        Text{
            anchors.fill: parent
            text: "Database Connetion LOST"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    TableRender{
        id: tableRender
        Layout.fillWidth: true
        Layout.fillHeight: true
        provider: AppCore.table
        editableColumns: ["Permission"]
        backgroundColor: AppCore.palette.app.firstBackgroundColor

        onResizingChanged:{
            tableRender.closePopup()
        }

        popupElement: Item{
            C.ComboBox{
                id: permission_Box
                radius: 0
                backgroundColor: AppCore.palette.app.lightBackgroundColor
                property var val: ["Deny", "Accept"]
                property bool compleated: false
                anchors.fill: parent
                model: val

                onCurrentIndexChanged: {
                    if (!compleated) return
                    console.log("change", currentIndex)
                    if (currentIndex){
                        AppCore.server.changeAccess(tableRender.currentRow, true)
                    }else if (currentIndex != -1){
                        AppCore.server.changeAccess(tableRender.currentRow, false)
                    }
                    tableRender.closePopup()
                }

                Component.onCompleted: compleated = true

                Connections{
                    target: tableRender
                    function onCurrentRowChanged() {
                        permission_Box.currentIndex = tableRender.provider.isPermitted(tableRender.currentRow)
                    }
                }
            }
        }

        style: TableStyle{
            verticalHeaderAvailable: true
            iconsColor: AppCore.palette.app.darkBackgroundColor
            iconsWidth: 1
            highlightColor: AppCore.palette.text.textColor
            selectionColor: "black"
            selectionOpacity: 0.3
            cell{
                height: 35
                backgroundColor: AppCore.palette.app.firstBackgroundColor
                textColor: AppCore.palette.text.textColor
                font{
                    pointSize: 15
                    family: "Arial"
                    letterSpacing: 0.1
                    wordSpacing: 0.1
                }
            }
            horizontalHeader{
                height: 27
                width: 240
                backgroundColor: AppCore.palette.app.lightBackgroundColor
                textColor: AppCore.palette.app.mainColor
                font{
                    pointSize: 16
                    family: "Arial"
                    letterSpacing: 0.1
                    wordSpacing: 0.1
                }
            }
            verticalHeader{
                width: 10
                backgroundColor: AppCore.palette.app.lightBackgroundColor
            }
        }

        Component.onCompleted: setCurrentRow(0)
    }

    C.BaseBackground{
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.maximumHeight: 50
        backgroundColor: AppCore.palette.app.firstBackgroundColor
        elevation: 3
        C.Button{
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            elevation: 3
            width: 90
            text: "Log"
            buttonBackground: AppCore.palette.app.lightBackgroundColor
            textColor: AppCore.palette.app.mainColor
            onReleased: {
                stackView.push(second)
            }
        }
    }

    //RowLayout{

    //
    //    Item{
    //        Layout.fillWidth: true
    //    }
    //
    //}
}
